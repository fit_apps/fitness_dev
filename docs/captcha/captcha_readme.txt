
reCaptcha 
reCaptcha is a Google owned product that seems to be widely used (> 100 million CAPTCHAs every day). One of its uses is to 
harvest the efforts of people solving the captchas to decipher text that can't be deciphered via OCR. In 2014 they 
implemented No CAPTCHA reCAPTCHA which normally does not use images, but simply requires the user to check a box.

http://www.google.com/recaptcha/intro/index.html
http://en.wikipedia.org/wiki/ReCAPTCHA

You need to register the URL at https://www.google.com/recaptcha/admin#list (lower level domains are automatically included) to 
get a key pair and run the pages using the reCaptcha via https. To ease testing, a URL of localhost is valid with any key.

https://github.com/VividCortex has provided an Angular directive to simplify integration with Google's code. (A copy of
the github source as it appeared 3/31/15 is included in this directory.)
 http://ngmodules.org/modules/angular-recaptcha
 https://github.com/VividCortex/angular-recaptcha
 