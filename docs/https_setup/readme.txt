To have the app run over https in a testing environment, you'll need to set up a self signed certificate and 
modify your Tomcat configuration.

A self-signed certificate can pretty easily be set up following http://tomcat.apache.org/tomcat-7.0-doc/ssl-howto.html, or you 
can also use the one included in the same folder as this readme.txt. 
To use the included, copy dot_keystore to your user.home directory, renaming it to ".keystore". 
The keystore's password is "changeit"

The Tomcat change is to add an https connector to your Tomcat's server.xml file. The following should work:
	<Connector
		protocol="org.apache.coyote.http11.Http11Protocol"
		port="8443" maxThreads="200"
		scheme="https" secure="true" SSLEnabled="true"
		keystoreFile="${user.home}/.keystore" keystorePass="changeit"
		clientAuth="false" sslProtocol="TLS"/>
		
If you are running using the standard ports (8080 and 8443), the above should changes should be all you need. If you are running on 
ports other than these, you'll need to add a port mappings clause to our securityContext.xml file. The changes should be in the 
file, but commented out. They are:
	<http>
	    ...
	    <port-mappings>
	      <port-mapping http="9080" https="9443"/>    <!-- Your non-standard ports go here -->
	    </port-mappings>
	  </http>	
	  
See 'Adding HTTP/HTTPS Channel Security' section in http://docs.spring.io/spring-security/site/docs/3.1.x/reference/ns-config.html
If you don't make this change you'll get an broken redirect to a URL with a doubled context name if you try to access 
the app via http.	 

See https://rally1.rallydev.com/#/11881120658d/detail/userstory/31479893480 for more info