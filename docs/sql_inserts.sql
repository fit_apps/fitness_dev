--ref tables---------

--insert into authority
insert into authority values(1, now(), now(), 'ROLE_MEMBER',   null, null);

--insert into ref_diagnosis_type
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (1, 'parent cancer type', 'CANCER', 'Cancer', null);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (2, 'Lung Cancer', 'LUNG_CANCER', 'Lung Cancer', 1);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (3, 'Breast Cancer', 'BREAST_CANCER', 'Breast Cancer', 1);

--insert into ref_chemo_freq
INSERT INTO ref_chemo_freq (ref_chemo_freq_id, displayName, ref_chemo_freq_key) VALUES (1, 'Daily', 'daily');
INSERT INTO ref_chemo_freq (ref_chemo_freq_id, displayName, ref_chemo_freq_key) VALUES (2, 'Weekly', 'week');
INSERT INTO ref_chemo_freq (ref_chemo_freq_id, displayName, ref_chemo_freq_key) VALUES (3, 'Every other week', 'every_oth_week');
INSERT INTO ref_chemo_freq (ref_chemo_freq_id, displayName, ref_chemo_freq_key) VALUES (4, 'Monthly', 'monthly');

--ref_activity_type----
INSERT INTO `ref_activity_type` (`ref_activity_type_id`, `description`, `name`, `icon_path`) 
VALUES 
(1,'Running','Run','/images/activityIcons16/Run.png'),
(2,'Swimming','Swim','/images/activityIcons16/Swim.png'),
(3,'Walking','Walk','/images/activityIcons16/Walk.png'),
(4,'Biking','Bike','/images/activityIcons16/Bike.png'),
(5,'Dancing','Dance','/images/activityIcons16/Dance.png'),
(6,'Doing yoga','Yoga','/images/activityIcons16/Yoga.png'),
(7,'Playing ping pong','Ping pong','/images/activityIcons16/Pingpong.png');

---------user tables-----------

--sample data for ref_diagnosis type
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (4, 'parent other', 'A_PARENT', 'A Parent', null);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (5, 'other child', 'A_CHILD', 'A child', 4);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (6, 'other child', 'B_CHILD', 'B child', 4);

INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (7, 'parent other', 'D_PARENT', 'B Parent', null);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (8, 'other child', 'D_CHILD', 'D child', 7);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (9, 'other child', 'E_CHILD', 'E child', 7);

INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (10, 'Cancer', 'CANCER', 'Cancer', null);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (11, 'Breast Cancer', 'BREAST_CANCER', 'Breast Cancer', 10);
INSERT INTO ref_diagnosis_type (ref_diag_type_id, description, diag_key, name, parent_diag_id) VALUES (12, 'Lung Cancer', 'LUNG_CANCER', 'Lung Cancer', 10);

--insert into users
INSERT
INTO
    users
    (
        user_id,
        display_name,
        email_address,
        enabled,
        first_name,
        last_name,
        salt 
    )
    VALUES
    (
        1,
        'amouradi',
        'amouradi@hotmail.com',
        1,
        'Anoush',
        'Mouradian',
        1
          )
          
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (1, null, null, '2014-04-29 00:00:00', null, 22.0, '2014-04-29 01:00:00', 12.0, 0.0, null, true, '2014-04-29 00:00:00', null, 'back pain', 'Long activity', null, null, null, 1, 225, 25, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (2, null, null, '2014-04-29 00:00:00', null, 22.0, '2014-04-29 01:00:00', null, null, null, true, '2014-04-29 00:00:00', null, 'headache', 'Long activity2', null, null, null, 3, 225, 24, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (3, null, null, '2014-04-29 00:00:00', null, 22.0, '2014-04-29 01:00:00', null, null, null, true, '2014-04-29 00:00:00', null, 'mood', 'Long activity3', null, null, null, 2, 225, 24, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (4, null, null, '2014-05-23 00:00:00', null, 22.0, '2014-05-23 01:00:00', 5.0, 0.0, null, true, '2014-05-23 00:00:00', null, 'dry skin', 'May activity', null, null, null, 1, 225, 38, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (5, null, null, '2014-05-23 00:00:00', null, 22.0, '2014-05-23 01:00:00', null, null, null, true, '2014-05-23 00:00:00', null, 'depression', 'May activity1', null, null, null, 2, 225, 24, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (6, null, null, '2014-05-23 00:00:00', null, 22.0, '2014-05-23 01:00:00', null, null, null, true, '2014-05-23 00:00:00', null, 'depression', 'May activity2', null, null, null, 3, 225, 22, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (7, null, null, '2014-05-23 00:00:00', null, 22.0, '2014-05-23 01:00:00', null, null, null, true, '2014-05-23 00:00:00', null, null, 'May activity3', null, null, null, 3, 225, 22, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (8, null, null, '2014-05-23 00:00:00', null, 22.0, '2014-05-23 01:00:00', null, null, null, true, '2014-05-23 00:00:00', null, null, 'May activity4', null, null, null, 3, 225, 26, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (9, null, null, '2014-05-23 00:00:00', null, 22.0, '2014-05-23 01:00:00', 12.0, 0.0, null, true, '2014-05-23 00:00:00', null, null, 'May activity5', null, null, null, 2, 225, 7, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (10, null, null, '2014-05-23 00:00:00', null, 22.0, '2014-05-23 01:00:00', null, null, null, true, '2014-05-23 00:00:00', null, null, 'May activity6', null, null, null, 2, 225, 7, 12);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (11, null, null, '2014-04-29 00:00:00', null, 22.0, '2014-04-29 01:00:00', null, null, null, true, '2014-04-29 00:00:00', null, null, 'Short activity', null, null, null, 1, 225, 2, 14);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (12, null, null, '2014-04-29 00:00:00', null, 22.0, '2014-04-29 01:00:00', null, null, null, true, '2014-04-29 00:00:00', null, null, 'Short activity2', null, null, null, 1, 225, 61, 14);
INSERT INTO activity (activity_id, create_dt, update_dt, activity_dt, city, distance, end_time, energy_level_after, energy_level_before, notes, is_public, start_time, street, symptoms, title, zip_code, created_by, updated_by, ref_activity_type_id, country_id, state_id, user_id) VALUES (13, null, null, '2014-04-29 00:00:00', null, 22.0, '2014-04-29 01:00:00', null, null, null, true, '2014-04-29 00:00:00', null, null, 'Bob activity3', null, null, null, 1, 225, 57, 14);



 
 -- create some user cancer records
 INSERT
INTO
    user_cancer_diagnosis
    (
        user_cancer_diag_id,
        cancer_free_dt,
        end_dt,
        is_primary,
        start_dt,
        title,
        ref_diagnosis_type_id,
        user_id
    )
    VALUES
    (
        4,
        now(),
        now(),
        true,
        now(),
        'Cancer diag4',
        1,
        34)
        
--- insert into activity comment
INSERT
INTO
    activity_comments
    (
        activity_comments_id,
        create_dt,
        update_dt,
        name,
        created_by,
        updated_by,
        activity_id
    )
    VALUES
    (
       5 ,
        now(),
        now(),
        'this is comment2',
        1,
        1,
        3
    )  
    
    
 INSERT
INTO
    user_cancer_treatment_phases
    (
        user_cancer_treat_phase_id,
		chemo_completed,
        chemo_total,
        radiation_completed,
        radiation_total,
        ref_chemo_freq_id,
        user_diag_id
    )
    VALUES
    (
        1,
        2,
       5,
        2,
        5,
        1,
        1
       )