	//collect selected diagnosis, will use this later for next/prev/reload buttons
		if(elem.diag.isChecked == "true") {
			$scope.diagKeysMap[elem.diag.childDiagKey] = elem.diag;
		} else {
			if($scope.diagKeysMap[elem.diag.childDiagKey] != null) {
				delete $scope.diagKeysMap[elem.diag.childDiagKey];
			}
		}
		//get all map values
		// get all values
		var diagArray = [];
		
		for (var key in $scope.diagKeysMap) {
			if ($scope.diagKeysMap.hasOwnProperty(key)) {
				diagArray.push($scope.diagKeysMap[key]);
		  }
		}
