package com.fit.utils;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import com.fit.model.base.Auditable;
import com.fit.model.security.User;

public class AuditInterceptor extends EmptyInterceptor {

    private static final long serialVersionUID = 7779984061690127170L;

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types) {

        if (entity instanceof Auditable) {
            Auditable a = (Auditable) entity;
        }

        return super.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
    }

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {

        boolean isSet = false;

        if (entity instanceof Auditable) {
            for (int i = 0; i < propertyNames.length; i++) {

                if (propertyNames[i].equals("createDate") && state[i] == null) {
                    state[i] = new Date();
                    isSet = true;
                }
                /*
                 * if (propertyNames[i].equals("createdBy")) { state[i] =
                 * SecurityUtil.getCurrentUser(); isSet = true; }
                 */
                if (propertyNames[i].equals("updateDate")) {
                    state[i] = new Date();
                    isSet = true;
                }
                /*
                 * if (propertyNames[i].equals("updatedBy")) { state[i] =
                 * SecurityUtil.getCurrentUser(); isSet = true; }
                 */
                // if createdBy is there and user is logged-in then capture who
                // is adding this entity.
                // would not need to do it for self-registered users.
                // Don't need to do it if it's set explicitly, like in cases
                // when
                // user is registering new account and creates bc at the same
                // time, so
                // club.createdBy becomes newly registered user
                if ("createdBy".equals(propertyNames[i]) && SecurityUtil.getCurrentUser() != null && 
                        !"anonymousUser".equals(SecurityUtil.getCurrentUser())
                        && ((Auditable) entity).getCreatedBy() == null) {
                    state[i] = (User) SecurityUtil.getCurrentUser() ;
                    isSet = true;
                }
            }
        }
        return isSet;
    }
}
