package com.fit.utils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * @author channah
 * 
 */
@Service
public class MailUtils {

	static final Logger logger = Logger.getLogger(MailUtils.class);

	@Autowired
	private MailSender mailSender;
	@Autowired
	private SimpleMailMessage preconfiguredMailMessage;

	public void sendEmail(final String messageTo, final String messageSubject,
			final String messageText) {

		SimpleMailMessage mailMessage = new SimpleMailMessage(
				preconfiguredMailMessage);
		mailMessage.setTo(messageTo);
		mailMessage.setSubject(messageSubject);
		mailMessage.setText(messageText);
		mailSender.send(mailMessage);

		// Use MimeMessagePreparator and MimeMessagePreparator if a mime message
		// is needed rather than a simple message
	}
}