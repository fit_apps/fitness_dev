package com.fit.utils;

/**
 * All user exception should extend from this class
 * 
 * @author amouradi
 * 
 */
public class BaseBusinessException extends Throwable implements GenericException {

    private static final long serialVersionUID = 9162666611173291009L;
    private String messageKey;
    private Object[] messageArgs;

    public BaseBusinessException() {
        super();
    }

    public BaseBusinessException(Throwable t) {
        super(t);
    }

    public BaseBusinessException(String m, Throwable t) {
        super(m, t);
    }

    public BaseBusinessException(String message) {
        super(message);
    }

    public BaseBusinessException(String messageKey, Object... args) {
        if (args != null && args.length > 0) {
            this.messageArgs = args;
        }
        this.messageKey = messageKey;
    }

    /**
     * get message for given Exception
     * 
     * @return the messageKey
     */
    public String getMessageKey() {
        return messageKey;
    }

    /**
     * get message for given Exception
     * 
     * @param messageKey
     *            the messageKey to set
     */
    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public Object[] getMessageArgs() {
        return messageArgs;
    }

    public void setMessageArgs(Object[] messageArgs) {
        this.messageArgs = messageArgs;
    }

}
