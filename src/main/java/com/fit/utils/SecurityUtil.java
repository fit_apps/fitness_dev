package com.fit.utils;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

public class SecurityUtil {
	
	public static final String CURRENT_USER = "CURRENT_USER";

    /**
     * Gets currently authenticated user
     * 
     * @return
     */
    public static Object getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null ? authentication.getPrincipal() : null;
    }

    /**
     * Gets the session Id of the currently authenticated user
     * 
     * @return
     */
    public static String getCurrentSession() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	return authentication != null ? ((WebAuthenticationDetails) authentication.getDetails()).getSessionId() : null;
    }
    
    /**
     * Gets the "real" user in cases where the user has masqueraded as someone
     * else.
     * 
     * @return
     */
    /*
     * public static User getAbsoluteUser() { Authentication auth =
     * SecurityContextHolder.getContext().getAuthentication(); if (auth != null) { //
     * check if the user was masqueraded for (GrantedAuthority ga :
     * auth.getAuthorities()) { if (ga instanceof SwitchUserGrantedAuthority) {
     * return ((SecurityUser) ((SwitchUserGrantedAuthority)
     * ga).getSource().getPrincipal()).getUser(); } } return ((SecurityUser)
     * auth.getPrincipal()).getUser(); } return null; }
     */
}
