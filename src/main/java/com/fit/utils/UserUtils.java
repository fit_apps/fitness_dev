package com.fit.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import com.fit.model.security.User;
import com.fit.mvc.command.RegisterCommand;

/**
 * @author amouradi
 * 
 */
public class UserUtils {

    static final Logger logger = Logger.getLogger(UserUtils.class);
    
    static final String RANDOM_STRING_LOWER_CASE = "abcdefghijkmnpqrstuvwxyz";
    static final String RANDOM_STRING_UPPER_CASE = "ABCDEFGHJKMNPQRSTUVWXYZ";
    static final String RANDOM_STRING_SPECIAL_CHAR = "@#$%&*";
    static final String RANDOM_STRING_INTEGER = "23456789";
	static final String RANDOM_STRING_ALL_CHOICES = RANDOM_STRING_LOWER_CASE + RANDOM_STRING_UPPER_CASE + RANDOM_STRING_SPECIAL_CHAR + RANDOM_STRING_INTEGER;    
    
    /**
     * determines if archiveDate is before today's date or empty, then returns
     * true, otherwise false
     * 
     * @param archiveDate
     * @return true if archiveDate==null or before today, otherwise false
     */
    public static boolean isAccountNonExpired(Date archiveDate) {
        boolean isActive = false;
        if (archiveDate == null || (archiveDate != null && archiveDate.before(new Date()))) {
            isActive = true;
        }
        return isActive;
    }

    public static void populate(User user, String emailAddress, String displayName, String firstName, String lastName,
            Boolean enabled, Boolean locked, String password, String userSalt, Boolean showPrivate) {
        user.setEmailAddress(emailAddress);
        user.setDisplayName(displayName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEnabled(enabled);
        user.setLocked(locked);
        user.setPassword(password);
        user.setSalt(userSalt);
        user.setShowPrivate(showPrivate);
    }

    public static void populate(Md5PasswordEncoder passwordEncoder, User user, RegisterCommand cmd) {
        String salt = generateUserSalt(cmd.getEmailAddress() + String.valueOf(System.currentTimeMillis()));
        user.setSalt(salt);
        // encode with salt + user Salt
        String passw = encodePassword(passwordEncoder, cmd.getPassword(), user.getUserSalt());
        logger.debug("-----user : " + user.getUsername() + ", " + passw);
        // set user enabled and unlocked, but not activated
        populate(user, cmd.getEmailAddress(), cmd.getDisplayName(), cmd.getFirstName(), cmd.getLastName(), user.getEnabled(),
                user.getLocked(), passw, salt, user.getShowPrivate());
    }

    public static String encodePassword(Md5PasswordEncoder passwordEncoder, String password, String salt) {

        return passwordEncoder.encodePassword(password, salt);
    }

    public static String generateUserSalt(String data) {
        String salt = DigestUtils.md5Hex(data);
        return salt;
    }

	/**
	 * Generate a random string based on the given pattern. The given pattern
	 * specifies both the type of character and, by virtue of the number of
	 * characters in the pattern, the length of the string to produce.
	 * <p>
	 * Note that some characters are not used to avoid ambiguity, e.g. the
	 * letter l and the number 1
	 * <table>
	 * <tr>
	 * <th>Pattern</th>
	 * <th>Character type</th>
	 * </tr>
	 * <tr>
	 * <td>*</td>
	 * <td>Anything</td>
	 * </tr>
	 * <tr>
	 * <td>l</td>
	 * <td>Lower case</td>
	 * </tr>
	 * <tr>
	 * <td>u</td>
	 * <td>Upper case</td>
	 * </tr>
	 * <tr>
	 * <td>s</td>
	 * <td>Special character</td>
	 * </tr>
	 * <tr>
	 * <td>n</td>
	 * <td>Numeric</td>
	 * </tr>
	 * </table>
	 * 
	 * @param pattern
	 *            - The pattern of character types to use
	 * @param randomOrder
	 *            - True if the pattern should be used in a random order
	 * @return
	 */
	public static String genString(String pattern, boolean randomOrder) {

		// Convert pattern to a list
		List<String> patternList = Arrays.asList(pattern.split(""));
		if (randomOrder) {
			Collections.shuffle(patternList, new Random());
		}

		// logger.debug("Password pattern: " + pattern);
		// logger.debug("Shuffled password pattern: " + patternList);

		Random r = new Random();
		StringBuffer sb = new StringBuffer();

		// Build a string based on the given pattern
		for (String theChar : patternList) {
			switch (theChar) {
			case "*":
				sb.append(RANDOM_STRING_ALL_CHOICES.charAt(r
						.nextInt(RANDOM_STRING_ALL_CHOICES.length())));
				break;
			case "l":
				sb.append(RANDOM_STRING_LOWER_CASE.charAt(r.nextInt(RANDOM_STRING_LOWER_CASE
						.length())));
				break;
			case "u":
				sb.append(RANDOM_STRING_UPPER_CASE.charAt(r.nextInt(RANDOM_STRING_UPPER_CASE
						.length())));
				break;
			case "s":
				sb.append(RANDOM_STRING_SPECIAL_CHAR.charAt(r
						.nextInt(RANDOM_STRING_SPECIAL_CHAR.length())));
				break;
			case "n":
				sb.append(RANDOM_STRING_INTEGER.charAt(r
						.nextInt(RANDOM_STRING_INTEGER.length())));
				break;
			// default:
			// break;
			}
		}
		return sb.toString();
	}

    public static void main(String[] args) {
        System.out.println(new String("amouradi@hotmail.com").hashCode());
        String dbSalt = generateUserSalt("amouradi@hotmail.com" + String.valueOf(System.currentTimeMillis()));
        System.out.println("first hex: " + dbSalt);
        System.out.println("second hex: " + generateUserSalt(dbSalt));
    }

}
