package com.fit.utils;

/**
 * All runtime/db exceptions should extend from it
 * 
 * @author amouradi
 * 
 */
public class BaseRuntimeException extends RuntimeException implements GenericException {

    private static final long serialVersionUID = 9162902611173291009L;
    private String messageKey;

    public BaseRuntimeException() {
        super();
    }

    public BaseRuntimeException(Throwable t) {
        super(t);
    }

    public BaseRuntimeException(String m, Throwable t) {
        super(m, t);
    }

    public BaseRuntimeException(String message) {
        super(message);
    }

    /**
     * get message for given Exception
     * 
     * @return the messageKey
     */
    public String getMessageKey() {
        return messageKey;
    }

    /**
     * get message for given Exception
     * 
     * @param messageKey
     *            the messageKey to set
     */
    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

}
