package com.fit.dao;

import com.fit.utils.BaseRuntimeException;

public class DaoException extends BaseRuntimeException {
    private static final long serialVersionUID = 74569061623627100L;
    
    public DaoException() {
        super();
    }

    public DaoException(Throwable t) {
        super(t);
    }

    public DaoException(String m, Throwable t) {
        super(m, t);
    }

    public DaoException(String message) {
        super(message);
    }
}
