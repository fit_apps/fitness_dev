package com.fit.dao.reference;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.refs.RefActivityType;
import com.fit.model.refs.RefStateProvinces;

@Repository
public class ActivityTypeDaoHibernate extends BaseDaoHibernate<RefActivityType>
		implements ActivityTypeDao {

		@Override
		@SuppressWarnings("unchecked")
		public List<RefActivityType> getAllActivityTypes() {
			getSession().clear();
			StringBuffer hql = new StringBuffer();
			hql.append("select new RefActivityType(rat.id as refActivityTypeId, rat.name as name, rat.description as description, ");
			hql.append("rat.iconPath as iconPath) from RefActivityType rat ");
			hql.append("order by description");
			
			Query query = getSession().createQuery(hql.toString());

			return query.list();
		}

		@Override
		public RefActivityType getActivityTypeById(Long id) {
			RefActivityType activityType = (RefActivityType) getSession().load(RefActivityType.class, id);
			return activityType;		
		}		
}		