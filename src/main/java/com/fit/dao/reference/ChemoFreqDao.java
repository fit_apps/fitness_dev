package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import com.fit.dao.Dao;
import com.fit.model.refs.RefChemoFreq;

/**
 * @author amourad
 *
 */
public interface ChemoFreqDao extends Dao<RefChemoFreq> {
	
	public List<Map<String, Object>> getAllChemoFreqs();

}
