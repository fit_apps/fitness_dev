package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.refs.RefStateProvinces;
import com.fit.model.security.User;

@Repository
public class StateProvinceDaoHibernate extends BaseDaoHibernate<RefStateProvinces>
		implements StateProvinceDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<RefStateProvinces> getAllStatesProvinces() {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("select new RefStateProvinces(rsp.id as refStateId, rsp.stateName as stateName, rsp.stateKey as stateKey) ");
		hql.append("from RefStateProvinces rsp ");
		hql.append("order by stateName");
		Query query = getSession().createQuery(hql.toString());
		return query.list();
	}

	@Override
	public RefStateProvinces getStateProvinceById(Long id) {

//      Query query = getSession().createQuery("from RefStateProvinces where id= :id");
//      query.setLong("id", id);
//      RefStateProvinces state1 = (RefStateProvinces) query.uniqueResult();
	
      RefStateProvinces state = (RefStateProvinces) getSession().load(RefStateProvinces.class, id);
      return state;						
	}	
	
}
