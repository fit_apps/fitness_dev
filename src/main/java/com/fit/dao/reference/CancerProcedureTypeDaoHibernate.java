package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.refs.RefCancerProcedureType;

/**
 * @author amouradian
 *
 */
@Repository
public class CancerProcedureTypeDaoHibernate extends
		BaseDaoHibernate<RefCancerProcedureType> implements
		CancerProcedureTypeDao {

	public List<Map<String, Object>> getAllCancerProcedureTypes() {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(p.id as id, p.name as name, p.description as description) ");
		hql.append("from RefCancerProcedureType p ");
		hql.append("order by name");

		Query query = getSession().createQuery(hql.toString());

		return query.list();
	}
}
