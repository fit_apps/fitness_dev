package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import com.fit.dao.Dao;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.refs.RefStateProvinces;

/**
 * All RefStateProvinces methods
 * @author channah
 *
 */
public interface StateProvinceDao extends Dao<RefStateProvinces> {
	
	public List<RefStateProvinces> getAllStatesProvinces();	
	
	public RefStateProvinces getStateProvinceById(Long id);
}
