package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;

@Repository
public class CountryDaoHibernate extends BaseDaoHibernate<RefCountries>
		implements CountryDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<RefCountries> getAllCountries() {
		getSession().clear();
		StringBuffer hql = new StringBuffer();

		hql.append("select new RefCountries(rc.id as refCountryId, rc.countryName as countryName, rc.countryKey as countryKey) ");
		hql.append("from RefCountries rc ");
		hql.append("order by  countryName");
		
		Query query = getSession().createQuery(hql.toString());

		return query.list();
	}

	@Override
	public RefCountries getCountryById(Long id) {

//        Query query = getSession().createQuery("from RefCountries where id= :id");
//        query.setLong("id", id);
//        RefCountries country = (RefCountries) query.uniqueResult();
		
      return (RefCountries) getSession().load(RefCountries.class, id);			
	}

}
