package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import com.fit.dao.Dao;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefDiagnosisType;

/**
 * All ref_countries methods
 * @author channah
 *
 */
public interface CountryDao extends Dao<RefCountries> {
	
	public List<RefCountries> getAllCountries();	
	
	public RefCountries getCountryById(Long id);
}
