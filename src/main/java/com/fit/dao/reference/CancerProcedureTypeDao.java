package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import com.fit.dao.Dao;
import com.fit.model.refs.RefCancerProcedureType;

/**
 * @author amourad
 *
 */
public interface CancerProcedureTypeDao extends Dao<RefCancerProcedureType> {
	
	public List<Map<String, Object>> getAllCancerProcedureTypes() ;

}
