package com.fit.dao.reference;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.refs.RefChemoFreq;

/**
 * @author amourad
 *
 */
@Repository
public class ChemoFreqDaoHibernate extends BaseDaoHibernate<RefChemoFreq>
		implements ChemoFreqDao {
	
	public List<Map<String, Object>> getAllChemoFreqs() {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(chemo.id as id, chemo.displayName as displayName, displayName.refChemoFreqKey as key) ");
		hql.append("from RefChemoFreq chemo ");
		hql.append("order by displayName");
		
		Query query = getSession().createQuery(hql.toString());

		return query.list();
	}

}
