package com.fit.dao.reference;

import java.util.List;

import com.fit.dao.Dao;
import com.fit.model.refs.RefActivityType;
import com.fit.model.refs.RefStateProvinces;

/**
 * All RefActivityType methods
 * @author channah
 *
 */
public interface ActivityTypeDao extends Dao<RefActivityType> {
	
	public List<RefActivityType> getAllActivityTypes();	
	
	public RefActivityType getActivityTypeById(Long id);
}
