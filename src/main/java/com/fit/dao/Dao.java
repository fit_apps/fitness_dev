package com.fit.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import com.fit.model.base.Identifiable;


/**
 * interface for DAO classes
 * 
 * @author amouradi
 * @param <T>
 */
public interface Dao<T extends Identifiable>
{
    T getNewInstance(); 

    List<T> getAll();

    T getById(Long id);

    /**
     * Gets values based on a collection of IDs
     */
    Collection<T> getByIds(Collection<Long> ids);

    /**
     * Saves and returns the id
     */
    Serializable save(T obj);

    void update(T obj);

    void saveOrUpdate(T obj);

    void delete(T obj);

    T merge(T obj);

    int getCountAll();

    int getCountByExample(T example);

    List<T> getByExample(T example);
    
    public Session getSession();
    
    /**
     * Dynamically validates the unique constraints of an Entity based on the
     * "unique" attribute of the <code>@Column</code> annotation.
     * @param obj
     * @throws DuplicateException
     */
    public void validateUniqueConstraints(T obj) throws Exception;
}
