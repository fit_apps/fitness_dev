package com.fit.dao;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.fit.model.base.Identifiable;
import com.fit.utils.BaseRuntimeException;

/**
 * Base Dao class with hibernate dependecy, since it uses Hibernate
 * SessionFactory. Dao classes could extend it for hibernate based data
 * retrievals. 
 * 
 * @author mouradan
 * @param <T>
 */
@SuppressWarnings("unchecked")
public abstract class BaseDaoHibernate<T extends Identifiable> implements Dao<T> {

    @Autowired
    public SessionFactory sessionFactory;
    private Class<T> entityClass;
    private String defaultOrder;
    private boolean defaultOrderAsc = true;
    

    public BaseDaoHibernate() {
        this.entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }


    public Session getSession() {
        return getSessionFactory().getCurrentSession();
    }

    /**
     * @return SessionFactory
     */
    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    

    public void setEntityClass(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * get new instance of the Entity.
     * 
     * @return T
     */
    public T getNewInstance() {
        try {
            return getEntityClass().newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Error creating new instance of : " + getEntityClass().getName(), e);
        }
    }

    /**
     * Class for the Entity of type <T>
     * 
     * @return Class<T>
     */
    protected Class<T> getEntityClass() {
        return entityClass;
    }

    /**
     * create Criteria for the {@code Class<T> }
     * 
     * @return Criteria
     */
    protected Criteria getBaseCriteria() {
        return getSession().createCriteria(getEntityClass());
    }

    /**
     * Gets all the records ordered by the defaultOrder if it is set.
     */
    public List<T> getAll() {
        return getDefaultOrder() == null ? getBaseCriteria().list() : addDefaultOrder(getBaseCriteria()).list();
    }

    /**
     * Gets a record by id. Returns null if no matching value is found.
     * 
     * @param Long
     *            id
     * @return T
     */
    public T getById(Long id) {
        return (T) getSession().createCriteria(getEntityClass()).add(Restrictions.eq("id", id)).uniqueResult();
    }

    /**
     * Gets all the records matching the collection of ids.
     */
    public Collection<T> getByIds(Collection<Long> ids) {
        return (List<T>) getSession().createCriteria(getEntityClass()).add(Restrictions.in("id", ids)).list();
    }

    /**
     * Save <T> Entity. Validates unique constraints first.
     * 
     * @param T
     */
    public Serializable save(T obj) throws DuplicateException{
        validateUniqueConstraints(obj);
        return getSession().save(obj);
    }

    /**
     * Update <T> Entity. Validates unique constraints first.
     * 
     * @param T
     */
    public void update(T obj) {
       // validateUniqueConstraints(obj);
        getSession().update(obj);
    }
    

    /**
     * Save or update <T> Entity. Validates unique constraints first.
     * 
     * @param T
     */
    public void saveOrUpdate(T obj) {
        validateUniqueConstraints(obj);
        getSession().saveOrUpdate(obj);
    }

    /**
     * Delete <T> Entity.
     * 
     * @param T
     */
    public void delete(T obj) {
        getSession().delete(obj);
    }

    /**
     * Reattaches the object to the session and updates the persistent state
     * with the detached object's state. Validates unique constraints first.
     * 
     * @param obj
     * @return
     * @see {@link Session#merge(Object)}
     */
    public T merge(T obj) {
        validateUniqueConstraints(obj);
        return (T) getSession().merge(obj);
    }

    /**
     * get row count for the Enity
     * 
     * @return int
     */
    public int getCountAll() {
        return getCount(getBaseCriteria());
    }

    /**
     * get row count for type <T>
     * 
     * @return int
     */
    public int getCountByExample(T example) {
        return getCount(getExampleCriteria(example));
    }

    /**
     * get number of rows for given criteria. If nothing returned will return 0.
     * 
     * @param criteria
     *            Criteria
     * @return int
     */
    protected int getCount(Criteria criteria) {
        criteria.setProjection(Projections.rowCount());

        Integer result = (Integer) criteria.uniqueResult();
        if (result == null) {
            return 0;
        } else {
            return result;
        }
    }

    protected Criteria getExampleCriteria(T example) {
        Example hibExample = Example.create(example);

        return getBaseCriteria().add(hibExample);
    }

    protected Criteria addOrder(Criteria criteria, String column, boolean sortAsc) {
        if (sortAsc) {
            criteria.addOrder(Order.asc(column));
        } else {
            criteria.addOrder(Order.desc(column));
        }

        return criteria;
    }

    protected Criteria addDefaultOrder(Criteria criteria) {
        return addOrder(criteria, getDefaultOrder(), isDefaultOrderAsc());
    }

    public List<T> getByExample(T example) {
        Criteria crit = getExampleCriteria(example);
        addDefaultOrder(crit);
        return crit.list();
    }

    public String getDefaultOrder() {
        return defaultOrder;
    }

    public void setDefaultOrder(String defaultOrder) {
        this.defaultOrder = defaultOrder;
    }

    public boolean isDefaultOrderAsc() {
        return defaultOrderAsc;
    }

    public void setDefaultOrderAsc(boolean defaultOrderAsc) {
        this.defaultOrderAsc = defaultOrderAsc;
    }

    public void validateUniqueConstraints(T obj) throws DuplicateException{
        StringBuffer hql = new StringBuffer();
        String entityName = this.getEntityClass().getSimpleName();
        Map<Method, String> constraints = new LinkedHashMap<Method, String>();

        // find all the unique constraints
        for (Method m : obj.getClass().getMethods()) {
            if (m.isAnnotationPresent(Column.class) && m.getAnnotation(Column.class).unique()) {
                try {
                    Object value = m.invoke(obj, (Object[]) null);
                    if (value != null) {
                        constraints.put(m, (String) value);
                    }
                } catch (Exception e) {
                    throw new DaoException(e);
                }
            }
        }
        if (constraints.isEmpty()) {
            return;
        }

        hql.append("select a from ").append(entityName).append(" a");
        hql.append(" where ");
        if (obj.getId() != null) {
            hql.append("a.id != ? and ");
        }
        hql.append("(");
        for (Method m : constraints.keySet()) {
            String field = m.getName();
            hql.append(" lower(a.");
            hql.append(field.substring(3, 4).toLowerCase());
            hql.append(field.substring(4, field.length()));
            hql.append(") = lower(?) or");
        }

        hql.delete(hql.length() - 3, hql.length());
        hql.append(")");

        Query query = getSession().createQuery(hql.toString());
        int i = 0;
        if (obj.getId() != null) {
            query.setLong(i++, obj.getId());
        }
        for (String value : constraints.values()) {
            query.setString(i++, value);
        }

        List<T> duplicates = query.list();
        if (!duplicates.isEmpty()) {
            DuplicateException dupException = new DuplicateException();
            for (T dup : duplicates) {
                for (Method m : constraints.keySet()) {
                    try {
                        if (constraints.get(m).equalsIgnoreCase((String) m.invoke(dup, (Object[]) null))) {
                            String name = m.getName();
                            name = name.substring(3, 4).toLowerCase() + name.substring(4, name.length());
                            dupException.addDuplicateField(name, constraints.get(m));
                        }
                    } catch (Exception e) {
                        throw new BaseRuntimeException(e);
                    }
                }
            }
            throw dupException;
        }
    }



    // --------------------- CONVENIENCE METHODS ------------------------//

    /**
     * Convenience method for adding a like criteria. Doesn't add it if the
     * value is null or is an empty string.
     */
    public void addLike(Criteria criteria, String property, String value) {
        addLike(criteria, property, value, MatchMode.START);
    }

    public void addLike(Criteria criteria, String property, String value, MatchMode matchMode) {
        if (value != null && value.length() > 0) {
            criteria.add(Restrictions.ilike(property, value, matchMode));
        }
    }

    public void addEqual(Criteria criteria, String property, Object value) {
        criteria.add((value == null) ? Restrictions.isNull(property) : Restrictions.eq(property, value));
    }

}
