package com.fit.dao.activities;

import com.fit.utils.BaseRuntimeException;

public class ActivityNotFoundException extends BaseRuntimeException
{
	private static final long serialVersionUID = -3361088862548859567L;
	private Long activityId;

    public ActivityNotFoundException()
    {
        super();
    }

    public ActivityNotFoundException(Long activityId)
    {
        super();
        this.activityId = activityId;
    }

	public Long getActivityId() {
		return activityId;
	}
}
