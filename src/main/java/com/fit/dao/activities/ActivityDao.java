package com.fit.dao.activities;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fit.dao.Dao;
import com.fit.dao.DuplicateException;
import com.fit.model.activity.Activity;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.security.User;
import com.fit.mvc.command.ActivityDisplayCommand;
import com.fit.mvc.command.SearchFilterCommand;
import com.fit.utils.BaseRuntimeException;

/**
 * All activity methods
 * 
 * @author amourad
 * 
 */
public interface ActivityDao extends Dao<Activity> {

	public List<Activity> getUserActivities(Long userId);
	public Activity getActivityById(Long activityId);

	public ActivityDisplayCommand getActivityDisplayCommand(Long loggedUserId, Long activityId);

	public List<Map<String, Object>> getFeedActivitiesWithCounts(
			Long loggedUserId, Set<UserDiagnosis> usrDiagnosisSet, SearchFilterCommand searchFilterCommand,
			int startRow, int maxRows);

	public List<Map<String, Object>> getAllActivityTypes();
	
	public List<Map<String, Object>> getTotalActivitiesByUserId(Long userId);
	
	public Activity newActivity(Activity activity) throws DuplicateException, BaseRuntimeException;	
	public void deleteActivity(Long activityId) throws ActivityNotFoundException;	
	
	public int deleteTag(Long loggedUserId, Long taggedUserId);
	public Long addTag(User loggedUser, Long taggedUserId);
	public Long addActivityCheer(User currentUser, Long activityId);
	public int deleteActivityCheer(User currentUser, Long activityId);
	public List<Map<String, Object>> getActivityCheerUsers(Long activityId);
	public List<Map<String, Object>> getActivityComments(Long activityId, int startRow, int maxRows);
}
