package com.fit.dao.activities;

import com.fit.dao.Dao;
import com.fit.model.activity.ActivityComments;
import com.fit.model.security.User;

/**
 * All activity comments methods
 * 
 * @author amourad
 * 
 */
public interface ActivityCommentsDao extends Dao<ActivityComments> {

	public Long addNewComment(Long activityId, User currentUser, String newComment);
	
	
	/**
	 * get number of comments for the given user for the given activity
	 * 
	 * @param user
	 * @param activityId
	 * @return count of comments
	 */
	public long getUserActivityCommentCount(User user, Long activityId) ;
}
