package com.fit.dao.activities;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.activity.Activity;
import com.fit.model.activity.ActivityComments;
import com.fit.model.security.User;

@Repository
public class ActivityCommentsDaoHibernate extends
		BaseDaoHibernate<ActivityComments> implements ActivityCommentsDao {

	public Long addNewComment(Long activityId, User currentUser, String newComment) {
		getSession().clear();
    	Activity act = new Activity();
    	act.setId(Long.valueOf(activityId));
    	
    	ActivityComments comment = new ActivityComments();
    	comment.setComment(newComment);
    	comment.setActivity(act);
    	comment.setCreateDate(new Date());
    	comment.setCreatedBy(currentUser);
    	
    	Long commentId = (Long)getSession().save(comment);
		return commentId;
     }
	
	
	@SuppressWarnings("rawtypes")
	public long getUserActivityCommentCount(User user, Long activityId) {
		long nOfComments = 0;
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("select count(ac.createdBy.id) as userCommentsCount ");
		hql.append("from ActivityComments ac where ac.createdBy.id= :userId and ac.activity.id= :activityId group by ac.activity.id");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", user.getId());
		query.setParameter("activityId", activityId);
		List result = query.list();
		nOfComments = (result != null && result.size() > 0)?(long)result.get(0):nOfComments;
		
		return nOfComments;
	}
}
