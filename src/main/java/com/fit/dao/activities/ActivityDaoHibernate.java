package com.fit.dao.activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.dao.DuplicateException;
import com.fit.model.activity.Activity;
import com.fit.model.activity.ActivityCheers;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;
import com.fit.model.security.UserTags;
import com.fit.mvc.command.ActivityDisplayCommand;
import com.fit.mvc.command.SearchFilterCommand;
import com.fit.utils.BaseRuntimeException;

@Repository
public class ActivityDaoHibernate extends BaseDaoHibernate<Activity> implements
		ActivityDao {
	private static final Logger logger = LoggerFactory.getLogger(ActivityDaoHibernate.class);

	@SuppressWarnings("unchecked")
	public List<Activity> getUserActivities(Long userId) {
		getSession().clear();

		Criteria criteria = getSession().createCriteria(Activity.class);

		criteria.createAlias("user", "u", JoinType.LEFT_OUTER_JOIN);
		criteria.setFetchMode("u", FetchMode.JOIN);

		criteria.add(Restrictions.eq("u.id", userId));

		criteria.addOrder(Order.asc("activityDate"));

		List<Activity> activities = criteria.list();

		return activities;
	}

	/* (non-Javadoc)
	 * @see com.fit.dao.activities.ActivityDao#getActivityById(java.lang.Long)
	 */
	public Activity getActivityById(Long activityId) {
		StringBuffer hql = new StringBuffer();
		hql.append("from Activity a where a.id=:activityId");
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("activityId", activityId);
		return (Activity) query.uniqueResult();
	}	 

	
	
	public ActivityDisplayCommand getActivityDisplayCommand(Long loggedUserId, Long activityId) {
				
		StringBuffer hql = new StringBuffer();				
		hql.append("SELECT "); 
		hql.append("a.id as activityId, usr.id as ownerUserId, usr.displayName as ownerUserDisplayName, "); 
		hql.append("rat.id as activityType,	rat.name as activityTypeName, ");
		hql.append("a.title as activityTitle, a.activityDate as activityDate, a.zipCode as zip, "); 
		hql.append("a.city as city,	rsp.id as stateProvinceId, rsp.stateName as stateProvince, "); 
		hql.append("rc.countryName as country, rc.id as countryId, a.distance as distance, ");
		hql.append("a.startTime as startTime, a.duration as duration, a.energyLevelBefore as energyBeforeRating, ");
		hql.append("a.energyLevelAfter as energyAfterRating, a.symptoms as symptomsHelped, a.notes as notes, ");
		hql.append("a.public as showPublic, ");
		
		hql.append("(select count(ach.activity.id) from a.activityCheers ach ");
		hql.append(" where ach.activity.id= a.id group by ach.activity.id) as actCheersCount, ");
		
		hql.append("(select count(ach.createdBy.id) from a.activityCheers ach "); 
		hql.append(" where ach.createdBy.id= :loggedUserId group by ach.activity.id) as cheersByUserCount, ");
		
		hql.append("(select count(ac.createdBy.id) from a.activityComments ac "); 
		hql.append(" where ac.createdBy.id= :loggedUserId group by ac.activity.id) as commentsByUserCount, ");
		
		hql.append("(select count(ac.activity.id) from a.activityComments ac ");
		hql.append(" where ac.activity.id = a.id group by ac.activity.id) as actComCount ");
		
		hql.append("FROM Activity a ");
		hql.append("INNER JOIN a.user as usr ");
		hql.append("INNER JOIN a.activityType as rat ");
		hql.append("LEFT JOIN a.country as rc ");
		hql.append("LEFT JOIN a.state as rsp ");
		hql.append("WHERE a.id = :activityId ");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("activityId", activityId);
		query.setParameter("loggedUserId", loggedUserId);

		query.setResultTransformer(Transformers.aliasToBean(ActivityDisplayCommand.class));
		
		@SuppressWarnings("unchecked")
		List <ActivityDisplayCommand> resultWithAliasedBean = query.list();
		if (resultWithAliasedBean.isEmpty()) {
			return null;
		} else {
			ActivityDisplayCommand adc =(ActivityDisplayCommand) resultWithAliasedBean.get(0);	
			return adc;
		}
	}	 


	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fit.dao.activities.ActivityDao#getFeedActivitiesWithCounts(java.lang
	 * .Long, java.util.Set, int, int)
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getFeedActivitiesWithCounts(
			Long loggedUserId, Set<UserDiagnosis> usrDiagnosisSet, SearchFilterCommand searchFilterCommand,
			int startRow, int maxRows) {
		getSession().clear();
		Long userParentDiagId = null ;
		boolean userHasCancerDiag = false;
		boolean diagFilHasCancerFilter = false;
		boolean diagFilterChecked = false;
		Date cancerFreeDt = null;
		
		for (UserDiagnosis userDiagnosis : usrDiagnosisSet) {
			RefDiagnosisType refDiagType = userDiagnosis.getRefDiagnosisType();

			if (refDiagType.getDiagKey().indexOf("CANCER") > -1) {
				userHasCancerDiag = true;
			}
			userParentDiagId = (userDiagnosis.getRefDiagnosisType().getParentDiagnosis() != null)?userDiagnosis.getRefDiagnosisType()
					.getParentDiagnosis().getId():userDiagnosis.getRefDiagnosisType().getId();
			if(userParentDiagId == null) {
				userParentDiagId = userDiagnosis.getRefDiagnosisType().getId();
			}
			break;
			
		}
		
		//apply filter diagnosis if it's been passed
    	List<Long> diagIds = null;
		List<Map<String, Object>> diagFilter = (searchFilterCommand != null)?searchFilterCommand.getDiagFilter():null;
		
    	if( diagFilter != null && diagFilter.size() > 0) {
    		diagIds = new ArrayList<Long>();
    		
    		for(Map<String, Object> map:diagFilter) {
	    		Boolean isChecked = (map.get("isChecked") != null)?Boolean.valueOf((String)map.get("isChecked")):false;
	    		
	    		if(isChecked){
	    			diagFilterChecked = true;
	    			diagIds.add(((Integer)map.get("childDiagId")).longValue());
	    			if( ((String)map.get("parentDiagKey")).equals(RefDiagnosisType.PARENT_CANCER)) {
	    				diagFilHasCancerFilter = true;
	    			}
	    		}
	    	}
    	}
    	
    	//act types filter
    	List<Long> actTypesList = null;
    	actTypesList = prepareActTypeFilter(searchFilterCommand, actTypesList);
    	
    	String sortStr = (searchFilterCommand != null)?searchFilterCommand.getSelectedSort():null;
    	Integer minRadiationCompleted = (searchFilterCommand != null)?searchFilterCommand.getRadFilter():null;
    	Integer minChemoCompleted = (searchFilterCommand != null)?searchFilterCommand.getChemoFilter():null;

		
		// get activities for users with diagnosis as a parent
		StringBuffer hql = new StringBuffer();
		hql.append("select distinct new map(act.id as actId, act.title as actTitle, act.distance as actDistace, ");
		hql.append("act.energyLevelBefore as actEnerBef, act.energyLevelAfter as actEnerAfter, act.activityDate as actDate, ");
		hql.append("act.symptoms as actSymptoms, acType.id as actTypeId, acType.name as actTypeName, ");
		hql.append("st.stateName as activityState, country.countryName as activityCountry, ");
		hql.append("u.id as userId, u.displayName as userDisplName, ");
		//hql.append("u.city as userCity, st.stateKey as userStateKey,");
		
		boolean includeCancerTables = false;
		//no filters applied or if applied it should be cancer
		if( (!diagFilterChecked && userHasCancerDiag ) || (diagFilterChecked && diagFilHasCancerFilter)) {
			includeCancerTables = true;
		}
		// go against cancer table if logged in user has cancer diagnosis, or if selected diagn filter has cancer parent
		if (includeCancerTables) {
			hql.append("phases.chemoCompleted as chemoCompl, phases.chemoTotal as chemoTot, ");
			hql.append("phases.radiationCompleted as radCompl, phases.radiatonTotal as radTot,");
		}
		hql.append("ud.id as usrDiagId, refDiagType.name as refDiagTypeName, refDiagType.id as refDiagTypeId, refDiagType.diagKey as refDiagTypeKey, ");

		hql.append("(select count(ut.createdBy.id) from u.userTags ut where ");
		hql.append("ut.createdBy.id= :loggedUserId and ut.taggedUser.id=u.id group by ut.taggedUser.id) as tagsByUserCount, ");
		
		hql.append("(select count(ac.createdBy.id) from act.activityComments ac where ");
		hql.append("ac.createdBy.id= :loggedUserId group by ac.activity.id) as commentsByUserCount,");

		hql.append("(select count(ac.activity.id) from act.activityComments ac where ");
		hql.append("ac.activity.id = act.id group by ac.activity.id) as actComCount,");

		hql.append("(select count(ach.createdBy.id) from act.activityCheers ach where ");
		hql.append("ach.createdBy.id= :loggedUserId group by ach.activity.id) as cheersByUserCount, ");

		hql.append("(select count(ach.activity.id) from act.activityCheers ach where ");
		hql.append("ach.activity.id= act.id group by ach.activity.id) as actCheersCount ) ");
		
		

		hql.append("from Activity as act left join act.activityType as acType ");
		hql.append("left join act.activityCheers cheers ");
		hql.append("inner join act.user as u inner join u.userDiagnosis as ud ");
		hql.append("inner join ud.refDiagnosisType as refDiagType ");
		hql.append("left join act.state as st ");
		hql.append("left join act.country as country ");
		if(includeCancerTables) {
			hql.append("left join ud.userCancerTreatmentPhases phases ");
		}
		hql.append("where ");
		
		//apply diag filter
		if(diagIds != null && diagIds.size() > 0) {
			hql.append("refDiagType.id in (:diags) ");
		} else {
			//only for initial loading I'll match parent diagnosis or diagnosis itself, or when other than diagnosis filters are applied
			hql.append("(refDiagType.parentDiagnosis.id =:parentDiagId or refDiagType.id =:parentDiagId ) ");
		}
		//activity types filter
		if(actTypesList != null && actTypesList.size() > 0 ){
			hql.append(" and acType.id in (:actTypes) ");
		}
		
		if(includeCancerTables) {
			//apply chemo and rad filter
			if(minChemoCompleted != null){
				hql.append(" and phases.chemoCompleted >= :chemoCompl ");
			}
			if(minRadiationCompleted != null){
				hql.append(" and phases.radiationCompleted >= :radCompl ");
			}
		}
		//add diag end dt if it's been entered in filter as cancerFree date
		if(searchFilterCommand != null && searchFilterCommand.getCancerFreeDt() != null & searchFilterCommand.getCancerFreeDt().length() > 0 ) {
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			try {
				cancerFreeDt = formatter.parse(searchFilterCommand.getCancerFreeDt());
				hql.append("and ud.endDate >= :cancerFreeDt ");
			} catch (ParseException e) {
				logger.error("ActivityDaoHibernate: getFeedActivitiesWithCounts(): cancerFreeDt date parsing error: " + e.getMessage());
			}
		}
		
		hql.append("and u.enabled=true and u.locked=false and (act.public=true or act.public is null) ");
		//hql.append("ud.isPrimary=true ");
		
		String sortOrder = (sortStr != null)?sortStr.substring(sortStr.indexOf("_")+1):"";
		//sorting applied
		if(sortStr != null && sortStr.indexOf("date") > -1) {
			hql.append("order by act.activityDate " +  sortOrder + ", act.id, actComCount desc, actCheersCount desc ");
		} else if(sortStr != null && sortStr.indexOf("state") > -1) {
			hql.append("order by activityState " + sortOrder +", activityCountry " + sortOrder + ", act.id " );
		} else if(sortStr != null && sortStr.indexOf("popular") > -1) {
			hql.append("order by actComCount " + sortOrder + ", actCheersCount " + sortOrder + ", act.id ");
		}
		//default sort
		else {
			hql.append("order by act.activityDate desc, act.id, actComCount desc, actCheersCount desc");
		}

		Query query = getSession().createQuery(hql.toString());

		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);
		query.setParameter("loggedUserId", loggedUserId);
		
		//match by diagnosis in filters, or by user parent diagnosis
		if(diagIds != null && diagIds.size() > 0) {
			//check if diags is not null
			query.setParameterList("diags", diagIds);
		} else {
			query.setParameter("parentDiagId", userParentDiagId);
		}
		//by activity types
		if(actTypesList != null && actTypesList.size() > 0 ){
			query.setParameterList("actTypes", actTypesList);
		}
		//cancer tables
		if(includeCancerTables) {
			if(minChemoCompleted != null){
				query.setParameter("chemoCompl", minChemoCompleted);
			}
			if(minRadiationCompleted != null){
				query.setParameter("radCompl", minRadiationCompleted);
			}
		}
		if(cancerFreeDt != null ){
			query.setParameter("cancerFreeDt", cancerFreeDt);
		}
		return query.list();
	}

	private List<Long> prepareActTypeFilter(
			SearchFilterCommand searchFilterCommand, List<Long> actTypesList) {
		List<Map<String, Object>> actTypeFilter = (searchFilterCommand != null)?searchFilterCommand.getActTypeFilter():null;
    	if( actTypeFilter != null && actTypeFilter.size() > 0) {
    		actTypesList = new ArrayList<Long>();
    		
    		for(Map<String, Object> map:actTypeFilter) {
	    		Boolean isChecked = (map.get("isChecked") != null)?Boolean.valueOf((String)map.get("isChecked")):false;
	    		if(isChecked){
	    			actTypesList.add(((Integer)map.get("id")).longValue());
	    		}
	    	}
    	}
		return actTypesList;
	}


	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getAllActivityTypes() {
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(rat.id as id, rat.name as name, rat.description as description, rat.iconPath as iconPath) ");
		hql.append("from RefActivityType rat order by name asc");
		getSession().clear();

		Query query = getSession().createQuery(hql.toString());

		return query.list();
	}

	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>>  getTotalActivitiesByUserId(Long userId) {
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(count(*) as activityCount, SUM(a.distance) as miles, MIN(a.activityDate) as sTime, MAX(a.activityDate) as eTime, " );		
		hql.append("SUM(TIME_TO_SEC(a.duration)/3600) as totalHours) ");
		hql.append("from Activity a where a.user.id= :userId group by a.user.id");
		getSession().clear();

		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", userId);

		return query.list();
	}
	 
	public int deleteTag(Long loggedUserId, Long taggedUserId){
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("delete from UserTags ut where ut.taggedUser.id= :taggedUserId and ");
		hql.append("ut.createdBy.id= :loggedUserId");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("taggedUserId", taggedUserId);
		query.setParameter("loggedUserId", loggedUserId);
		
		int numDeleted = query.executeUpdate();
		return numDeleted;
	}
	
	public Long addTag(User loggedUser, Long taggedUserId) {
		getSession().clear();
		UserTags userTag =  new UserTags();
		userTag.setCreateDate(new Date());
		userTag.setCreatedBy(loggedUser);
		User taggedUser = new User();
		taggedUser.setId(taggedUserId);
		userTag.setTaggedUser(taggedUser);
		Long newTagId = (Long) getSession().save(userTag);
		
		return newTagId;
	}
	
	public Long addActivityCheer(User currentUser, Long activityId) {
		getSession().clear();
		ActivityCheers actCheers = new ActivityCheers();
		actCheers.setCreateDate(new Date());
		actCheers.setCreatedBy(currentUser);
		
		Activity activity = new Activity();
		activity.setId(activityId);
		actCheers.setActivity(activity);
		
		Long cheerId = (Long)getSession().save(actCheers);
		return cheerId;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getActivityComments(Long activityId, int startRow, int maxRows) {
		
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(ac.id as commentId, ac.comment as comment, usr.id as createdById, usr.displayName as displayName, ");
		hql.append("ac.createDate as commentCreateDt, ac.updateDate as commentUpdateDt ) ");
		hql.append("from ActivityComments ac  inner join ac.createdBy as usr ");
		hql.append("where ac.activity.id= :activityId order by ac.updateDate desc, ac.createDate desc ");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("activityId", activityId);
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);
		
		return query.list();
	}
	
	public int deleteActivityCheer(User currentUser, Long activityId) {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("delete from ActivityCheers ac where ac.createdBy.id= :loggedUserId and ");
		hql.append("ac.activity.id= :activityId");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("loggedUserId", currentUser.getId());
		query.setParameter("activityId", activityId);
		
		
		int numDeleted = query.executeUpdate();
		return numDeleted;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getActivityCheerUsers(Long activityId){
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(ac.id as cheerId, act.id as actId, usr.id as userId, usr.displayName as displayName )");
		hql.append("from ActivityCheers ac inner join ac.activity act inner join ac.createdBy as usr ");
		hql.append("where ac.activity.id= :activityId");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("activityId", activityId);
		
		return query.list();
		
	}

		/*
		 * Create a new Activity with passed in values.
		 * 
		 * @param Activity
		 * 
		 * @return Activity new activity with db identity
		 */
		@Override
		public Activity newActivity(Activity activity) throws DuplicateException,
				BaseRuntimeException {
			getSession().save(activity);
			return activity;
		}

		@Override
		public void deleteActivity(Long activityId)
				throws ActivityNotFoundException {
			Activity actToDelete = getActivityById(activityId);
			if (actToDelete == null) {
				throw new ActivityNotFoundException(activityId);
			} else {
				getSession().delete(getActivityById(activityId));
			}
		}
	}
