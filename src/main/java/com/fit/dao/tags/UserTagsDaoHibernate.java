/**
 * 
 */
package com.fit.dao.tags;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.security.UserTags;

/**
 * @author pavula1
 *
 */

@Repository
public class UserTagsDaoHibernate extends BaseDaoHibernate<UserTags> implements UserTagsDao {

	@Override
	public List<Map<String, Object>> getUserTagsById(Long userId) {
		
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(ut.id as tagId, usr.id as userId, usr.displayName as name, rdiag.name as diagnosis, ucdiag.chemoCompleted as chemoCompleted, ucdiag.radiationCompleted as radiationCompleted, ucdiag.chemoTotal as chemoTotal,  ucdiag.radiatonTotal as radiatonTotal, ut.createDate as taggedDate) ");
		hql.append("from UserTags ut  inner join ut.taggedUser as usr ");
		hql.append("left outer join usr.userDiagnosis as udiag ");
		hql.append("left outer join udiag.userCancerTreatmentPhases as ucdiag ");
		hql.append("inner join udiag.refDiagnosisType as rdiag ");
		hql.append("where ut.createdBy.id= :currentUser ");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("currentUser", userId);
		return query.list();
	}
	
	public int deleteTagById(Long tagId){
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		hql.append("delete from UserTags ut where ut.id= :tagId ");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("tagId", tagId);
		
		int numDeleted = query.executeUpdate();
		return numDeleted;
	}
	

}
