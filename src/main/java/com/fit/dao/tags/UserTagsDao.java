/**
 * 
 */
package com.fit.dao.tags;

import java.util.List;
import java.util.Map;

import com.fit.dao.Dao;
import com.fit.model.security.UserTags;

/**
 * @author pavula1
 * 
 */
public interface UserTagsDao extends Dao<UserTags> {

	public List<Map<String, Object>> getUserTagsById(Long userId);
	
	public int deleteTagById(Long tagId);

}
