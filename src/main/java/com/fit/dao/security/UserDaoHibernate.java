package com.fit.dao.security;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.dao.DuplicateException;
import com.fit.model.security.Authority;
import com.fit.model.security.User;
import com.fit.utils.BaseRuntimeException;

/**
 * @author amouradi
 *
 */
@Repository
public class UserDaoHibernate extends BaseDaoHibernate<User> implements UserDao {

	 static final Logger logger = Logger.getLogger(UserDaoHibernate.class);
	 @Resource(name="passwordEncoder")
	 private Md5PasswordEncoder passwordEncoder;


	/**
	 * return User based on emailAddress. There should be only one user bound to the same
	 * email address, which will be enforced when user gets created
	 */
	public User getUserByEmail(String emailAddress) {
		StringBuffer hql = new StringBuffer();
		User user = (User) getSession().createQuery(
					hql.append("from User u where u.emailAddress=?").toString())
					.setString(0, emailAddress).uniqueResult();
		return user;
	}

	public User loadUserAuthorities(User user){
	    User userWithAuths = (User) getSession().createCriteria(User.class)
                .setFetchMode("userAuthorities", FetchMode.JOIN)
                .add( Restrictions.idEq(user.getId()) )
                .uniqueResult();
	    
	    return userWithAuths;
	}
	
    
	
	
	/**
	 * create new user with passed values. check if user email address already has been
	 * created, and if it is throw DuplicateException. 
	 * BaseRuntimeException will be thrown
	 * if unique emailAdress constraint is violated or for any other sql exception
	 * @param User
	 * @return User new user with db identity
	 */
	public User newUser(User user) throws DuplicateException, BaseRuntimeException {
		StringBuffer hql = new StringBuffer();
		
		try {
			hql.append("from User u where u.emailAddress=?");
			List<User> users = getSession().createQuery(hql.toString()).setString(0, user.getEmailAddress()).list();
			if(users.size() > 0) {
				throw new DuplicateException("emailAddress", "exception.user.unique");
			} else {
				user.setPassword(this.passwordEncoder.encodePassword(user.getPassword(), user.getUserSalt()));
				getSession().save(user);
			}
		
		} catch (Exception e) {
			logger.error("-> Exception in UserDAOHibernate:newUser(): ", e);
			throw new BaseRuntimeException(e);
		}
		return user;
	}
	
	/**
	 * gets Authority entity
	 * @param roleName String, refer to Authority constants for the names
	 */
	public Authority getAuthority(String roleName) throws BaseRuntimeException {
		StringBuffer hql = new StringBuffer();
		Authority authority = null;
		try {
			hql.append("from Authority a where a.authorityName=?");
			authority = (Authority)getSession().createQuery(hql.toString()).setString(0, roleName).uniqueResult();
		} catch(Exception e) {
			logger.error("-> Exception in UserDAOHibernate:getAuthority(): ", e);
			throw new BaseRuntimeException(e);
		}
		return authority;
	}
	
	public Map<String, Object> loadUserAllProfileData(Long userId) {
		
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(u.id as userId, u.displayName as displName, u.firstName as firstName, u.streetAddress1 as address1, " );
		hql.append("u.streetAddress2 as address2, u.lastName as lastName, u.zipCode as zipCode, u.emailAddress as emailAddress, u.cellPhone as cellPhone, ");
		hql.append("u.birthDate as birthDate, u.city as city, c.id as countryId, c.countryKey as countryKey, c.countryName as countryName, ");
		hql.append("u.showPrivate as showPrivate, u.healthyDate as healthyDate, st.id as stateId, st.stateKey as stateKey, st.stateName as stateName, u.salt as salt ) ");
		hql.append("from User u left join u.country c left join u.state st where u.id= :userId");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", userId);
		
		Map<String, Object> usrMap = new HashMap<String, Object>();
		List<Map<String, Object> > list = query.list();
		usrMap = (list != null && list.size() > 0)?list.get(0):usrMap;
		
		return usrMap;
	}
	
	public Map<String, Object> verifyUserPassword(Long userId, String password) {
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(u.id as userId) ");
		hql.append("from User u where u.id= :userId and u.password= :password");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", userId);
		query.setParameter("password", password);
		
		Map<String, Object> usrMap = new HashMap<String, Object>();
		List<Map<String, Object> > list = query.list();
		usrMap = (list != null && list.size() > 0)?list.get(0):usrMap;
		
		return usrMap;
	}
	
	public Map<String, Object> loadUserPublicProfileData(Long userId) {
		
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(u.id as userId, u.displayName as displName, u.zipCode as zipCode, " );
		hql.append("c.id as countryId, c.countryKey as countryKey, c.countryName as countryName, u.showPrivate as showPrivate,");
		hql.append("st.id as stateId, st.stateKey as stateKey, st.stateName as stateName) ");
		hql.append("from User u left join u.country c left join u.state st where u.id= :userId ");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", userId);
		
		Map<String, Object> usrMap = new HashMap<String, Object>();
		List<Map<String, Object> > list = query.list();
		usrMap = (list != null && list.size() > 0)?list.get(0):usrMap;
		
		return usrMap;
	}
	
	public boolean canShowPrivateData(Long userId) {
		StringBuffer hql = new StringBuffer();
		hql.append("select u.showPrivate as showPrivate from User u where u.id= :userId");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", userId);
		
		Boolean showPrivate = false;
		List<Boolean> list = query.list();
		showPrivate = (list != null && list.size() > 0)?(Boolean)list.get(0):showPrivate;
		
		return showPrivate;
	}
	

 
	 
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTaggedByUser(Long userId) {
		StringBuffer hql = new StringBuffer();
		hql.append("select new map(u.id as userId, u.displayName as displayName, ut.id as userTagId )");
		hql.append("from UserTags ut inner join ut.taggedUser u ");
		hql.append("where ut.createdBy.id= :userId order by u.displayName asc");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", userId);
		
		return query.list();
	}
	
	/**
	 * return authenticated user
	 * @return
	 */
	public User getAuthUser() {
		Authentication a = SecurityContextHolder.getContext().getAuthentication();
		return (User) a.getPrincipal();
	}
	
	/**
	 * is user authenticated
	 * @return boolean
	 */
	public boolean isAuthenticated(){
		Authentication a = SecurityContextHolder.getContext().getAuthentication();
		return a.isAuthenticated();
	}

	/**
	 * @return the passwordEncoder
	 */
	public Md5PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}



	/**
	 * @param passwordEncoder the passwordEncoder to set
	 */
	public void setPasswordEncoder(Md5PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}


}
