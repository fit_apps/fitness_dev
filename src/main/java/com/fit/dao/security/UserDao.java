package com.fit.dao.security;

//import org.springframework.security.providers.encoding.PasswordEncoder;

import java.util.List;
import java.util.Map;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import com.fit.dao.Dao;
import com.fit.dao.DuplicateException;
import com.fit.model.security.Authority;
import com.fit.model.security.User;
import com.fit.utils.BaseRuntimeException;

public interface UserDao extends Dao<User> {
	
	public User getUserByEmail(String emailAddress);
	public User newUser(User user) throws DuplicateException, BaseRuntimeException;
	public Md5PasswordEncoder getPasswordEncoder();
	public void setPasswordEncoder(Md5PasswordEncoder passwordEncoder);
	public Authority getAuthority(String roleName) throws BaseRuntimeException ;
	public User getAuthUser() ;
	public boolean isAuthenticated();
	public User loadUserAuthorities(User user);
	public List<Map<String, Object>> getTaggedByUser(Long userId);
	public Map<String, Object> loadUserAllProfileData(Long userId);
	public Map<String, Object> loadUserPublicProfileData(Long userId);
	public boolean canShowPrivateData(Long userId);
	public Map<String, Object> verifyUserPassword(Long userId, String password);
}
