package com.fit.dao;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fit.utils.BaseRuntimeException;


/**
 * This exception is thrown when a duplicate record is detected when performing
 * save, saveOrUpdate, update or merge
 * 
 */
public class DuplicateException extends BaseRuntimeException
{
    private static final long serialVersionUID = -8409979046135696990L;

    private Map<String, String> m_duplicateFields = new LinkedHashMap<String, String>();

    public DuplicateException()
    {
    }

    public DuplicateException(String propertyName, String propertyValue)
    {
        this.addDuplicateField(propertyName, propertyValue);
    }

    public Map<String, String> getDuplicateFields()
    {
        return this.m_duplicateFields;
    }

    public void addDuplicateField(String propertyName, String propertyValue)
    {
        this.m_duplicateFields.put(propertyName, propertyValue);
    }
}
