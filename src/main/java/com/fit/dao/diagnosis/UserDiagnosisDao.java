package com.fit.dao.diagnosis;

import com.fit.dao.Dao;
import com.fit.model.diagnosis.UserDiagnosis;

public interface UserDiagnosisDao extends Dao<UserDiagnosis>{

}
