package com.fit.dao.diagnosis;

import com.fit.dao.Dao;
import com.fit.model.diagnosis.UserCancerTreatmentPhases;

/**
 * @author amourad
 *
 */
public interface UserCanTreatPhasesDao extends Dao<UserCancerTreatmentPhases>{

}
