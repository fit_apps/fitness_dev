package com.fit.dao.diagnosis;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;

@Repository
public class DiagnosisDaoHibernate extends BaseDaoHibernate<RefDiagnosisType>
		implements DiagnosisDao {

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getAllParentDiagnosis() {

		StringBuffer hql = new StringBuffer();
		hql.append("select new map(rft.id as refDiagId, rft.diagKey as diagKey, rft.name as refDiagName) ");
		hql.append("from RefDiagnosisType rft where rft.parentDiagnosis is null");
		getSession().clear();

		Query query = getSession().createQuery(hql.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getAllDiagnHierarchy() {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		// inner join act.user as u
		hql.append("select new map(rdt1.id as childDiagId, rdt1.diagKey as childDiagKey, rdt1.name as childDiagName, ");
		hql.append("rdt2.id as parentDiagId, rdt2.diagKey as parentDiagKey, rdt2.name as parentDiagName  )");
		hql.append("from RefDiagnosisType rdt1 inner join rdt1.parentDiagnosis rdt2 ");
		hql.append("order by  parentDiagName, childDiagName");
		
		Query query = getSession().createQuery(hql.toString());

		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getUserTreatments(User user){
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		Map<String, Object> trMap = null;
		
		hql.append("select new map(uct.id as phaseId, uct.chemoCompleted as chemoCompleted, uct.chemoTotal as chemoTotal, ");
		hql.append("uct.radiationCompleted as radCompleted, uct.radiatonTotal as radTotal, rcf.id as refChemoFreqId ) ");
		hql.append("from User user inner join user.userDiagnosis ud inner join ud.userCancerTreatmentPhases uct ");
		hql.append("inner join uct.refChemoFreq rcf ");
		hql.append("where user.id= :userId");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", user.getId());
		List<Map<String, Object>> result = query.list();
		trMap = (result != null && result.size() > 0)?result.get(0):null;
		
		return trMap;
	}
	
	/** 
	* just get user diagnosis, not joining to cancer tables yet.
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getUserDiagnosis(User user) {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		
		hql.append("select new map(ud.id as userDiagId, ud.startDate as userDiagStartDt, ud.endDate as userDiagEndDt, ");
		hql.append("ud.isPrimary as userDiagIsPrimary, ud.user.id as userId, rdt.name as refDiagTypeName, rdt.diagKey as refDiagKey) ");
		hql.append("from UserDiagnosis ud inner join ud.refDiagnosisType rdt where ud.user.id= :userId ");
		hql.append("order by ud.isPrimary desc, ud.startDate desc");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", user.getId());
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getUserDiagProcedures(Long userDiagId) {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		
		hql.append("select new map(udp.userDiagnosis.id as userDiagId, udp.id as procId, udp.procedureDate as procDt, ");
		hql.append("pt.id as procTypeId, pt.name as procTypeName, pt.description as procTypeDesc )");
		hql.append("from UserCancerDiagnosisProcedures udp inner join udp.refCancerProcedureType pt ");
		hql.append("where udp.userDiagnosis.id= :userDiagId order by udp.procedureDate desc");
		
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userDiagId", userDiagId);
		return query.list();
	}
	
	/**
	 * getting diagnoses and procedures and treatments
	 *  (non-Javadoc)
	 * @see com.fit.dao.diagnosis.DiagnosisDao#getUserDiagAll(java.lang.Long)
	 */
	public List<Map<String, Object>> getUserDiagAll(Long userId) {
		getSession().clear();
		StringBuffer hql = new StringBuffer();
		
		hql.append("select new map(ud.id as userDiagId, ud.startDate as userDiagStartDt, ud.endDate as userDiagEndDt,");
		hql.append("ud.isPrimary as userDiagIsPrimary, ud.user.id as userId, rdt.name as refDiagTypeName,rdt.diagKey as refDiagKey, ");
		hql.append("rdt.id as refDiagTypeId, rpt.id as procId, rpt.name as procName, up.procedureDate as procDate, uct.id as treatmId, ");
		hql.append("uct.chemoCompleted as chemoCompleted, uct.chemoTotal as chemoTotal, chFr.displayName as chemFreq, ");
		hql.append("chFr.refChemoFreqKey as chemoFreqKey, chFr.id as chemoFreqId, uct.radiationCompleted as radCompleted, uct.radiatonTotal as radTotal ) ");
		hql.append("from UserDiagnosis ud inner join ud.refDiagnosisType rdt left join ud.userCancerDiagnosisProcedures up ");
		hql.append("left join up.refCancerProcedureType rpt ");
		hql.append("left join ud.userCancerTreatmentPhases uct left join uct.refChemoFreq chFr ");
		hql.append("where ud.user.id= :userId order by ud.isPrimary desc, ud.startDate asc, up.procedureDate asc");
		
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("userId", userId);
		return query.list();
	}

}
