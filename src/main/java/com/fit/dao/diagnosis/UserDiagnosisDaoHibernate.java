package com.fit.dao.diagnosis;

import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.diagnosis.UserDiagnosis;

/**
 * @author amouradian
 *
 */
@Repository
public class UserDiagnosisDaoHibernate extends
		BaseDaoHibernate<UserDiagnosis> implements
		UserDiagnosisDao {

}
