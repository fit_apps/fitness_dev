package com.fit.dao.diagnosis;

import java.util.List;
import java.util.Map;

import com.fit.dao.Dao;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;

/**
 * All activity methods
 * @author amourad
 *
 */
public interface DiagnosisDao extends Dao<RefDiagnosisType> {
	
	public List<Map<String, Object>> getAllParentDiagnosis();
	public List<Map<String, Object>> getAllDiagnHierarchy();
	public Map<String, Object> getUserTreatments(User user);
	public List<Map<String, Object>> getUserDiagnosis(User user);
	public List<Map<String, Object>> getUserDiagProcedures(Long userDiagId);
	public List<Map<String, Object>> getUserDiagAll(Long userDiagId) ;
	
}
