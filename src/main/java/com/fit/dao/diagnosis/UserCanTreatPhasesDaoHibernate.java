package com.fit.dao.diagnosis;

import org.springframework.stereotype.Repository;

import com.fit.dao.BaseDaoHibernate;
import com.fit.model.diagnosis.UserCancerTreatmentPhases;

/**
 * @author amourad
 *
 */
@Repository
public class UserCanTreatPhasesDaoHibernate extends
		BaseDaoHibernate<UserCancerTreatmentPhases> implements
		UserCanTreatPhasesDao {

}
