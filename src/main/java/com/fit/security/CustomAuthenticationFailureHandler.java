package com.fit.security;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.fit.mvc.command.LoginCommand;
import com.google.gson.Gson;

/**
 * Custom handler to process the logon failure
 * 
 * @author channah
 * 
 */

public class CustomAuthenticationFailureHandler extends
		SimpleUrlAuthenticationFailureHandler {

	@Autowired
	private ApplicationContext context;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

		Gson gson = new Gson();

		LoginCommand loginCommand = new LoginCommand();
		loginCommand.setSuccess(false);
		loginCommand.setRedirect(false);
		loginCommand.setRedirectURL(null);
		loginCommand.setMessage(null);

		if (exception.getClass()
				.isAssignableFrom(BadCredentialsException.class)) {
			loginCommand.setMessage(context.getMessage(
					"err.login.bad.credentials", null, Locale.getDefault()));
		} else if (exception.getClass().isAssignableFrom(
				DisabledException.class)) {
			loginCommand
					.setMessage(context.getMessage(
							"err.login.diabled.credentials", null,
							Locale.getDefault()));
		} else {
		    logger.error("Unknown authentication exception, Message: " + exception.getLocalizedMessage()
		            + (exception.getCause() != null ? ",  Cause:" + exception.getCause().getLocalizedMessage() : ""));
		    loginCommand.setMessage(context.getMessage("err.admin", null, Locale.getDefault()));
		}
		try {
			response.getWriter().println(gson.toJson(loginCommand));
			response.getWriter().flush();
		} catch (IOException e) {
			logger.error("Unable to write login object to response");
		}
		return;
	}
}
