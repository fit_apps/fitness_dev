package com.fit.security;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;
import com.fit.utils.UserUtils;

public class CustomJdbcDaoImpl extends JdbcDaoImpl {

	private String userDiagnosisByUsernameQuery;

	public String getUserDiagnosisByUsernameQuery() {
		return userDiagnosisByUsernameQuery;
	}

	public void setUserDiagnosisByUsernameQuery(
			String userDiagnosisByUsernameQuery) {
		this.userDiagnosisByUsernameQuery = userDiagnosisByUsernameQuery;
	}

	@Override
	/*
	 * protected void initMappingSqlQueries() { this.usersByUsernameQuery = new
	 * UsersByUsernameMapping(getDataSource());
	 * this.authoritiesByUsernameMapping = new
	 * AuthoritiesByUsernameMapping(getDataSource()); }
	 */
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		List users = new UsersByUsernameMapping(getDataSource())
				.execute(username);
		if (users.size() == 0) {
			throw new UsernameNotFoundException("User not found");
		}
		User user = (User) users.get(0); // contains no
		// GrantedAuthority[]
		List dbAuths = new AuthoritiesByUsernameMapping(getDataSource())
				.execute(user.getUsername());
		addCustomAuthorities(user.getUsername(), dbAuths);
		if (dbAuths.size() == 0) {
			throw new UsernameNotFoundException("User has no GrantedAuthority");
		}
		// this.passwordEncoder.isPasswordValid(user.getPassword() , rawPass,
		// salt)
		Collection<? extends GrantedAuthority> arrayAuths = dbAuths;
		List userDiagnosis = new UserDiagnosisByUsernameMapping(getDataSource())
		.execute(user.getUsername());

		User newUser = new User(user.getId(), user.getFirstName(),
				user.getLastName(), user.getDisplayName(), user.getSalt(),
				new org.springframework.security.core.userdetails.User(
						user.getUsername(), user.getPassword(),
						user.isEnabled(), true, true, true, arrayAuths));
		// load user with diagnosis
		newUser.setUserDiagnosis(new HashSet<UserDiagnosis>(userDiagnosis));
		return newUser;
	}

	/**
	 * Query object to look up user's authorities.
	 */
	protected class AuthoritiesByUsernameMapping extends MappingSqlQuery {
		protected AuthoritiesByUsernameMapping(DataSource ds) {
			super(ds, getAuthoritiesByUsernameQuery());
			declareParameter(new SqlParameter(Types.VARCHAR));
			compile();
		}

		protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
			String roleName = getRolePrefix() + rs.getString(2);
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority(
					roleName);

			return authority;
		}
	}

	@SuppressWarnings("rawtypes")
	protected class UserDiagnosisByUsernameMapping extends MappingSqlQuery {
		protected UserDiagnosisByUsernameMapping(DataSource ds) {
			super(ds, getUserDiagnosisByUsernameQuery());
			declareParameter(new SqlParameter(Types.VARCHAR));
			compile();
		}

		protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
			UserDiagnosis userDiag = new UserDiagnosis();
			userDiag.setId(rs.getLong("user_diag_id"));
			userDiag.setIsPrimary(rs.getBoolean("is_primary"));
			
			RefDiagnosisType diagType = new RefDiagnosisType();
		    diagType.setId(rs.getLong("ref_diag_type_id"));
		    diagType.setDiagKey(rs.getString("ref_diag_key"));
		    Object parentDiagId = rs.getObject("parent_diag_id");
		    
		    if( parentDiagId != null) {
		    	RefDiagnosisType parentDiagType = new RefDiagnosisType();
		    	parentDiagType.setId(Long.valueOf((parentDiagId.toString())));
		    	diagType.setParentDiagnosis(parentDiagType);
		    } else {
		    	diagType.setParentDiagnosis(null);
		    }
			 
		    userDiag.setRefDiagnosisType(diagType);
			return userDiag;
		}
	}

	/**
	 * Query object to look up a user.
	 */
	protected class UsersByUsernameMapping extends MappingSqlQuery {
		protected UsersByUsernameMapping(DataSource ds) {
			super(ds, getUsersByUsernameQuery());
			declareParameter(new SqlParameter(Types.VARCHAR));
			compile();
		}

		protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
			List<GrantedAuthority> ga = new ArrayList<GrantedAuthority>();
			ga.add(new SimpleGrantedAuthority("HOLDER"));
			
			UserDetails user = new User(rs.getLong("user_id"),
					rs.getString("first_name"), rs.getString("last_name"), rs.getString("display_name"),
					rs.getString("salt"),
					new org.springframework.security.core.userdetails.User(rs
							.getString("username"), rs.getString("password"),
							rs.getBoolean("enabled"), UserUtils
									.isAccountNonExpired(rs
											.getDate("archive_dt")), UserUtils
									.isAccountNonExpired(rs
											.getDate("archive_dt")), rs
									.getBoolean("locked"), ga));;

			return user;
		}
	}
}
