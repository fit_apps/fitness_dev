package com.fit.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.fit.model.security.User;
import com.fit.mvc.command.LoginCommand;
import com.fit.service.security.UserService;
import com.fit.utils.SecurityUtil;
import com.google.gson.Gson;

/**
 * Custom handler to redirect to original url after login, for requests that are
 * bookmarked or come from email links.
 * 
 * @author amouradi
 * 
 */
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	private UserService userService;
	
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
    	
		Gson gson = new Gson();
		
		LoginCommand loginCommand = new LoginCommand();
		loginCommand.setSuccess(true);
		loginCommand.setRedirect(true);
		loginCommand.setRedirectURL(null);
		
    	User currentUser = (User) SecurityUtil.getCurrentUser();
    	currentUser.setLoginDate(new Date());
    	
    	// Set<RefDiagnosisType> curDiags = currentUser.getUserDiagnosisTypes();
    	 //currentUser = userService.loadUserWithAuths(currentUser);
    	 HttpSession session = (request.getSession(false) == null)? request.getSession(true):request.getSession(false);
    	 session.setAttribute(SecurityUtil.CURRENT_USER, currentUser);
     	
    	//load user diagnosis and set them into user set
    	///currentUser.setUserDiagnosisTypes(userService.getUserDiagnosis(currentUser.getId()));
    	
        SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
        if (savedRequest == null) {
           // super.onAuthenticationSuccess(request, response, authentication); 
        	 //response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
//        	 String targetUrl = request.getContextPath() + determineTargetUrl(request, response);
//        	 response.sendRedirect(targetUrl);
          //  response.setHeader("Location", response.encodeRedirectURL(targetUrl));
        	loginCommand.setRedirectURL(request.getContextPath() + determineTargetUrl(request, response));
        } else {
        	loginCommand.setRedirectURL(savedRequest.getRedirectUrl());
        }
		try {
			response.getWriter().println(gson.toJson(loginCommand));
			response.getWriter().flush();
		} catch (IOException e) {
			logger.error("Unable to write login object to response");
		}
    }

}
