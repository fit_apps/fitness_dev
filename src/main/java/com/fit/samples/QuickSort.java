package com.fit.samples;

public class QuickSort {
	private int array[];
	private int length;
	
	QuickSort(){}
	
	public void sort(int[] inputArr){
		if(inputArr == null || inputArr.length == 0){
			return;
		}
		
		this.array = inputArr;
		this.length = this.array.length;
		quickSort(0, length-1);
	}
	
	//  int[] input = {24,2,45,20,56,75,2,56,99,53,12};
	private void quickSort(int lowerIndex, int higherIndex){
		int i = lowerIndex;
		int j = higherIndex;
		
		int pivotElem = array[(lowerIndex + (higherIndex - lowerIndex))/2];
		System.out.println("---pivotElem: " + pivotElem);
		
		//divide into two arrays
		while (i<=j){
			while(array[i]<pivotElem){
				System.out.println("---i: " + i);
				System.out.println("---array[i]: " + array[i]);
				i++;
			}
			
			
			while (array[j]>pivotElem){
				System.out.println("---j: " + j);
				System.out.println("---array[j]: " + array[j]);
				j--;
			}

			if(i <= j){
				//swap
				int temp = array[i];
				System.out.println("---swap array[i] is: " + array[i]);
				System.out.println("---swap array[j] is: " + array[j]);
				array[i] = array[j];
				array[j] = temp;
				
				i++;
				j--;
				if(lowerIndex < j){
					quickSort(lowerIndex, i);
				}
				if(i<higherIndex){
					quickSort(i, higherIndex);
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int[] input = {24,2,45,20,56,75,2,56,99,53,12};
		QuickSort quickSort = new QuickSort();
		quickSort.sort(input);
		
	}

}
