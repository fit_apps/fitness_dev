package com.fit.samples;

import java.util.LinkedList;

public class ReverseList {

	public void revers (){
		LinkedList<String> list = new LinkedList<String>();
		list.add("I am");
		list.add("smart");
		list.add("gal");
		list.add("really");

		LinkedList<String> newList = new LinkedList<String>();
		int i=list.size()-1;
		while(i>=0){
			newList.add(list.get(i));
			i--;
		}
		System.out.println("---newList size: " + newList.size());
		for(int j=0; j<newList.size(); j++) {
			System.out.println("---newList: " + newList.get(j));
		}
	}
	
	public static void main(String[] args) {
		ReverseList revList = new ReverseList();
		revList.revers();
	}
}
