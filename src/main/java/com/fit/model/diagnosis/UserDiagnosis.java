/**
 * 
 */
package com.fit.model.diagnosis;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

import com.fit.model.base.GeneratedIdEntity;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;

/**
 * @author pavula1
 * 
 */
/**
 * @author amourad
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "user_diag_id")) })
@Table(name = "user_diagnosis")
public class UserDiagnosis extends GeneratedIdEntity{

	private static final long serialVersionUID = 7778832054700127170L;

	private User user;
	private RefDiagnosisType refDiagnosisType;
	private Boolean isPrimary;
	private Date startDate;
	private Date endDate;
	private UserCancerTreatmentPhases userCancerTreatmentPhases;
	private List<UserCancerDiagnosisProcedures> userCancerDiagnosisProcedures;
	
	public UserDiagnosis() {
		
	}

	//@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable=false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ref_diagnosis_type_id", nullable=false)
	@ForeignKey(name = "user_diag_to_ref_diag_type_fk", inverseName = "ref_diag_type_to_user_diag_fk")	
	public RefDiagnosisType getRefDiagnosisType() {
		return refDiagnosisType;
	}

	public void setRefDiagnosisType(RefDiagnosisType refDiagnosisType) {
		this.refDiagnosisType = refDiagnosisType;
	}
	

	@Column(name = "is_primary", columnDefinition = "bit DEFAULT false")
	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean primary) {
		this.isPrimary = primary;
	}
	

	@Temporal(TemporalType.DATE)
	@Column(name = "start_dt")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "end_dt")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	@OneToOne(mappedBy = "userDiagnosis", cascade=CascadeType.ALL)
	public UserCancerTreatmentPhases getUserCancerTreatmentPhases() {
		return userCancerTreatmentPhases;
	}

	public void setUserCancerTreatmentPhases(UserCancerTreatmentPhases userCancerTreatmentPhases) {
		this.userCancerTreatmentPhases = userCancerTreatmentPhases;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userDiagnosis", fetch = FetchType.LAZY)
	public List<UserCancerDiagnosisProcedures> getUserCancerDiagnosisProcedures() {
		return userCancerDiagnosisProcedures;
	}

	public void setUserCancerDiagnosisProcedures(
			List<UserCancerDiagnosisProcedures> userCancerDiagnosisProcedures) {
		this.userCancerDiagnosisProcedures = userCancerDiagnosisProcedures;
	}

	

}
