/**
 * 
 */
package com.fit.model.diagnosis;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.fit.model.base.GeneratedIdEntity;
import com.fit.model.refs.RefCancerProcedureType;

/**
 * @author pavula1
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "procedure_id")) })
@Table(name = "user_cancer_diagnosis_procedures")
public class UserCancerDiagnosisProcedures extends GeneratedIdEntity{

	private static final long serialVersionUID = 7778832054700127170L;
	
	private RefCancerProcedureType refCancerProcedureType;
	private UserDiagnosis userDiagnosis;
	private Date procedureDate;

	
	public UserCancerDiagnosisProcedures() {
	}


	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "ref_cancer_procedure_type_id", nullable=false)
	@ForeignKey(name = "can_proc_to_can_proc_type_fk", inverseName = "can_proc_type_to_can_proc_fk")
	public RefCancerProcedureType getRefCancerProcedureType() {
		return refCancerProcedureType;
	}


	public void setRefCancerProcedureType(
			RefCancerProcedureType refCancerProcedureType) {
		this.refCancerProcedureType = refCancerProcedureType;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_diag_id", nullable=false)
	@ForeignKey(name = "can_proc_to_user_diag_fk", inverseName = "user_diag_to_can_proc_fk")	
	public UserDiagnosis getUserDiagnosis() {
		return userDiagnosis;
	}


	public void setUserDiagnosis(UserDiagnosis userDiagnosis) {
		this.userDiagnosis = userDiagnosis;
	}


	public Date getProcedureDate() {
		return procedureDate;
	}


	public void setProcedureDate(Date procedureDate) {
		this.procedureDate = procedureDate;
	}

}
