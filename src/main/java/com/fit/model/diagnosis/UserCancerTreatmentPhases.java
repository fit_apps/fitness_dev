/**
 * 
 */
package com.fit.model.diagnosis;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.fit.model.base.GeneratedIdEntity;
import com.fit.model.refs.RefChemoFreq;

/**
 * @author pavula1
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "user_cancer_treat_phase_id")) })
@Table(name = "user_cancer_treatment_phases")
public class UserCancerTreatmentPhases extends GeneratedIdEntity {
	
	private static final long serialVersionUID = 7778832054700127170L;
	
	private Integer chemoCompleted;
	private Integer chemoTotal;
	private Integer radiationCompleted;
	private Integer radiatonTotal;
	private RefChemoFreq refChemoFreq;
	private UserDiagnosis userDiagnosis;

	public UserCancerTreatmentPhases() {
	}

	@Column(name="chemo_completed")
	public Integer getChemoCompleted() {
		return chemoCompleted;
	}

	public void setChemoCompleted(Integer chemoCompleted) {
		this.chemoCompleted = chemoCompleted;
	}

	@Column(name="chemo_total")
	public Integer getChemoTotal() {
		return chemoTotal;
	}

	public void setChemoTotal(Integer chemoTotal) {
		this.chemoTotal = chemoTotal;
	}

	@Column(name="radiation_completed")
	public Integer getRadiationCompleted() {
		return radiationCompleted;
	}

	public void setRadiationCompleted(Integer radiationCompleted) {
		this.radiationCompleted = radiationCompleted;
	}

	@Column(name="radiation_total", length=100)
	public Integer getRadiatonTotal() {
		return radiatonTotal;
	}

	public void setRadiatonTotal(Integer radiatonTotal) {
		this.radiatonTotal = radiatonTotal;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ref_chemo_freq_id", nullable=true)
	@ForeignKey(name = "can_treat_to_ref_chemo_freq_fk", inverseName = "ref_chemo_freq_to_can_treat_fk")	
	public RefChemoFreq getRefChemoFreq() {
		return refChemoFreq;
	}

	public void setRefChemoFreq(RefChemoFreq refChemoFreq) {
		this.refChemoFreq = refChemoFreq;
	}


	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "user_diag_id", nullable=true )
	@ForeignKey(name = "can_treat_to_user_diag_fk", inverseName = "user_diag_to_can_treat_fk")	
	public UserDiagnosis getUserDiagnosis() {
		return userDiagnosis;
	}

	public void setUserDiagnosis(UserDiagnosis userDiagnosis) {
		this.userDiagnosis = userDiagnosis;
	}

}
