package com.fit.model.base;

import java.io.Serializable;


/**
 * Interface to implement if entity has a numeric identity.
 * 
 * @author mouradan
 */
public interface Identifiable extends Serializable
{
    public Long getId();

    public void setId(Long id);
}
