package com.fit.model.base;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 * Abstract Base entity, could be extended by model entities, in case they need
 * to define their own ID generation strategy. If {@code GenerationType.AUTO} is
 * acceptable , then it's advised to extend {@link GeneratedIdEntity} .
 * 
 * @author mouradan
 */
@MappedSuperclass
public abstract class BaseEntity implements Identifiable
{

    private Long id;

    @Transient
    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Equality is based on the id and class
     */
    public boolean equals(Object obj)
    {
        boolean isEq = false;

        // not null, is same class, has same id
        if (obj != null && obj.getClass().isInstance(this))
        {
            Identifiable identifiableObj = (Identifiable) obj;
            if (identifiableObj.getId() != null && identifiableObj.getId().equals(this.getId()))
            {
                isEq = true;
            }
        }

        return isEq;
    }

    /**
     * Returns the Id if it's available
     */
    @Override
    public int hashCode()
    {
        if (getId() == null)
        {
            return super.hashCode();
        }
        else
        {
            return getId().intValue();
        }
    }

    /**
     * Convenience method for trimming strings.
     * 
     * @param s
     * @return trimmed string
     */
    protected String trim(String s)
    {
        return s != null ? s.trim() : s;
    }

}
