package com.fit.model.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fit.model.security.User;


@MappedSuperclass
public abstract class AuditableEntity extends GeneratedIdEntity implements Auditable
{
	private static final long serialVersionUID = 1455623109662796609L;
	private User createdBy;
    private User updatedBy;
    private Date createDate;
    private Date updateDate;

  //  @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", updatable = false)
    public Date getCreateDate()
    {
        return this.createDate;
    }

    public void setCreateDate(Date date)
    {
        this.createDate = date;
    }

    
    /**
     * @return the updateDate
     */
   // @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    public Date getUpdateDate()
    {
        return updateDate;
    }

    /**
     * @param updateDate Date
     */
    public void setUpdateDate(Date updateDate)
    {
        this.updateDate = updateDate;
    }

    /**
     * User who created the entity.
     * 
     * @return User
     */
   // @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by", updatable = false)
    public User getCreatedBy()
    {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy)
    {
        this.createdBy = createdBy;
    }

    /**
     * User who last updated an entity.
     * 
     * @return User
     */
   // @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "updated_by")
    public User getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(User updatedBy)
    {
        this.updatedBy = updatedBy;
    }

}
