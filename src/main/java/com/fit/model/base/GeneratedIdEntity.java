package com.fit.model.base;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Entities can extend this class when {@code GenerationType.AUTO} strategy for ID
 * generation. Override generation strategy if it's not acceptable.
 * 
 * @author mouradan
 *
 */
@MappedSuperclass
public abstract class GeneratedIdEntity extends BaseEntity {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
		return super.getId();
	}

	public void setId(Long id) {
		super.setId(id);
	}
}
