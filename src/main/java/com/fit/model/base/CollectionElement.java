package com.fit.model.base;

public interface CollectionElement<P extends Identifiable, E extends Identifiable>
    extends Auditable
{
    public P getParent();

    public void setParent(P parent);

    public E getElement();

    public void setElement(E element);
}
