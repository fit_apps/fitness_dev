package com.fit.model.base;

import java.util.Date;

import com.fit.model.security.User;





/**
 * Entities can extend this interface to make take advantage of Auditing capabilities.
 * 
 * @author mouradan
 *
 */
public interface Auditable {

	public User getCreatedBy();
	public void setCreatedBy(User user);
	public User getUpdatedBy();
	public void setUpdatedBy(User user);
	public Date getCreateDate();
	public void setCreateDate(Date date);
	public Date getUpdateDate();
	public void setUpdateDate(Date date);
}
