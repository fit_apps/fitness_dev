/**
 * 
 */
package com.fit.model.activity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fit.model.base.AuditableEntity;

/**
 * @author pavula1
 *
 */
@Entity
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "activity_cheers_id")) })
@Table(name = "activity_cheers")
public class ActivityCheers extends AuditableEntity {
	private static final long serialVersionUID = 7778832054700127170L;
	
	private Activity activity;
	
	public ActivityCheers() {
		
	}

	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "activity_id")
	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}
}
