/**
 * 
 */
package com.fit.model.activity;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

import com.fit.model.base.AuditableEntity;
import com.fit.model.refs.RefActivityType;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefStateProvinces;
import com.fit.model.security.User;

/**
 * @author pavula1
 * 
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "activity_id")) })
@Table(name = "activity")
public class Activity extends AuditableEntity {
	private static final long serialVersionUID = 7778832054700127170L;

	private String title;
	private Float distance;
	private Date activityDate;
	private Time startTime;
	private Time duration;
	private String zipCode;
	private RefStateProvinces state;
	private RefCountries country;
	private String city;
	private String street;
	private Integer energyLevelBefore;
	private Integer energyLevelAfter;
	private Boolean isPublic;
	private String symptoms;
	private String notes;
	private User user;
	private List<ActivityComments> activityComments;
	private RefActivityType activityType;
	private List<ActivityCheers> activityCheers;
	
	public Activity() {
		
	}

	@Column(name = "title", length=100)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "distance")
	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}

	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "activity_dt")
	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_time")
	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	@Column(name = "duration")
	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	@Column(name = "zip_code", length=10)
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "state_id")
	@ForeignKey(name = "act_to_state_fk", inverseName = "state_to_act_fk")
	public RefStateProvinces getState() {
		return state;
	}

	public void setState(RefStateProvinces state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "ref_activity_type_id")
	@ForeignKey(name = "act_to_actType_fk", inverseName = "actType_to_act_fk")
	public RefActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(RefActivityType activityType) {
		this.activityType = activityType;
	}

	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "country_id")
	public RefCountries getCountry() {
		return country;
	}

	public void setCountry(RefCountries country) {
		this.country = country;
	}

	@Column(name = "city", length=150)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "street", length=300)
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Column(name = "energy_level_before")
	public Integer getEnergyLevelBefore() {
		return energyLevelBefore;
	}

	public void setEnergyLevelBefore(Integer energyLevelBefore) {
		this.energyLevelBefore = energyLevelBefore;
	}

	@Column(name = "energy_level_after")
	public Integer getEnergyLevelAfter() {
		return energyLevelAfter;
	}

	public void setEnergyLevelAfter(Integer energyLevelAfter) {
		this.energyLevelAfter = energyLevelAfter;
	}

	@Column(name = "is_public", columnDefinition = "bit DEFAULT true", nullable=false)
	public Boolean isPublic() {
		return isPublic;
	}

	public void setPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	@Column(name = "symptoms", length=200)
	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	@Column(name = "notes", length=200)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	//@JsonBackReference("user-activity")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable=false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	//@JsonManagedReference("activity-activityComment")
	@OneToMany(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE}, mappedBy = "activity", fetch = FetchType.LAZY)
	@ForeignKey(name = "act_to_act_comment_fk", inverseName = "act_comment_to_act_fk")
	public List<ActivityComments> getActivityComments() {
		return activityComments;
	}

	public void setActivityComments(List<ActivityComments> activityComments) {
		this.activityComments = activityComments;
	}

	@OneToMany(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE}, mappedBy = "activity", fetch = FetchType.LAZY)
	@ForeignKey(name = "act_to_act_cheers_fk", inverseName = "act_cheers_to_act_fk")
	public List<ActivityCheers> getActivityCheers() {
		return activityCheers;
	}

	public void setActivityCheers(List<ActivityCheers> activityCheers) {
		this.activityCheers = activityCheers;
	}
}
