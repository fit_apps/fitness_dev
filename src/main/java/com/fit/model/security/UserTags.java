package com.fit.model.security;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.fit.model.base.AuditableEntity;

/**
 * @author amourad
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "user_tag_id")) })
@Table(name = "user_tags")
public class UserTags  extends AuditableEntity {
 
	private static final long serialVersionUID = 6892486810241721927L;

	private User taggedUser;
	
	public UserTags(){
		
	}
	
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "tagged_user_id")
	@ForeignKey(name = "tagged_to_user", inverseName = "user_to_tagged")	
	public User getTaggedUser() {
		return taggedUser;
	}

	public void setTaggedUser(User taggedUser) {
		this.taggedUser = taggedUser;
	}
 
 
	

}
