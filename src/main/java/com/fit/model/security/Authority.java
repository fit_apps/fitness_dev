package com.fit.model.security;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fit.model.base.AuditableEntity;

@Entity
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "authority_id")) })
@Table(name = "authority")
public class Authority  extends AuditableEntity { 
	private static final long serialVersionUID = 7778832054700127170L;

	private String authorityName;
	public final static String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	public final static String ROLE_MEMBER = "ROLE_MEMBER";
	public final static String ROLE_BC_MODERATOR = "ROLE_MODERATOR";
	
	public Authority() {}
	
	public Authority(String authorityName) {
		this.authorityName = authorityName;
	}

	@Column(name="authority_name")
	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authority_name) {
		this.authorityName = authority_name;
	}
}
