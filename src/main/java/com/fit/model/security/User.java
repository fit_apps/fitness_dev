package com.fit.model.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fit.model.activity.Activity;
import com.fit.model.base.AuditableEntity;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefStateProvinces;

//@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" }) 
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "user_id")) })
@Table(name = "users"
// , uniqueConstraints={@UniqueConstraint(columnNames={"username"})}
)
public class User extends AuditableEntity implements UserDetails {

	private static final long serialVersionUID = 7778832061690127170L;

	private String password;
	private String firstName;
	private String lastName;
	private String displayName;
	private String emailAddress;
	private String cellPhone;
	private Date birthDate;
	private Date loginDate;
	private Date archiveDate;
	private Boolean enabled;
	private Boolean locked;
	private String zipCode;
	private RefStateProvinces state;
	private RefCountries country;
	private String city;
	private String streetAddress1;
	private String streetAddress2;
	private String timeZone;
	private Boolean showPrivate;
	private Date healthyDate;
	
	private Long externalUserId;

	private String salt;
	private org.springframework.security.core.userdetails.User user;
	//private Map<String, Authority> userAuthoritiesMap = new HashMap<String, Authority>();
	private Set<Authority> userAuthorities = new HashSet<Authority>();
	private List<Activity> activities = new ArrayList<Activity>();
	private Set<UserDiagnosis> userDiagnosis = new HashSet<UserDiagnosis>();
	private List<UserTags> userTags = new ArrayList<UserTags>();
 
	public static String userPrivateSalt = "dsa13870611529d53c756146e8555993e9c";

	public User() {
	}

	public User(Long id, String firstName, String lastName, String salt, org.springframework.security.core.userdetails.User user) {
		setId(id);
		setFirstName(firstName);
		setLastName(lastName);
		setSalt(salt);
		this.user = user;
		setUsername(user.getUsername());
		setPassword(user.getPassword());
		setEnabled(new Boolean(user.isEnabled()));
	}
	
	public User(Long id, String firstName, String lastName, String displayName, String salt, org.springframework.security.core.userdetails.User user) {
		this(id, firstName, lastName, salt, user);
		setDisplayName(displayName);
	}

	
	@Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "display_name", nullable = true)
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Column(name = "email_address", nullable = false, unique = true)
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	@Column(name = "cell_phone", nullable = true, length=30)
	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "birth_dt")
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	// @JsonIgnore
	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// @JsonIgnore
	@Column(name = "login_dt", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date geLoginDate() {
		return loginDate;
	}

	
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	// @JsonIgnore
	@Column(name = "archive_dt", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getArchiveDate() {
		return archiveDate;
	}

	public void setArchiveDate(Date archiveDate) {
		this.archiveDate = archiveDate;
	}
	
	/**
	 * if enabled is null by default it's set to true
	 * 
	 * @param enabled
	 */
	// @JsonIgnore
	@Column(name = "enabled", columnDefinition = "bit DEFAULT true")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		if (enabled == null) {
			enabled = Boolean.TRUE;
		}
		this.enabled = enabled;
	}

	/**
	 * if locked is null, then by default it's set to false
	 * 
	 * @param locked
	 *            the locked to set
	 */
	// @JsonIgnore
	@Column(name = "locked", columnDefinition = "bit DEFAULT false")
	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		if (locked == null) {
			locked = Boolean.FALSE;
		}
		this.locked = locked;
	}

	@Column(name="zip_code", length=10, nullable=true)
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade =  {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE} )
	@JoinColumn(name="state_id", nullable=true)
	@ForeignKey(name = "user_to_state_fk", inverseName = "state_to_user_fk")
	public RefStateProvinces getState() {
		return state;
	}

	public void setState(RefStateProvinces state) {
		this.state = state;
	}

	@ManyToOne(fetch=FetchType.LAZY,  cascade =  {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
	@JoinColumn(name="country_id", nullable=true)
	@ForeignKey(name = "user_to_country_fk", inverseName = "country_to_user_fk")
	public RefCountries getCountry() {
		return country;
	}

	public void setCountry(RefCountries country) {
		this.country = country;
	}

	@Column(name="city", nullable=true, length=150)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name="street_address1", nullable=true, length=300)
	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	@Column(name="street_address2", nullable=true, length=300)
	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	@Column(name="time_zone", nullable=true, length=14)
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * normalized customId string, defined by java TimeZone, e.g. GMT + 11 : 59
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	@Column(name = "show_private", columnDefinition = "bit DEFAULT false")
	public Boolean getShowPrivate() {
		return showPrivate;
	}

	public void setShowPrivate(Boolean showPrivate) {
		this.showPrivate = showPrivate;
	}

	//@JsonIgnore
	@Column(name="external_id", nullable=true)
	public Long getExternalUserId() {
		return externalUserId;
	}

	public void setExternalUserId(Long externalUserId) {
		this.externalUserId = externalUserId;
	}

	/**
	 * used when authenticating user, appended to encryption to make it more
	 * secure
	 * 
	 * @return user salt as user.id+user.createdDate
	 */
	// @JsonIgnore
	@Column(name = "salt", nullable = false)
	public String getSalt() {
		// TODO see if getID works in web controller when new user is registered
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	// @JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade={CascadeType.ALL})
	@JoinTable(name = "user_authority", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = @JoinColumn(name = "authority_id"))
	@ForeignKey(name = "user_to_auth_fk", inverseName = "auth_to_user_fk")
	public Set<Authority> getUserAuthorities() {
		return userAuthorities;
	}

	public void setUserAuthorities(Set<Authority> userAuthorities) {
		this.userAuthorities = userAuthorities;
	}

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
	@ForeignKey(name = "user_to_user_diag_fk", inverseName = "user_diag_to_user_fk")
	public Set<UserDiagnosis> getUserDiagnosis() {
		return userDiagnosis;
	}

	public void setUserDiagnosis(Set<UserDiagnosis> userDiagnosis) {
		this.userDiagnosis = userDiagnosis;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="healthy_dt", nullable=true)
	public Date getHealthyDate() {
		return healthyDate;
	}

	public void setHealthyDate(Date healthyDate) {
		this.healthyDate = healthyDate;
	}

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    @ForeignKey(name = "user_to_act_fk", inverseName = "act_to_user_fk")
	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
	
	
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE},  mappedBy = "taggedUser", fetch = FetchType.LAZY)
	public List<UserTags> getUserTags() {
		return userTags;
	}

	public void setUserTags(List<UserTags> userTags) {
		this.userTags = userTags;
	}
	
	// /// Transient

	

	/**
	 * make sure setUserSalt(salt) is set before calling this method
	 * 
	 * @return
	 */
	// @JsonIgnore
	@Transient
	public String getUserSalt() {
		return getSalt() + userPrivateSalt;
	}

	// @JsonIgnore
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.user.getAuthorities();
	}

	/**
	 * accounts is non expired, or on other words is active if archiveDate is
	 * before today's date
	 */
	// @JsonIgnore
	@Transient
	public boolean isAccountNonExpired() {
		return this.user.isAccountNonExpired();
	}

	/**
	 * overrides UserDetails.isAccountNonLocked() returns
	 * <code>! getLocked()</code>
	 * 
	 */
	// @JsonIgnore
	@Transient
	public boolean isAccountNonLocked() {
		return this.user.isAccountNonLocked();
	}

	/**
	 * returns {@link isAccountNonExpired()}
	 * 
	 * @see org.acegisecurity.userdetails.UserDetails#isCredentialsNonExpired()
	 */
	// @JsonIgnore
	@Transient
	public boolean isCredentialsNonExpired() {
		return this.user.isCredentialsNonExpired();
	}

	/**
	 * return {@linkp getEnabled()}
	 * 
	 * @see org.acegisecurity.userdetails.UserDetails#isEnabled()
	 */
	// @JsonIgnore
	@Transient
	public boolean isEnabled() {
		return this.user != null ? this.user.isEnabled()
				: enabled != null ? enabled : true;
	}

	@Transient
	public String getUsername() {
		return this.getEmailAddress();
	}

	@Transient
	public void setUsername(String userName) {
		this.setEmailAddress(userName);
	}



}
