/**
 * 
 */
package com.fit.model.refs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fit.model.base.GeneratedIdEntity;

/**
 * @author pavula1
 *
 */
@Entity
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "ref_activity_type_id")) })
@Table(name = "ref_activity_type")
public class RefActivityType extends GeneratedIdEntity {
	private static final long serialVersionUID = 7778832054700127170L;
	
	private String name;
	private String description;
	private String iconPath;
	
	public RefActivityType() {
	}
	
	public RefActivityType(Long id, String name, String description, String iconPath) {
		super();
		super.setId(id);
		this.name = name;
		this.description = description;
		this.iconPath = iconPath;
	}
	
	@Column(name="name", length=100)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="description", length=300)
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="icon_path", length=100)
	public String getIconPath() {
		return iconPath;
	}
	
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
}
