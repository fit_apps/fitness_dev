package com.fit.model.refs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fit.model.base.GeneratedIdEntity;

@Entity
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "ref_country_id")) })
@Table(name = "ref_countries")
public class RefCountries extends GeneratedIdEntity {

	private static final long serialVersionUID = -6499603720645563369L;
	
	private String countryKey;
	private String countryName;

	public RefCountries() {
		super();
	}
	
	public RefCountries(Long id, String countryName,  String countryKey) {
		super();
		super.setId(id);
		this.countryKey = countryKey;
		this.countryName = countryName;
	}
			
	@Column(name="country_key", nullable=false, length=3, unique=true)
	public String getCountryKey() {
		return countryKey;
	}
	public void setCountryKey(String key) {
		this.countryKey = key;
	}
	
	@Column(name="country_name", nullable=false, length=50, unique=true)
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String name) {
		this.countryName = name;
	}
	
}
