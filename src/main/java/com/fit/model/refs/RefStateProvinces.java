package com.fit.model.refs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.fit.model.base.GeneratedIdEntity;

@Entity
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "ref_state_id")) })
@Table(name = "ref_state_provs")
public class RefStateProvinces extends GeneratedIdEntity {

	private static final long serialVersionUID = 1857525580350093494L;
	
	private String stateKey;
	private String stateName;
	private RefCountries country;
	
	public RefStateProvinces() {
	super();
}

	public RefStateProvinces(Long id, String stateName, String stateKey) {
//			RefCountries country) {
		super();
		super.setId(id);
		this.stateKey = stateKey;
		this.stateName = stateName;
	}
	
	@Column(name="state_key", nullable=false, length=2, unique=true)
	public String getStateKey() {
		return stateKey;
	}
	public void setStateKey(String key) {
		this.stateKey = key;
	}
	
	@Column(name="state_name", nullable=false, length=50, unique=true)
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String name) {
		this.stateName = name;
	}
	
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "ref_country_id", nullable=false)
	@ForeignKey(name = "st_to_cntr_id", inverseName = "cntr_to_st_id")
	public RefCountries getCountry() {
		return country;
	}
	public void setCountry(RefCountries country) {
		this.country = country;
	}
	
	

}
