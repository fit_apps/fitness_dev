/**
 * 
 */
package com.fit.model.refs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fit.model.base.GeneratedIdEntity;

/**
 * @author pavula1
 *
 */
@Entity
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "ref_chemo_freq_id")) })
@Table(name = "ref_chemo_freq")
public class RefChemoFreq extends GeneratedIdEntity {
	private static final long serialVersionUID = 7778832054700127170L;
	
	private String displayName;
	private String refChemoFreqKey;
	
	public RefChemoFreq() {
	}
	

	@Column(name="displayName", length=100, unique=true, nullable=false)
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	@Column(name="ref_chemo_freq_key", length=15, unique=true, nullable=false)
	public String getRefChemoFreqKey() {
		return refChemoFreqKey;
	}



	public void setRefChemoFreqKey(String refChemoFreqKey) {
		this.refChemoFreqKey = refChemoFreqKey;
	}

	
}
