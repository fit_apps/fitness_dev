/**
 * 
 */
package com.fit.model.refs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.fit.model.base.GeneratedIdEntity;

/**
 * @author pavula1
 * 
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "ref_diag_type_id")) })
@Table(name = "ref_diagnosis_type")
public class RefDiagnosisType extends GeneratedIdEntity {

	private static final long serialVersionUID = -6499603720645563369L;

	private String name;
	private String description;
	private String diagKey;
	private RefDiagnosisType parentDiagnosis = null;
	private List<RefDiagnosisType> childrenDiagnosis = new ArrayList<RefDiagnosisType>();
	
	public static final String BREAST_CANCER = "BREAST_CANCER";
	public static final String LUNG_CANCER = "LUNG_CANCER";
	public static final String PARENT_CANCER = "CANCER";

	public RefDiagnosisType() {

	}
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "parent_diag_id", nullable=true, insertable=false,updatable=false)
	public RefDiagnosisType getParentDiagnosis() {
		return parentDiagnosis;
	}
	
	public void setParentDiagnosis(RefDiagnosisType parentDiagnosis) {
		this.parentDiagnosis = parentDiagnosis;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_diag_id") 
	@ForeignKey(name = "child_to_parent_diag_fk", inverseName = "parent_to_child_diag_fk")
	public List<RefDiagnosisType> getChildrenDiagnosis() {
		return childrenDiagnosis;
	}

	public void setChildrenDiagnosis(List<RefDiagnosisType> childrenDiagnosis) {
		this.childrenDiagnosis = childrenDiagnosis;
	}
	

	@Column(name = "name", length = 100, unique = true, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 300, nullable = true)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "diag_key", length = 30, nullable = false, unique = true)
	public String getDiagKey() {
		return diagKey;
	}

	public void setDiagKey(String diagKey) {
		this.diagKey = diagKey;
	}

	public boolean equals(Object obj) {
		boolean isEq = false;

		// not null, is same class, has same id
		if (obj != null && obj.getClass().isInstance(this)) {
			RefDiagnosisType refDiagTypeObj = (RefDiagnosisType) obj;
			if (refDiagTypeObj.getDiagKey() != null
					&& refDiagTypeObj.getDiagKey().equals(this.getDiagKey())) {
				isEq = true;
			}
		}

		return isEq;
	}

	/**
	 * Returns the key if it's available
	 */
	@Override
	public int hashCode() {
		if (getDiagKey() == null) {
			return super.hashCode();
		} else {
			return getDiagKey().hashCode();
		}
	}

}
