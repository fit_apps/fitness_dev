/**
 * 
 */
package com.fit.model.refs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fit.model.base.GeneratedIdEntity;

/**
 * @author pavula1
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "ref_cancer_procedure_type_id")) })
@Table(name = "ref_cancer_procedure_type")
public class RefCancerProcedureType extends GeneratedIdEntity {
	
	private static final long serialVersionUID = 7778832054700127170L;
	
	private String name;
	private String description;

	public RefCancerProcedureType() {
	}

	@Column(name="title", length=100, nullable=false, unique=true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="description", length=300, nullable=false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
