package com.fit.service.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fit.dao.diagnosis.DiagnosisDao;
import com.fit.dao.diagnosis.UserCanTreatPhasesDao;
import com.fit.model.diagnosis.UserCancerTreatmentPhases;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefChemoFreq;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;
import com.fit.mvc.command.UserProfileCommand;
import com.fit.service.BaseService;
import com.fit.utils.SecurityUtil;

/**
 * all diagnosis related actions
 * 
 * @author amourad
 *
 */
@Service
public class DiagnosisService implements BaseService {
	
	@Autowired
	private DiagnosisDao diagnosisDao;
	
	@Autowired
	private UserCanTreatPhasesDao userCanTreatPhasesDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly=true)
	public List<Map<String, Object>> getAllParentDiagnosis(){
		
		return diagnosisDao.getAllParentDiagnosis();
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly=true)
	public List<Map<String, Object>> getAllDiagnHierarchy(){
		
		return diagnosisDao.getAllDiagnHierarchy();
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly=true)
	public Map<String, Object> getUserCancerData(){
		 User currentUser = (User)SecurityUtil.getCurrentUser();
		 Map<String, Object> cancerMap = new HashMap<String, Object>();
		 cancerMap.put("currentUserHasCancer", false);
		 for(UserDiagnosis userDiag:currentUser.getUserDiagnosis()){
			 if(userDiag.getRefDiagnosisType().getDiagKey().contains(RefDiagnosisType.PARENT_CANCER)) {
				 
				 cancerMap.put("currentUserHasCancer", true);
				//get treatments for the current user
				 Map<String, Object> trMap = this.diagnosisDao.getUserTreatments(currentUser);
				 if(trMap != null) {
					 cancerMap.put("treatmentData", trMap);
				 }
				 break;
			 }
		 }
		 return cancerMap;
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateCurUserTreatments(UserProfileCommand command) {
		userCanTreatPhasesDao.getSession().clear();
		 User currentUser = (User)SecurityUtil.getCurrentUser();
		 UserCancerTreatmentPhases treatmentPhase = new UserCancerTreatmentPhases();
		 boolean isUpdate = false;
		 
		 //if phaseId is present then its update, otherwise it's new record for a primary diagnosis
		 if(command.getPhaseId() != null ){
			 treatmentPhase.setId(command.getPhaseId().longValue());
			 treatmentPhase = userCanTreatPhasesDao.getById(command.getPhaseId().longValue());
			 isUpdate = true;
		 }
		 treatmentPhase.setChemoCompleted(command.getChemoCompleted());
		 treatmentPhase.setChemoTotal(command.getChemoTotal());
		 treatmentPhase.setRadiationCompleted(command.getRadCompleted());
		 treatmentPhase.setRadiatonTotal(command.getRadTotal());
		 
		 RefChemoFreq refChemoFreq = new RefChemoFreq();
		 if(isUpdate) {
			 refChemoFreq.setId(command.getRefChemoFreqId().longValue());
		 } else {
			 //set weekly frequency for chemo by default
			 refChemoFreq.setId(2L);
		 }
		 treatmentPhase.setRefChemoFreq(refChemoFreq );
		 
		 //add userdiagnosis only if it's new treatment record
		 UserDiagnosis userPrimDiag = null; 
		 //get primary diagnosis
		 for(UserDiagnosis userDiag:currentUser.getUserDiagnosis()){
			 if(userDiag.getIsPrimary()) {
				 userPrimDiag = userDiag;
				 break;
			 }
		 }
		 userPrimDiag.setUser(currentUser);
		 treatmentPhase.setUserDiagnosis(userPrimDiag);
		 
		 userCanTreatPhasesDao.saveOrUpdate(treatmentPhase);
		
	}

	@Transactional(propagation = Propagation.REQUIRED,  readOnly=true)
	public Map<String, Object> getUserDiagInfo(String userId) {
		Map<String, Object> diagMap = new HashMap<String, Object>();
		User user = new User();
		user.setId(Long.valueOf(userId));
		
		List<Map<String, Object>> diagList = this.diagnosisDao.getUserDiagnosis(user);
		diagMap.put("diagData", diagList);
		
		//should I get cancer data?
		boolean isPrimCancerDiag = false;
		Long primDiagId = null;
		for(Map<String, Object> map:diagList) {
			String refDiagKey = (String) map.get("refDiagKey");
			Boolean isPrimary = Boolean.valueOf(map.get("userDiagIsPrimary").toString());
			if(refDiagKey.indexOf("CANCER") > -1 && isPrimary) {
				isPrimCancerDiag = true;
				primDiagId = (Long)map.get("userDiagId");
				break;
			}
		}
		//yes, get procedures data
		List<Map<String, Object>> cancerProcList = new ArrayList<Map<String, Object>>();
		if(isPrimCancerDiag) {
			cancerProcList = this.diagnosisDao.getUserDiagProcedures(primDiagId);
		}
		diagMap.put("cancerProcData", cancerProcList);
		
		return diagMap;
	}
	

}
