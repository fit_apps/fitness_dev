package com.fit.service.activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fit.dao.activities.ActivityCommentsDao;
import com.fit.dao.activities.ActivityDao;
import com.fit.dao.activities.ActivityNotFoundException;
import com.fit.model.activity.Activity;
import com.fit.model.activity.ActivityComments;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefActivityType;
import com.fit.model.refs.RefCountries;
import com.fit.model.security.User;
import com.fit.mvc.command.ActivityCommand;
import com.fit.mvc.command.ActivityDisplayCommand;
import com.fit.mvc.command.SearchFilterCommand;
import com.fit.service.BaseService;
import com.fit.service.reference.ReferenceService;
import com.fit.utils.SecurityUtil;

@Service
public class ActivityService implements BaseService {
    
    private static final Logger logger = Logger.getLogger(ActivityService.class);
    public static SimpleDateFormat TIME_SDF = new SimpleDateFormat("HH:mm:ss");
    	
    @Autowired
    private ActivityDao activityDao;
    
    @Autowired
    private ActivityCommentsDao activityCommentsDao;
   
	 @Autowired
	 private ReferenceService refServices; 
	 
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Activity> getUserActivities(String viewUserId) {
		//User currentUser = (User) SecurityUtil.getCurrentUser();
		return this.activityDao.getUserActivities(Long.valueOf(Long.valueOf(viewUserId)));
	}
    
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ActivityDisplayCommand getActivityDisplayCommand(Long activityId) {
		User currentUser = (User)SecurityUtil.getCurrentUser();
		ActivityDisplayCommand adc  = this.activityDao.getActivityDisplayCommand(currentUser.getId(), activityId);
		return adc;
	}
	
    @Transactional(propagation = Propagation.REQUIRED, readOnly=true)
    public List<Map<String, Object>> getFeedActivities(int startRow, int numOfRows) {
    	return this.getFeedActivities(null, startRow, numOfRows);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly=true)
    public List<Map<String, Object>> getFeedActivities(SearchFilterCommand searchFilterCommand, int startRow, int numOfRows) {
    	User currentUser = (User)SecurityUtil.getCurrentUser();
    	//load user diagnosis
    	List<Map<String, Object>> activities = new ArrayList<Map<String, Object>>();
    	Set<UserDiagnosis> userDiagnosisTypes = currentUser.getUserDiagnosis();

    	activities = this.activityDao.getFeedActivitiesWithCounts(currentUser.getId(), userDiagnosisTypes, searchFilterCommand, startRow, 
    			numOfRows);
    
    	return activities;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly=true)
    public List<Map<String, Object>> getAllActivityTypes(){
    	
    	return this.activityDao.getAllActivityTypes();
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public Map<String, Object> updateActivityUserTag(String actUserId, int tagCount){
    	User currentUser = (User)SecurityUtil.getCurrentUser();
    	Map<String, Object> retMap = new HashMap<String, Object>();
    	boolean success = false;
    	//update user_tags where tagged_user_id=actuserId
    	if(tagCount == 0) {
    		Long newTagId = this.activityDao.addTag(currentUser, Long.valueOf(actUserId));
    		success = (newTagId != null)?true:false;
    	} else {
    		//delete record
    		int deletedTags = this.activityDao.deleteTag(currentUser.getId(), Long.valueOf(actUserId));
    		success = (deletedTags > 0)?true:false;
    	}
    	retMap.put("success", success);
    	int newTagCount = (tagCount == 1)?0:1;
    	retMap.put("newTagCount", newTagCount);
    	return retMap;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public Map<String, Object> updateActivityCheer(String activityId, int cheerCountByUser){
    	User currentUser = (User)SecurityUtil.getCurrentUser();
    	Map<String, Object> retMap = new HashMap<String, Object>();
    	boolean success = false;
    	//update activity_cheers
    	
    	if(cheerCountByUser == 0) {
    		Long newCheerId = this.activityDao.addActivityCheer(currentUser, Long.valueOf(activityId));
    		success = (newCheerId != null)?true:false;
    	} else {
    		//delete record
    		int deletedCheers = this.activityDao.deleteActivityCheer(currentUser,Long.valueOf(activityId));
    		success = (deletedCheers > 0)?true:false;
    	}
    	retMap.put("success", success);
    	int newCheerCount = (cheerCountByUser == 1)?0:1;
    	retMap.put("newCheerCount", newCheerCount);
    	return retMap;
    }
    
    @Transactional(propagation = Propagation.REQUIRED,  readOnly=true)
    public List<Map<String, Object>> getActivityCheerUsers(String activityId) {
    	return this.activityDao.getActivityCheerUsers(Long.valueOf(activityId));
    }
    
    @Transactional(propagation = Propagation.REQUIRED,  readOnly=true)
    public List<Map<String, Object>> getActivityComments(String activityId, int startRow, int maxRows) {
		return this.activityDao.getActivityComments(Long.valueOf(activityId), startRow, maxRows);
	}
    
    @Transactional(propagation = Propagation.REQUIRED,  readOnly=true)
    public long getCurrentUserActivityCommentsCount(String activityId ){
    	User currentUser = (User)SecurityUtil.getCurrentUser();
    	return this.activityCommentsDao.getUserActivityCommentCount(currentUser, Long.valueOf(activityId));
    }
    
    @Transactional(propagation = Propagation.REQUIRED )
    public void addNewComment(String newComment, String activityId) {
    	User currentUser = (User)SecurityUtil.getCurrentUser();
		this.activityCommentsDao.addNewComment(Long.valueOf(activityId), currentUser, newComment);
	}
    
    @Transactional(propagation = Propagation.REQUIRED )
    public Activity updateActivity(ActivityCommand command) throws Exception {
        Activity activity = activityDao.getActivityById(command.getActivityId());
        activity.setUpdatedBy(command.getUser());
        activity.setUpdateDate(new Date());
        return saveActivity(activity, command);
    }	    

    @Transactional(propagation = Propagation.REQUIRED )
    public Activity saveActivity(ActivityCommand command) throws Exception {
    	Activity activity = new Activity();
    	activity.setUser(command.getUser()); 
    	return saveActivity(activity, command);
    }
            
    @Transactional(propagation = Propagation.REQUIRED )
    public Activity saveActivity(Activity activity, ActivityCommand command) throws Exception {
   
        activity.setActivityDate(new SimpleDateFormat("MM/dd/yyyy").parse(command.getActivityDate()));
    	activity.setTitle(command.getActivityTitle());
    	activity.setActivityType(refServices.getActivityTypeById(command.getActivityType()));

    	activity.setZipCode(command.getZip());
    	activity.setCity(command.getCity());
    	activity.setState(refServices.getStateProvinceById(command.getStateProvince()));
    	activity.setCountry(refServices.getCountryById(command.getCountry()));
    	activity.setDistance(command.getDistance());
    	if (command.getStartTime() != null) {
	        try {
	        	activity.setStartTime(new java.sql.Time(TIME_SDF.parse(command.getStartTime()).getTime()));
	        } catch (ParseException e) {
	            throw new Exception("Unable to parse activity start time");
	        }	
    	}
       	if (command.getDuration() != null) {
	        try {
	        	activity.setDuration(new java.sql.Time(TIME_SDF.parse(command.getDuration()).getTime()));
	        } catch (ParseException e) {
	            throw new Exception("Unable to parse activity duration");
	        }	
    	} 
   		activity.setEnergyLevelBefore(command.getEnergyBeforeRating());
   		activity.setEnergyLevelAfter(command.getEnergyAfterRating());
       	activity.setSymptoms(command.getSymptomsHelped());       	
       	activity.setNotes(command.getNotes());
    	activity.setPublic(command.getIsPublic());
 
    	activityDao.save(activity);
        
        return activity;
    }	

    @Transactional(propagation = Propagation.REQUIRED )
    public void deleteActivity(Long activityId) throws ActivityNotFoundException {
    	activityDao.deleteActivity(activityId);
    }	    
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly=true)
    public List<Map<String, Object>> getTotalActivitiesByUserId(String userId) {
    	//User currentUser = (User)SecurityUtil.getCurrentUser();
        return this.activityDao.getTotalActivitiesByUserId(Long.valueOf(userId));
    }

    @Transactional(propagation = Propagation.REQUIRED)
	public void deleteActivityComment(String commentId) {
		ActivityComments comment = new ActivityComments();
		comment.setId(Long.valueOf(commentId));
		this.activityCommentsDao.delete(comment);
	}

    @Transactional(propagation = Propagation.REQUIRED)
	public Map<String, Object> updateComment(String newComment, String activityId, String commentId) {
		User currentUser = (User)SecurityUtil.getCurrentUser();
		ActivityComments comment = new ActivityComments();
		Long commentIdLong = Long.valueOf(commentId);
		comment.setId(commentIdLong);
		comment.setComment(newComment);
		Date updateDate = new Date();
		comment.setUpdateDate(updateDate);
		comment.setUpdatedBy(currentUser);
		
		Activity activity = new Activity();
		activity.setId(Long.valueOf(activityId));
		
		comment.setActivity(activity);
		
		this.activityCommentsDao.update(comment);
		
		ActivityComments updatedComment = (ActivityComments)this.activityCommentsDao.getSession().load(ActivityComments.class, commentIdLong);
		
		//map for json
		Map<String, Object> commentMap = new HashMap<String, Object>();
		commentMap.put("commentCreateDt",updatedComment.getCreateDate());
		commentMap.put("displayName",currentUser.getDisplayName());
		commentMap.put("comment",updatedComment.getComment());
		commentMap.put("createdById",currentUser.getId());
		commentMap.put("commentId",updatedComment.getId());
		commentMap.put("commentUpdateDt",updateDate);
		
		return commentMap;
    }

	

	
}
