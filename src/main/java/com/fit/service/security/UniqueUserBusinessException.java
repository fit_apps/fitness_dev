package com.fit.service.security;

import com.fit.utils.BaseBusinessException;


/**
 * Business Exception to be thrown when duplicate user is registered
 * 
 * @author amouradi
 * 
 */
public class UniqueUserBusinessException extends BaseBusinessException {

	private static final long serialVersionUID = -8728696460776454182L;

	public UniqueUserBusinessException(String messageKey, Object... args) {
        super(messageKey, args);
    }

}
