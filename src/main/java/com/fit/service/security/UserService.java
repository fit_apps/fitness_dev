package com.fit.service.security;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fit.dao.diagnosis.DiagnosisDao;
import com.fit.dao.diagnosis.UserDiagnosisDao;
import com.fit.dao.reference.CancerProcedureTypeDao;
import com.fit.dao.reference.ChemoFreqDao;
import com.fit.dao.security.UserDao;
import com.fit.model.diagnosis.UserCancerDiagnosisProcedures;
import com.fit.model.diagnosis.UserCancerTreatmentPhases;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefCancerProcedureType;
import com.fit.model.refs.RefChemoFreq;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.refs.RefStateProvinces;
import com.fit.model.security.Authority;
import com.fit.model.security.User;
import com.fit.mvc.command.ProfilePasswordCommand;
import com.fit.mvc.command.RegisterCommand;
import com.fit.service.BaseService;
import com.fit.utils.SecurityUtil;
import com.fit.utils.UserUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * User related operations
 * 
 * @author amourad
 *
 */
@Service
public class UserService implements BaseService {
	private static final Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    private UserDao userDao;
    
    @Autowired
    private DiagnosisDao diagnosisDao;
    
    @Autowired
    private ChemoFreqDao chemoFreqDao;
    
    @Autowired
    private CancerProcedureTypeDao cancerProcDao;
    
    @Autowired
    private UserDiagnosisDao userDiagnosisDao;
    
    @Resource(name="passwordEncoder")
	 private Md5PasswordEncoder passwordEncoder;
    
 
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdate(User user) {
        this.userDao.saveOrUpdate(user);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly=true)
    public User loadUser(User user) {
    	return this.userDao.getById(user.getId());
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public User registerUser(RegisterCommand command) throws Exception {
    	userDao.getSession().clear();
        // save successfully 
    	List<String> roles = new ArrayList<String>();
    	roles.add(Authority.ROLE_MEMBER);
        User user = this.saveUser(command, roles);
        
        //TODO add email later
        return user;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public User getUserByEmail(String emailAddress) {
        User user = this.userDao.getUserByEmail(emailAddress);
        return user;
    }

    
    @Transactional(propagation = Propagation.REQUIRED, readOnly=true)
    public User loadUserWithAuths(User user){
        return this.userDao.loadUserAuthorities(user);
    }


    @Transactional(propagation = Propagation.REQUIRED, rollbackFor={UserVerificationException.class})
    public void enableUser(String emailAddress, String uiSalt, String uiPassword, String clubId) throws UserVerificationException {

        User user = userDao.getUserByEmail(emailAddress);
        this.verifyUser(user, emailAddress);

        String dbSalt = DigestUtils.md5Hex(user.getSalt()).substring(0, 6);
        String uiPasswordEncoded = UserUtils.encodePassword(userDao.getPasswordEncoder(), uiPassword, user.getUserSalt());

        if (dbSalt.equals(uiSalt) && uiPasswordEncoded.equals(user.getPassword())) {
            user.setLocked(Boolean.FALSE);
            user.setEnabled(Boolean.TRUE);
            userDao.saveOrUpdate(user);
             
        } else if (!dbSalt.equals(uiSalt)) {
            throw new UserVerificationException("verify.salt.error");
        } else if (!uiPasswordEncoded.equals(uiPassword)) {
            throw new UserVerificationException("verify.password.error");
        }

    }
    
    /** Reset the password to the given value
     * @param emailAddress - the email address of the user's account
     * @param uiPassword - the new password
     * @return The {@link User} object of the user just reset
     * @throws UserVerificationException
     */

    @Transactional(propagation = Propagation.REQUIRED)
    public User resetPasswordByAdmin(String emailAddress, String uiPassword) throws UserVerificationException {
        User user = userDao.getUserByEmail(emailAddress);
        this.verifyUser(user, emailAddress);
    	resetPassword(user, uiPassword);
    	return user;
    }  

    /**
     * Reset the password to the given value if the passed in salt hash is correct. 
     * The normal flow would be:
     * <ol><li>The user requests a password reset.</li>
     * <li>An email is sent to the user with a link to the password reset in the UI. The link contains the hash of the salt used for the user's current password.</li>
     * <li>The user clicks the link, is taken to the password reset dialog, enters the new password and submits.</li>
     * <li>This method is invoked and verifies that the given salt matches the current one.</li>
     * <li>If the salt is correct, the password is updated.</li>
     * </ol> 
     * @param emailAddress - Identifies the user account
     * @param uiSalt - The hash of the salt in use when the password reset email was generated
     * @param uiPassword - The new password
     * @throws UserVerificationException
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void resetPasswordUsingSalt(String emailAddress, String uiSalt, String uiPassword) throws UserVerificationException {
        User user = userDao.getUserByEmail(emailAddress);
        this.verifyUser(user, emailAddress);

        if (!user.isEnabled()) {
            throw new UserVerificationException("verify.user.locked");
        }
        String dbSalt = DigestUtils.md5Hex(user.getSalt()).substring(0, 6);

        // entered salt should correspond to what has been saved
        if (dbSalt.equals(uiSalt)) {
        	resetPassword(user, uiPassword);
        } else {
            throw new UserVerificationException("verify.salt.error", uiSalt);
        }
    }    
    
    @Transactional(propagation = Propagation.REQUIRED)
    public void resetPassword(User user, String uiPassword) throws UserVerificationException {

    	// Create new salt for new password
    	String salt = UserUtils.generateUserSalt(user.getEmailAddress() + String.valueOf(System.currentTimeMillis()));
    	user.setSalt(salt);
    	String password = UserUtils.encodePassword(userDao.getPasswordEncoder(), uiPassword, user.getUserSalt());
    	user.setPassword(password);
//    	user.setLocked(Boolean.FALSE);		//For now don't change this on a reset
//    	user.setEnabled(Boolean.TRUE);		//For now don't change this on a reset
    	userDao.save(user);
    }
    
    
    /**
     * @param user
     * @param emailAddress
     * @return errCode
     */
    private void verifyUser(User user, String emailAddress) throws UserVerificationException {
        if (user == null) {
            throw new UserVerificationException("verify.username.error", emailAddress);
        }
    }
    
    
    private User saveUser(RegisterCommand command, List<String> roles) throws Exception {
        User user = new User();
        user.setEnabled(Boolean.TRUE);
        user.setLocked(Boolean.FALSE);
        user.setShowPrivate(false);
        
        UserUtils.populate(this.passwordEncoder, user, command);
        for (String role : roles) {
            user.getUserAuthorities().add(userDao.getAuthority(role));
        }
        Set<UserDiagnosis> userDiagSet = new HashSet<UserDiagnosis>();
        if(command.getDiagHierarchy() != null) {
        	
        	for(Map<String, Object> diagMap:command.getDiagHierarchy()) {
        		RefDiagnosisType refDiagnosisType = new RefDiagnosisType();
        		Boolean isChecked = (diagMap.get("isChecked") != null)?Boolean.valueOf(diagMap.get("isChecked").toString()):false; 
        		if(isChecked) {
	    		  	refDiagnosisType.setId(((Integer)diagMap.get("childDiagId")).longValue());
	    		  	
	    		  	UserDiagnosis userDiag = new UserDiagnosis();
	          		userDiag.setRefDiagnosisType(refDiagnosisType);
	          		//make sure the real one is selected
	          		if(diagMap.get("isPrimary") != null && Boolean.valueOf(diagMap.get("isPrimary").toString())) {
	          			userDiag.setIsPrimary(true);
	          		} else {
	          			userDiag.setIsPrimary(false);
	          		}
	          		
	          		userDiag.setUser(user);
	        		userDiagSet.add(userDiag);
        		}
        	}
        	user.setUserDiagnosis(userDiagSet);
        }
        
        userDao.saveOrUpdate(user);
        return user;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Map<String, Object>> getTaggedByUser(String userId) {
		
		return this.userDao.getTaggedByUser(Long.valueOf(userId));
	}

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Map<String, Object> getCurrentUserProfileData(String userId, boolean isEdit) {
    	Long userIdLong = Long.valueOf(userId);
    	User currentUser = (User)SecurityUtil.getCurrentUser();
    	boolean isCurrentUser = (currentUser.getId().equals(userIdLong))?true:false;
    	Map<String, Object> dataMap = new HashMap<String, Object>(); 
    	//strictly user profile data
    	Map<String, Object>  userData = new HashMap<String, Object>();
    	
    	if(isCurrentUser) {
    		userData = this.userDao.loadUserAllProfileData(userIdLong);
    		
    		dataMap.put("isCurrentUser", true);
    	} else {
    		boolean showPrivate = this.userDao.canShowPrivateData(userIdLong);
    		if(showPrivate) {
    			userData = this.userDao.loadUserAllProfileData(userIdLong);
    		} else {
    			userData = this.userDao.loadUserPublicProfileData(userIdLong);
    		}
    		dataMap.put("isCurrentUser", false);
    	}
    	
    	//get treatment and procedure data
    	
    	List<Map<String, Object>> allDiagList = this.diagnosisDao.getUserDiagAll(userIdLong);
    	dataMap.put("allDiagData", allDiagList);
    	dataMap.put("profileData",userData);
    	
    	if(isEdit) {
    		List<Map<String, Object>> allChemoFreqs = this.chemoFreqDao.getAllChemoFreqs();
    		dataMap.put("allChemoFreqs",allChemoFreqs);
    		List<Map<String, Object>> allProcTypes = this.cancerProcDao.getAllCancerProcedureTypes();
    		dataMap.put("allCancerProcedureTypes",allProcTypes);
    	}
    	
    	//don't need those for profile view page
    	/*dataMap.put("allCountries", refService.getAllCountries());
    	//get reference of states and countries
    	dataMap.put("allStates", refService.getAllStateProvinces());*/
		return dataMap;
	}

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public boolean verifyCurrentUserPassword(ProfilePasswordCommand profilePasswordCommand) {
    	boolean isValidPass = false;
		User currentUser = (User)SecurityUtil.getCurrentUser();
		
		String uiPassword = profilePasswordCommand.getCurrentPassword();
		
		currentUser.setSalt(profilePasswordCommand.getSalt());
		String salt = currentUser.getUserSalt();
		String encodedPass = UserUtils.encodePassword(this.passwordEncoder, uiPassword, salt);
		
		Map<String, Object> userMap = this.userDao.verifyUserPassword(currentUser.getId(), encodedPass);
		if(userMap.size() > 0 && userMap.get("userId").equals(currentUser.getId())) {
			isValidPass = true;
		}
		
		return isValidPass;
	}
    
    @SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED)
    public boolean saveUserProfile(Map<String, Object> userMap)  {

    
    Map<String, Object> userDataMap = (LinkedHashMap<String, Object>)userMap.get("userData");
    Map<String, Object> userDiagMap = (LinkedHashMap<String, Object>)userMap.get("selectedDiag");
    
    User user = new User();
    
    //populate user, selected diagnosis and procedures, updated password if needed
    user.setId(Long.valueOf((Integer) userDataMap.get("userId")));
    user = userDao.getById(Long.valueOf((Integer) userDataMap.get("userId")));
    
    user.setFirstName((String) userDataMap.get("firstName"));
    user.setLastName((String) userDataMap.get("lastName"));
    user.setDisplayName((String) userDataMap.get("displName"));
    user.setEmailAddress((String) userDataMap.get("emailAddress"));
    user.setCellPhone((String) userDataMap.get("cellPhone"));
    user.setSalt((String) userDataMap.get("salt"));
    try {
    	Date birthDate = (userDataMap.get("birthDate") != null)?
    			DateUtils.parseDate((String) userDataMap.get("birthDate"), "yyyy-MM-dd"):null;
    	user.setBirthDate(birthDate);
    	//convert cancer free date
  	    Date healthyDate = (userDataMap.get("healthyDate") != null)?
  	    		DateUtils.parseDate((String)userDataMap.get("healthyDate"), "yyyy-MM-dd"):null;
    	user.setHealthyDate(healthyDate);
    } catch (ParseException e) {
		logger.warn("Problem with date parsing: " + e.getMessage());
	}
    
    if(userDataMap.get("addressRadio") != null && userDataMap.get("addressRadio").equals("location") ) {
    	
    	resetAddressFields(user, (String)userDataMap.get("address1"), (String)userDataMap.get("address2"), 
    			(String)userDataMap.get("city"), userDataMap.get("countrySelected"), 
    			userDataMap.get("stateSelected"), null);
    	
    } else if(userDataMap.get("addressRadio") != null && userDataMap.get("addressRadio").equals("zipCode")) {
    	//reset location fields
    	resetAddressFields(user, null, null, null, null, null, (String) userDataMap.get("zipCode"));
    }
    RefDiagnosisType refDiagType = null;
    UserDiagnosis userDiag = null;
    
    //create user diagnosis only if there was selected diagnosis
    if(userDiagMap != null) {
    	
	     //add diagnosis type
	     refDiagType = new RefDiagnosisType();
	     if(this.getStringValFromMap(userDiagMap, "changedDiagTypeId") != null) {
	      refDiagType.setId(Long.valueOf(userDiagMap.get("changedDiagTypeId").toString()));
	     }
	      
	      //check if diag exists for the user and then delete it with all cascading instances
	      userDiag = new UserDiagnosis();
	      if(userDiagMap != null && userDiagMap.get("refDiagKey") != null && !userDiagMap.get("refDiagKey").equals("new")) {
	    	 Long oldUserDiagId = Long.valueOf(userDiagMap.get("userDiagId").toString());
	    	  //select and delete userdiagnosis with all associated cascades
	    	 deleteDiagnosis(user, oldUserDiagId);
	      }
	      Boolean isPrimaryDiag = (userDiagMap.get("userDiagIsPrimary") == null || 
	    		  userDiagMap.get("userDiagIsPrimary").toString().trim().length() > 0)?false:
	    			  Boolean.valueOf(userDiagMap.get("userDiagIsPrimary").toString());
	      userDiag.setIsPrimary(isPrimaryDiag);
		  userDiag.setRefDiagnosisType(refDiagType);
		  userDiag.setUser(user);
		  Date diagStartDate = null;
		  Date diagEndDate = null;
			try {
				diagStartDate = (userDiagMap.get("userDiagStartDt") != null)?
						  			DateUtils.parseDate((String)userDiagMap.get("userDiagStartDt") , "yyyy-MM-dd"):null;
				diagEndDate = (userDiagMap.get("userDiagEndDt") != null)?
								  DateUtils.parseDate((String)userDiagMap.get("userDiagEndDt") , "yyyy-MM-dd"):null;		  
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				logger.warn("Problem with diagnosis date parsing: " + e.getMessage());
			}
		  userDiag.setStartDate(diagStartDate);
		  userDiag.setEndDate(diagEndDate);
		  
		  UserCancerTreatmentPhases treatm = null; 
		  // if any of the fields is set at all, then create treatment record
		  if(this.getIntegerFromMap(userDiagMap, "chemoCompleted") != null ||  this.getIntegerFromMap(userDiagMap, "chemoTotal") != null 
				  || this.getIntegerFromMap(userDiagMap, "radCompleted") != null || this.getIntegerFromMap(userDiagMap, "radTotal") != null ||
				  userDiagMap.get("chemoFreqId") != null) {
			  treatm = new UserCancerTreatmentPhases();
			  treatm.setChemoCompleted(this.getIntegerFromMap(userDiagMap, "chemoCompleted"));
			  treatm.setChemoTotal(this.getIntegerFromMap(userDiagMap, "chemoTotal"));
			  treatm.setRadiationCompleted(this.getIntegerFromMap(userDiagMap, "radCompleted"));
			  treatm.setRadiatonTotal(this.getIntegerFromMap(userDiagMap, "radTotal"));
			  
			  if(userDiagMap.get("chemoFreqId") != null) {
				  RefChemoFreq refChemoFreq = new RefChemoFreq();
				  refChemoFreq.setId(Long.valueOf(userDiagMap.get("chemoFreqId").toString()));
				  treatm.setRefChemoFreq(refChemoFreq );
			  }
			  treatm.setUserDiagnosis(userDiag);
			  userDiag.setUserCancerTreatmentPhases(treatm);
		  }
		  
		  //set procedures if they were submitted
	      List<LinkedHashMap<String, Object>> procListWithMap = (List<LinkedHashMap<String, Object>>) userDiagMap.get("procArr");
		  List<UserCancerDiagnosisProcedures> userProcList = new ArrayList<UserCancerDiagnosisProcedures>();
		  userDiag.setUserCancerDiagnosisProcedures(userProcList);
		 
	 
		  try {
			  if(procListWithMap != null && procListWithMap.size() > 0) {
		
				  for(LinkedHashMap<String, Object> procMap:procListWithMap) {
					  UserCancerDiagnosisProcedures userProc = new UserCancerDiagnosisProcedures();
					  
					  if(procMap.get("procId") != null) {
						   RefCancerProcedureType procType = new RefCancerProcedureType();
						   procType.setId(Long.valueOf(procMap.get("procId").toString()));
						   userProc.setRefCancerProcedureType(procType);
						   userProc.setUserDiagnosis(userDiag);
					  }
					  Date procDate = null;
					  String procDateStr = (String)procMap.get("procDate");
					  if( procDateStr != null && procDateStr.trim().length() >0 ) {
						  if(procDateStr.indexOf("T") > -1){
							  String correctDate = procDateStr.substring(0,procDateStr.indexOf("T") );
							  procDate = DateUtils.parseDate(correctDate, "yyyy-MM-dd");
						  } else {
							  procDate = DateUtils.parseDate(procDateStr, "yyyy-MM-dd");
						  }
					  }
					
					  userProc.setProcedureDate(procDate);
					  userProcList.add(userProc);   
				   }
			  } else {
				  //no procedures submitted so maybe just delete them? it should delete them automatically
			  }
			  
			
			  
		  } catch (ParseException e) {
				// TODO Auto-generated catch block
			  logger.warn("Problem with diagnosis procedure date parsing: " + e.getMessage());
			}
		  //make diagnosis type required on a page
		 Set<UserDiagnosis> userDiagSet = new HashSet<UserDiagnosis>();
		 userDiagSet.add(userDiag);
		  user.setUserDiagnosis(userDiagSet);
    }
    user.setUpdateDate(new Date());
    user.setUpdatedBy(user);
    //update password
    String newPassword = (String)userDataMap.get("newPass");
    if(newPassword != null && newPassword.length() > 0) {
    	String password = UserUtils.encodePassword(userDao.getPasswordEncoder(), newPassword, user.getUserSalt());
    	user.setPassword(password);
    }
	userDao.saveOrUpdate(user);
    
     return false;
    }
    
    
	private void deleteDiagnosis(User user, Long userDiagId) {
		UserDiagnosis oldUserDiag = userDiagnosisDao.getById(userDiagId);
    	 user.getUserDiagnosis().remove(oldUserDiag);
    	  userDiagnosisDao.delete(oldUserDiag);
    	  userDao.saveOrUpdate(user);
	}
    
	@Transactional(propagation = Propagation.REQUIRED)
    public void deleteUserDiagnosis(Map<String, Object> userDiagDataMap) {
    	User user = new User();
    	    
	    //populate user, selected diagnosis and procedures, updated password if needed
	    user.setId(Long.valueOf((Integer) userDiagDataMap.get("userId")));
	    user = userDao.getById(Long.valueOf((Integer) userDiagDataMap.get("userId")));
	    Long userDiagId = (userDiagDataMap.get("userDiagId") != null)?Long.valueOf(userDiagDataMap.get("userDiagId").toString()):null;
	    if(userDiagId != null) {
	    	this.deleteDiagnosis(user, userDiagId);
	    }
	}
    
    private String getStringValFromMap(Map<String, Object> map, String key) {
    	return (map == null || (map.get(key) == null ||
    			(map.get(key) != null && map.get(key).toString().trim().length() == 0)))? null:map.get(key).toString();
    }
    
    private Integer getIntegerFromMap(Map<String, Object> map, String key) {
    	return (map == null || (map.get(key) == null ||
    			map.get(key).toString().trim().length() == 0))? null:Integer.valueOf(map.get(key).toString());
    }
    
    private void resetAddressFields(User user, String address1, String address2, String city, Object countrySelected,
    		Object stateSelected, String zipCode){
    	user.setStreetAddress1(address1);
    	user.setStreetAddress2(address2);
    	user.setCity(city);
    	
    	if(countrySelected != null && 
    			 countrySelected.toString().trim().length() > 0) {
    		RefCountries country = new RefCountries();
    		Long countrySelectedParsed = Long.valueOf(countrySelected.toString());
    		country.setId(countrySelectedParsed);
        	user.setCountry(country);
    	} else {
    		user.setCountry(null);
    	}

    	if(stateSelected != null && stateSelected.toString().trim().length() > 0) {
    		Long stateSelectedParsed = Long.valueOf(stateSelected.toString());
    		RefStateProvinces state = new RefStateProvinces();
    		state.setId(stateSelectedParsed);
        	
        	user.setState(state);
    	} else {
    		user.setState(null);
    	}
    	
    	user.setZipCode(zipCode);
    	
    }    
}

