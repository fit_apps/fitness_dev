package com.fit.service.security;

import com.fit.utils.BaseBusinessException;


/**
 * User registration or verification failed due to invalid submitted credentials
 * 
 * @author amouradi
 * 
 */
public class UserVerificationException extends BaseBusinessException {

	private static final long serialVersionUID = -152241991844135592L;

	public UserVerificationException() {
    };

    public UserVerificationException(String messageKey, Object... args) {
        super(messageKey, args);
    };
}
