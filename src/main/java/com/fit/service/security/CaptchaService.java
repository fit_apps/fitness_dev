package com.fit.service.security;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import com.fit.service.BaseService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Captcha related operations
 * 
 * @author channah
 *
 */
@Service
public class CaptchaService implements BaseService {

	public static final String RECAPTCHA_PUBLIC_KEY_PROP_NAME = "captcha.recaptcha.public.key";
	public static final String RECAPTCHA_PRIVATE_KEY_PROP_NAME = "captcha.recaptcha.private.key";
	
	public static String RECAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify";
	public static String RECAPTCHA_KEY_KEY = "secret=";
	public static String RECAPTCHA_RESPONSE_KEY = "response=";
	public static String RECAPTCHA_IP_KEY = "remoteip=";	
	
   @Autowired
   private ApplicationContext context; 

	private static final Logger logger = Logger.getLogger(CaptchaService.class);
   
    public String getRecaptchaPublicKey() {
        try {
        	MessageSource myProperties = (MessageSource) context.getBean("captchaSource");
        	return myProperties.getMessage("captcha.recaptcha.public.key", null, null);
        }
        catch (NoSuchBeanDefinitionException nsbde) {
        	logger.error("captchaSource bean not found: " + nsbde.getLocalizedMessage());
        	return null;
        }
        catch (BeansException be) {
        	logger.error("Exception retireving captchaSource: " + be.getLocalizedMessage());
        	return null;
        }     
        catch (NoSuchMessageException nsme) {
        	logger.error("Unable to find property:  " + RECAPTCHA_PUBLIC_KEY_PROP_NAME + " (" + nsme.getLocalizedMessage() +")");
        	return null;
        }     
    }  

    public String getRecaptchaPrivateKey() {
        try {
        	MessageSource myProperties = (MessageSource) context.getBean("captchaSource");
        	return myProperties.getMessage("captcha.recaptcha.private.key", null, null);
        }
        catch (NoSuchBeanDefinitionException nsbde) {
        	logger.error("captchaSource bean not found: " + nsbde.getLocalizedMessage());
        	return null;
        }
        catch (BeansException be) {
        	logger.error("Exception retireving captchaSource: " + be.getLocalizedMessage());
        	return null;
        }     
        catch (NoSuchMessageException nsme) {
        	logger.error("Unable to find property:  " + RECAPTCHA_PRIVATE_KEY_PROP_NAME + " (" + nsme.getLocalizedMessage() +")");
        	return null;
        }     
    }  
 

    /** Verify the given reCaptcha response.
     * 
     * Call reCaptcha's site verify URL passing the (potentially forged) token from 
     * our client's reCaptcha call and our secret key. 
     * 
     * @param reCaptchaResponse The response from our client's call to Google's reCaptcha API 
     * @throws UserVerificationException If Google does not return 'success : true'
     * 
     * @see <a href="https://developers.google.com/recaptcha/docs/verify">https://developers.google.com/recaptcha/docs/verify</a>
     */
    public void verifyReCaptcha(String reCaptchaResponse) throws UserVerificationException {
    	
		logger.debug("In verifyReCaptcha, response: " + reCaptchaResponse);

		StringBuffer getRequest = new StringBuffer();
		getRequest.append(RECAPTCHA_URL).append("?").append(RECAPTCHA_KEY_KEY).append(getRecaptchaPrivateKey());
		getRequest.append("&").append(RECAPTCHA_RESPONSE_KEY).append(reCaptchaResponse);
		JsonObject rootobj;
		boolean valResult = false;
		
		try {
			URL getRequestUrl = new URL(getRequest.toString());
			HttpURLConnection request = (HttpURLConnection) getRequestUrl.openConnection();
			request.connect();
		    JsonParser jp = new JsonParser(); //from gson
		    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); 
		    rootobj = root.getAsJsonObject(); //may be an array, may be an object.
		    logger.debug("reCaptcha full response: " + rootobj);
		    valResult = (boolean) (rootobj.get("success") == null ? false : rootobj.get("success").getAsBoolean());		    
		} catch (MalformedURLException e) {
			logger.error("reCaptcha error - Malformed URL " + e.getLocalizedMessage());
    		throw new UserVerificationException("verify.recaptcha.exception", e.getLocalizedMessage());
		} catch (IOException e) {
			logger.error("reCaptcha error - IOException " + e.getLocalizedMessage());
    		throw new UserVerificationException("verify.recaptcha.exception", e.getLocalizedMessage());
		}
		
	    if (!valResult) {
    		UserVerificationException uvt = new UserVerificationException("verify.recaptcha.error");
	    	JsonElement errors = rootobj.get("error-codes");
	    	if (errors != null) {
	    		logger.debug("Error code(s) returned from reCaptcha validation: " + errors);
	    		String[] errorArr = {errors.getAsString()};
	    		uvt.setMessageArgs(errorArr);
	    	}
	    	throw uvt;
	    }
		return;    	
    }        
}
