/**
 * 
 */
package com.fit.service.tags;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fit.dao.tags.UserTagsDao;
import com.fit.model.security.User;
import com.fit.service.BaseService;
import com.fit.utils.SecurityUtil;

/**
 * @author pavula1
 * 
 */
@Service
public class UserTagsService implements BaseService {

	private static final Logger logger = Logger
			.getLogger(UserTagsService.class);

	@Autowired
	private UserTagsDao userTagsDao;

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Map<String, List<List<String>>> getUserTagsById(String userId) {
		//User currentUser = (User) SecurityUtil.getCurrentUser();
		List<Map<String, Object>> userTags = this.userTagsDao
				.getUserTagsById(Long.valueOf(userId));
		
		
		Map<String, List<List<String>>> map = new HashMap<String, List<List<String>>>();
    	List<List<String>> valuesList = new ArrayList<List<String>>();
		
		for(Map<String, Object> userTag:userTags) {
			List<String> values = new ArrayList<String>();
			values.add((String) userTag.get("tagId").toString());
			values.add((String) userTag.get("userId").toString());
			values.add((String)userTag.get("name"));
			values.add((String)userTag.get("diagnosis"));
			String phases="";
			if(userTag.get("chemoCompleted") != null) {
				phases += "Chemo therapy treatments:" + userTag.get("chemoCompleted") + " of " + userTag.get("chemoTotal");
				//phases = userTag.get("chemoCompleted") + " chemos";
			}
			if(!phases.isEmpty() && userTag.get("radiationCompleted") != null) {
				phases = phases+", "+"Radiation therapy treatments:"+ userTag.get("radiationCompleted")+ " of "+userTag.get("radiatonTotal") ;
			}
			
			if(phases.isEmpty() && userTag.get("radioTotal") != null) {
				phases = "Radiation therapy treatments:"+ userTag.get("radiationCompleted")+ " of "+userTag.get("radiatonTotal") ;
			}
			values.add(phases);
			values.add(userTag.get("taggedDate").toString());
			values.add("Untag");
			values.add("Message");
			valuesList.add(values);
			
		}

		map.put("tagData", valuesList);
		return map;
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public Map<String, List<List<String>>> deleteUserTag(String tagId, String userId) {
		userTagsDao.deleteTagById(Long.valueOf(tagId));
		return this.getUserTagsById(userId);
	}

}
