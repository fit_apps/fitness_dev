package com.fit.service.reference;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fit.dao.reference.ActivityTypeDao;
import com.fit.dao.reference.CountryDao;
import com.fit.dao.reference.StateProvinceDao;
import com.fit.model.refs.RefActivityType;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefStateProvinces;
import com.fit.service.BaseService;

/**
 * reference data related operations
 * 
 * @author channah
 *
 */
@Service
public class ReferenceService implements BaseService {

	@Autowired
	private CountryDao countryDao;

	@Autowired
	private StateProvinceDao stateProvinceDao;

	@Autowired
	private ActivityTypeDao activityTypeDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<RefCountries> getAllCountries() {
		List<RefCountries> countries = this.countryDao.getAllCountries();
		return countries;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public RefCountries getCountryById(Long id) {
		if (id != null) {
			RefCountries country = this.countryDao.getCountryById(id);
			return country;			
		} else {
			return null;
		}
	}	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<RefStateProvinces> getAllStateProvinces() {
		List<RefStateProvinces> stateProvinces = this.stateProvinceDao.getAllStatesProvinces();
		return stateProvinces;
	}	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public RefStateProvinces getStateProvinceById(Long id) {
		if (id != null) {
			RefStateProvinces stateProvince = this.stateProvinceDao.getStateProvinceById(id);
			return stateProvince;
		} else {
			return null;
		}
	}	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<RefActivityType> getAllActivityTypes() {
		List<RefActivityType> activityTypes = this.activityTypeDao.getAllActivityTypes();
		return activityTypes;
	}		
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public RefActivityType getActivityTypeById(Long id) {
		RefActivityType activityType = this.activityTypeDao.getActivityTypeById(id);
		return activityType;
	}		
}
