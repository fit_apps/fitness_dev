package com.fit.mvc.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.filter.GenericFilterBean;

import com.fit.utils.SecurityUtil;

/**
 * Filter that will check if web session is valid or no, if not valid will redirect to login page
 * Since we have restful methods in controllers, when they get called by AJAX calls, they are not aware of session expiring,
 * and will allow calls to DB, which results in 500 errors
 * 
 * @author amourad
 *
 */
public class SessionCheckFilter extends GenericFilterBean {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
    	Object currentUser = SecurityUtil.getCurrentUser();
    	HttpServletRequest httpReq = (HttpServletRequest) request;
    	HttpServletResponse httpResp = (HttpServletResponse) response;
    	HttpSession session = httpReq.getSession(false);
    	session.invalidate();
    	String contextPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + httpReq.getContextPath();
    	
    	if(session == null || currentUser instanceof String) {
    		httpResp.sendRedirect(contextPath);
    	} else {
    		chain.doFilter(request, response);
    	}
		
	}

}
