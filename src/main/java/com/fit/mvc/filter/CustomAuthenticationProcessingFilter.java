package com.fit.mvc.filter;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fit.service.security.UserService;

/*import com.bc.services.clubs.ClubNotFoundException;
import com.bc.services.security.UserService;
import com.bc.services.security.UserVerificationException;*/

/**
 * overrides few methods to be able to log users in after they verify their code
 * 
 * @author amouradi
 * 
 */
public class CustomAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter {

    static final Logger logger = Logger.getLogger(CustomAuthenticationProcessingFilter.class);
    
    @Autowired
    private UserService userService;
    private ResourceBundleMessageSource messageSource;

    protected void onSuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            Authentication authResult) throws IOException {
        logger.debug("onSuccessfulAuthentication");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        // TODO I don't need to call it here since user gets registered right away.
      //  this.onPreAuthentication(request, response);
        return super.attemptAuthentication(request, response);
    }

    protected void onUnsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) throws IOException {

        logger.debug("onUnsuccessfulAuthentication");
    }

    /**
     * try to enable user, given provided username, password and salt. If fails
     * will throw DisabledException
     */
   /* private void onPreAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        logger.debug("onPreAuthentication");
        boolean isVerifyErr = false;

        request.getSession().setAttribute("isVerifyErr", isVerifyErr);
        try {
            // verify registration, enable user if all is fine
            this.getUserService().enableUser(this.obtainUsername(request), request.getParameter("salt"),
                    this.obtainPassword(request), request.getParameter("clubId"));

        } catch (UserVerificationException e) {
            isVerifyErr = true;
            request.getSession().setAttribute("isVerifyErr", isVerifyErr);
            logger.error("-------CustomAuthenticationProcessingFilter:onPreAuthentication: " + e.getMessage());
            throw new DisabledException(messageSource.getMessage(e.getMessageKey(), e.getMessageArgs(),
                    Locale.getDefault()));
        }  
    }*/

    /**
     * overrides parent method check for password in request attribute, while
     * parent method only checks request parameter
     * 
     * @param request
     * @return password
     */
    @Override
    protected String obtainPassword(HttpServletRequest request) {
        String password = super.obtainPassword(request);
        if (StringUtils.isEmpty(password)) {
            password = (String) request.getSession().getAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY);
        }
        return password;
    }

    /**
     * overrides parent method check for username in request attribute, while
     * parent method only checks request parameter
     * 
     * @param request
     * @return username
     */
    @Override
    protected String obtainUsername(HttpServletRequest request) {
        String userName = super.obtainUsername(request);
        if (StringUtils.isEmpty(userName)) {
            userName = (String) request.getSession().getAttribute(SPRING_SECURITY_FORM_USERNAME_KEY);
        }
        return userName;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ResourceBundleMessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        super.setMessageSource(messageSource);
        this.messageSource = messageSource;
    }

}
