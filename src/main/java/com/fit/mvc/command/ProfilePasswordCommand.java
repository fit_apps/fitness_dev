package com.fit.mvc.command;

import java.io.Serializable;

/**
 * @author amouradian
 *
 */
public class ProfilePasswordCommand implements Serializable{

	private static final long serialVersionUID = 8904069113718855996L;
	
	private String currentPassword;
	private String salt;
	
	public String getCurrentPassword() {
		return currentPassword;
	}
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	
}
