package com.fit.mvc.command;


public abstract class MethodCommandImpl implements MethodCommand{
    protected String commandMethod;
    
    public String getCommandMethod() {
        return this.commandMethod;
    }

    public void setCommandMethod(String commandMethod) {
       this.commandMethod = commandMethod;
    }
}
