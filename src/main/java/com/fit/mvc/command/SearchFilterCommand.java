package com.fit.mvc.command;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author amourad
 *
 */
public class SearchFilterCommand implements Serializable {

	private static final long serialVersionUID = 956799001580212688L;

	private List<Map<String, Object>> diagFilter;
	private List<Map<String, Object>> actTypeFilter;
	private String selectedSort;
	private Integer radFilter ;
	private Integer chemoFilter ;
	private String cancerFreeDt; 

	public List<Map<String, Object>> getDiagFilter() {
		return diagFilter;
	}

	public void setDiagFilter(List<Map<String, Object>> diagFilter) {
		this.diagFilter = diagFilter;
	}

	public List<Map<String, Object>> getActTypeFilter() {
		return actTypeFilter;
	}

	public void setActTypeFilter(List<Map<String, Object>> actTypeFilter) {
		this.actTypeFilter = actTypeFilter;
	}

	public String getSelectedSort() {
		return selectedSort;
	}

	public void setSelectedSort(String selectedSort) {
		this.selectedSort = selectedSort;
	}

	public Integer getRadFilter() {
		return radFilter;
	}

	public void setRadFilter(Integer radFilter) {
		this.radFilter = radFilter;
	}

	public Integer getChemoFilter() {
		return chemoFilter;
	}

	public void setChemoFilter(Integer chemoFilter) {
		this.chemoFilter = chemoFilter;
	}

	public String getCancerFreeDt() {
		return cancerFreeDt;
	}

	public void setCancerFreeDt(String cancerFreeDt) {
		this.cancerFreeDt = cancerFreeDt;
	}
}
