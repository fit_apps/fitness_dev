package com.fit.mvc.command;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class ActivityDisplayCommand implements Serializable {

	private static final long serialVersionUID = -9201943093663375601L;
	
	private Long activityId;
	private Long ownerUserId;
	private String ownerUserDisplayName;
	private Long activityType;
	private String activityTypeName;
	private String activityTitle;
	private Date activityDate;
	private String zip;
	private String city;
	private Long stateProvinceId;
	private String stateProvince;
	private String country;
	private Long countryId;
	private Float distance;
	private Time startTime;
	private Time duration;
	private Integer energyAfterRating;
	private Integer energyBeforeRating;
	private String symptomsHelped;
	private String notes;
	private Boolean showPublic;
	private Long actCheersCount;
	private Long cheersByUserCount;
	private Long commentsByUserCount;
	private Long actComCount;
	
	public ActivityDisplayCommand() {
		super();
	}

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public Long getOwnerUserId() {
		return ownerUserId;
	}

	public void setOwnerUserId(Long ownerUserId) {
		this.ownerUserId = ownerUserId;
	}

	public String getOwnerUserDisplayName() {
		return ownerUserDisplayName;
	}

	public void setOwnerUserDisplayName(String ownerUserDisplayName) {
		this.ownerUserDisplayName = ownerUserDisplayName;
	}

	public Long getActivityType() {
		return activityType;
	}

	public void setActivityType(Long activityType) {
		this.activityType = activityType;
	}

	public String getActivityTypeName() {
		return activityTypeName;
	}

	public void setActivityTypeName(String activityTypeName) {
		this.activityTypeName = activityTypeName;
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getStateProvinceId() {
		return stateProvinceId;
	}

	public void setStateProvinceId(Long stateProvinceId) {
		this.stateProvinceId = stateProvinceId;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public Integer getEnergyAfterRating() {
		return energyAfterRating;
	}

	public void setEnergyAfterRating(Integer energyAfterRating) {
		this.energyAfterRating = energyAfterRating;
	}

	public Integer getEnergyBeforeRating() {
		return energyBeforeRating;
	}

	public void setEnergyBeforeRating(Integer energyBeforeRating) {
		this.energyBeforeRating = energyBeforeRating;
	}

	public String getSymptomsHelped() {
		return symptomsHelped;
	}

	public void setSymptomsHelped(String symptomsHelped) {
		this.symptomsHelped = symptomsHelped;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getShowPublic() {
		return showPublic;
	}

	public void setShowPublic(Boolean showPublic) {
		this.showPublic = showPublic;
	}

	public Long getActCheersCount() {
		return actCheersCount;
	}

	public void setActCheersCount(Long actCheersCount) {
		this.actCheersCount = actCheersCount;
	}

	public Long getCheersByUserCount() {
		return cheersByUserCount;
	}

	public void setCheersByUserCount(Long cheersByUserCount) {
		this.cheersByUserCount = cheersByUserCount;
	}

	public Long getCommentsByUserCount() {
		return commentsByUserCount;
	}

	public void setCommentsByUserCount(Long commentsByUserCount) {
		this.commentsByUserCount = commentsByUserCount;
	}

	public Long getActComCount() {
		return actComCount;
	}

	public void setActComCount(Long actComCount) {
		this.actComCount = actComCount;
	}

}