package com.fit.mvc.command;


/**
 * Commands that include commandMethod parameter, which
 * dictates method called by a Controller
 * @author amouradian
 *
 */
public interface MethodCommand  {
    public String getCommandMethod() ;
    public void setCommandMethod(String commandMethod);

}
