package com.fit.mvc.command;

import java.io.Serializable;

public class UserProfileCommand implements Serializable {
	 
	private static final long serialVersionUID = 435605808262008641L;
	private Integer chemoCompleted = null;
	private Integer chemoTotal = null;
	private Integer radCompleted = null;
	private Integer radTotal = null;
	private Integer phaseId = null;
	private Integer refChemoFreqId = null;
	
	
	public Integer getChemoCompleted() {
		return chemoCompleted;
	}
	public void setChemoCompleted(Integer chemoCompleted) {
		this.chemoCompleted = chemoCompleted;
	}
	public Integer getChemoTotal() {
		return chemoTotal;
	}
	public void setChemoTotal(Integer chemoTotal) {
		this.chemoTotal = chemoTotal;
	}
	public Integer getRadCompleted() {
		return radCompleted;
	}
	public void setRadCompleted(Integer radCompleted) {
		this.radCompleted = radCompleted;
	}
	public Integer getRadTotal() {
		return radTotal;
	}
	public void setRadTotal(Integer radTotal) {
		this.radTotal = radTotal;
	}
	public Integer getPhaseId() {
		return phaseId;
	}
	public void setPhaseId(Integer phaseId) {
		this.phaseId = phaseId;
	}
	public Integer getRefChemoFreqId() {
		return refChemoFreqId;
	}
	public void setRefChemoFreqId(Integer refChemoFreqId) {
		this.refChemoFreqId = refChemoFreqId;
	}
	
	

}
