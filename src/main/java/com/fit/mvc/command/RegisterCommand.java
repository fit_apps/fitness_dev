package com.fit.mvc.command;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class RegisterCommand implements Serializable {
	private static final long serialVersionUID = 4310929205196523432L;
	
	private String displayName;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String password;
	private String confirmPassword;
    private String message;
    List<Map<String, Object>>  diagHierarchy;
    private boolean success;
    
    public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("emailAddress=" + emailAddress + "\n");
		buf.append("firstName="+ firstName + "\n");
		buf.append("lastName=" + lastName + "\n");
		buf.append("displayName=" + displayName + "\n");
		buf.append("password=" + password + "\n");
		buf.append("confirmPassword=" + confirmPassword + "\n");
		return buf.toString();
	}
	
	public List<Map<String, Object>> getDiagHierarchy() {
		return diagHierarchy;
	}
	public void setDiagHierarchy(List<Map<String, Object>> diagHierarchy) {
		this.diagHierarchy = diagHierarchy;
	}
	
}
