package com.fit.mvc.command;

import java.io.Serializable;
import java.util.Date;

import com.fit.model.security.User;

public class ActivityCommand implements Serializable {

	private static final long serialVersionUID = -6726455983379614607L;
	
	private String activityTitle;
	private String activityDate;
	private Date activityDateAsDate;	
	private Long activityType;
	private Float distance;
	private String duration;
	private String startTime;
	private String zipOrLocation;
	private String zip;
	private String city;
	private Long stateProvince;
	private Long country;
	private Integer energyAfterRating;
	private Integer energyBeforeRating;
	private String symptomsHelped;
	private String notes;
	
	private Long userId;
	private String userDisplayName;
	private User user;	
	private Boolean isPublic;
	private String message;
	private String messageId;
    private boolean success;
    
    private Long activityId;
	
	public ActivityCommand() {
		super();
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public String getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public Date getActivityDateAsDate() {
		return activityDateAsDate;
	}

	public void setActivityDateAsDate(Date activityDateAsDate) {
		this.activityDateAsDate = activityDateAsDate;
	}

	public Long getActivityType() {
		return activityType;
	}

	public void setActivityType(Long activityType) {
		this.activityType = activityType;
	}

	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getZipOrLocation() {
		return zipOrLocation;
	}

	public void setZipOrLocation(String zipOrLocation) {
		this.zipOrLocation = zipOrLocation;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(Long stateProvince) {
		this.stateProvince = stateProvince;
	}

	public Long getCountry() {
		return country;
	}

	public void setCountry(Long country) {
		this.country = country;
	}

	public Integer getEnergyAfterRating() {
		return energyAfterRating;
	}

	public void setEnergyAfterRating(Integer energyAfterRating) {
		this.energyAfterRating = energyAfterRating;
	}

	public Integer getEnergyBeforeRating() {
		return energyBeforeRating;
	}

	public void setEnergyBeforeRating(Integer energyBeforeRating) {
		this.energyBeforeRating = energyBeforeRating;
	}

	public String getSymptomsHelped() {
		return symptomsHelped;
	}

	public void setSymptomsHelped(String symptomsHelped) {
		this.symptomsHelped = symptomsHelped;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserDisplayName() {
		return userDisplayName;
	}

	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

}