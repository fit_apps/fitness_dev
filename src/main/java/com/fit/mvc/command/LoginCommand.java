package com.fit.mvc.command;

import java.io.Serializable;

public class LoginCommand implements Serializable {

	private static final long serialVersionUID = 1818666416180590376L;

	private String message;
	private String redirectURL;
    private boolean redirect;
    private boolean success;
    
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRedirectURL() {
		return redirectURL;
	}
	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}
	public boolean isRedirect() {
		return redirect;
	}
	public void setRedirect(boolean redirect) {
		this.redirect = redirect;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	@Override
	public String toString() {
		return "LoginCommand [message=" + message + ", redirectURL="
				+ redirectURL + ", redirect=" + redirect + ", success="
				+ success + "]";
	}
    

}