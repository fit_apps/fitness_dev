package com.fit.mvc.command;

import java.io.Serializable;

public class ResetPasswordCommand implements Serializable {

	private static final long serialVersionUID = 4255399358333670946L;

	private String providedEmail;
    private String message;
    private boolean success;

	public String getProvidedEmail() {
		return providedEmail;
	}

	public void setProvidedEmail(String providedEmail) {
		this.providedEmail = providedEmail;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("providedEmail=" + providedEmail + "\n");
		buf.append("message="+ message + "\n");
		buf.append("success=" + success + "\n");
		return buf.toString();
	}	
}