package com.fit.mvc.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.service.diagnosis.DiagnosisService;

/**
 * @author amourad
 *
 */
@Controller
public class DiagnosisController { 
	 private static final Logger logger = LoggerFactory.getLogger(DiagnosisController.class);
	 
	 @Autowired
	 private DiagnosisService diagServices;
	 
	 @RequestMapping(value = "/rest/diagnosis/allParents.do", method = RequestMethod.GET)
	 public @ResponseBody List<Map<String, Object>> getAllDiagnosis(){
		 return this.diagServices.getAllParentDiagnosis();
	 }
	 
	 @RequestMapping(value = "/rest/diagnosis/allHierarchy.do", method = RequestMethod.GET)
	 public @ResponseBody List<Map<String, Object>> getAllDiagnHierarchy() {
		 return this.diagServices.getAllDiagnHierarchy();
	 }
	 
	@RequestMapping(value = "/rest/diagnosis/getUserCancerData.do", method = RequestMethod.GET)
	 public @ResponseBody Map<String, Object> getUserCancerData(HttpServletRequest request) {
		 Map<String, Object> cancerData = this.diagServices.getUserCancerData();
		 return cancerData;
	 }
	 
	@RequestMapping(value = "/rest/feed/userDiagInfo/{userId}.do", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getUserDiagInfo(HttpServletRequest request, @PathVariable String userId) {
		Map<String, Object> diagData = this.diagServices.getUserDiagInfo(userId);
		
		return diagData;
	}
	 

}
