/**
 * 
 */
package com.fit.mvc.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.service.tags.UserTagsService;

/**
 * @author pavula1
 *
 */
@Controller
public class UserTagsController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserTagsController.class);
	
	 @Autowired
	   private UserTagsService userTagsService;
	 
	 @RequestMapping(value="/rest/user/userTags/{userId}.do", method = RequestMethod.GET)
	    public @ResponseBody Map<String, List<List<String>>> getUserTagsById(HttpServletRequest request,
	    		@PathVariable String userId) {
		 
		 return   this.userTagsService.getUserTagsById(userId);
	    	
	 }
	 
	 @RequestMapping(value="/rest/user/untagUser/{tagId}/{userId}.do", method = RequestMethod.GET)
	    public @ResponseBody Map<String, List<List<String>>> unTagUser(HttpServletRequest request,
	    		@PathVariable String tagId, @PathVariable String userId) {
		 
		 return   this.userTagsService.deleteUserTag(tagId, userId);
	    	
	 }

}
