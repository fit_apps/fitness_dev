package com.fit.mvc.controller;

import java.util.Locale;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fit.model.security.User;
import com.fit.mvc.command.ResetPasswordCommand;
import com.fit.service.security.UserService;
import com.fit.service.security.UserVerificationException;
import com.fit.utils.MailUtils;
import com.fit.utils.UserUtils;

/**
 * password reset related actions
 * 
 * @author channah
 *
 */
@Controller
public class ResetPasswordController {

	public static final String GENERATED_PASSWORD_FORMAT = "u*l*s*n*";
			
	private static final Logger logger = Logger
			.getLogger(ResetPasswordController.class);

	@Autowired
	private MailUtils mailUtils;
	
	@Autowired
	private UserService userService;

	@Autowired
	private ApplicationContext context;

	/**
	 * @param resetData
	 *            will take in json data and spring will convert it into ResetPasswordCommand object
	 * @return
	 */
	
	@RequestMapping(value = "/rest/resetpassword.do", method = RequestMethod.POST)
	public @ResponseBody ResetPasswordCommand resetPassword(@RequestBody ResetPasswordCommand resetData) {

		ResetPasswordCommand retData = new ResetPasswordCommand();

		String generatedPassword = UserUtils.genString(GENERATED_PASSWORD_FORMAT, true);

		try {
			User user = userService.resetPasswordByAdmin(resetData.getProvidedEmail(), generatedPassword);
			retData.setSuccess(true);
			
			String emailMsg = String.format(context.getMessage("reset.password.email.notice.body", null,
					Locale.getDefault()), user.getFirstName(), user.getEmailAddress(), generatedPassword);
			mailUtils.sendEmail(resetData.getProvidedEmail(), context
					.getMessage("reset.password.email.notice.subject", null, Locale.getDefault()), emailMsg);
			
			retData.setMessage(context.getMessage("reset.password.succeeded", null, Locale.getDefault()));
		} catch (UserVerificationException uve) {
			retData.setSuccess(false);
			retData.setMessage(context.getMessage("exception.user.notFound", null, Locale.getDefault()));
			logger.error(uve.toString());
		} catch (Exception e) {
			retData.setSuccess(false);
			retData.setMessage(context.getMessage("err.admin", null, Locale.getDefault()));
			logger.error(e.getMessage());
		}
		return retData;
	}	
}
