package com.fit.mvc.controller;

import java.io.Serializable;

/**
 * Model of Person Contact, data are simplified and don't match tables. Otherwise would have to model
 * person, address, state, country tables as well.
 * 
 * @author amourad
 *
 */
public class SampleData implements Serializable{

	private static final long serialVersionUID = -9168449480128712114L;
	
	private String firstName;
	private String lastName;
	private String fullAddress;
	private String mobilePhone;
	private String officePhone;
	private String fax;
	private String email;
	
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFullAddress() {
		return fullAddress;
	}
	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
