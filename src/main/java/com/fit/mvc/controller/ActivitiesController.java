package com.fit.mvc.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.dao.activities.ActivityNotFoundException;
import com.fit.model.activity.Activity;
import com.fit.model.security.User;
import com.fit.mvc.command.ActivityCommand;
import com.fit.mvc.command.ActivityDisplayCommand;
import com.fit.mvc.command.SearchFilterCommand;
import com.fit.service.activities.ActivityService;
import com.fit.utils.SecurityUtil;

/**
 * Contacts spring RESTfull controller
 * @author amourad
 *
 */
@Controller
public class ActivitiesController {

	   private static final Logger logger = LoggerFactory.getLogger(ActivitiesController.class);
	   
	   @Autowired
	   private ActivityService actService;
	    
	   @Autowired
	   private ApplicationContext context; 
	    
	    /**
	     * get user activities
	     * 
	     * @param request
	     * @param userId
	     * @param mv
	     * @return
	     */
	    @RequestMapping(value="/rest/user/activities/{userId}.do", method = RequestMethod.GET)
	    public @ResponseBody Map<String, List<List<String>>> getUserActivities(HttpServletRequest request,
	    		@PathVariable String userId) {
	    	Map<String, List<List<String>>> map = new HashMap<String, List<List<String>>>();
	    	List<List<String>> valuesList = new ArrayList<List<String>>();
	    	List<Activity> activities =  this.actService.getUserActivities(userId);
	    	
	    	for(Activity activity:activities) {
	    		List<String> values = new ArrayList<String>();
	    		values.add(activity.getId().toString());
	    		values.add(activity.getTitle());
	    		values.add(activity.getActivityDate().toString());
	    		
	    		values.add(activity.getActivityType().getDescription());
	    		
	    		if(activity.getDuration()!=null) {
	    			values.add(activity.getDuration().toString());
	    		}else {
	    			values.add("00:00:00");
	    		}
	    		if(activity.getDistance() != null) {
	    			values.add(activity.getDistance().toString());
	    		}else {
	    			values.add("0");
	    		}
	    		
	    		values.add("View");
	    		valuesList.add(values);
	    	}
	    	map.put("myData", valuesList);
	    	
	        return map;
	    }
	    
	    /**
	     * get activities feed for the users with similar diagnosis to the one logged in.
	     * 
	     * @param request
	     * @param userId
	     * @return
	     */
	    @RequestMapping(value="/rest/feed/activities/{startRow}/{numOfRows}.do", method = RequestMethod.GET)
	    public @ResponseBody List<Map<String, Object>> getFeedActivities(HttpServletRequest request, @PathVariable int startRow,  
	    							@PathVariable int numOfRows) {
	    	
	    	List<Map<String, Object>> activities =  this.actService.getFeedActivities(startRow, numOfRows);
	    	return activities; 
	    }
	    
	    
	    /**
	     * gets called after login. Does not really do anything.
	     * 
	     * @param  
	     * @return
	     */
	    @RequestMapping(value = "/member/activities/feed.do", method = RequestMethod.GET)
	    public @ResponseBody ResponseEntity<?>  loadActivities( ) {
	   //  Object currentUser = SecurityUtil.getCurrentUser();
		  return new ResponseEntity<String>("", HttpStatus.NO_CONTENT);
	    	
	    }
	    
	    
	    @RequestMapping(value = "/rest/feed/applyFilter/{startRow}/{numOfRows}.do", method = RequestMethod.POST)
	    public @ResponseBody List<Map<String, Object>> applySearchFilter(HttpServletRequest request, @PathVariable int startRow,  
				@PathVariable int numOfRows, @RequestBody SearchFilterCommand searchFilterCommand) {
	    	
	    	List<Map<String, Object>> activities =  this.actService.getFeedActivities(searchFilterCommand, startRow, numOfRows);
	    	return activities;
	    }
	    
	    @RequestMapping(value="/rest/feed/updateTag/{activityUserId}/{activityTagCount}.do", method = RequestMethod.GET)
	    public @ResponseBody Map<String, Object> updateActivityTag(HttpServletRequest request, @PathVariable String activityUserId,  
				@PathVariable int activityTagCount){
	    	
	    	Map<String, Object> retMap =  this.actService.updateActivityUserTag(activityUserId, activityTagCount);
	    	
	    	return retMap;
	    }
	    
	    @RequestMapping(value="/rest/feed/updateCheer/{activityId}/{cheerCountByUser}.do", method = RequestMethod.GET)
	    public @ResponseBody Map<String, Object> updateActivityCheer(HttpServletRequest request, @PathVariable String activityId,  
				@PathVariable int cheerCountByUser){
	    	
	    	Map<String, Object> retMap =  this.actService.updateActivityCheer(activityId, cheerCountByUser);
	    	return retMap;
	    }
	    
	    
	    @RequestMapping(value="/rest/feed/getActivityCheerUsers/{activityId}.do", method = RequestMethod.GET)
	    public @ResponseBody List<Map<String, Object>> getActivityCheerUsers(HttpServletRequest request, @PathVariable String activityId){
	    	
	    	return this.actService.getActivityCheerUsers(activityId);
	    }
	    
	     
	    @RequestMapping(value="/rest/feed/getActivityComments/{activityId}/{startRow}/{maxRows}.do", method = RequestMethod.GET)
	    public @ResponseBody List<Map<String, Object>> getActivityComments(HttpServletRequest request, 
	    		@PathVariable String activityId, @PathVariable int startRow, @PathVariable int maxRows){
	    	
	    	return this.actService.getActivityComments(activityId, startRow, maxRows);
	    }
	    
	    @RequestMapping(value="/rest/feed/deleteActivityComment/{activityId}/{commentId}/{maxRows}.do", method = RequestMethod.GET)
	    public @ResponseBody Map<String, Object>  deleteActivityComment(HttpServletRequest request, 
	    		@PathVariable String commentId, @PathVariable String activityId, @PathVariable int maxRows){
	    	
	    	this.actService.deleteActivityComment(commentId);
	    	Map<String, Object> retData = new HashMap<String, Object>();
			retData.put("comments", this.actService.getActivityComments(activityId, 0, maxRows));
			retData.put("userCommentsCount", this.actService.getCurrentUserActivityCommentsCount(activityId));
	    	return retData;
	    }
	     
	     /**
	      * Add new comment
	     * @param request
	     * @param activityId
	     * @param maxRows
	     * @param newComment
	     * @return
	     */
		@RequestMapping(value = "/rest/feed/activity/newComment/{activityId}/{maxRows}.do", method = RequestMethod.POST)
		public @ResponseBody List<Map<String, Object>> addNewComment(HttpServletRequest request,
				@PathVariable String activityId, @PathVariable int maxRows,
				@RequestBody String newComment) {
			actService.addNewComment(newComment, activityId);
			
			return this.actService.getActivityComments(activityId, 0, maxRows); 		
		}
		
		@RequestMapping(value = "/rest/feed/activity/updateComment/{activityId}/{commentId}.do", method = RequestMethod.POST)
		public @ResponseBody Map<String, Object> updateComment(HttpServletRequest request, @PathVariable String activityId,
				@PathVariable String commentId,  @RequestBody String newComment) {
			Map<String, Object> retCommentMap = actService.updateComment(newComment, activityId, commentId);
			 
			return retCommentMap; 		
		}
	   
	     /**
	      * gets called to display activity types in activity feed and anywhere else it's needed
	     * @return
	     */
	    @RequestMapping(value = "/rest/activityTypes/all.do", method = RequestMethod.GET)
		 public @ResponseBody List<Map<String, Object>> getAllDiagnosis(){
			 return this.actService.getAllActivityTypes();
		 }


	     /**
	      * Add a new activity
	     * @return
	     */
	    @RequestMapping(value = "/rest/activity/add.do", method = RequestMethod.POST)
		 public @ResponseBody ActivityCommand addNew(HttpServletRequest request, 
				 @RequestBody ActivityCommand activityCommand){
	    	//Validate what's needed in activitycommand
	    	activityCommand.setUser((User) SecurityUtil.getCurrentUser());
	    	
	    	Activity newActivity = null;
	    	try {
				newActivity = actService.saveActivity(activityCommand);
			} catch (Exception e) {
		    	ActivityCommand retData = new ActivityCommand();
		    	retData.setSuccess(false);
		    	retData.setMessage(MessageFormat.format(context.getMessage("act.add.failure", null, Locale.getDefault()), 
		    			e.getLocalizedMessage()));
		    	retData.setMessageId("act.add.failure");
		    	return retData;
		    	}
	    	ActivityCommand retData = new ActivityCommand();
	    	retData.setSuccess(true);
	    	retData.setActivityId(newActivity.getId());
	        retData.setMessage(context.getMessage("act.add.success", null, Locale.getDefault()));
	    	retData.setMessageId("act.add.success");
	    	return retData;
	    }  

	    /**
	      * Update an existing activity
	     * @return
	     */
	    @RequestMapping(value = "/rest/activity/update.do", method = RequestMethod.POST)
		 public @ResponseBody ActivityCommand update(HttpServletRequest request, 
				 @RequestBody ActivityCommand activityCommand){

	    	activityCommand.setUser((User) SecurityUtil.getCurrentUser());
	    	
	    	Activity newActivity = null;
	    	try {
				newActivity = actService.updateActivity(activityCommand);
			} catch (Exception e) {
		    	ActivityCommand retData = new ActivityCommand();
		    	retData.setSuccess(false);
		    	retData.setMessage(MessageFormat.format(context.getMessage("act.update.failure", null, Locale.getDefault()), 
		    			e.getLocalizedMessage()));
		    	retData.setMessageId("act.update.failure");
		    	return retData;
		    	}
	    	ActivityCommand retData = new ActivityCommand();
	    	retData.setSuccess(true);
	    	retData.setActivityId(newActivity.getId());
	        retData.setMessage(context.getMessage("act.update.success", null, Locale.getDefault()));
	    	retData.setMessageId("act.update.success");
	    	return retData;
	    }  	    
	    	    
	    @RequestMapping(value="/rest/feed/deleteActivity/{activityId}.do", method = RequestMethod.GET)
	    public @ResponseBody String  deleteActivity(@PathVariable Long activityId){
	    	
	    	try {
	    		this.actService.deleteActivity(activityId);
	    		return "act.del.success";
	    	}
	    	catch (ActivityNotFoundException anfe) {
	    		return "act.del.notFound";
	    	}

	    }	         

	    /**
	     * @param activityId
	     * @return
	     */
	    @RequestMapping(value="/rest/activity/view/{activityId}.do", method = RequestMethod.GET)
	    public @ResponseBody ActivityDisplayCommand getActivityDisplayCommand(@PathVariable String activityId){
	        logger.debug("------------/rest/activity/view/{activityId}/.do for activityId: " +  activityId);
	        return this.actService.getActivityDisplayCommand(Long.valueOf(activityId));
	    }		    
	    
	    
	     /**
	      * gets called to display activity types in activity feed and anywhere else it's needed
	     * @return
	     */
	    @RequestMapping(value = "/rest/activity/myActivities/{userId}.do", method = RequestMethod.GET)
		 public @ResponseBody List<Map<String, Object>> getMyActivites(@PathVariable String userId){
			 return this.actService.getTotalActivitiesByUserId(userId);
		 }
	    
	  		
}
