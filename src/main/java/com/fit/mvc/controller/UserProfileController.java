package com.fit.mvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.mvc.command.ProfilePasswordCommand;
import com.fit.mvc.command.UserProfileCommand;
import com.fit.service.diagnosis.DiagnosisService;
import com.fit.service.security.UserService;

/**
 * 
 * User profile data updates/actions
 * @author amourad
 *
 */
@Controller
public class UserProfileController {
	
	@Autowired
	private DiagnosisService diagService;
	@Autowired
	private UserService userService;
	
	
	@RequestMapping(value = "/rest/feed/updateTreatments.do", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> updateCurUserTreatments(HttpServletRequest request, 
    		@RequestBody UserProfileCommand userProfileCommand) {
    	
    	   diagService.updateCurUserTreatments(userProfileCommand);
    	   
    	return diagService.getUserCancerData();
    }
	
	@RequestMapping(value = "/rest/profile/saveUserProfile.do", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveUserProfileData(HttpServletRequest request, 
    		@RequestBody Map<String, Object> userDataMap) {
		System.out.println("----submitted: " + userDataMap);
		Map<String, Object> retMap = new HashMap<String, Object>();
		//save profile
		 
			this.userService.saveUserProfile(userDataMap);
	 
		retMap.put("success", "true");
		return retMap; 
	}
	
	@RequestMapping(value = "/rest/profile/deleteDiagnosis.do", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteUserDiagnosis(HttpServletRequest request, 
			@RequestBody Map<String, Object> userDiagDataMap) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		 
		this.userService.deleteUserDiagnosis(userDiagDataMap);
	 
		retMap.put("success", "true");
		return retMap; 
	}
	    
	@RequestMapping(value="/rest/profile/userTags/{userId}.do", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> getTaggedByUser(HttpServletRequest request,
			@PathVariable String userId) {  
		
		return userService.getTaggedByUser(userId);
	}
	
	@RequestMapping(value="/rest/profile/viewUserProfileData/{userId}.do", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> viewUserProfileData(HttpServletRequest request, @PathVariable String userId) {  
		
		return userService.getCurrentUserProfileData(userId, false);
	}
	
	@RequestMapping(value="/rest/profile/editUserProfileData/{userId}.do", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> editUserProfileData(HttpServletRequest request, @PathVariable String userId) {  
		
		return userService.getCurrentUserProfileData(userId, true);
	}
	
	@RequestMapping(value="/rest/profile/verifyCurrentUserPass.do", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> verifyCurrentUserPassword(HttpServletRequest request, 
			@RequestBody ProfilePasswordCommand profilePasswordCommand) {  
		
		boolean isValidPass = userService.verifyCurrentUserPassword(profilePasswordCommand);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isValidPass", isValidPass);
		
		return map;
	}
}
