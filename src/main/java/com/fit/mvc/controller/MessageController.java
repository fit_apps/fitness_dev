package com.fit.mvc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.utils.BaseRuntimeException;
import com.fit.utils.SecurityUtil;

/**
 * Handle message related actions
 * 
 * @author channah
 *
 */
@Controller
public class MessageController {
	
	@Autowired
	private ApplicationContext context; 

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	 
	private static Map <String, Map<String, String>> msgMap = new HashMap <String, Map<String, String>> ();
	 
	 /*
	  * ToDo
	  * Logon and logoff should call clearMessages
	  * Things will still accumulate, maybe keep time in map and periodically clear old stuff
	  */

    @RequestMapping(value="/message/put/{msgId}/{msgValue}.do", method = RequestMethod.GET)
    public @ResponseBody String putMessage(HttpServletRequest request, @PathVariable String msgId, @PathVariable String msgValue){
    	logger.debug("------------putMessage/x/y.do for msgId: " +  msgId + " msgValue: " + msgValue);
    	
    	String currentSessionId = SecurityUtil.getCurrentSession();
    	if (currentSessionId == null) {
    		throw new BaseRuntimeException("No current session");			//s/ use another type?
    	}
    	return putMessageDetail(msgId, msgValue, currentSessionId);    	
    }

	String putMessageDetail(String msgId, String msgValue,
			String currentSessionId) {
		String oldValue;
    	 synchronized(msgMap) {
	    	if (!msgMap.containsKey(currentSessionId)) {
	    		msgMap.put(currentSessionId, new HashMap<String, String>());
	    	}
	    	Map <String, String> currentUserMap = msgMap.get(currentSessionId);
	    	oldValue = currentUserMap.put(msgId,  msgValue);
    	 }
    	return oldValue;
	}	    	 

    @RequestMapping(value="/message/get/{msgId}.do", method = RequestMethod.GET)
    public @ResponseBody String getMessage(HttpServletRequest request, @PathVariable String msgId){
    	logger.debug("------------getMessage/x.do for msgId: " +  msgId);
    	
    	String currentSessionId = SecurityUtil.getCurrentSession();
    	if (currentSessionId == null) {
    		return null;
    	}
   	 	return getMessageDetail(msgId, currentSessionId);
    }

	String getMessageDetail(String msgId, String currentSessionId) {
		synchronized(msgMap) {
	    	if (!msgMap.containsKey(currentSessionId)) {
	    		return null;
	    	}
	    	Map <String, String> currentUserMap = msgMap.get(currentSessionId);
	    	return currentUserMap.get(msgId);
	   	 }
	}
    
    @RequestMapping(value="/message/delete/{msgId}.do", method = RequestMethod.GET)
    public @ResponseBody String deleteMessage(HttpServletRequest request, @PathVariable String msgId){
    	logger.debug("------------deleteMessage/x.do for msgId: " +  msgId);

    	String currentSessionId = SecurityUtil.getCurrentSession();
    	if (currentSessionId == null) {
    		throw new BaseRuntimeException("No current session");
    	}
   	 	return deleteMessageDetail(msgId, currentSessionId);
    }

	String deleteMessageDetail(String msgId, String currentSessionId) {
		synchronized(msgMap) {
	    	if (!msgMap.containsKey(currentSessionId)) {
	    		return null;
	    	}
	    	Map <String, String> currentUserMap = msgMap.get(currentSessionId);
	    	return currentUserMap.remove(msgId);
   	 	}
	}

    @RequestMapping(value="/message/clear.do", method = RequestMethod.GET)
    public @ResponseBody Integer clearMessages(HttpServletRequest request){
    	logger.debug("------------clearMessages/x.do");
    	String currentSessionId = SecurityUtil.getCurrentSession();
    	if (currentSessionId == null) {
    		throw new BaseRuntimeException("No current session");
    	}
    	return clearMessageDetail(currentSessionId);
    }

	Integer clearMessageDetail(String currentSessionId) {
		Integer currentSize;
   	 	synchronized(msgMap) {
	    	if (!msgMap.containsKey(currentSessionId)) {
	    		return null;
	    	}
	    	Map <String, String> currentUserMap = msgMap.get(currentSessionId);
	    	currentSize = currentUserMap.size();
	    	msgMap.remove(currentSessionId);
   	 	}
    	return currentSize;
	}    
}
