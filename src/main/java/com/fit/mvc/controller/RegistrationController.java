package com.fit.mvc.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.dao.DuplicateException;
import com.fit.mvc.command.RegisterCommand;
import com.fit.service.security.UserService;

/**
 * registration related actions
 * 
 * @author amourad
 *
 */
@Controller
public class RegistrationController {

	 private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);
	 
	 @Autowired
	 private UserService userService;
	 
	 @Autowired
	 private ApplicationContext context; 
	 
	 /**
     * @param regData will take in json data and spring will convert it into Contact object
     * @return
     */
    @RequestMapping(value = "/rest/register.do", method = RequestMethod.POST)
    public @ResponseBody RegisterCommand register(@RequestBody RegisterCommand regData) {
    	RegisterCommand retData = new RegisterCommand();
    	
    	try {
			userService.registerUser(regData);
			retData.setSuccess(true);
	    	retData.setDisplayName(regData.getDisplayName());
	    	retData.setMessage(context.getMessage("txt.reg.success", null, Locale.getDefault()));
		} 
    	catch(DuplicateException de){
    		retData.setSuccess(false);
    		retData.setMessage(context.getMessage("exception.user.unique", null, Locale.getDefault()));
			logger.error(de.toString());
		}
    	catch (Exception e) {
    		retData.setSuccess(false);
    		retData.setMessage(context.getMessage("err.admin", null, Locale.getDefault()));
			logger.error(e.getMessage());
		}
    	
        return retData;
    }
	    
}
