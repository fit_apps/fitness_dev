package com.fit.mvc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * RESTfull controller for retrieving server side defined literals
 * @author channah
 *
 */
@Controller
public class LiteralController {

//	   private static final Logger logger = LoggerFactory.getLogger(LiteralController.class);
	   
	   @Autowired
	   private ApplicationContext context; 
	    
	     /**
	      * Get a single Spring message corresponding to the passed in key
	     * @param request http request object
	     * @param id the keys to the messages to be retrieved
	     * @return a String of the retrieved value
	     */
	    @RequestMapping(value="/rest/literal/stringByName.do", method = RequestMethod.POST)
	    public @ResponseBody String getStringByName(HttpServletRequest request, @RequestBody String id) {
	    	try {
	    		return context.getMessage(id, null, Locale.getDefault());			
	    	}
	    	catch (NoSuchMessageException nsme) {
	    		return "Missing";
	    	}
	    }

	     /**
	      * Get multiple Spring messages corresponding to the passed in keys
	     * @param request http request object
	     * @param ids array of the keys to the messages to be retrieved
	     * @return a list of maps with the key requested and the retrieved value
	     */
	    @RequestMapping(value = "/rest/literal/stringByNames.do", method = RequestMethod.POST)
		public @ResponseBody List<Map<String, String>> getStringByNames(HttpServletRequest request, 
				@RequestBody String[] ids) {
	    	
	    	String defaultRef = null;
	    	Map <String, String> map;
	    	List  <Map<String, String>> results = new ArrayList<Map<String, String>>();

		    for (String id : ids) {
		    	try {
		    		map = new HashMap<String, String>();
		    		map.put("stringName", id);
		    		map.put("stringValue", context.getMessage(id, null, Locale.getDefault()));
		    		results.add(map);
		    	}
		    	catch (NoSuchMessageException nsme) {
		    		map = new HashMap<String, String>();
		    		map.put("stringName", id);
		    		map.put("stringValue", defaultRef==null?"Missing":defaultRef);
		    		results.add(map);	    		
		    	}			
		    }
		    return results;
	    }     
}