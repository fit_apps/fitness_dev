package com.fit.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.service.security.CaptchaService;
import com.fit.service.security.UserVerificationException;

/**
 * capcha related actions
 * 
 * @author channah
 *
 */
@Controller
public class CaptchaController {

	private static final Logger logger = Logger.getLogger(CaptchaController.class);

	@Autowired
	private CaptchaService captchaService;

	@Autowired
	private ApplicationContext context;

	/**  
	 * @return the public key for our URL's use of Google's reCaptcha tool
	 */
	
	@RequestMapping(value = "/rest/recaptcha/publicKey.do", method = RequestMethod.GET)
	public @ResponseBody String getRecaptchaPublicKey (HttpServletRequest request) {
		logger.debug("In getRecaptchaPublicKey");
		return captchaService.getRecaptchaPublicKey();
	}	
	
	/** Call the service to validate with Google that the passed in reCaptcha 
	 * response is authentic 
	 * @param the reCaptcha response as provided by our web page reCaptcha script
	 * @return a success or failure message
	 */
	
	@RequestMapping(value = "/rest/validate/captcha.do", method = RequestMethod.POST)
	public @ResponseBody Object validateReCaptchaResponse (@RequestBody String response) {
		logger.debug("In validateReCaptchaResponse, response: " + response);
		String ourResponse = new String("{ \"success\" : \"true\" }");
		try {
			captchaService.verifyReCaptcha(response);
		} catch (UserVerificationException uve) {
			Object[] errorTemp = uve.getMessageArgs();
			String message = "";
			if (errorTemp != null) {
				message = (String) errorTemp[0];
			}
			ourResponse = new String (
					"{ \"success\" : \"false\"," +
					"\"messageKey\" : \"" + uve.getMessageKey() + "\"," +
					"\"code\" : \"" + message + "\" }"
					);
		}
		logger.debug("validateReCaptchsResponse, ourResponse: " + ourResponse);	
		
		 return ourResponse;
	}		
}
