package com.fit.mvc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.model.refs.RefActivityType;
import com.fit.model.refs.RefCountries;
import com.fit.model.refs.RefStateProvinces;
import com.fit.service.reference.ReferenceService;

/**
 * @author channah
 *
 */
@Controller
public class ReferenceController { 
	 private static final Logger logger = LoggerFactory.getLogger(ReferenceController.class);
	 
	 @Autowired
	 private ReferenceService refServices;
	 
	 @RequestMapping(value = "/rest/reference/allCountries.do", method = RequestMethod.GET)
	 public @ResponseBody List<RefCountries> getAllDiagnosis(){
		 return this.refServices.getAllCountries();
	 }
	 
	 @RequestMapping(value = "/rest/reference/allStates.do", method = RequestMethod.GET)
	 public @ResponseBody List<RefStateProvinces> getAllStateProvinces() {
		 return this.refServices.getAllStateProvinces();
	 }

	 @RequestMapping(value = "/rest/reference/allActivityTypes.do", method = RequestMethod.GET)
	 public @ResponseBody List<RefActivityType> getAllActivityTypes() {
		 return this.refServices.getAllActivityTypes();
	 }	 
}