'use strict';

// Declare app level module which depends on filters, and services
var fitApp = angular.module('fitApp', ['ngRoute', 
                                       'registrationModule', 'loginModule', 'resetPasswordModule', 'activitiesModule', 
                                       'editActivityModule', 'viewActivityModule', 'activityDetailModule', 'profileModule',
                                       'commonService']); 