'use strict';

var diagService = angular.module('fitApp.diagService', []);

diagService.factory("DiagnosisService", function($http) {
	return {
	   getAllParentDiagosisTypes : function(contextPath) {
		   return $http.get(contextPath + "/rest/diagnosis/allParents.do");
	   },
	   
	   getAllDiagnosisHierarchy: function(contextPath){
	   	return $http.get(contextPath + "/rest/diagnosis/allHierarchy.do");
	   },
	   
	   currentUserHasCancer: function(contextPath){
	   	return $http.get(contextPath + "/rest/diagnosis/getUserCancerData.do");
	   }	
	 };
	
});