'use strict';

/* Registration Module and Controller */

var registrationModule = angular.module('fitApp.registrationModule', ['fitApp.diagService', 'fitApp.commonService', 'vcRecaptcha']);

registrationModule.directive('buttonname', function() {
    return {
    restrict: "A",						//Restrict directive to only attributes
    link: function(scope, element, attributes) {
        	element.bind("click", function(){
            scope.buttons.chosen = attributes.buttonname;
          });           
        }
    };
});

registrationModule.controller('registrationController', ['$scope','$window', '$http', 'DiagnosisService', 'CommonService', 'vcRecaptchaService' ,
                                                         function($scope, $window, $http, DiagnosisService, CommonService, vcRecaptchaService) {
     $scope.submitted = false;
     $scope.diagHierarchy = [];
     $scope.primaryDiag = {};
     $scope.contextPath = "";
     $scope.regformData = {};
     $scope.errorMessage = "";
     $scope.passwordErr = {};
     $scope.buttons = { chosen: "" };		//Used by the custom directive
     $scope.recaptchaData = {};
     
     //load all diagnosis types
     $scope.init = function(contextPath) {
    	 $scope.contextPath = contextPath;
    	 CommonService.getRecaptchaPublicKey($scope.contextPath).success(function (data) {
     		$scope.recaptchaData.key = data;
 			console.log("$scope.recaptchaData.key:" + $scope.recaptchaData.key);					
 			}).error(function (data, status, headers, config) {
 				console.log("--err with getting all recaptcha public key:" + status);
 			});
    	 DiagnosisService.getAllDiagnosisHierarchy($scope.contextPath).success(function(data) {
 			$scope.diagHierarchy = data;
 			 $scope.primaryDiag = $scope.diagHierarchy[0];
 		}).error(function(data, status, headers, config){
 			console.log("--err with getting diagnosis hierarhy status:" + status);
 		});
 	 };
 	 
 	$scope.displayDiagParent = function(index, allDiags) {
		return CommonService.displayDiagParent(index, allDiags);
	};

	$scope.setRecaptchaResponse = function (response) {
		$scope.recaptchaData.captchaIncomplete = false;
		$scope.recaptchaData.response = response;
	};
	$scope.setRecaptchaWidgetId = function (widgetId) {
		$scope.recaptchaData.captchaIncomplete = false;
		$scope.recaptchaData.widgetId = widgetId;
	};	
	
	//called on passwords type, so that previous errors don't show up
	$scope.resetSubmitted = function(){
		 $scope.submitted = false;
	};
	
	$scope.processRegForm = function(isValid ){
		
		 if($scope.buttons.chosen == "cancelRegister")  {
    		 $window.location.href = $scope.contextPath;  
    		 return;
    	 }
		 
		 $scope.submitted = true;
		 
		 if (CommonService.isEmpty($scope.recaptchaData.response)) {
			 $scope.recaptchaData.captchaIncomplete = true;
			 return; 
		 } 
		 
		 $scope.passwordErr = CommonService.validatePassword($scope.regformData.password, $scope.regformData.confirmPassword);
		 var isValidPassword =  $scope.passwordErr.isValidPass;
		//mark selected primary diagnosis as checked
		 
		 for(var i=0; i<$scope.diagHierarchy.length; i++) {
			var diag = $scope.diagHierarchy[i];
			if($scope.primaryDiag.childDiagKey == diag.childDiagKey) {
				diag.isChecked = true;
				diag.isPrimary = true;
			} else {
				diag.isPrimary = false;
			}			 
		 }
	     isValid = (!isValidPassword)?false:isValid;
		 
		 if(isValid) {
			 CommonService.validateReCaptchaResponse($scope.contextPath, $scope.recaptchaData.response).
			 	success(function (data) {
		            $scope.regSuccess = data.success=='true'?true:false;
		            if (!$scope.regSuccess) {
		                $scope.errorMessage = data.messageKey
				 		console.log("Captcha validation failure, result: " + data);
				 		console.log("Success: " + data.success);
				 		console.log("MessageKey: " + data.messageKey);
				 		console.log("Code: " + data.code);
		            }
		            else {		            	
			   			 $scope.regformData.diagHierarchy =  $scope.diagHierarchy;
			 			//post to rest service
			 			 $http({
			 			        method  : 'POST',
			 			        url     : $scope.contextPath + '/rest/register.do',
			 			        data    : $scope.regformData
			 			    })
			 			    .success(function(data) {
			 		            console.log(data);
			 		            $scope.regSuccess = data.success;
			 		            if (!data.success) {
			 		            	// if not successful, bind errors to error variables
			 		                $scope.errorMessage = data.message;
			 		            	$scope.setRecaptchaResponse(null);
			 		            	$scope.setRecaptchaWidgetId(null);
			       	                // Call our hack to refresh the reCaptcha
			       	                CommonService.resetRecaptcha($('#recaptchaId iframe'));			       	                
			 		            } else {
			 		            	// if successful, bind success message to message
			 		                $scope.message = data.message;
			 		                $scope.regformData= {};
			 		                $scope.displayName = data.displayName;
			 		            }
			 		        });
		            	}
			 	}).error(function (data, status, headers, config) {
			 		console.info("Error, status:" + status);
			 	}); 
		 }  
	 };
	 
  }]);

 
