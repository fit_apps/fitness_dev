'use strict';

/* Password reset Module, Controller and directives */

var resetPasswordModule = angular.module('fitApp.resetPasswordModule', ['fitApp.commonService', 'vcRecaptcha']);


/* Create a custom directive to identify the button clicked  */
resetPasswordModule.directive('buttonname', function() {
    return {
    restrict: "A",						//Restrict directive to only attributes
    link: function(scope, element, attributes) {
        	element.bind("click", function(){
            scope.buttons.chosen = attributes.buttonname;
          });           
        }
    }
});

resetPasswordModule.controller('resetPasswordController', ['$scope','$window', '$http', 'CommonService', 'vcRecaptchaService',
                                                           function($scope, $window, $http, CommonService, vcRecaptchaService) {
     $scope.submitted = false;
     $scope.busy = false;
     $scope.contextPath = "";
     $scope.resetFormData = {};
     $scope.errorMessage = "";
     $scope.recaptchaData = {};
     
     $scope.buttons = { chosen: "" };		//Used by the custom directive

     $scope.init = function(contextPath) {
    	 $scope.contextPath = contextPath;
    	 CommonService.getRecaptchaPublicKey($scope.contextPath).success(function (data) {
    		$scope.recaptchaData.key = data;
			console.log("$scope.recaptchaData.key:" + $scope.recaptchaData.key);					
			}).error(function (data, status, headers, config) {
				console.log("--err with getting all recaptcha public key:" + status);
			});
     };

 	$scope.setRecaptchaResponse = function (response) {
		$scope.recaptchaData.captchaIncomplete = false;
		$scope.recaptchaData.response = response;
	};
	$scope.setRecaptchaWidgetId = function (widgetId) {
		$scope.recaptchaData.captchaIncomplete = false;
		$scope.recaptchaData.widgetId = widgetId;
	};	
     
     $scope.processResetForm = function(isValid ){
    	 if($scope.buttons.chosen == "cancelReset")  {
    		 $window.location.href = $scope.contextPath + '/pages/login.jsp';  
    		 return;
    	 }
  
    	 $scope.submitted = true;
		 if (CommonService.isEmpty($scope.recaptchaData.response)) {
			 $scope.recaptchaData.captchaIncomplete = true;
			 return;
		 } 
    	 if(isValid){
        	 $scope.busy = true;    
           	 CommonService.validateReCaptchaResponse($scope.contextPath, $scope.recaptchaData.response).
        	 	success(function (data) {
		            $scope.regSuccess = data.success=='true'?true:false;
		            if (!$scope.regSuccess) {
		            	$scope.busy = false;
		            	$scope.errorMessage = data.messageKey
		                console.log("Captcha validation failure, result: " + data);
		                console.log("Success: " + data.success);
		                console.log("MessageKey: " + data.messageKey);
		                console.log("Code: " + data.code);
		            }
		            else {
		        		//Initiate the password reset
			       		 $http({
			       		        method  : 'POST',
			       		        url     : $scope.contextPath + '/rest/resetpassword.do',
			       		        data    : $scope.resetFormData
			       		    })
			       		    .success(function(data) {
			       	            console.log(data);
			       	            $scope.resetSuccess = data.success;
			       	            $scope.busy = false;
			       	            if (!data.success) {
			       	            	// if not successful, bind errors to error variables
			       	                $scope.errorMessage = data.message;
			 		            	$scope.setRecaptchaResponse(null);
			 		            	$scope.setRecaptchaWidgetId(null);
			       	                // Call our hack to refresh the reCaptcha
			       	                CommonService.resetRecaptcha($('#recaptchaId iframe'));
			       	            } else {
			       	            	// if successful, bind success message to message
			       	                $scope.message = data.message;
			       	                $scope.resetFormData= {};		 
			       	            }
			       	        });
		            	}
					}).error(function (data, status, headers, config) {
						console.info("Error, status:" + status);
					});
    	 } else {
    	 }
     }; 
     
  }]);
