'use strict';

/* Login Module and Controller */

var loginModule = angular.module('fitApp.loginModule', []);

loginModule.controller('loginController', ['$scope','$window', '$http', function($scope, $window, $http) {
     $scope.loginPageData = {};
     $scope.loginPageSubmitted = false;

    
     
     //on a login page
     $scope.processLoginPage = function(isValid, contextPath){
    	// var pass = $('#j_password')[0].value;
    	// $scope.loginPageData.j_password = pass;
    	//var err = $scope.error;
    	
    	 $scope.loginPageSubmitted = true;
    	 var xsrf = $.param($scope.loginPageData);
    	 if(isValid) {
    		 $http({
    			    method: 'POST', 
    			    url: contextPath + '/j_spring_security_check',
    			    data: xsrf,
    			    headers: {'Content-Type': 'application/x-www-form-urlencoded'}

    			}).success(function (login) {
    	            $scope.loginSuccess = login.success;
    	            if (!login.success) {
    	                $scope.errorMessage = login.message;
    	            } else {
    	            	if (login.redirect) {   	            		
    	     			   //$window.location.href = contextPath + login.redirectURL;    	            		
    	     			   $window.location.href = login.redirectURL;
    	            	}		 
    	            }    				
    			}).error(function(login) {
    				console.log("j_spring_security_check failure, Success: " + success + " redirect: " + redirect + " redirectURL: " + redirectURL + " message: " + message);
    				
    				
    				
    				
    			});
    	 }
     };
  }]);

 
