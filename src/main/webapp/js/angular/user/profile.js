'use strict';

/* Activities Module and Controller */

var profileModule = angular.module('fitApp.profileModule', ['fitApp.myActivitiesService', 'fitApp.profileService', 
                                                            'fitApp.diagService','ui.date', 'fitApp.commonService']);

profileModule.directive('buttonname', function() {
    return {
    restrict: "A",						//Restrict directive to only attributes
    link: function(scope, element, attributes) {
        	element.bind("click", function(){
            scope.buttons.chosen = attributes.buttonname;
          });           
        }
    };
});


profileModule.controller('profileController', ['$scope', '$http', '$window', '$timeout', '$q', 'ProfileService', 'MyActivitiesService', 'DiagnosisService',
                                               'CommonService', function($scope, $http, $window, $timeout, $q,
                                            		        ProfileService, MyActivitiesService, 
                                            		        DiagnosisService, CommonService) {
 
	$scope.buttons = { chosen: "" };		//Used by the custom directive
	$scope.submittedEditProfileForm = false;
	$scope.passwordErr = {};
	
	$scope.init = function(currentUserId, contextPath, viewUserId) {						 
		resetAllVars();
		$scope.viewUserId = '';
		$scope.contextPath = contextPath;
		
		if(viewUserId != null &&  viewUserId.length>0 ) {
			$scope.viewUserId = viewUserId;
		} else {
			$scope.viewUserId = currentUserId;
		}
		
		$scope.dateOptions = {
			     showOn: "both",
			      buttonImage: contextPath + '/images/icon_calendar.png',
			      buttonImageOnly: true,
			      dateFormat: "mm/dd/yy"
		    };
		 
	};
	
	$scope.initViewProfile = function(currentUserId, contextPath, viewUserId) {
		//profileEditId
		$scope.profileEditSuccess= null;
		CommonService.getMessage($scope.contextPath, "profileEditId").
		success(function (data) {
			if (data.length == 0) {
				
			} else {
				$scope.profileEditSuccess = true;
				if($scope.profileEditSuccess) {
					CommonService. deleteMessage($scope.contextPath, "profileEditId");
				}
			}
		}).error(function (data, status, headers, config) {
			console.log("Error encountered trying to determine if profile has been previously saved:" + status);
		});

		
 		if(viewUserId != null && viewUserId.length > 0) {
			ProfileService.getUserViewData($scope.contextPath, viewUserId).success(function(data){
				getUserData(data);
			})
			.error(function(data, status, headers, config) {
				//TODO do something nice here
			     alert("AJAX failed!");
			});
			 
		} else {
			ProfileService.getUserViewData($scope.contextPath, currentUserId)
			.success(function(data){
				getUserData(data, false);
			})
			.error(function(data, status, headers, config) {
				//TODO do something nice here
			     alert("AJAX failed!");
			});
		}
	};
	
	$scope.initEditProfile = function(userId, contextPath, viewUserId) {
		
		DiagnosisService.getAllDiagnosisHierarchy($scope.contextPath).success(function(data) {
			$scope.diagHierarchy = data;
			//need to select a diag to match first one in a scope
		}).error(function(data, status, headers, config){
			console.log("--err with getting diagnosis hierarhy status:" + status);
		});
		var responsePromise = ProfileService.getUserEditData($scope.contextPath, userId);
		
		responsePromise.success(function(data){
			$timeout(function() {
				$("#diagDetailsContainer").hide();
				getUserData(data, true);
				
			}, 100); 
			
		});
		
		responsePromise.error(function(data, status, headers, config) {
			//TODO do something nice here
		     alert("AJAX failed!");
		});
		
		//get all countries
		CommonService.getAllCountries($scope.contextPath).success(function(data) {
			$scope.allCountries = data;
		}).error(function(data, status, headers, config){
			console.log("--err with getting all countries:" + status);
		});

		CommonService.getAllStateProvinces($scope.contextPath).success(function(data) {
			$scope.allStateProvinces = data;
		}).error(function(data, status, headers, config){
			console.log("--err with getting all state/provinces:" + status);
		});
		 
	};
	
	 $scope.showViewActivity = function(activityId) {
		 $window.location.href =  $scope.contextPath + "/pages/secure/dashboard/viewActivity.jsp?actId=" + activityId;
	 };
	
	$scope.formatDate = function(longValue) {
		return new Date(longValue).format("yyyy-mm-dd");
	};

		$scope.isSameDiag = function(currentIndex) {
			if (currentIndex == 0) {
				return false;
			}
			if ($scope.userData.allDiagData[currentIndex].userDiagId != $scope.userData.allDiagData[currentIndex - 1].userDiagId) {
				return false;
			} else {
				return true;
			}
		};

		$scope.showSecondDiagTitle = function(currentIndex) {
			if (currentIndex > 0
					&& $scope.userData.allDiagData[currentIndex - 1].userDiagIsPrimary) {
				return true;
			} else {
				return false;
			}
		};

		$scope.showTreatment = function(currentIndex) {

			if (currentIndex > 0
					&& $scope.userData.allDiagData[currentIndex].treatmId != null
					&& $scope.userData.allDiagData[currentIndex].treatmId != $scope.userData.allDiagData[currentIndex - 1].treatmId) {
				return true;
			} else if (currentIndex == 0
					&& $scope.userData.allDiagData[currentIndex].treatmId != null) {
				return true;
			} else {
				return false;
			}
		};

		$scope.showProcedure = function(currentIndex) {

			if (currentIndex > 0
					&& $scope.userData.allDiagData[currentIndex].procId != null
					&& $scope.userData.allDiagData[currentIndex].procId != $scope.userData.allDiagData[currentIndex - 1].procId) {
				return true;
			} else if (currentIndex == 0
					&& $scope.userData.allDiagData[currentIndex].procId != null) {
				return true;
			} else {
				return false;
			}
		};

		$scope.showProcedureTitle = function(currentIndex) {
			// new diagnosis then show Procedures title
			if (currentIndex > 0
					&& $scope.userData.allDiagData[currentIndex - 1].userDiagId != $scope.userData.allDiagData[currentIndex].userDiagId) {
				return true;
			} else if (currentIndex == 0
					&& $scope.userData.allDiagData[currentIndex].procId != null) {
				return true;
			} else {
				return false;
			}
		};

		$scope.showProcedureEndLine = function(currentIndex) {

			if (currentIndex + 1 < $scope.userData.allDiagData.length
					&& $scope.userData.allDiagData[currentIndex + 1].userDiagId != $scope.userData.allDiagData[currentIndex].userDiagId) {
				return true;
			} else {
				return false;
			}
		};
		
		$scope.selectedAddress = function(selectedRadioValue) {
			if(selectedRadioValue == 'location') {
				//disable all zipCodeContainer children
				$("#locationContainer").find(":input").attr("disabled", false);
				$scope.userProfData.zipCode="";
				$("#zipCodeContainer").find(":input").attr("disabled", true);
			} else if(selectedRadioValue == 'zipCode') {
				$scope.userProfData.address1="";
				$scope.userProfData.address2="";
				$scope.userProfData.city="";
				$("#locationContainer").find(":input").attr("disabled", true);
				$("#zipCodeContainer").find(":input").attr("disabled", false);
			}
		};

		function resetAllVars() {
			$scope.contextPath = "";
			$scope.viewUserId = '';
			$scope.submittedEditProfileForm = false;
			$scope.passwordErr = {};
			$scope.profileEditSuccess= "";
		}

		function getUserData(data, isEditProfile) {
			$("#diagDetailsContainer").hide();
			$scope.userData = data;

			// loop through data, create different structure
			// for profile and procedures
			if (isEditProfile) {
				var allDiagData = data.allDiagData;
				$scope.userEditData = [];
				$scope.chemoFreqAll = data.allChemoFreqs;
				$scope.cancerProcTypesAll = data.allCancerProcedureTypes;
				
				// add profile data
				$scope.userProfData = data.profileData;
				
				//add country
				$scope.userProfData.countrySelected = "";
				//add state
				//add country
				$scope.userProfData.stateSelected = "";
				//make location or zip code radio button checked
				if($scope.userProfData.zipCode != null && $scope.userProfData.zipCode.trim().length > 0) {
					$scope.userProfData.addressRadio = "zipCode";
					$scope.selectedAddress("zipCode"); 
				} else {
					$scope.userProfData.addressRadio = "location";
					$scope.selectedAddress("location"); 
				}

				var diagObj = {};
				var procArray = [];
				for (var i = 0; i < allDiagData.length; i++) {

					// same userDiagId so add user diag data
					// once
					if (i == 0
							|| allDiagData[i].userDiagId != allDiagData[i - 1].userDiagId) {
						diagObj = {};

						diagObj.userDiagId = allDiagData[i].userDiagId;
						diagObj.userDiagIsPrimary = allDiagData[i].userDiagIsPrimary;
						diagObj.userId = allDiagData[i].userId;
						diagObj.userDiagStartDt = allDiagData[i].userDiagStartDt;
						diagObj.userDiagEndDt = allDiagData[i].userDiagEndDt;
						diagObj.chemoTotal = allDiagData[i].chemoTotal;
						diagObj.chemoCompleted = allDiagData[i].chemoCompleted;
						diagObj.chemFreq = allDiagData[i].chemFreq;
						diagObj.chemFreqKey = allDiagData[i].chemoFreqKey;
						diagObj.chemoFreqId = allDiagData[i].chemoFreqId;
						diagObj.radTotal = allDiagData[i].radTotal;
						diagObj.radCompleted = allDiagData[i].radCompleted;
						diagObj.treatmId = allDiagData[i].treatmId;
						diagObj.refDiagTypeName = allDiagData[i].refDiagTypeName;
						diagObj.refDiagKey = allDiagData[i].refDiagKey;
						diagObj.refDiagTypeId = allDiagData[i].refDiagTypeId;

						procArray = [];
						var procObj = {};
						if(allDiagData[i].procId != null) {
							procObj.procId = allDiagData[i].procId;
							procObj.procDate = (allDiagData[i].procDate != null)?new Date(allDiagData[i].procDate):'';
							procObj.procName = allDiagData[i].procName;
							procArray.push(procObj);
						}
						diagObj.procArr = procArray;

						$scope.userEditData.push(diagObj);
					} else if (allDiagData[i].userDiagId == allDiagData[i - 1].userDiagId) {
						var procObj = {};
						procObj.procId = allDiagData[i].procId;
						procObj.procDate = (allDiagData[i].procDate != null)?new Date(allDiagData[i].procDate):'';
						procObj.procName = allDiagData[i].procName;

						procArray.push(procObj);
						diagObj.procArr = procArray;
					}
				}
				if($scope.userEditData[0] != undefined && $scope.userEditData[0] != null) {
					$scope.selectedDiagnosis = $scope.userEditData[0];
					$("#diagDetailsContainer").show();
					$scope.changedDiagnosis = $scope.selectedDiagnosis.refDiagTypeId;
					
					$scope.chemoFreqSelected =$scope.selectedDiagnosis.chemoFreqId;
					
					$scope.cancerProcTypeSelected = {
						id : $scope.selectedDiagnosis.procId
					};
				} else {
					$scope.selectedDiagnosis = null;
				}
			}
		}
		
		//delete procedure when clicked on trash icon
		$scope.deleteProcedure = function(index) {
			$scope.selectedDiagnosis.procArr.splice(index,1);
		};
		
		
		//delete procedure when clicked on trash icon
		$scope.addProcedure = function(index) {
			var procObj = {};

			procObj.procId = '';
			procObj.procDate = '';
			procObj.procName = '';
			$scope.selectedDiagnosis.procArr.push(procObj);
		};
		
		//add new diagnosis
		$scope.addDiagnosis = function() {
			 
  
			$("#deletedDiagConfirmContainer").hide();
			$("#diagDetailsContainer").show();
			
			//add new diag and make that selected, don't make it primary, let user choose a prim. diag checkbox
			var diagObj = {};

			diagObj.userDiagId = '';
			diagObj.userDiagIsPrimary = '';
			diagObj.userId = '';
			diagObj.userDiagStartDt = '';
			diagObj.userDiagEndDt = '';
			diagObj.chemoTotal = '';
			diagObj.chemoCompleted = '';
			diagObj.chemFreq = '';
			diagObj.chemFreqKey = '';
			diagObj.radTotal = '';
			diagObj.radCompleted = '';
			diagObj.treatmId = '';
			diagObj.refDiagTypeName = 'New diagnosis';
			diagObj.refDiagKey = 'new';
			diagObj.refDiagTypeId='-1';
			diagObj.chemoFreqId='';

			var procArray = [];
			diagObj.procArr = procArray;
			$scope.userEditData.push(diagObj);
			//$scope.selectedDiagnosis.refDiagTypeName = '--New Diagnosis--';
			$scope.selectedDiagnosis = diagObj;
			$scope.changedDiagnosis =  $scope.selectedDiagnosis.refDiagTypeId;
		};
		
		//delete selected diagnosis when Delete icon or link is clicked
		$scope.deleteDiagnosis = function() {
			var diagObj = $scope.selectedDiagnosis;
			if(diagObj == null) {
				return;
			}
			var selectedDiagRemoved = false;
			//remove selected diagnosis from scope
			for(var i=0; i<$scope.userEditData.length; i++) {
					if(diagObj.userDiagId == $scope.userEditData[i].userDiagId) {
						$scope.deletedDiagName = diagObj.refDiagTypeName;
						selectedDiagRemoved = true;
						$scope.userEditData.splice(i,1);
						
						if($scope.userEditData.length > 0) {
							$scope.selectedDiagnosis = $scope.userEditData[0];
							$scope.loadDifferentDiagnosis($scope.selectedDiagnosis);
						} else {
							$scope.selectedDiagnosis = null;
						}
						break;
					}
			}
			
			if(diagObj.userDiagId != undefined && diagObj.userDiagId != null && diagObj.userDiagId != "" ) { 
				//delete from back-end if it's existing diagnosis
				 $http({
				        method  : 'POST',
				        url     : $scope.contextPath + '/rest/profile/deleteDiagnosis.do',
				        data    : diagObj
				    })
				    .success(function(data) {
			            console.log(data);
			            
			            if(data.success != null && data.success != undefined) {
			            	//redirect to a new page, send a message with success code
			            	CommonService.putMessage($scope.contextPath, "profileEditId", "profile.saved.success").
							success(function (data) {
								//show delete success message
								$("#deletedDiagConfirmContainer").show();
								$("#diagDetailsContainer").hide();
							}).
							error(function (data, status, headers, config) {
								console.log("--err profile.js, unable to delete diagnosis:" + status);
							});
			            }
			             
			        });
			} else {
				//show delete success message when it was new diag and back-end did not need to be deleted
				if(selectedDiagRemoved) {
					$("#deletedDiagConfirmContainer").show();
				}
				//hide diagn details if there are no more diag to display
				if($scope.userEditData.length == 0) {
					$("#diagDetailsContainer").hide();
				}
			}
			
		};
		
		$scope.handleDeleteDiagnosisDialog = function(
				clickAction) {
			var dialElem = $('#deleteDiagnosisDialog');
			if (clickAction == 'cancel') {
				dialElem.dialog('close');
			} else if (clickAction == 'ok') {
				$scope.deleteDiagnosis();
				dialElem.dialog('close');
			}
		};

		$scope.showDeleteDiagnosisDialog = function(tagId) {
			var dialElem = $('#deleteDiagnosisDialog');
			dialElem.dialog('open');
		};

		$scope.loadDifferentDiagnosis = function(selectedDiag) {
			$("#deletedDiagConfirmContainer").hide();
			$("#diagDetailsContainer").show();
			$scope.selectedDiagnosis = selectedDiag;
			$scope.changedDiagnosis =  selectedDiag.refDiagTypeId; //$scope.selectedDiagnosis.refDiagTypeId;
		};
		
		$scope.changeDiagnosis = function(changedDiagnosis){
			$scope.changedDiagnosis = changedDiagnosis;
		};

		$scope.isValidDiagDate = function(selectedDiag) {
			$scope.diagDate = {};
			$scope.diagDate.valid = true;
			if(selectedDiag == null) {
				return true;
			}
			var startDate = $scope.selectedDiagnosis.userDiagStartDt;
			var endDate = $scope.selectedDiagnosis.userDiagEndDt;
			
			var startDt = (startDate == null || startDate.trim().length == 0)?null:new Date(startDate);
			var endDt = (endDate == null || endDate.trim().length == 0)?null:new Date(endDate);
			
			//check if it's valid dates
			if(!CommonService.isValidDate(startDate)){
				$scope.diagDate.fromDateInvalid = true;
				return;
			}
			if(!CommonService.isValidDate(endDate)) {
				$scope.diagDate.toDateInvalid = true;
				return;
			}
			
			
			$scope.diagDate = CommonService.areDatesValid(startDt, endDt);
		};
		
		//check if whatever us entered is a valid date
		$scope.isValidCancerSurvDate =  function(date) {
			$scope.cancSurvDt = {};
			if(CommonService.isValidDate(date)) {
				$scope.cancSurvDt.isValid = true;
			} else {
				$scope.cancSurvDt.isValid = false;
			}
		};
		
		$scope.submitEditProfile = function(isValid) {
			$scope.passwordErr = {};
			$scope.allFieldsValid = false;
			var dataToSubmit = $scope.userProfData;
			var isValid = false;
			
			if($scope.buttons.chosen == "editProfileCancel")  {
	    		 $window.location.href = $scope.contextPath + "/pages/secure/dashboard/profile.jsp";  
	    		 return;
	        }
			$scope.submittedEditProfileForm = true;
			
			var validCurrPass = true;
			 var deferred = $q.defer();
			 
			//check passwords, cur pass is not empty
			if(!CommonService.isEmpty($scope.userProfData.currentPass)) {
				//check db pass
				 
				var passwordData = {};
				passwordData.salt = $scope.userProfData.salt;
				passwordData.currentPassword = $scope.userProfData.currentPass;

				
				//verifyUserPass, pass userId, currentPass
	    		 var passVerfPromise = $http({
	    		        method  : 'POST',
	    		        url     : $scope.contextPath + '/rest/profile/verifyCurrentUserPass.do',
	    		        data    : passwordData
	    		    });
    		    passVerfPromise.success(function(data) {
    		 
	    		    	validCurrPass = data.isValidPass;
	    	            if(!validCurrPass) {
	    	            	$scope.passwordErr.validCurrPass = validCurrPass;
	    	            	return;
	    	            }
	    	            // see if new pass is empty
	    				if($scope.userProfData.newPass == undefined ) {
	    					$scope.passwordErr.passwordLengthErr = true;
	    					$scope.passwordErr.hasRuleError = true;
	    				} else {
	    					//validate new and confirmed ones
	    					$scope.passwordErr = CommonService.validatePassword($scope.userProfData.newPass, $scope.userProfData.confirmNewPass);
	    					$scope.passwordErr.newPassValid = $scope.passwordErr.isValidPass;
	    					
	    					//if new and confirmed passes are ok, not make sure it's different from current
	    					if($scope.passwordErr.newPassValid && validCurrPass  && 
	    							$scope.userProfData.currentPass.trim().toLowerCase() == $scope.userProfData.newPass.trim().toLowerCase()) {
	    						$scope.passwordErr.sameAsCurrent = true;
	    					}
	    				}	
	    				$scope.passwordErr.validCurrPass = validCurrPass;
	    				$scope.allPasswordsValid = $scope.passwordErr.validCurrPass && !$scope.passwordErr.hasRuleError &&
								  !$scope.passwordErr.passwordLengthErr && $scope.passwordErr.newPassValid && 
								  !$scope.passwordErr.sameAsCurrent;
	    				//no need to check the rest if passwords are not valid
	    				if(!$scope.allPasswordsValid ) {
	    					return;
	    				}
	    				validateAndSubmit();
    		    });
				
			}//empty current pass 
			else {
				//but new pass or conf pass is not empty
				if(!CommonService.isEmpty($scope.userProfData.newPass) || !CommonService.isEmpty($scope.userProfData.confirmNewPass)) {
					$scope.passwordErr.emptyCurPass = true;
				} else {
					//no errors if all fields are empty
					$scope.passwordErr.validCurrPass = true;
				}
				//either all empty or (not empty current, new complies with rules and new ones match)
				$scope.allPasswordsValid = $scope.passwordErr.validCurrPass;
				if(!$scope.allPasswordsValid ) {
					return;
				}
				validateAndSubmit();
			}
		};
		
		function validateAndSubmit() {
			//check dates
			$scope.isValidCancerSurvDate($scope.userProfData.healthyDate);
		    $scope.isValidDiagDate($scope.selectedDiagnosis);
			
			$scope.allFieldsValid = $scope.allPasswordsValid && $scope.diagDate.valid && $scope.cancSurvDt.isValid  ;
									   
			if($scope.selectedDiagnosis != null && $scope.changedDiagnosis != null && $scope.changedDiagnosis != undefined ) {
				$scope.allFieldsValid = $scope.allFieldsValid && $scope.changedDiagnosis>-1;
			}
			//submit now, what I need to submit
			var submitUserProfData = {};
			submitUserProfData.userData = $scope.userProfData;
			submitUserProfData.selectedDiag = $scope.selectedDiagnosis;
			
			if($scope.selectedDiagnosis != null) {
				$scope.selectedDiagnosis.chemoFreqId = $scope.chemoFreqSelected;
				$scope.selectedDiagnosis.changedDiagTypeId=$scope.changedDiagnosis;
			}
				
			console.log("userProfData:" );
			console.log(submitUserProfData);
			
			//submit
			//post to rest service
			if($scope.allFieldsValid) {
				 $http({
				        method  : 'POST',
				        url     : $scope.contextPath + '/rest/profile/saveUserProfile.do',
				        data    : submitUserProfData
				    })
				    .success(function(data) {
			            console.log(data);
			            
			            if(data.success != null && data.success != undefined) {
			            	//redirect to a new page, send a message with success code
			            	CommonService.putMessage($scope.contextPath, "profileEditId", "profile.saved.success").
							success(function (data) {
								$window.location.href = $scope.contextPath + "/pages/secure/dashboard/profile.jsp";
							}).
							error(function (data, status, headers, config) {
								console.log("--err profile.js, unable to send profile save success message:" + status);
							});
			            }
			        });
		   };
		}
		
		//need to manually capture spin inputs for treatments
		$timeout(function () {
	        $("input[id^='spin_profile']").spinner({
	        	 
	            spin: function (event, ui) {
	            	//$scope.profile.chemoCompleted =  ui.value;
	            	var evalName =  event.target.attributes["ng-model"].nodeValue;
	            	
	            	if(evalName == "selectedDiagnosis.chemoCompleted") {
	            		$scope.selectedDiagnosis.chemoCompleted = ui.value;
	            	}
	            	else if(evalName == "selectedDiagnosis.chemoTotal") {
	            		$scope.selectedDiagnosis.chemoTotal = ui.value;
	            	}
	            	else if(evalName == "selectedDiagnosis.radCompleted"){
	            		$scope.selectedDiagnosis.radCompleted = ui.value;
	            	}
	            	else if(evalName == "selectedDiagnosis.radTotal") {
	            		$scope.selectedDiagnosis.radTotal = ui.value;
	            	}
	            },
	            change: function (event, ui) {
	            	var id = event.target.attributes["id"].nodeValue;
	            	var evalName = event.target.attributes["ng-model"].nodeValue;
	            	
	            	if(evalName == "profile.chemoCompleted") {
	            		$scope.profile.chemoCompleted =  $("#" + id + "")[0].value;
	            	}
	            	else if(evalName == "profile.chemoTotal") {
	            		$scope.profile.chemoTotal =  $("#" + id + "")[0].value;
	            	}
	            	else if(evalName == "profile.radCompleted"){
	            		$scope.profile.radCompleted =  $("#" + id + "")[0].value;
	            	}
	            	else if(evalName == "profile.radTotal") {
	            		$scope.profile.radTotal =  $("#" + id + "")[0].value;
	            	}
	            }
	        }, 0);
	        
	        
	    });
		
		 
		
		//called on passwords type, so that previous errors don't show up
		$scope.resetSubmitted = function(){
			 $scope.submittedEditProfileForm = false;
		};
		 
		
		$scope.taggedByMe = function() {
			$('#taggedTable')
					.dataTable(
							{
								"bJQueryUI" : true,
								"bProcessing" : true,
								"bserverSide" : true,
								"sAjaxSource" : $scope.contextPath
										+ '/rest/user/userTags/'+$scope.viewUserId+'.do',
								"sAjaxDataProp" : "tagData",
								"bRetrieve" : true,
								"bAutoWidth" : false,
								"fnRowCallback" : function(
										nRow, tagData,
										iDisplayIndex,
										iDisplayIndexFull) {
									$(nRow).attr("id",
											tagData[0]);
									$(nRow).attr("userId",
											tagData[1]);
									return nRow;
								},
								"aoColumnDefs" : [
										{
											"bSearchable" : false,
											"bVisible" : false,
											"aTargets" : [ 0 ]
										},

										{
											"bSearchable" : false,
											"bVisible" : false,
											"aTargets" : [ 1 ]
										},
										{
											"aTargets" : [ 2 ],
											"bSortable" : false,
											"mRender" : function(
													url,
													type,
													full) {
												return '<a href="#" name="profile" id="profile">'
														+ url
														+ '</a>';
											}
										},
										{
											"aTargets" : [ 6 ],
											"bSortable" : false,
											"mRender" : function(
													url,
													type,
													full) {
												return '<a href="#" name="untag" id="untag">'
														+ url
														+ '</a>';
											}
										} ]
							});
		};
		
		$scope.untagUser = function(tagId) {
			$http(
					{
						method : 'GET',
						url : $scope.contextPath
								+ '/rest/user/untagUser/'
								+ tagId +'/'+$scope.viewUserId+ '.do'
					}).success(function(data) {
				$('#taggedTable').dataTable().fnDestroy();
				$scope.taggedByMe();

			})
					.error(
							function(data, status, headers,
									config) {
								// TODO do something nice
								// here
								// alert("AJAX failed!");
							});

		};

		$scope.handleUntagUserDialog = function(
				clickAction, tagId) {
			var dialElem = $('#untagUserDialog');
			if (clickAction == 'cancel') {
				dialElem.dialog('close');
				$scope.tagId = "";
			} else if (clickAction == 'ok') {
				$scope.untagUser($scope.tagId);
				dialElem.dialog('close');
				$scope.tagId = "";
			}
		};

		$scope.showUntagUserDialog = function(tagId) {
			var dialElem = $('#untagUserDialog');
			$scope.tagId = tagId;
			// $scope.activWithCommentToDelete = activity;
			dialElem.dialog('open');
		};

		$scope.showUserProfile = function(userId) {
			$window.location.href = $scope.contextPath
					+ "/pages/secure/dashboard/profile.jsp?viewUserId="
					+ userId;
		};

		$scope.getMyActivities = function() {
				$('#activityDetail').hide();
				MyActivitiesService
						.getMyActivitiesTotal(
											$scope.contextPath, $scope.viewUserId)
						.success(function(data) {
							$scope.myActivitiesTotal = data;
						})
						.error(
								function(data, status, headers,
										config) {
									console
											.log("--err with getting Total of MyActivities status:"
													+ status);
								});

				// List of Activities
				$('#activitiesTable')
						.dataTable(
								{
									"bJQueryUI" : true,
									"bProcessing" : true,
									"bserverSide" : true,
									"sAjaxSource" : $scope.contextPath
 
															+ "/rest/user/activities/"+$scope.viewUserId+".do",
									"sAjaxDataProp" : "myData",
									"bRetrieve" : true,
									"fnRowCallback" : function(
											nRow, myData,
											iDisplayIndex,
											iDisplayIndexFull) {
										$(nRow).attr("id",
												myData[0]);
										return nRow;
									},
									"aoColumnDefs" : [
											{
												"bSearchable" : false,
												"bVisible" : false,
												"aTargets" : [ 0 ]
											},
											{
												"aTargets" : [ 6 ],
												"bSortable" : false,
												"mRender" : function(
														url,
														type,
														full) {
													return '<a href="#" name="activityTab" id="activityTab" title="Activity Detail">'
															+ url
															+ '</a>';
												}
											} ]
								});
			};

		} ]);
