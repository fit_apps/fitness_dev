'use strict';

/* Profile Service  */
 
var profileService = angular.module('fitApp.profileService', []);

profileService.factory("ProfileService", function($http) {
	return {
		getUserViewData: function(contextPath, userId){
			return $http.get(contextPath + '/rest/profile/viewUserProfileData/' + userId + '.do' );
		},
		getUserEditData:function(contextPath, userId) {
			return $http.get(contextPath + '/rest/profile/editUserProfileData/' + userId + '.do');
		}
	};
});
