'use strict';

var activityTypeService = angular.module('fitApp.activityTypeService', []);

diagService.factory("activityTypeService", function($http) {
	return {
	   getAllActTypes : function(contextPath) {
		   return $http.get(contextPath + "/rest/activityTypes/all.do");
	   } 
	 };
	
});