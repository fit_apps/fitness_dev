'use strict';

/* editActivity Module and Controller */

var editActivityModule = angular.module('fitApp.editActivityModule', ['fitApp.commonService', 'fitApp.myActivitiesService']);

var TIME_REGEX_3_PART_12_MAX_HOUR = "^(0?[0-9]|1[012])(:([0-5]?[0-9]))?(:([0-5]?[0-9]))?$";
var TIME_REGEX_3_PART_23_MAX_HOUR = "^([0-1]?[0-9]|2[0-3])(:([0-5]?[0-9]))?(:([0-5]?[0-9]))?$";

var durationHours = -1;
var durationMinutes = -1;
var durationSeconds = -1;

/* Create a custom directive to identify the button clicked  */
editActivityModule.directive('buttonname', function() {
    return {
    restrict: "A",						//Restrict directive to only attributes
    link: function(scope, element, attributes) {
        	element.bind("click", function(){
            scope.buttons.chosen = attributes.buttonname;
          });           
        }
    }
});

//A directive to transform an XML attribute into a $scope variable
editActivityModule.directive('scopeAdd', function ($filter) {
	  return {
		  	restrict: "E",	
		    link: function (scope, element, attrs) {
		    	scope[attrs.key] = attrs.value ;
		    }
		  };
		});

editActivityModule.service('editActivityService', function($http) {
	return {
		getActivityDisplayCommand: function(contextPath, activityId){
			return	$http({
						method: 'GET',
						url: contextPath + '/rest/activity/view/' + activityId + '.do'
					});
		} 
	};
});


editActivityModule.controller('editActivityController', ['$scope', '$window', '$http', 'CommonService', 'editActivityService', 'MyActivitiesService',
                                                     function($scope, $window, $http, CommonService, editActivityService, MyActivitiesService) {
	
	$scope.$watch('create', function (newVal, oldVal) {
		console.log("create has changed: ", newVal, oldVal, $scope.userId, $scope.contextPath);
		if (!newVal) return;
		if (newVal != null && newVal == 'true') {
			$scope.createMode = true;
			$scope.editMode = false;
		} else {
			$scope.createMode = false;
			$scope.editMode = true;
		}
		delayedInit();
	})
		
	
	function resetAllVars() {
		
		$scope.allCountries = [];
		$scope.allStateProvinces = [];
		$scope.allActivityTypes = [];
		
	    $scope.buttons = { chosen: "" };		//Used by the custom directive
	    $scope.activityFormData = {};
	    $scope.energyBefore = {};
	    $scope.energyBefore.rating = -1;
	    $scope.energyBefore.text = "";
	    $scope.energyAfter = {};
	    $scope.energyAfter.rating = -1;
	    
	    	    
	    // Prefill activity date to today
	    var currentDate = new Date()
	    $scope.activityFormData.activityDate = pad(currentDate.getMonth() + 1, 2, '0') + '/' + 
	    	pad(currentDate.getDate(), 2, '0') + '/' + pad(currentDate.getFullYear(), 2, '0');
	    
	    $scope.showHidePublic="Show"
	    	
	    $scope.messageToShow = "";	    	
	    $scope.dataValidationErr = Object.create;
	    $scope.formSubmittable = true;
		
		$scope.lastZipEntered = null;
		$scope.lastCityEntered = null;
		$scope.lastStateEntered = null;
		$scope.lastCountryEntered = null;	
		};		
	
	/*
	 * init - Set up the page
	 */
	function delayedInit() {
		resetAllVars();

		CommonService.getAllEnergyIcons($scope.contextPath, $scope);
		
		CommonService.getAllCountries($scope.contextPath).success(function(data) {
			$scope.allCountries = data;
		}).error(function(data, status, headers, config){
			console.log("--err with getting all countries:" + status);
		});

		CommonService.getAllStateProvinces($scope.contextPath).success(function(data) {
			$scope.allStateProvinces = data;
		}).error(function(data, status, headers, config){
			console.log("--err with getting all state/provinces:" + status);
		});	
		
		CommonService.getAllActivityTypes($scope.contextPath).success(function(data) {
			$scope.allActivityTypes = data;
		}).error(function(data, status, headers, config){
			console.log("--err with getting all activity types:" + status);
		});
		
		getPendedMsg($scope);

		if ($scope.editMode) {
			CommonService.getMessage($scope.contextPath, "activityId").
			success(function(activityId) {
				if (activityId.length == 0) {
					 $scope.formSubmittable = false;
					 //err.activity.id.notFound
					console.log("--err editActivity.js, invalid activity Id retrieved, status:" + status);
				}
				else {	
					var responsePromise = editActivityService.getActivityDisplayCommand($scope.contextPath, activityId);
					responsePromise.success(function(adc){
						$scope.adc = adc;
						$scope.activityFormData.activityId = adc.activityId;
						$scope.activityFormData.activityTitle = adc.activityTitle;
						$scope.activityTypeEntered = adc.activityType;
						if (!CommonService.isEmpty(adc.zip)) {
							$scope.zipOrLocation = 'zip'
							$scope.activityFormData.zip = adc.zip;
						} else {
							$scope.zipOrLocation = 'location'
							$scope.activityFormData.city = adc.city;
							$scope.stateProvinceEntered = adc.stateProvinceId;
							$scope.countryEntered = adc.countryId;							
						}
						if (!CommonService.isEmpty(adc.activityDate)) {
							var activityDate = new Date(adc.activityDate);
							$scope.activityFormData.activityDate  = (activityDate.getMonth()+1) + "/" + 
								activityDate.getDate() + "/" + activityDate.getFullYear();
						}
						if (!CommonService.isEmpty(adc.startTime)) {
							var timeParts = parseTime(adc.startTime, TIME_REGEX_3_PART_23_MAX_HOUR); 
							var hourParts = convertFromMilitaryHours (timeParts.parsedHours);
							$scope.startTimeEntered = pad(hourParts.hour, 2, "0") + ":" + pad(timeParts.parsedMinutes, 2, "0") + ":" + pad(timeParts.parsedSeconds, 2, "0");
							$scope.startTimeAmPm = hourParts.amPeriod ? "AM" : "PM";
						}
						if (!CommonService.isEmpty(adc.duration)) {
							var timeParts = parseTime(adc.duration, TIME_REGEX_3_PART_23_MAX_HOUR); 
							$scope.durationEntered = pad(timeParts.parsedHours, 2, "0") + ":" + pad(timeParts.parsedMinutes, 2, "0") + ":" + pad(timeParts.parsedSeconds, 2, "0");
						}						
						$scope.activityFormData.distance = adc.distance;
						if (!CommonService.isEmpty(adc.energyBeforeRating)) {
							$scope.energyBefore.rating  = adc.energyBeforeRating;
							$scope.energyBefore.text = $scope.getEnergyLevelTitle(adc.energyBeforeRating); 
						}
						if (!CommonService.isEmpty(adc.energyAfterRating)) {
							$scope.energyAfter.rating  = adc.energyAfterRating;
							$scope.energyAfter.text = $scope.getEnergyLevelTitle(adc.energyAfterRating); 
						}
						$scope.activityFormData.symptomsHelped = adc.symptomsHelped;
						$scope.activityFormData.notes = adc.notes;
						
						$scope.showHidePublic = adc.showPublic ? "Show": "Hide";
						
					}).error(function(data, status, headers, config){
			 			$scope.formSubmittable = false;
			 			//err.activity.id.notFound
		   	 			console.log("--err editActivity.js, unable to get activity, status:" + status);
		   	 		});		
				}
	 		}).error(function(data, status, headers, config){
	 			$scope.formSubmittable = false;
	 			//err.activity.id.notFound
   	 			console.log("--err editActivity.js, unable to get activity Id, status:" + status);
   	 		});			
		} else {
		    $scope.zipOrLocation = "zip";
		    $scope.startTimeAmPm = "AM";   

		    $scope.energyAfter.text = "";
		    	    
		    // Prefill activity date to today
		    var currentDate = new Date()
		    $scope.activityFormData.activityDate = pad(currentDate.getMonth() + 1, 2, '0') + '/' + 
		    	pad(currentDate.getDate(), 2, '0') + '/' + pad(currentDate.getFullYear(), 2, '0');
		    
		    $scope.showHidePublic="Show"			
		}
		
		
		
		return;
	};

	
	
	
	//get the Activities History for the Logged In User.
	$scope.getMyActivities = function(){
		$('#activityDetail').hide();
		MyActivitiesService.getMyActivitiesTotal($scope.contextPath, $scope.userId).success(function(data){
	    	$scope.myActivitiesTotal = data;
		 }).error(function(data, status, headers, config){
				console.log("--err with getting Total of MyActivities status:" + status);
		});
		
		//List of Activities
		$('#activitiesTable').dataTable({
            "bJQueryUI": true,
            "bProcessing": true,
            "bserverSide": true,
            "sAjaxSource": $scope.contextPath + "/rest/user/activities/"+$scope.userId+".do",
            "sAjaxDataProp":"myData",
            "bRetrieve": true,
            "fnRowCallback": function(nRow, myData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", myData[0]);
                return nRow;
            },
            "aoColumnDefs":[
            {
            "bSearchable": false,
            "bVisible": false,
            "aTargets": [0]
            },
             { 
            	"aTargets": [ 6 ],
            	"bSortable": false, 
            	"mRender": function ( url, type, full )  {         
            		return  '<a href="#" name="activityTab" id="activityTab" title="Activity Detail">' + url + '</a>';      
            	}   }
             ]
        });
	};
	
	$scope.showViewActivity = function(activityId) {
	    $window.location.href =  $scope.contextPath + "/pages/secure/dashboard/viewActivity.jsp?actId=" + activityId;
	};
	
	/*
	 * Handle the 'energy level before' change 
	 */
	$scope.energyBeforeChange = function(energyLevel) {
		$scope.energyBefore.text = energyLevel.messageText;
	}

	/*
	 * Handle the 'energy level after' change 
	 */
	$scope.energyAfterChange = function(energyLevel) {
		$scope.energyAfter.text = energyLevel.messageText;
	}
	
	/*
	 * zipOrLocationChanged - whether the user is entering a zip or a location changed, 
	 * 	initialize/restore things accordingly
	 */
	$scope.zipOrLocationChange = function() {
		if ($scope.zipOrLocation != 'zip') {
			$scope.lastZipEntered = $scope.activityFormData.zip;
			$scope.activityFormData.zip = "";
			$scope.activityFormData.city = $scope.lastCityEntered;
			$scope.stateProvinceEntered = $scope.lastStateEntered;
			$scope.countryEntered = $scope.lastCountryEntered;
		}
		if ($scope.zipOrLocation != 'location') {
			$scope.lastCityEntered = $scope.activityFormData.city;
			$scope.activityFormData.city = "";
			$scope.lastStateEntered = $scope.stateProvinceEntered;
			$scope.stateProvinceEntered = null;
			$scope.lastCountryEntered = $scope.countryEntered; 
			$scope.countryEntered = "";
			$scope.activityFormData.zip = $scope.lastZipEntered;
		}
		return;
	}

	 $scope.validateActivityDate = function() {
		var validateResponse = CommonService.validateDate($scope.activityFormData.activityDate, 'mm/dd/yy', -5, 1);
		// Note that 'yy' indicates a 4-digit year value			 
		$scope.dataValidationErr.activityDateInvalid = validateResponse.dateInvalid;
		$scope.dataValidationErr.activityDateUnreasonable = validateResponse.dateUnreasonable;
	 }
	 
	 $scope.validateStartTime = function() {
		 // Start time may omit minutes and seconds
		 $scope.dataValidationErr.startTimeError = false;
		 $scope.startTimeHours = -1;
		 $scope.startTimeMinutes = -1;
		 $scope.startTimeSeconds = -1;
		 if (CommonService.isEmpty($scope.startTimeEntered) == false) {
			 var timeParts = parseTime($scope.startTimeEntered, TIME_REGEX_3_PART_12_MAX_HOUR);
			 if (timeParts.isValid) {
				 $scope.startTimeHours = convertToMilitaryHours(timeParts.parsedHours, $scope.startTimeAmPm); 
				 $scope.startTimeMinutes = isNaN(timeParts.parsedMinutes) ? 0 : timeParts.parsedMinutes;
				 $scope.startTimeSeconds = isNaN(timeParts.parsedSeconds) ? 0 : timeParts.parsedSeconds;
				 var hoursAs12 = convertFromMilitaryHours($scope.startTimeHours);
				 $scope.startTimeEntered=pad(hoursAs12.hour, 2, '0') +":"+
				 	pad($scope.startTimeMinutes, 2, '0') + ":" + 
				 	pad($scope.startTimeSeconds, 2, '0');
				 if (hoursAs12.amPeriod) {
					 $scope.startTimeAmPm = 'AM'
				 } else {
					 $scope.startTimeAmPm = 'PM'
				 }
			 } else {
				 $scope.dataValidationErr.startTimeError = true;
			 };
		 }
	 }
	 
	 $scope.validateDuration = function() {
		 // Duration may omit minutes and seconds
		 $scope.dataValidationErr.durationError = false;
		 durationHours = -1;
		 durationMinutes = -1;
		 durationSeconds = -1;
		 if (CommonService.isEmpty($scope.durationEntered) == false) {
			 var timeParts = parseTime($scope.durationEntered, TIME_REGEX_3_PART_23_MAX_HOUR);
			 if (timeParts.isValid) {
				 durationHours = timeParts.parsedHours; 
				 durationMinutes = isNaN(timeParts.parsedMinutes) ? 0 : timeParts.parsedMinutes;
				 durationSeconds = isNaN(timeParts.parsedSeconds) ? 0 : timeParts.parsedSeconds;
				 $scope.durationEntered=pad(durationHours, 2, '0') +":"+
				 	pad(durationMinutes, 2, '0') + ":" + 
				 	pad(durationSeconds, 2, '0');
				 
			 } else {
				 $scope.dataValidationErr.durationError = true;
			 };
		 }
	 }
	 
	 $scope.validateDistance = function() {
		 if (!CommonService.isEmpty($scope.activityFormData.distance) && isNaN(Number($scope.activityFormData.distance))) {
			 $scope.dataValidationErr.distanceError = true;
		 }
	 }

		/*
		 * validateData function - Apply business like rules to the entered data
		 */	
		 function validateData() {

			 if (!CommonService.isEmpty($scope.activityFormData.activityTitle) && 
					 $scope.activityFormData.activityTitle.length > 100) {
				 $scope.dataValidationErr.titleError = true;
			 } else {
				 $scope.dataValidationErr.titleError = false;
			 }
			 
			 // If zip is being provided, it must not be empty
			 if ($scope.zipOrLocation == 'zip' && CommonService.isEmpty($scope.activityFormData.zip)) {
				 $scope.dataValidationErr.zipMissing = true;
			 } else {
				 $scope.dataValidationErr.zipMissing = false;
			 }

			 if ($scope.zipOrLocation == 'zip' && !CommonService.isEmpty($scope.activityFormData.zip) && 
					 $scope.activityFormData.zip.length > 10) {
				 $scope.dataValidationErr.zipError = true;
			 } else {
				 $scope.dataValidationErr.zipError = false;
			 }

			 // If a location is being provided, at least 1 item must be provided
			 if ($scope.zipOrLocation == 'location' && 
					 CommonService.isEmpty($scope.activityFormData.city) &&
					 CommonService.isEmpty($scope.stateProvinceEntered) &&
					 CommonService.isEmpty($scope.countryEntered)) {
				 $scope.dataValidationErr.locationError = true;
			 } else {
				 $scope.dataValidationErr.locationError = false;
			 }

			$scope.validateActivityDate();
			$scope.validateStartTime();
			$scope.validateDuration();
			$scope.validateDistance(); 
			 
			 if (!CommonService.isEmpty($scope.activityFormData.symptomsHelped) && 
					 $scope.activityFormData.symptomsHelped.length > 200) {
				 $scope.dataValidationErr.symptomError = true;
			 } else {
				 $scope.dataValidationErr.symptomError = false;
			 }
			 if (!CommonService.isEmpty($scope.activityFormData.notes) && 
					 $scope.activityFormData.notes.length > 200) {
				 $scope.dataValidationErr.notesError = true;
			 } else {
				 $scope.dataValidationErr.notesError = false;
			 }
			 
			 //Return false if there were any errors
			 if ($scope.dataValidationErr.titleError ||
					 $scope.dataValidationErr.zipError || $scope.dataValidationErr.zipMissing || 
					 $scope.dataValidationErr.locationError || $scope.dataValidationErr.activityDateInvalid || 
					 $scope.dataValidationErr.activityDateUnreasonable || $scope.dataValidationErr.startTimeError || 
					 $scope.dataValidationErr.durationError|| $scope.dataValidationErr.distanceError ||
					 $scope.dataValidationErr.symptomError || $scope.dataValidationErr.notesError) {
				 return false;
			 }
			 return true;
		 }

		 
		/*
		 * getPendedMsg function - Get, delete, and translate any msg waiting for display
		 */	
		 function getPendedMsg() {	 
			var responsePromise = CommonService.deleteMessage($scope.contextPath, "lastMsgId");
			responsePromise.success(function(getData) {
					var lastMsgId = getData;
					if (lastMsgId.length > 0) {
					   $http.post($scope.contextPath + "/rest/literal/stringByName.do", lastMsgId).
						     success(function(strData, status, headers, config) {
						    	 $scope.messageToShow = strData;
						     }).
						     error(function(strData, status, headers, config) {
					   	 			console.log("--err editActivity.js, unable to translate pending msg id, status:" + status);
						     });			   
					   }	
			 		});
			responsePromise.error(function(getData, status, headers, config){
		   	 			console.log("--err editActivity.js, unable to get pending msg Id, status:" + status);
			 		});			 
			 return;
			 }

/*
 * process ActForm - Submit the entered data
 */
    $scope.processactivityForm = function(isValid ){
   	 if($scope.buttons.chosen == "cancelActivity")  {
  		 $window.location.href = $scope.contextPath + '/pages/secure/dashboard/activityFeed.jsp';  
   		 return;
   	 }
 
   	$scope.submitted = true;
   	 
   	if (validateData() == false) {
   		return;
   	};
   	
   	 if(isValid){  		  		 
   		$scope.activityFormData.activityType = $scope.activityTypeEntered;
   		$scope.activityFormData.stateProvince = ($scope.stateProvinceEntered != null? $scope.stateProvinceEntered : null);
   		$scope.activityFormData.country = ($scope.countryEntered != null ? $scope.countryEntered : null);

   		$scope.activityFormData.startTime = ($scope.startTimeHours > -1 ? pad($scope.startTimeHours, 2, '0') + ':' + 
   				pad($scope.startTimeMinutes, 2, '0') + ':' + pad($scope.startTimeSeconds, 2, '0') : null);
   		$scope.activityFormData.duration = (durationHours > -1 ? pad(durationHours, 2, '0') + ':' + 
   				pad(durationMinutes, 2, '0') + ':' + pad(durationSeconds, 2, '0') : null);
   		$scope.activityFormData.isPublic = ($scope.showHidePublic == "Show"? true : false);
   		$scope.activityFormData.energyBeforeRating = ($scope.energyBefore.rating > 0? $scope.energyBefore.rating : null);
   		$scope.activityFormData.energyAfterRating = ($scope.energyAfter.rating > 0? $scope.energyAfter.rating : null);

   		$scope.busy = true;    	
   		//Initiate the activity create
   		 $http({
   		        method  : 'POST',
		        url     : $scope.contextPath + '/rest/activity/' + ($scope.editMode?'update':'add') + '.do',
		        data	:	$scope.activityFormData
   		    }).
   		    success(function(data) {
   	            console.log(data);
   	            $scope.busy = false;
   	            if (!data.success) {
   	                $scope.messageToShow = data.message;
   	            } else {
	   	            //Put the message returned into the message broker
   	            	CommonService.putMessage($scope.contextPath, "lastMsgId", encodeURIComponent(data.messageId)).
	   	     			success(function(data) { }).
	   	     			error(function(data, status, headers, config){
	     	   	 			console.log("--err editActivity.js, unable to save msgId:" + status);
	     	   	 		});  
   	   	     		if ($scope.editMode) {   	            	
   	     				$window.location.href =  $scope.contextPath + "/pages/secure/dashboard/viewActivity.jsp?actId=" + 
   	     					encodeURIComponent(data.activityId) + "&ret=activityFeed";
   	   	     		} else {
   	   	     			$window.location.href =  $scope.contextPath + "/pages/secure/dashboard/activityFeed.jsp";
   	   	     		}   	            	
   	            }
   	        });
   	 } else {  }
   	 return;
    }; 	
    
	$scope.getEnergyLevelTitle = function(level){
		return $scope.energyLevels[level-1].messageText;
	};
	
    /* parseTime - Pick apart the given time string
     * 
     * Minutes and seconds are optional
	 * Return a map with the results
     */    
    function parseTime(timeString, regEx){
    	// A helpful Regex tester	->	http://regex101.com/#javascript
    	var results = {};     	
    	var patt = new RegExp(regEx);
   	    results.isValid = patt.test(timeString);
 	    
   	    if (results.isValid) {
 	    	var componentResults = patt.exec(timeString);
 	    	//Coerce to a number or NaN
 	    	results.parsedHours = componentResults[1] * 1;
     	    results.parsedMinutes = componentResults[3] * 1;
     	    results.parsedSeconds = componentResults[5] * 1;
 	    }
   	    return results;  
    }   

    /* convertToTime24 - Convert the given hour from 12 hour format to 24
     * 
     */    
    function convertToMilitaryHours(hours12, amOrPm){
		if (amOrPm == "PM" && hours12 != 12) {
			 return hours12 + 12;
		 } else if (amOrPm == "AM" && hours12 == 12) {
			 return hours12 - 12;
		 }
		return hours12;
    }   
    
    /* convertToTime12 - Convert the given hour from 24 hour format to 12
     * 
     */    
    function convertFromMilitaryHours(hours24){
    	var results = {};
		if (hours24 >= 12) {
			results.amPeriod = false;
			results.hour = (hours24 == 12 ? 12 : hours24 - 12);
		} else {
			results.amPeriod = true;
			results.hour = (hours24 == 0 ? 12 : hours24);
			}
			 return results;
    }               

    function pad(toPad, len, c){
    	toPad = '' + toPad;
        var c= c || '0';
        while(toPad.length< len) toPad= c+ toPad;
        return toPad;
    }
    
}]);
