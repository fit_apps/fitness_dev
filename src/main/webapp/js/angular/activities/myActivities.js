'use strict';

var myActivitiesService = angular.module('fitApp.myActivitiesService', []);

myActivitiesService.factory('MyActivitiesService', function($http) {
	return {
		
	  getMyActivitiesTotal : function(contextPath, userId) {
		   var returnedMyTotalActivities =  $http.get(contextPath + "/rest/activity/myActivities/"+userId+".do");
		   return returnedMyTotalActivities;
	   }
	
	};
	
});