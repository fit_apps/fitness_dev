'use strict';

/* viewActivity Module and Controller */

var viewActivityModule = angular.module('fitApp.viewActivityModule',
		['fitApp.commonService', 'fitApp.myActivitiesService', 'xeditable']);

viewActivityModule.service('viewActivityService', function ($http) {
	return {
		getActivityDisplayCommand : function (contextPath, activityId) {
			return $http({
				method : 'GET',
				url : contextPath + '/rest/activity/view/' + activityId + '.do'
			});
		}
	};
});

//A directive to transform an XML attribute into a $scope variable
viewActivityModule.directive('scopeAdd', function ($filter) {
	return {
		restrict : "E",
		link : function (scope, element, attrs) {
			scope[attrs.key] = attrs.value;
		}
	};
});

viewActivityModule.controller('viewActivityController', ['$scope', '$window', '$http', 'CommonService', 'viewActivityService',
		'MyActivitiesService', '$location', '$anchorScroll',
		function ($scope, $window, $http, CommonService, viewActivityService,
			MyActivitiesService, $location, $anchorScroll) {

			$scope.$watch('ReadyToInit', function (newVal, oldVal) {
				//		console.log("ReadyToInit has changed: ", newVal, oldVal, $scope.userId, $scope.contextPath);
				if (newVal != null && newVal == 'true') {
					localInit();
				}
			})

			/*
			 * localInit - Set up the page
			 */
			function localInit() {
				resetAllVars();

				// If activityId is not supplied as a query parm, try to get it from the message broker
				if ($scope.activityId == 'null') {
					CommonService.getMessage($scope.contextPath, "activityId").
					success(function (data) {
						if (data.length == 0) {
							alert("Unable to determine the activity to display");
							console.log("--err viewActivity.js, 0 length activityId returned");
							$scope.adc = null;
						} else {
							$scope.activityId = data;
							getActivity();
						}
					}).error(function (data, status, headers, config) {
						alert("Error encountered trying to determine the activity to display, status:" + status);
						console.log("--err viewActivity.js, unable to get activityId, status:" + status);
					});
				} else {
					getActivity();
				}

				CommonService.deleteMessage($scope.contextPath, "lastMsgId").
				success(function (get1Data) {
					var lastMsgId = get1Data;
					if (lastMsgId.length > 0) {
						$http.post($scope.contextPath + "/rest/literal/stringByName.do", lastMsgId).
						success(function (str1Data, status, headers, config) {
							$scope.messageToShow = str1Data;
						}).
						error(function (str2Data, status, headers, config) {
							console.log("--err viewActivity.js, stringByName Failure: " + str2Data + " " + status);
						});
					}
				}).error(function (get1Data, status, headers, config) {
					console.log("--err viewActivity.js, unable to get lastMsgId, status:" + status);
				});

				// Set up our return destination
				var returnLocPage = ($scope.returnLocParm == 'null') ? "activityFeed" : $scope.returnLocParm;
				$scope.returnLoc = "/pages/secure/dashboard/" + returnLocPage + ".jsp";
				// Display text assumes a reasonable camel case page name
				$scope.returnLocText = returnLocPage.replace(/([A-Z0-9])/g, ' $1')
					.replace(/(?:^|\s)[a-z]/g, function (m) {
						return m.toUpperCase();
					});

				CommonService.getAllEnergyIcons($scope.contextPath, $scope);

				CommonService.getAllCountries($scope.contextPath).success(function (data) {
					$scope.allCountries = data;
				}).error(function (data, status, headers, config) {
					console.log("--err with getting all countries:" + status);
				});

				CommonService.getAllStateProvinces($scope.contextPath).success(function (data) {
					$scope.allStateProvinces = data;
				}).error(function (data, status, headers, config) {
					console.log("--err with getting all state/provinces:" + status);
				});

				CommonService.getAllActivityTypes($scope.contextPath).success(function (data) {
					$scope.allActivityTypes = data;
					angular.copy(data, $scope.allActivityTypesById);
					$scope.allActivityTypesById.sort(
						function (a, b) {
						if (a.id < b.id) {
							return -1;
						}
						if (a.id > b.id) {
							return 1;
						}
						return 0;
					});
				}).error(function (data, status, headers, config) {
					console.log("--err with getting all activity types:" + status);
				});
				return;
			};

			function resetAllVars() {
				$scope.allCountries = [];
				$scope.allStates = [];
				$scope.allActivityTypes = [];
				$scope.allActivityTypesById = [];
				$scope.energyLevels = [];

				$scope.messageToShow = "";
				$scope.userDisplayNamePossessive = "";
			};

			function getActivity() {
				var responsePromise = viewActivityService.getActivityDisplayCommand($scope.contextPath, $scope.activityId);
				responsePromise.success(function (data) {
					$scope.adc = data;
					$scope.userDisplayNamePossessive = data.ownerUserDisplayName +
						"'" + (CommonService.endsWith(data.ownerUserDisplayName, 's') ? '' : 's');

					var conjunction = "";
					$scope.formattedLocation = "";
					if (!CommonService.isEmpty(data.zip)) {
						$scope.formattedLocation = data.zip;
					} else {
						if (!CommonService.isEmpty(data.city)) {
							$scope.formattedLocation = data.city;
							conjunction = ", ";
						}
						if (!CommonService.isEmpty(data.stateProvince)) {
							$scope.formattedLocation += conjunction + data.stateProvince;
							conjunction = ", ";
						}
						if (!CommonService.isEmpty(data.country)) {
							$scope.formattedLocation += conjunction + data.country;
							conjunction = ", ";
						}
					}
					$scope.showActivityComments();
				});
				responsePromise.error(function (data) {
					Console.log("viewActivityService.getActivityDisplayCommand error, data: " + data);
				});
				return;
			}

			$scope.getEnergyLevelTitle = function (level) {
				return $scope.energyLevels[level - 1].messageText;
			};

			$scope.updateActivityComment = function (newCommentTxt, comment) {
				$http({
					method : 'POST',
					url : $scope.contextPath + '/rest/feed/activity/updateComment/' + $scope.activityId + "/" + comment.commentId + '.do',
					data : newCommentTxt
				})
				.success(function (data) {
					comment = data;
				})
				.error(function (data, status, headers, config) {
					//TODO do something nice here
					//alert("AJAX failed!");
				});
			};

			$scope.showEditActivityDialog = function (activityId) {
				// Ensure that the activityId is stored in the message broker
				CommonService.putMessage($scope.contextPath, "activityId", encodeURIComponent(activityId)).
				success(function (data) {
					$window.location.href = $scope.contextPath + "/pages/secure/dashboard/editActivity.jsp";
				}).
				error(function (data, status, headers, config) {
					console.log("--err viewActivity.js, unable to save activityId:" + status);
				});
			};

			$scope.showDeleteActivityDialog = function (activityId, activityTitle) {
				var dialElem = $('#deleteActivityDialog');
				$scope.activityIdToDelete = activityId;
				$scope.activityTitleToDelete = activityTitle
					dialElem.dialog('open');
			};

			//click cancel or ok on delete Comment confirmation dialog
			$scope.handleDeleteActivityDialog = function (clickAction) {
				var dialElem = $('#deleteActivityDialog');

				if (clickAction == 'cancel') {
					dialElem.dialog('close');
				} else if (clickAction == 'ok') {
					$http({
						method : 'GET',
						url : $scope.contextPath + '/rest/feed/deleteActivity/' + $scope.activityIdToDelete + '.do'
					})
					.success(function (data) {
						CommonService.putMessage($scope.contextPath, "lastMsgId", encodeURIComponent(data)).
						success(function (data) {}).
						error(function (data, status, headers, config) {
							console.log("--err viewActivity.js, unable to save msgId:" + status);
						});
						dialElem.dialog('close');
						$window.location.href = $scope.contextPath + $scope.returnLoc;
					})
					.error(function (data, status, headers, config) {
						alert("AJAX act delete failed!");
					});
				}
			};

			//click on cheers link, shows cheer dialog with the names of ppl who cheered
			$scope.showCheersDialog = function (title) {
				$scope.actCheerUsers = [];
				//get cheer users
				$http({
					method : 'GET',
					url : $scope.contextPath + '/rest/feed/getActivityCheerUsers/' + $scope.activityId + '.do'
				})
				.success(function (data) {
					//see if I have success and update the tag count
					$scope.actCheerUsers = data;
					title += " (" + data.length + ")";

					var dialElem = $('#cheerDialog');
					dialElem.dialog("option", "title", title);
					dialElem.dialog('open');

				})
				.error(function (data, status, headers, config) {
					//TODO do something nice here
					//alert("AJAX failed!");
				});
			};

			//User is toggling their cheer of this activity
			$scope.toggleCheer = function () {

				var cheeredByUser = ($scope.adc.cheersByUserCount == 1) ? 1 : 0;
				$http({
					method : 'GET',
					url : $scope.contextPath + '/rest/feed/updateCheer/' + $scope.activityId + '/' + cheeredByUser + '.do'
				})
				.success(function (data) {
					// Update the counts now that the cheer is toggled
					if (cheeredByUser == 1) {
						$scope.adc.cheersByUserCount = 0;
						$scope.adc.actCheersCount--;
					} else {
						$scope.adc.cheersByUserCount = 1;
						$scope.adc.actCheersCount++;
					}

				})
				.error(function (data, status, headers, config) {
					//TODO do something nice here
					//alert("AJAX failed!");
				});
			};
			
			// A user in the cheer dialog is clicked
			$scope.cheerUserJump = function(userId) {
				var dialElem = $('#cheerDialog');
				dialElem.dialog( 'close' );
				$window.location.href =  $scope.contextPath + "/pages/secure/dashboard/profile.jsp?viewUserId=" + userId;
			};			
			
			//click on Comment link
			$scope.showActivityComments = function () {
				$scope.adc.comments = [];
				var commentContainerId = "#commentContainer";
				var endRow = 5;
				if ($scope.adc.actComCount > 5 && $scope.adc.hasOwnProperty("lastCommentRow") && $scope.adc.lastCommentRow != null) {
					endRow = $scope.adc.lastCommentRow + 5;
				}

				$scope.adc.lastCommentRow = endRow;

				//get comments, make them available
				$http({
					method : 'GET',
					url : $scope.contextPath + '/rest/feed/getActivityComments/' + $scope.activityId + "/" + 0 + "/" + endRow + '.do'
				})
				.success(function (data) {
					//see if I have success and update the tag count
					$scope.adc.comments = data;
					//focus on input field
					var inputFieldId = "#addComment";
					//only focus on input field if show more link was not clicked
					if (endRow <= 5) {
						$(inputFieldId).focus();
					} else {
						//scroll to the last one
						var lastCommentId = "commentBox_readonly_" + $scope.adc.comments[$scope.adc.comments.length - 4].commentId;
						$location.hash(lastCommentId);
						$anchorScroll();

					}
				})
				.error(function (data, status, headers, config) {
					//TODO do something nice here
					alert("AJAX failed!");
				});
				$(commentContainerId).show();
			};

			//new comment post
			$scope.postActivityComment = function () {
				//show same comments as before + new one
				var endRow = $scope.adc.lastCommentRow + 1;
				$http({
					method : 'POST',
					url : $scope.contextPath + '/rest/feed/activity/newComment/' + $scope.activityId + '/' + endRow + '.do',
					data : $scope.adc.newComment
				})
				.success(function (data) { //post and show latest comments again
					$scope.adc.comments = data;
					//focus out of the edit field
					$scope.adc.newComment = "";
					var inputFieldId = "#addComment";
					$(inputFieldId).trigger('blur');
					//update original activity comment count
					var newComCount = ($scope.adc.actComCount != null) ? $scope.adc.actComCount + 1 : 1;
					$scope.adc.actComCount = newComCount;
					$scope.adc.commentsByUserCount += 1;
					$scope.adc.lastCommentRow += 1;
				})
				.error(function (data, status, headers, config) {
					//TODO do something nice here
					//alert("AJAX failed!");
				});
			};

			$scope.showDeleteCommentDialog = function (comment) {
				var dialElem = $('#deleteCommentDialog');
				$scope.commentToDelete = comment;
				//	$scope.activWithCommentToDelete = activity;
				dialElem.dialog('open');
			};

			//click cancel or ok on delete Comment confirmation dialog
			$scope.handleDeleteCommentDialog = function (clickAction) {
				var dialElem = $('#deleteCommentDialog');

				if (clickAction == 'cancel') {
					dialElem.dialog('close');
					//	resetDeleteCommentVars();  ??
				} else if (clickAction == 'ok') {
					//delete comment ajax call
					$http({
						method : 'GET',
						url : $scope.contextPath + '/rest/feed/deleteActivityComment/' + $scope.activityId +
						"/" + $scope.commentToDelete.commentId + "/" + $scope.adc.lastCommentRow + '.do'
					})
					.success(function (data) {
						$scope.adc.comments = data.comments;
						$scope.adc.commentsByUserCount = data.userCommentsCount;
						dialElem.dialog('close');
						$scope.adc.actComCount = $scope.adc.actComCount - 1;

						// resetDeleteCommentVars(); ??
					})
					.error(function (data, status, headers, config) {
						//TODO do something nice here
						//alert("AJAX failed!");
					});
				}
			};

			$scope.getCommentElapsedTime = function (commentCreateDate, commentUpdateDate) {
				var today = new Date();
				var compareDate = new Date(commentCreateDate);
				if (commentUpdateDate != null) {
					compareDate = new Date(commentUpdateDate);
				}
				return getElapsedTime(compareDate, today);
			};

			function getElapsedTime(compareDate, today) {
				//Get 1 day in milliseconds
				var one_day = 1000 * 60 * 60 * 24;
				var one_hour = 1000 * 60 * 60;
				var one_minute = 1000 * 60;

				// Convert both dates to milliseconds
				var date1_ms = compareDate.getTime();
				var date2_ms = today.getTime();

				// Calculate the difference in milliseconds
				var difference_ms = date2_ms - date1_ms;

				// Convert back to days or hours, return hours if less than 1 day
				var difDays = Math.floor(difference_ms / one_day);
				var difHours = Math.floor(difference_ms / one_hour);
				var difMins = Math.round(difference_ms / one_minute);
				var retString = "";

				if (difDays < 1) {
					//show mins if less than an hour
					if (difHours > 0) {
						retString = difHours + "h ago";
					} else {
						//don't show anything if less than a min ago
						if (difMins > 0) {
							retString = difMins + "mins ago";
						}
					}
				} else {
					retString = difDays + "d ago";
				}
				return retString;
			}

			//used to show count numbers, 0 if null
			$scope.getCount = function (countMe) {
				if (countMe == null) {
					return 0;
				} else {
					return countMe;
				}
			};

			//get the Activities History for the Logged In User.
			$scope.getMyActivities = function () {
				$('#activityDetail').hide();
				MyActivitiesService.getMyActivitiesTotal($scope.contextPath, $scope.userId).success(function (data) {
					$scope.myActivitiesTotal = data;
				}).error(function (data, status, headers, config) {
					console.log("--err with getting Total of MyActivities status:" + status);
				});

				//List of Activities
				$('#activitiesTable').dataTable({
					"bJQueryUI" : true,
					"bProcessing" : true,
					"bserverSide" : true,
					"sAjaxSource" : $scope.contextPath + "/rest/user/activities/"+$scope.userId+".do",
					"sAjaxDataProp" : "myData",
					"bRetrieve" : true,
					"fnRowCallback" : function (nRow, myData, iDisplayIndex, iDisplayIndexFull) {
						$(nRow).attr("id", myData[0]);
						return nRow;
					},
					"aoColumnDefs" : [{
							"bSearchable" : false,
							"bVisible" : false,
							"aTargets" : [0]
						}, {
							"aTargets" : [6],
							"bSortable" : false,
							"mRender" : function (url, type, full) {
								return '<a href="#" name="activityTab" id="activityTab" title="Activity Detail">' + url + '</a>';
							}
						}
					]
				});
			};
			
			$scope.showViewActivity = function(activityId) {
				    $window.location.href =  $scope.contextPath + "/pages/secure/dashboard/viewActivity.jsp?actId=" + activityId;
			};
		}
	]);
