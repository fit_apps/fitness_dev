'use strict';

/* Activities Module and Controller */

var activitiesModule = angular.module('fitApp.activitiesModule', ['fitApp.diagService', 
                                                                  'fitApp.commonService', 
                                                                  'fitApp.myActivitiesService','xeditable']);

activitiesModule.run(function(editableOptions, editableThemes) {
	  // set `default` theme
	  editableOptions.theme = 'default';
	  
	  // overwrite submit button template
	  editableThemes['default'].submitTpl = '<button type="submit" style="margin-top:2px;"><span class="ui-icon ui-icon-check"></span></button>';
	  editableThemes['default'].cancelTpl = ' ';
	});

activitiesModule.service('activitiesService', function($http) {
	return {
		getFeedActivities: function(contextPath, startRow, numOfRows){
			return	$http({
						method: 'GET',
						url: contextPath + '/rest/feed/activities/' + startRow +'/' + numOfRows + '.do'
					});
		} 
	};
}); 

activitiesModule.controller('activitiesController', ['$scope', '$window', '$http',  '$timeout', 'activitiesService', 
                                                     'DiagnosisService', 'activityTypeService', 'CommonService', 'MyActivitiesService',
                                                     '$location', '$anchorScroll',
                                                     function($scope, $window, $http, $timeout,  activitiesService, 
                                                    		 DiagnosisService, activityTypeService, CommonService, MyActivitiesService,
                                                    		 $location, $anchorScroll) {
	
	$scope.startRow = 0;
	$scope.endRow = 0;
	$scope.numOfRows = 12;
	$scope.diagHierarchy = [];
	$scope.allFilters = {diagFilter:[], actTypeFilter:[], selectedSort:'na'};
	$scope.allActivityTypes = [];
	$scope.selectedSort='na';
	$scope.chemoFilter='';
	$scope.radFilter='';
	$scope.cancerFreeDt='';
	$scope.curUserHasCancer = false;
	$scope.profile = {chemoCompleted:'', chemoTotal:"", radCompleted:"", radTotal:"", phaseId:"", refChemoFreqId:""};
	$scope.chemoCompl = '';
	$scope.actCheerUsers = [];
	//don't reset this one
	$scope.energyLevels = [];
	$scope.taggedByMe = [];
	
	function resetAllVars() {
		$scope.startRow = 0;
		$scope.endRow = 0;
		$scope.diagHierarchy = [];
		$scope.allFilters = {diagFilter:[], actTypeFilter:[], selectedSort:'na', cancerFreeDt:""};
		$scope.allActivityTypes = [];
		$scope.allActivityTypesById = [];
		$scope.selectedSort='na';
		$scope.chemoFilter='';
		$scope.radFilter='';
		$scope.curUserHasCancer = false;
		$scope.actCheerUsers = [];
		$scope.cancerFreeDt='';
		$scope.taggedByMe = [];
	};
	
	$scope.init = function(userId, contextPath) {
		$scope.userId = userId;
		$scope.contextPath = contextPath;
		resetAllVars();
		console.log("init startRow: " + $scope.startRow);
		console.log("init numOfRows: " + $scope.numOfRows);
		//load feed activities when page activities.jsp loaded, only first 6 rows at the start
		var responsePromise = activitiesService.getFeedActivities(contextPath, $scope.startRow, $scope.numOfRows);
		
		responsePromise.success(function(data){
			$scope.feedActivities = data;
		});
		
		responsePromise.error(function(data, status, headers, config) {
			//TODO do something nice here
		     //alert("AJAX failed!");
		});
		
		//get all diagnosis hierarchy
		getDiagHierarchy();
		
		//get all activity types
		getAllActivityTypes();
		
		//check if current user's diagnosis has cancer
		getUserCancerData();
		
		getTaggedByMe();
		
		//load energy strings and icon names
		 CommonService.getAllEnergyIcons($scope.contextPath, $scope);
		 
		 // Get any message that wants displaying
		CommonService.deleteMessage($scope.contextPath, "lastMsgId").
			success(function(get1Data) {
				var lastMsgId = get1Data;
				if (lastMsgId.length > 0) {
				   $http.post(contextPath + "/rest/literal/stringByName.do", lastMsgId).
					     success(function(str1Data, status, headers, config) {
					    	 $scope.lastMsgText = str1Data;
					     }).
					     error(function(str2Data, status, headers, config) {
				   	            console.log("stringByName Failure: " + str2Data + " " + status);
					     });
				   }	
		 		}).error(function(get1Data, status, headers, config){
	   	 			console.log("--err activities.js, unable to get lastMsgId, status:" + status);
	 		});	
		 
	};
	
	 
	
	$scope.getEnergyLevelTitle = function(level){
		return $scope.energyLevels[level-1].messageText;
	};
	
	$scope.getMyActivities = function(){
		
		$('#activityDetail').hide();
		//Activities Total
		MyActivitiesService.getMyActivitiesTotal($scope.contextPath, $scope.userId).success(function(data){
	    	$scope.myActivitiesTotal = data;
		 }).error(function(data, status, headers, config){
				console.log("--err with getting Total of MyActivities status:" + status);
		});
		
		//List of Activities
    	$('#activitiesTable').dataTable({
            "bJQueryUI": true,
            "bProcessing": true,
            "bserverSide": true,
            "sAjaxSource": $scope.contextPath+"/rest/user/activities/"+$scope.userId+".do",
            "sAjaxDataProp":"myData",
            "bRetrieve": true,
            "fnRowCallback": function(nRow, myData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", myData[0]);
                return nRow;
            },
            "aoColumnDefs":[
            {
            "bSearchable": false,
            "bVisible": false,
            "aTargets": [0]
            },
             { 
            	"aTargets": [ 6 ],
            	"bSortable": false, 
            	"mRender": function ( url, type, full )  {         
            		return  '<a href="#" name="activityTab" id="activityTab" title="Activity Detail">' + url + '</a>';      
            	}   }
             ]
        });
    			
	};
	
	function getTaggedByMe() {
		//get all users I tagged, username, userId
		CommonService.getTaggedByUser($scope.contextPath, $scope.userId).success(function(data) {
			$scope.taggedByMe = data;
		}).error(function(data, status, headers, config){
			console.log("--err with getTaggedByMe:" + status);
		});
		
	};
	
	 $scope.showViewActivity = function(activityId) {
	    $window.location.href =  $scope.contextPath + "/pages/secure/dashboard/viewActivity.jsp?actId=" + activityId;
	 };

	
	function getDiagHierarchy() {
		//get all diagnosis hierarchy
	    DiagnosisService.getAllDiagnosisHierarchy($scope.contextPath).success(function(data) {
			$scope.diagHierarchy = data;
		}).error(function(data, status, headers, config){
			console.log("--err with getting diagnosis hierarhy status:" + status);
		});
	};
	
	function getUserCancerData(){
		DiagnosisService.currentUserHasCancer($scope.contextPath).success(function(data){
			$scope.curUserHasCancer = data.currentUserHasCancer;
			if(data.hasOwnProperty("treatmentData")) {
				initiProfile(data);
			}
			
		}).error(function(data, status, headers, config){
			console.log("--err with checking if current user has cancer:" + status);
		});
	};
	
	function initiProfile(data){
		$scope.profile.chemoCompleted = data.treatmentData.chemoCompleted;
		$scope.profile.chemoTotal = data.treatmentData.chemoTotal;
		$scope.profile.radCompleted = data.treatmentData.radCompleted;
		$scope.profile.radTotal = data.treatmentData.radTotal;
		$scope.profile.phaseId =  data.treatmentData.phaseId;
		$scope.profile.refChemoFreqId = data.treatmentData.refChemoFreqId;
	}
	

	//update button for updating treatments
	$scope.updateTreatments = function(){
		//post to rest service to apply filter
		   $http({
		        method  : 'POST',
		        url     : $scope.contextPath + '/rest/feed/updateTreatments.do',
		        data    : $scope.profile
		    })
		    .success(function(data) {
		    	initiProfile(data);
	        })
	        .error(function(data, status, headers, config) {
				//TODO do something nice here
			     //alert("AJAX failed!");
			});
	};
	
	$scope.updateTreatments = function(){
		//post to rest service to apply filter
		   $http({
		        method  : 'POST',
		        url     : $scope.contextPath + '/rest/feed/updateTreatments.do',
		        data    : $scope.profile
		    })
		    .success(function(data) {
		    	initiProfile(data);
	        })
	        .error(function(data, status, headers, config) {
				//TODO do something nice here
			     //alert("AJAX failed!");
			});
	};
	
	//Tag click handler
	$scope.updateTag = function(actArrIndex) {
		var taggedActivity = $scope.feedActivities[actArrIndex];
		var activityUserId = taggedActivity.userId;
		var actTagCount = (taggedActivity.tagsByUserCount == 1)?1:0;
		var newTagCount = 0;
		
		 $http({
		        method  : 'GET',
		        url     : $scope.contextPath + '/rest/feed/updateTag/'+ activityUserId + '/' + actTagCount + '.do'
		    })
		    .success(function(data) {
		    	//see if I have success and update the tag count
		    	if(actTagCount == 1) {
		    		newTagCount = 0;
		    	} else {
		    		newTagCount = 1;
		    	}
		    	//update all records on a page for that user
		    	for(var i=0; i<$scope.feedActivities.length; i++){
		    		var curUserId = $scope.feedActivities[i].userId;
		    		if(curUserId == activityUserId) {
		    			 $scope.feedActivities[i].tagsByUserCount =  newTagCount;
		    		}
		    	}
		    	//update tagged by me data
		    	//remove tag
		    	if( newTagCount == 0) {
			    	for(var i=0; i<$scope.taggedByMe.length; i++) {
			    		var taggedUsrId = $scope.taggedByMe[i].userId;
			    		if(taggedUsrId == activityUserId) {
			    			  //removed tag
			    				$scope.taggedByMe.splice(i,1);
			    				break;
			    		}  
			    	}
		    	}
		    	//new tagged
		    	else if ( newTagCount == 1) {
	    			var newTagged = {"displayName":taggedActivity.userDisplName,"userId":activityUserId};
	    			$scope.taggedByMe.push(newTagged);
    			}
	        })
	        .error(function(data, status, headers, config) {
				//TODO do something nice here
			     //alert("AJAX failed!");
			});
	};
	
	//click on cheer icon
	$scope.updateCheer = function(actArrIndex) {
		var activity = $scope.feedActivities[actArrIndex];
		var activityId = activity.actId;
		var cheersByUserCount = (activity.cheersByUserCount==1)?1:0;
		var newCheerCount = 0;
		 $http({
		        method  : 'GET',
		        url     : $scope.contextPath + '/rest/feed/updateCheer/'+ activityId + '/' + cheersByUserCount + '.do'
		    })
		    .success(function(data) {
		    	//see if I have success and update the tag count
		    	if(cheersByUserCount == 1) {
		    		newCheerCount = 0;
		    		$scope.feedActivities[actArrIndex].actCheersCount --;
		    	} else {
		    		newCheerCount = 1;
		    		$scope.feedActivities[actArrIndex].actCheersCount ++;
		    	}
		    	$scope.feedActivities[actArrIndex].cheersByUserCount = newCheerCount;
		    	 
	        })
	        .error(function(data, status, headers, config) {
				//TODO do something nice here
			     //alert("AJAX failed!");
			});
	};
	
	//click on cheers link, shows cheer dialog with the names of ppl who cheered
	$scope.showCheersDialog = function(actArrIndex, title) {
		var activity = $scope.feedActivities[actArrIndex];
		var activityId = activity.actId;
		$scope.actCheerUsers = [];
		//get cheer users
		$http({
	        method  : 'GET',
	        url     : $scope.contextPath + '/rest/feed/getActivityCheerUsers/'+ activityId + '.do'
	    })
	    .success(function(data) {
	    	//see if I have success and update the tag count
	    	$scope.actCheerUsers = data;
	    	title += " (" + data.length + ")";
	    	
	    	var dialElem = $('#cheerDialog');
	    	dialElem.dialog( "option", "title", title );
	    	dialElem.dialog( 'open' );
	    	 
        })
        .error(function(data, status, headers, config) {
			//TODO do something nice here
		     //alert("AJAX failed!");
		});
	};
	
	//click on Comment link
	$scope.showActivityComments = function(activity) {
		//var activity = $scope.feedActivities[actArrIndex];
		activity.comments = [];
		var activityId = activity.actId;
		var commentContainerId = "#commentContainer_" + activityId;
		var endRow = 4;
		if(activity.actComCount > 4 && activity.hasOwnProperty("lastCommentRow") && activity.lastCommentRow != null) {
			endRow = activity.lastCommentRow + 4;
		} 
		 
		activity.lastCommentRow = endRow;
		
		//get comments, make them available
		$http({
	        method  : 'GET',
	        url     : $scope.contextPath + '/rest/feed/getActivityComments/'+ activityId + "/" + 0 + "/" + endRow + '.do'
	    })
	    .success(function(data) {
	    	//see if I have success and update the tag count
	    	activity.comments = data;
	    	//focus on input field
	    	var inputFieldId = "#addComment_" + activityId;
	    	//only focus on input field if show more link was not clicked
	    	if(endRow <=4) {
	    		$(inputFieldId).focus();
	    	} else {
	    		//scroll to the last one
	    		var lastCommentId = "commentBox_readonly_" + activity.comments[activity.comments.length -4 ].commentId;
	    		 $location.hash(lastCommentId);
	    	     $anchorScroll();

	    	}
        })
        .error(function(data, status, headers, config) {
			//TODO do something nice here
		     //alert("AJAX failed!");
		});
		$(commentContainerId).show();
	};
	
	//new comment post
	$scope.postActivityComment = function(activity) {
		//show same comments as before + new one
		var endRow = activity.lastCommentRow +1;
		 $http({
		        method  : 'POST',
		        url     : $scope.contextPath + '/rest/feed/activity/newComment/'+  activity.actId + '/' + endRow + '.do',
		        data    :  activity.newComment 
		    })
		    .success(function(data) {   //post and show latest comments again
		    	activity.comments = data;
		    	//focus out of the edit field
		    	activity.newComment = "";
		    	var inputFieldId = "#addComment_" + activity.actId;
		    	$(inputFieldId).trigger('blur');
		    	//update original activity comment count
		    	var newComCount= (activity.actComCount != null)?activity.actComCount + 1:1;
		    	activity.actComCount = newComCount;
		    	activity.commentsByUserCount += 1;
		    	
	        })
	        .error(function(data, status, headers, config) {
				//TODO do something nice here
			     //alert("AJAX failed!");
			});
	};
	
	$scope.showDeleteCommentDialog = function(comment, activity) {
		var dialElem = $('#deleteCommentDialog');
		$scope.commentToDelete = comment;
		$scope.activWithCommentToDelete = activity;
    	dialElem.dialog( 'open' );
	};
	
	//click on i icon on the activity, will show diagnosis dialog
	$scope.showDiagInfoDialog = function(activity, diagTitle) {
		$scope.activityDiagData = [];
		$scope.actDiagCancerProcData = [];
		$scope.activityForDiagInfo = null;
		var dialElem = $('#diagnosisInfoDialog');
		//get diagnosis data
		$http({
	        method  : 'GET',
	        url     : $scope.contextPath + '/rest/feed/userDiagInfo/'+ activity.userId + '.do'
	    })
	    .success(function(data) {
	    	$scope.activityDiagData = data.diagData;
	    	$scope.actDiagCancerProcData = data.cancerProcData;
	    	//if it's cancer diag, and it has treatm info, then show it in primary diagn, since activities contain 
	    	//prim diagnosis only
	    	if(activity.refDiagTypeKey.toLowerCase().indexOf("cancer") > -1) {
	    		$scope.activityForDiagInfo = activity;
	    	}
	    	
        })
        .error(function(data, status, headers, config) {
			//TODO do something nice here
		     //alert("AJAX failed!");
		});
		dialElem.dialog( "option", "title", diagTitle + " " + activity.userDisplName );
    	dialElem.dialog( 'open' );
	};
	
	$scope.closeDiagInfoDialog = function(isViewMore) {
		var dialElem = $('#diagnosisInfoDialog');
		//get userId of the displayed diagnosis before closing it
		var userId = $scope.activityDiagData[0].userId;
		$scope.activityDiagData = [];
		$scope.actDiagCancerProcData = [];
		$scope.activityForDiagInfo = null;
		dialElem.dialog( 'close' );
		//go to profile page if viewMore link is clicked, maybe will be handled by service?
		if(isViewMore) {
			 $window.location.href =  $scope.contextPath + "/pages/secure/dashboard/profile.jsp?viewUserId=" + userId;
		}
	};
	
	$scope.closeCheerDialog = function(userId) {
		var dialElem = $('#cheerDialog');
		dialElem.dialog( 'close' );
		$window.location.href =  $scope.contextPath + "/pages/secure/dashboard/profile.jsp?viewUserId=" + userId;
	};
	
	//click cancel or ok on delete Comment confirmation dialog
	$scope.handleDeleteCommentDialog = function(clickAction){
		var dialElem = $('#deleteCommentDialog');
    	
		if(clickAction == 'cancel') {
			dialElem.dialog( 'close' );
			resetDeleteCommentVars();
		} else if(clickAction == 'ok') {
			
			//delete comment ajax call
			$http({
		        method  : 'GET',
		        url     : $scope.contextPath + '/rest/feed/deleteActivityComment/'+ $scope.activWithCommentToDelete.actId +
		        				"/" + $scope.commentToDelete.commentId +"/" +$scope.activWithCommentToDelete.lastCommentRow + '.do'
		    })
		    .success(function(data) {
		    	$scope.activWithCommentToDelete.comments = data.comments;
		    	$scope.activWithCommentToDelete.commentsByUserCount = data.userCommentsCount;
				dialElem.dialog( 'close' );
				$scope.activWithCommentToDelete.actComCount = $scope.activWithCommentToDelete.actComCount -1;
				
				resetDeleteCommentVars();
	        })
	        .error(function(data, status, headers, config) {
				//TODO do something nice here
			     //alert("AJAX failed!");
			});
		}
	};
	
	$scope.updateActivityComment = function(newCommentTxt, comment, activity) {
		$http({
	        method  : 'POST',
	        url     : $scope.contextPath + '/rest/feed/activity/updateComment/'+ activity.actId + "/" +  comment.commentId + '.do',
	        data    : newCommentTxt 
	    })
	    .success(function(data) {   //return update comment object
	    	comment = data;our
        })
        .error(function(data, status, headers, config) {
			//TODO do something nice here
		     //alert("AJAX failed!");
		});
	};
	
	function resetDeleteCommentVars(){
		$scope.commentToDelete = "";
		$scope.activWithCommentToDelete = "";
	}
	
	$scope.getCommentElapsedTime = function(commentCreateDate, commentUpdateDate) {
		var today = new Date();
		var compareDate = new Date(commentCreateDate);
		if(commentUpdateDate != null ){
			compareDate = new Date(commentUpdateDate);
		}
		return getElapsedTime(compareDate, today);
	};
	
	function getElapsedTime(compareDate, today) {
		//Get 1 day in milliseconds
		  var one_day=1000*60*60*24;
		  var one_hour = 1000*60*60;
		  var one_minute = 1000*60;

		  // Convert both dates to milliseconds
		  var date1_ms = compareDate.getTime();
		  var date2_ms = today.getTime();

		  // Calculate the difference in milliseconds
		  var difference_ms = date2_ms - date1_ms;
		    
		  // Convert back to days or hours, return hours if less than 1 day
		  var difDays= Math.floor(difference_ms/one_day); 
		  var difHours = Math.floor(difference_ms/one_hour);
		  var difMins =  Math.round(difference_ms/one_minute);
		  var retString = "";
		  
		  if(difDays < 1) {
			  //show mins if less than an hour
			  if(difHours > 0) {
				  retString = difHours + "h ago";
			  } else {
				  //don't show anything if less than a min ago
				  if(difMins > 0) {
					  retString = difMins + "mins ago";
				  }
			  }
		  } else {
			  retString = difDays + "d ago";
		  }
		  return retString;

	}
	
	// Get all activity types by name and by ID
	function getAllActivityTypes() {
		activityTypeService.getAllActTypes($scope.contextPath).success(function(data) {
			$scope.allActivityTypes = data;
			angular.copy(data, $scope.allActivityTypesById);
			$scope.allActivityTypesById.sort(
					function (a,b) {
						  if (a.id < b.id) {return -1;}
						  if (a.id > b.id) {return 1;}
						  return 0;
						}
			);			
		}).error(function(data, status, headers, config){
			console.log("--err with getting activity types tatus:" + status);
		});
	}
	
	$scope.latestHandler = function() {
		$scope.startRow =  0;
		//$scope.numOfRows = 12;
		$scope.selectedSort='na';
		
		//reset diagnosis filter
		getDiagHierarchy();
		
		console.log("startRow: " + $scope.startRow);
		console.log("numOfRows: " + $scope.numOfRows);
		var responsePromise = activitiesService.getFeedActivities($scope.contextPath, $scope.startRow, $scope.numOfRows);
	
		responsePromise.success(function(data){
			$scope.feedActivities = data;
		});
		
		responsePromise.error(function(data, status, headers, config) {
			//TODO do something nice here
		     //alert("AJAX failed!");
		});
	};
	
	$scope.nextHandler = function() {
		$scope.endRow = ($scope.endRow == 0)?$scope.numOfRows-1:$scope.endRow;
		$scope.startRow =  $scope.endRow + 1;
		$scope.endRow = $scope.endRow  + $scope.numOfRows;
		console.log("next startRow: " + $scope.startRow);
		console.log("next endRow: " + $scope.endRow);
		getActivitiesWithFilter($scope.startRow, $scope.endRow);
	};
	
	$scope.prevHandler = function() {
		$scope.startRow =  $scope.startRow - $scope.numOfRows;
		$scope.endRow = $scope.startRow + $scope.numOfRows - 1;
		console.log("prev startRow: " + $scope.startRow);
		console.log("prev endRow: " + $scope.endRow);
		getActivitiesWithFilter($scope.startRow, $scope.endRow);
	};
	
	$scope.getPaginatorCssClass = function(disabled){
		if(disabled) {
			return "fit_text_color_disabled";
		} else {
			return "fit_text_color";
		}
	};
		
	$scope.displayDate = function(index, feedActivities){
		var curDate = feedActivities[index].actDate;
		if(index == 0) {
			return true;
		} else if (index > 0) {
			var prevDate = feedActivities[index-1].actDate;
			if(prevDate == curDate) {
				return false;
			} else {
				return true;
			}
		}
	};
	
	$scope.displayDiagParent = function(index, allDiags) {
		return CommonService.displayDiagParent(index, allDiags);
	};
	
	//clicks on diagnosis check boxes filter
	$scope.applyFilterByDiag = function(elem){
		console.log($scope.diagHierarchy );
		console.log("elem diag: " + elem.diag.isChecked);
		
		$scope.startRow = 0;
		//$scope.numOfRows = 12;
		
		$scope.allFilters.diagFilter =  $scope.diagHierarchy ;
		var showCancerFilters = true;
		
		for(var i=0; i<$scope.diagHierarchy.length; i++) {
			var diag = $scope.diagHierarchy[i];
			if(diag.hasOwnProperty ("isChecked") && diag.isChecked == 'true' && diag.parentDiagKey == 'CANCER') {
				showCancerFilters = true;
				break;
			}  
		}
		if(showCancerFilters) {
			$("#cancerFilters").removeClass("fit_hidden").addClass("fit_visible");
		} else {
			$("#cancerFilters").removeClass("fit_visible").addClass("fit_hidden");
		}
		
		//post to rest service to apply filter
		getActivitiesWithFilter($scope.startRow, $scope.numOfRows);
	};
	
	$scope.applyFilterByCancerFreeDt = function(elem) {
		$scope.allFilters.cancerFreeDt = $scope.cancerFreeDt;
		console.log($scope.allFilters);
		getActivitiesWithFilter($scope.startRow, $scope.numOfRows);
	};
	
	function getActivitiesWithFilter(startRow, numOfRows){
		
		  //post to rest service to apply filter
		   $http({
		        method  : 'POST',
		        url     : $scope.contextPath + '/rest/feed/applyFilter/'+  startRow +'/' + numOfRows + '.do',
		        data    : $scope.allFilters 
		    })
		    .success(function(data) {
		    	$scope.feedActivities = data;
	        })
	        .error(function(data, status, headers, config) {
				//TODO do something nice here
			     //alert("AJAX failed!");
			});
	}
	
	//click on activity type check box
	$scope.applyFilterByActType = function(elem){
		
		$scope.startRow = 0;
		//$scope.numOfRows = 6;
		
		$scope.allFilters.actTypeFilter =  $scope.allActivityTypes ;
		
		//post to rest service to apply filter
		getActivitiesWithFilter($scope.startRow, $scope.numOfRows);
	};
	
	$scope.applySort = function(){
		//move to first page
		$scope.startRow = 0;
		//$scope.numOfRows = 12;
		
		$scope.allFilters.selectedSort = $scope.selectedSort;
		getActivitiesWithFilter($scope.startRow, $scope.numOfRows);
	};
	
	$scope.applyChemoFilter = function() {
		$scope.allFilters.chemoFilter = $scope.chemoFilter;
		getActivitiesWithFilter($scope.startRow, $scope.numOfRows);
	};
	
	$scope.applyRadFilter = function() {
		$scope.allFilters.radFilter = $scope.radFilter;
		getActivitiesWithFilter($scope.startRow, $scope.numOfRows);
	};
	
	$timeout(function () {
        $('#searchChemoCompleted').spinner({
            spin: function (event, ui) {
                $scope.chemoFilter = ui.value;
                $scope.applyChemoFilter();
            },
            change: function (event, ui) {
                $scope.chemoFilter = $('#searchChemoCompleted')[0].value;
                $scope.applyChemoFilter();
            }
        }, 0);
        
        $('#searchRadCompleted').spinner({
            spin: function (event, ui) {
                $scope.radFilter = ui.value;
                $scope.applyRadFilter();
            },
            change: function (event, ui) {
                $scope.radFilter = $('#searchRadCompleted')[0].value;
                $scope.applyRadFilter();
            }
        }, 0);
        
        
        $("input[id^='spin_updateTreatment']").spinner({
        	 
            spin: function (event, ui) {
            	//$scope.profile.chemoCompleted =  ui.value;
            	var evalName =  event.target.attributes["ng-model"].nodeValue;
            	
            	if(evalName == "profile.chemoCompleted") {
            		$scope.profile.chemoCompleted = ui.value;
            	}
            	else if(evalName == "profile.chemoTotal") {
            		$scope.profile.chemoTotal = ui.value;
            	}
            	else if(evalName == "profile.radCompleted"){
            		$scope.profile.radCompleted = ui.value;
            	}
            	else if(evalName == "profile.radTotal") {
            		$scope.profile.radTotal = ui.value;
            	}
            },
            change: function (event, ui) {
            	var id = event.target.attributes["id"].nodeValue;
            	var evalName = event.target.attributes["ng-model"].nodeValue;
            	
            	if(evalName == "profile.chemoCompleted") {
            		$scope.profile.chemoCompleted =  $("#" + id + "")[0].value;
            	}
            	else if(evalName == "profile.chemoTotal") {
            		$scope.profile.chemoTotal =  $("#" + id + "")[0].value;
            	}
            	else if(evalName == "profile.radCompleted"){
            		$scope.profile.radCompleted =  $("#" + id + "")[0].value;
            	}
            	else if(evalName == "profile.radTotal") {
            		$scope.profile.radTotal =  $("#" + id + "")[0].value;
            	}
            }
        }, 0);
        
        
    });
	
	
	//used to show count numbers, 0 if null
	$scope.getCount = function(countMe){
		if(countMe == null){
			return 0;
		} else {
			return countMe;
		}
	};
	
	$scope.showTreatmentInfo = function(diagType) {
		if(diagType.toLowerCase().indexOf("cancer") > -1){
			return true;
		} else {
			return false;
		}
	};
	
		 
}]);

 
