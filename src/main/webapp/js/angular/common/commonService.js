'use strict';

/* Common Utils  */
 
var commonService = angular.module('fitApp.commonService', []);

commonService.factory("CommonService", function($http) {
	return {
	     
	 	displayDiagParent: function(index, allDiags) {
			 var curParent = allDiags[index].parentDiagKey;
			 if(index == 0) {
				 return true;
			 } else if (index > 0) {
				var prevParent = allDiags[index-1].parentDiagKey;
				if(prevParent == curParent) {
					return false;
				} else {
					return true;
				}
			 }
		},
		
	   getAllCountries : function(contextPath) {
		   var returnedAllCountries =  $http.get(contextPath + "/rest/reference/allCountries.do");
		   return returnedAllCountries;
	   },
	   
	   getAllStateProvinces: function(contextPath){
	   	return $http.get(contextPath + "/rest/reference/allStates.do");
	   },		

	   getAllActivityTypes : function(contextPath) {
		   var returnedAllActivityTypes =  $http.get(contextPath + "/rest/reference/allActivityTypes.do");
		   return returnedAllActivityTypes;
	   },
	   
	   getAllEnergyIcons: function(contextPath, scope) {			
	    	scope.energyLevels = [
	    		     {val:1, messageText: null, msgKey: 'energy.level.1', imageUrl: '/images/energyIcons/enLevel_1.png'},
	    		     {val:2, messageText: null, msgKey: 'energy.level.2', imageUrl: '/images/energyIcons/enLevel_2.png'},
	    		     {val:3, messageText: null, msgKey: 'energy.level.3', imageUrl: '/images/energyIcons/enLevel_3.png'},
	    		     {val:4, messageText: null, msgKey: 'energy.level.4', imageUrl: '/images/energyIcons/enLevel_4.png'},
	    		     {val:5, messageText: null, msgKey: 'energy.level.5', imageUrl: '/images/energyIcons/enLevel_5.png'},
	    		     {val:6, messageText: null, msgKey: 'energy.level.6', imageUrl: '/images/energyIcons/enLevel_6.png'},
	    		     {val:7, messageText: null, msgKey: 'energy.level.7', imageUrl: '/images/energyIcons/enLevel_7.png'},];		

    		// Loop through the energy levels to add in the passed in context and to build a temp structure to pass to the server
	    	var stringsToRetrieve =[];
	    	for (var x in scope.energyLevels) {
	   	   	 	scope.energyLevels[x].imageUrl = contextPath + scope.energyLevels[x].imageUrl;
	   	   	 	stringsToRetrieve[x] = scope.energyLevels[x].msgKey;
	   	   	 }

			   $http.post(contextPath + "/rest/literal/stringByNames.do", stringsToRetrieve).
			     success(function(data, status, headers, config) {
			    	 //Move the returned text strings to our energyLevels object
			    	 for (var x in scope.energyLevels) {
	   	            	for (var y in data) {
	   	            		if (scope.energyLevels[x].msgKey == data[y].stringName) {
	   	            			scope.energyLevels[x].messageText = data[y].stringValue;
	   	            		}
	   	            	}
	   	            }
			     }).
			     error(function(data, status, headers, config) {
		   	            console.log("getAllEnergyTexts Failure: " + data + " " + status);
		   	            return null;
			     });
	   },
	   
	   getTaggedByUser: function(contextPath, userId) {
		   return $http.get(contextPath + '/rest/profile/userTags/'+ userId + '.do');
	   },   	   
   	   
	   validateReCaptchaResponse: function(contextPath, response) {
		   console.log("In commonService.validateReCaptchaResponse, contextPath: " + contextPath + " response: " + response);
		   return $http.post(contextPath + "/rest/validate/captcha.do", response);
	   },	   	   
	   
	   getRecaptchaPublicKey : function(contextPath) {
		   console.log("In commonService.getRecaptchaPublicKey, contextPath: " + contextPath);		   
		   var recaptchaPublicKey =  $http.get(contextPath + "/rest/recaptcha/publicKey.do");
		   return recaptchaPublicKey;
	   },

	   
	   /*
	    *  Hack to allow the recaptcha to be reset. Just calling "vcRecaptchaService.reload($scope.recaptchaData.widgetId)"
	    *  results in "Error: Permission denied to access property "
	    *  See "https://groups.google.com/forum/#!topic/recaptcha/CZNuFp1ct_Y"
	    */
//	   resetRecaptcha : function (widgetId) {
//           $('.pls-container').remove();
//           grecaptcha.reset(widgetId);
//       },  
       /*
        * The above hack should have worked, but didn't. The following, based
        * on 'http://stackoverflow.com/questions/27217778/google-recaptcha-throwing-errors-on-reset' did
        */
       resetRecaptcha : function (divId) {
            var recaptchaSoure = divId[0].src;
            divId[0].src = '';
            setInterval(function () { divId[0].src = recaptchaSoure; }, 500);	
       },

	   validateDate: function(dateToCheck, datePattern, rangeMin, rangeMax) {
		   // See http://api.jqueryui.com/datepicker/ for pattern info
			 var validateResponse = {};
			 validateResponse.parsedDate = null;
			 validateResponse.dateInvalid = false;
			 validateResponse.dateEmpty = false;
			 validateResponse.dateUnreasonable = false;
			 
			 if (this.isEmpty(dateToCheck) == true) {
				 validateResponse.dateEmpty = true;
			 } else {
				 try {
					 validateResponse.parsedDate = $.datepicker.parseDate(datePattern, dateToCheck);
					 var yearDif = validateResponse.parsedDate.getFullYear() - (new Date().getFullYear());			 
					 if (yearDif < rangeMin || yearDif > rangeMax) {
							 validateResponse.dateUnreasonable = true;
						 }
					} catch (e)
					{
						validateResponse.dateInvalid = true;
					} 
				 }
			 return validateResponse;
	   },
	   
	   
	   /**
	    * check if secondDate is after firstDate, and if To is entered when From is entered
	    * */
	   areDatesValid: function(firstDate, secondDate) {
		   var dateErr = {};
		   dateErr.valid = true;
		   //empty From, but To is not empty
		   if((firstDate == null || firstDate ==  undefined) && secondDate != null && secondDate != undefined) {
			   dateErr.valid = false;
			   dateErr.fromDateErr = "err.diag.dateFrom";
			   
		   } //not empty From, but empty To 
		   else if (firstDate != null && firstDate != undefined && (secondDate == null || secondDate == undefined) ){
			   dateErr.valid = true;
		   } //first date and secondDate have values
		   else if(firstDate != null && firstDate != undefined && secondDate != null && secondDate != undefined ) {
			   var isFromValid = secondDate >= firstDate;
			   if(!isFromValid) {
				   dateErr.valid = false;
				   dateErr.toDateErr = "err.diag.dateTo"; 
			   }
		   }
		   return dateErr;
	   },
	   
	   isValidDate: function(date) {
		   if(date != null && date != undefined && date.trim() != "" && isNaN(Date.parse(date))) {
				return false;
			} else {
				return true;
			}
	   },
	   

	   /*
	    * Add the msgId/msgValue pair to the MessageController for the currently logged in session
	    */
	   putMessage: function(contextPath, msgId, msgValue){
		   var urlFragment = "/message/put/" + encodeURIComponent(msgId) + "/" + encodeURIComponent(msgValue) + ".do";
		   return $http.get(contextPath + urlFragment);
	   },	   

	   /*
	    * Get the msgValue associated with the given msgId for the currently logged in session
	    */
	   getMessage: function(contextPath, msgId){
		   var urlFragment = "/message/get/" + encodeURIComponent(msgId) + ".do";
		   	return $http.get(contextPath + urlFragment);
	   },

	   /*
	    * Delete and return the msgValue associated with the given msgId for the currently logged in session
	    */
	   deleteMessage: function(contextPath, msgId){
		   var urlFragment = "/message/delete/" + encodeURIComponent(msgId) + ".do";
		   	return $http.get(contextPath + urlFragment);
	   },
	   
	   /*
	    * Delete all msgId/msgValue pairs for the currently logged in session
	    */
	   clearMessages: function(contextPath){
		   var urlFragment = "/message/clear.do";
		   	return $http.get(contextPath + urlFragment);
	   },
	   
	   /*
	    * isEmpty - A utility function to check if a given variable should be consider to be empty
	    * Returns true if the provided data is empty
	    */    
	   	isEmpty: function(data){
	         if(typeof(data) == 'number' || typeof(data) == 'boolean')
	         {
	           return false;
	         }
	         if(typeof(data) == 'undefined' || data === null)
	         {
	           return true;
	         }
	         if(typeof(data.length) != 'undefined')
	         {
	           return data.length == 0;
	         }
	         if(typeof(data.length) != 'undefined')
	         {
	           if(/^[s]*$/.test(data.toString()))
	           {
	             return true;
	           }
	           return data.length == 0;
	         }
	         var count = 0;
	         for(var i in data)
	         {
	           if(data.hasOwnProperty(i))
	           {
	             count ++;
	           }
	         }
	         return count == 0;
	       },
	       

	   	endsWith: function (value, pattern) {
	       	return value.substr( value.length - pattern.length ) === pattern;
	    }, 
	       
	    validatePassword: function(password, confirmPassword) {
	    	 var errObj = {};
			 if(password.length < 8 || password.length > 15 ){
				 errObj.passwordLengthErr = true;
				 errObj.isValidPass = false;
			 } else {
				 errObj.passwordLengthErr = false;
			 }
			 
			 var matchResult = /^[A-Za-z0-9\d=!\-@._*]*$/.test(password) // consists of only these
			 				&& /[A-Z]/.test(password) // has uppercase letter
			 					&& /\d/.test(password)
			// var matchResult = passPattern.test(password);
			 if(!matchResult) {
				 errObj.hasRuleError = true;
				 errObj.isValidPass = false;
			 } else {
				 errObj.hasRuleError = false;
			 }
			 //password matches the rules, now check if confirm password is the same
			 if(matchResult ){
				if( password != confirmPassword){
					errObj.matches = false;
					errObj.isValidPass = false;
				} else {
					errObj.matches = true;
					errObj.isValidPass = true;
				}
			 }
			 return errObj;
		 }   
	       
	};
});
