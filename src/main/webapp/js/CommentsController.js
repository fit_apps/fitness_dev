var CommentsController = {

	showComments:function(commentContainerId) {
    	//make comments div visible
        $("div[id^=" + commentContainerId + "]" ).show();
    },
    
    showAllCommentsDlg:function(commentsDialogId) {
    	//make comments dialog visible
    	$( "#" + commentsDialogId + "" ).dialog( "open" );
    }
};