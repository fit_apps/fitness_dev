<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>     
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
String contextPath = request.getContextPath();
contextPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + contextPath;
System.out.println("----contextPath what's going on: " + contextPath);
pageContext.setAttribute("contextPath", contextPath);
%>
<title>Fit4Life</title>
 <link href="<%=contextPath%>/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="all" href="<%=contextPath%>/styles/960.css" />
    <script src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <script src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
   <script src="<%=contextPath%>/js/angular/angular.js"></script>
  <script src="<%=contextPath%>/js/angular/angular-route.js"></script>
  <script src="<%=contextPath%>/js/angular/app.js"></script>
  <script src="<%=contextPath%>/js/angular/controllers.js"></script>
<link href="<%=contextPath%>/styles/main.css" rel="stylesheet">
</head>

<body style="background-color:#778899;">
        
  <div class="container_16"  style="">
     <div class="grid_4" >	  
     		<%-- <img src="<%=contextPath%>/images/icon_tab.png" alt="some_text"> --%>
     </div> 	
   
    <div class="grid_5 homeTopMenu">
      <span style="padding-right:30px;">
         <a href="<%=contextPath%>/pages/login.jsp"><spring:message code="txt.login" /></a>
     </span> 
     <span style="padding-right:30px;">
         <a href=""><spring:message code="txt.ourStory" /></a>
     </span>
      <span>
         <a href=""><spring:message code="txt.features" /></a>
     </span>
	</div>   
	<div class="clear"></div>
	<div  class="grid_16" >
	<p>
		<img alt="" src="<%=contextPath%>/images/home_background.jpg"  >	
	</p>

	<!-- ui-dialog -->
	<div  class="homeOverlayContainer ">
		<div class="homeOverlay homeOverlaySignup" >
			<div>
			 <a class="homeOverlayLink" href="<%=contextPath%>/pages/registration/register.jsp"><spring:message code="txt.signupNow"/> </a>
			</div>
			<div class="homeOverlayText">
				<spring:message code="txt.fightCancer"/>
			</div> 
		</div>
	</div>

</div>  
  </div>
</body>
</html>