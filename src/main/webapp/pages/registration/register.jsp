<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>     
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
String contextPath = request.getContextPath();
contextPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + contextPath;
System.out.println("----contextPath what's here: " + contextPath);
pageContext.setAttribute("contextPath", contextPath);
%>
<title>Register</title>
 <link href="<%=contextPath%>/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="all" href="<%=contextPath%>/styles/960.css" />
  <script src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
  <script src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
  <script src="<%=contextPath%>/js/angular/angular.js"></script>
  <script src="<%=contextPath%>/js/angular/angular-route.js"></script>
  <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
  <script src="<%=contextPath%>/js/angular/angular-recaptcha.js"></script>
  <script src="<%=contextPath%>/js/angular/common/commonService.js"></script>
  <script src="<%=contextPath%>/js/angular/diagnoses/diagnoses.js"></script>
  <script src="<%=contextPath%>/js/angular/security/registration.js"></script>
  <link href="<%=contextPath%>/styles/main.css" rel="stylesheet">
 
 <script>
$(function() {
	
	$( "#registerBtn" ).button();
	$( "#cancelRegister" ).button();
	//$( "#registerPrimDiagnosis").selectmenu();
});
</script>
 

</head>
<body ng-cloak ng-app="fitApp.registrationModule" ng-controller="registrationController" 
	ng-init="init('<%=contextPath%>')" style="background-color:#778899;"> 
<div class="container_16">
	<div class="grid_4" >	 
     	<img src="<%=contextPath%>/images/icon_tab.png" alt="some_text">
     </div> 	
   
    <div class="grid_5 homeTopMenu">
      <span style="padding-right:30px;">
         <a href="<%=contextPath%>/pages/login.jsp"><spring:message code="txt.login" /></a>
     </span> 
     <span style="padding-right:30px;">
         <a href=""><spring:message code="txt.ourStory" /></a>
     </span>
      <span>
         <a href=""><spring:message code="txt.features" /></a>
     </span>
	</div>    
</div>
	
<%--success registration --%>
<div class="container_16">
<div class="clear"></div>
 <div class="grid_16">
 	<p>
		<img alt="" src="<%=contextPath%>/images/home_background.jpg"  >	
	</p>
</div>
 </div>
    
 	<div class="registerOverlayContainer" >
 		
 	
 	    <div ng-show="regSuccess" >
		  <div class="container_16" style="padding-top:200px;"> 
		     <div   class="grid_2"> &nbsp; </div>	
			 <div class=" whiteLabel"  class="grid_8">
			    {{displayName}}, <spring:message code="reg.txt.success"/>
			  </div> 
			<div class="clear"></div>
			 <div   class="grid_5"> &nbsp; </div>
			 <div   class="grid_7" style="margin-top:50px;">
			   <span  ><a class="registerOverlayTitle"  href="<%=contextPath%>/pages/login.jsp">  <spring:message code="reg.txt.login"/>  </a></span>
			  </div> 
			</div>   
		 </div>
		 
 	  <%-- registration form --%>
       <div ng-show="!regSuccess" >
       <div class="homeOverlay registerOverlayPadding registerOverlayTitle" >
			  <spring:message code="txt.register"/>
		</div>
       	  <div class="container_16"> 
        <form id="regForm" name="regForm" class="formElements" ng-submit="processRegForm(regForm.$valid)" novalidate>
       
            <div style="margin-bottom:20px;" class="grid_6">
	           <%--  <div class="grid_16 fit_required"  style="padding:10px;">
                   * <spring:message code="field.required" />
               </div> --%>
               <div ng-show="errorMessage.length > 0" class="ui-state-error fit_error" style="padding:10px;">{{errorMessage}}</div>
        		<div  style="padding:10px;">
                   <span class="whiteLabelSmall"><spring:message code="reg.label.displayName"  /> <span class="fit_required">*</span> </span><br/>
                   <input type="text" maxlength="50" value="{{regformData.displayName}}"  name="displayName" ng-model="regformData.displayName" required><br/>
                   <span class="fit_error_orange" ng-show="submitted && regForm.displayName.$error.required"><spring:message code="err.displayName.required" /></span>
	           </div>
               <div style="padding:10px;" >
                   <span class="whiteLabelSmall"><spring:message code="reg.label.firstName"  /> </span><br/>
                   <input type="text" maxlength="50" value="{{regformData.firstName}}"  name="firstName" ng-model="regformData.firstName">
                </div>
               <div style="padding:10px;" >
                        <span class="whiteLabelSmall">
                        	<spring:message code="reg.label.lastName"  /> <span class="fit_required"></span> 
                        	<br/>
                        </span> 	
                        <input type="text" maxlength="150" value="{{regformData.lastName}}"  name="lastName" ng-model="regformData.lastName">
                </div>
               
	            <div style="padding:10px;" >
                        <span class="whiteLabelSmall"><spring:message code="reg.label.email" /> <span class="fit_required">*</span><br/>  </span>
                        <input type="email" value="{{regformData.emailAddress}}" maxlength="150"  name="emailAddress" ng-model="regformData.emailAddress" required><br/>
                        <span class="fit_error_orange"  ng-show="submitted && regForm.emailAddress.$error.required"><spring:message code="err.email.required" /></span>
                         <span class="fit_error_orange" ng-show="submitted && regForm.emailAddress.$error.email"><spring:message code="err.email.notValid" /></span>
                       
	           </div> 
                 <div style="padding:10px;" >
                        <span class="whiteLabelSmall"><spring:message code="reg.label.password"/></span><span class="fit_required">*</span> 
                     		  <span class="whiteLabelSmaller" style="margin:5px 0px 5px 6px;">
                     			<span><spring:message code="reg.pass.length" /> </span><br/>
                     			<span style="margin-left:77px"><spring:message code="reg.pass.upperCase"/> </span><br/>
                     			<span style="margin-left:77px"><spring:message code="reg.pass.digit"/> </span> <br/>
                     			<span style="margin-left:77px"><spring:message code="reg.pass.chars"/> </span>
                     		</span> <br>
	                        <input type="password" value="" maxlength="15"  name="password" ng-model="regformData.password"
	                             ng-change="resetSubmitted()" required>
	                        <br/>
	                        <span class="fit_error_orange" ng-show="submitted && regForm.password.$error.required"><spring:message code="err.password.required"></spring:message> </span>
                        	<span class="fit_error_orange" ng-show="submitted && !regForm.password.$error.required && passwordErr.hasRuleError"><spring:message code="err.password.rules"></spring:message> </span>
                        	<span class="fit_error_orange" ng-show="submitted && !regForm.password.$error.required && passwordErr.passwordLengthErr"><spring:message code="err.password.length"></spring:message> </span>
	           </div>
                <div style="padding:10px;" >
                        <span class="whiteLabelSmall"><spring:message code="reg.label.confPassword"/> <span class="fit_required">*</span></span>
                        <input type="password" value=""  maxlength="15" name="confirmPassword" ng-model="regformData.confirmPassword" required><br/>
                        <span class="fit_error_orange" ng-show="submitted && regForm.confirmPassword.$error.required"><spring:message code="err.confPassword.required"></spring:message> </span>
                        <span class="fit_error_orange" ng-show="submitted && !regForm.confirmPassword.$error.required && !passwordErr.matches"><spring:message code="err.password.dontMatch"></spring:message> </span>
	           </div>
               
           </div>
            <div class="grid_6" style="padding:10px;">
                <div style="padding:10px;" >
                       <span class="whiteLabelSmall"><spring:message code="reg.diag.activity.match"/> </span>
                </div> 
            	<div style="padding:10px;" >
                       <span class="whiteLabelSmall"><spring:message code="reg.diagnosis.prim.select"/> <span class="fit_required"> * </span></span>
                </div> 
                <div style="padding-left:10px;" >
                	<select ng-model="primaryDiag" 
                	        ng-options="diag.childDiagName group by diag.parentDiagName for diag in diagHierarchy">
  					</select>
                </div>
            </div>
           <div class="grid_6" style="padding:10px;">
             	<div style="padding:10px;" >
                       <span class="whiteLabelSmall"><spring:message code="reg.diagnosis.second.select"  /> </span>
                </div> 
           		<div  ng-repeat="diag in diagHierarchy" style="padding-left:10px;">
           			<div  class="registerSubHeader" ng-if="displayDiagParent($index, diagHierarchy)" >
                  		{{diag.parentDiagName}}
                  	</div>
                  	<div class="fit_checkboxes whiteLabelSmall">
                  	 	<span ng-if="primaryDiag.childDiagKey !=  diag.childDiagKey">
		                      <input ng-model="diag.isChecked"  
		                      	 type="checkbox" ng-true-value="true" ng-false-value="false" > {{diag.childDiagName}}
                      	  </span>
                      	<span ng-if="primaryDiag.childDiagKey ==  diag.childDiagKey" style="padding-left:25px;color:#EEE8AA;">
		                     {{diag.childDiagName}} -  <spring:message code="reg.diagnosis.alreadyPrimary"/>
                      	</span>
                   </div>
                  
                 </div>
                 
           </div>
            <div class="clear"></div>
             <div class="grid_5">&nbsp; </div>  
           <div class="grid_6">     
              <div  style="padding:10px;" >
                 <span style="margin-right:10px;">
                 	<input type="submit"  id="registerBtn" value="Create" title="<spring:message code='btn.create' />">
                 </span>
                 <button buttonname="cancelRegister" id="cancelRegister"  name="cancelRegister"><spring:message code="btn.cancel"></spring:message> </button>
              </div>
                        
		</div>
		<div class="clear"></div>
		<div class="grid_12">
			<div class="grid_4">&nbsp; </div> 
			<div class="grid_4" style="padding: 10px 0px 0px 0px;" 
				vc-recaptcha
				id="recaptchaId" 
				theme="'clean'"
				key="recaptchaData.key"
				on-create="setRecaptchaWidgetId(widgetId)"
				on-success="setRecaptchaResponse(response)">
			</div>
			<div class="grid_4">&nbsp; </div> 
			<div class="grid_6 fit_error_nopad" style="padding: 0px 0px 0px 0px;" 
			ng-show="submitted && recaptchaData.captchaIncomplete">
				<spring:message code="err.captcha.incomplete" /></div>
		</div>	 
		<div class="clear"></div>
       </form>   
       </div>
       
    </div>
   
</div>
</body>
</html>