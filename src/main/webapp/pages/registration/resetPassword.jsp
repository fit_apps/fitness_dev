<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
	String contextPath = request.getContextPath();
	contextPath = request.getScheme() + "://" + request.getServerName()
			+ ":" + request.getServerPort() + contextPath;
	System.out.println("----contextPath: " + contextPath);
	pageContext.setAttribute("contextPath", contextPath);
%>
<title>Reset Password</title>
<link
	href="<%=contextPath%>/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="<%=contextPath%>/styles/960.css" />
<script
	src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
<script
	src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
<script src="<%=contextPath%>/js/angular/angular.js"></script>
<script src="<%=contextPath%>/js/angular/angular-route.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
<script src="<%=contextPath%>/js/angular/angular-recaptcha.js"></script>
<script src="<%=contextPath%>/js/angular/diagnoses/diagnoses.js"></script>
<script src="<%=contextPath%>/js/angular/security/resetPassword.js"></script>
<script src="<%=contextPath%>/js/angular/common/commonService.js"></script>
<link href="<%=contextPath%>/styles/main.css" rel="stylesheet">

<script>
	$(function() {
		$("#resetPasswordBtn").button();
		$("#cancelResetBtn").button();
	});
</script>

</head>
<body ng-cloak ng-app="fitApp.resetPasswordModule"
	ng-controller="resetPasswordController"
	ng-init="init('<%=contextPath%>')" style="background-color: #778899;">
	<div class="container_16">
		<div class="grid_4">
			<img src="<%=contextPath%>/images/icon_tab.png" alt="some_text">
		</div>

		<div class="grid_7 homeTopMenu">
			<span style="padding-right: 30px;"> <a
				href="<%=contextPath%>/pages/login.jsp"><spring:message
						code="txt.login" /></a>
			</span> 
			<span style="padding-right:30px;">
		         <a href="<%=contextPath%>/pages/registration/register.jsp"><spring:message code="txt.signup" /></a>
		     </span> 
			<span style="padding-right: 30px;"> <a href=""><spring:message
						code="txt.ourStory" /></a>
			</span> <span> <a href=""><spring:message code="txt.features" /></a>
			</span>
		</div>
	</div>

	<%--success passwordReset --%>
	<div class="clear"></div>
	<div class="grid_16">
		<p>
			<img alt="" src="<%=contextPath%>/images/home_background.jpg">
		</p>
		<div class="resetOverlayContainer">
			<div class="homeOverlay resetOverlayPadding resetOverlayTitle">
				<spring:message code="txt.resetPassword" />
			</div>

			<div ng-show="resetSuccess">
			<p>Message: {{message}} </p>
			
				<div ng-show="message.length > 0" class="ui-state-highlight" style="padding: 10px;">{{message}}</div>
				<br /><br />
			    <div class="whiteLinkSmall" >
					<a href="<%=contextPath%>/pages/login.jsp"><spring:message code="reset.login.link" /></a>	    	
			    </div>				
			</div>
			
			<%-- reset form --%>
			<div ng-show="!resetSuccess">
				<form id="resetForm" name="resetForm" class="formElements"
					ng-submit="processResetForm(resetForm.$valid)" novalidate>
					<div style="margin-bottom: 20px;">

						<div ng-show="errorMessage.length > 0" class="ui-state-error fit_error" style="padding: 10px;">{{errorMessage}}</div>
						<div style="padding: 10px;">
							<span class="whiteLabelSmall">
								<spring:message code="reset.label.email" /> <span class="fit_required">*</span>
							</span>
								<p />
								<input type="email" value="{{resetFormData.providedEmail}}"
									maxlength="150" name="providedEmail" ng-model="resetFormData.providedEmail" required><br /> 
								<span class="fit_error" ng-show="submitted && resetForm.providedEmail.$error.required">
									<spring:message	code="err.email.required" /></span> 
								<span class="fit_error"	ng-show="submitted && resetForm.providedEmail.$error.email">
									<spring:message	code="err.email.notValid" /></span> 
							
						</div>
						<div style="padding: 10px 0px 0px 100px;">
							<span style="margin-right: 10px;"> 
								<input type="submit" buttonname="resetBtn" id="resetPasswordBtn"
								 value="<spring:message code='btn.resetPassword' />"	
								 title="<spring:message code='btn.resetPassword' />"
								 	ng-disabled="busy"/>
							</span>
							<button buttonname="cancelReset" title="<spring:message code='btn.cancel' />"  id="cancelResetBtn">
								<spring:message code="btn.cancel"></spring:message>
							</button>
						</div>
					</div>

					<div style="padding: 10px 0px 0px 100px;" 
						vc-recaptcha
						id="recaptchaId" 
						theme="'clean'"
						key="recaptchaData.key"
						on-create="setRecaptchaWidgetId(widgetId)"
						on-success="setRecaptchaResponse(response)">
					</div>
					<div class="fit_error_nopad" style="padding: 0px 0px 0px 100px;" 
						ng-show="submitted && recaptchaData.captchaIncomplete">
						<br /><spring:message code="err.captcha.incomplete" /></div>
				</form>
				<div class="grid_16 fit_required" style="padding: 10px;">
					*
					<spring:message code="field.required" />
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	</div>
</body>
</html>