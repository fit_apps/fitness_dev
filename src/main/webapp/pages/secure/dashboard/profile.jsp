<%@include file="header.jsp" %>
<script src="<%=contextPath%>/js/angular/angular.js"></script>
<script src="<%=contextPath%>/js/angular/date.js"></script>
<script src="<%=contextPath%>/js/angular/common/commonService.js"></script>
<script src="<%=contextPath%>/js/angular/diagnoses/diagnoses.js"></script>
<script src="<%=contextPath%>/js/angular/user/profileService.js"></script>
<script src="<%=contextPath%>/js/angular/user/profile.js"></script>
<script src="<%=contextPath%>/js/angular/activities/myActivities.js"></script>
<script type="text/javascript">
 $(function() {

	 $('#activitiesTable').on('click', 'a[id^="activity"]', function(){
   		 var activityId = $(this).closest('tr').attr('id');
   		 angular.element('#profiletabs').scope().showViewActivity(activityId);
	       
     	});
	 
    	 $('#taggedTable').on('click', 'a[id^="untag"]', function(){
    		 var tagId = $(this).closest('tr').attr('id');
            angular.element('#profiletabs').scope().showUntagUserDialog(tagId);
    	 }); 
    	 
    	 $('#taggedTable').on('click', 'a[id^="profile"]', function(){
    		 var userId = $(this).closest('tr').attr('userId');
            angular.element('#profiletabs').scope().showUserProfile(userId);
    	 }); 
    	 
    	 
    	 $( "#untagUserDialog" ).dialog({
             autoOpen: false,
             title:'Untag user' ,
             modal: true,
             position: [400,150],
             width: 285,
             height:140
         });
      	
});
</script>

<body ng-cloak ng-app="fitApp.profileModule" ng-controller="profileController" ng-init="init('<%=currentUser.getId() %>', '<%=contextPath%>', '${param.viewUserId}')">
<div id="profiletabs" >
    <ul>
        <li>
            <a href="#tabs-2-1"><span ng-if="userData.isCurrentUser"><spring:message code="profile.txt.myProfile"/> </span><span ng-if="!userData.isCurrentUser">{{userData.profileData.displName}}'s <spring:message code="profile.txt.profile"/></span></a>
        </li>
        <li>
            <a href="#tabs-2-2"  ng-click="taggedByMe()"><spring:message code="profile.txt.taggedBy"/> <span ng-if="userData.isCurrentUser"><spring:message code="profile.txt.me"/></span><span ng-if="!userData.isCurrentUser">{{userData.profileData.displName}}</span></a>
        </li>
        <li><a href="#tabs-2-3" ng-click="getMyActivities()"><spring:message code="profile.txt.activities"/> </a></li>
    </ul>
    
    <div id="tabs-2-1" style="margin-left:10px;padding-left:1px;overflow-x:visible;overflow-y:visible;">
    <c:if test="${param.profile == 'edit' && param.viewUserId == currentUser.id}" >
         <%@include file="editProfile.jsp" %>
    </c:if>
    <c:if test="${param.viewUserId != currentUser.id || param.profile == null}" >
        <%@include file="viewProfile.jsp" %>
    </c:if> 
    </div>
    
    <div id="tabs-2-2">
    	<%@include file="taggedByMe.jsp" %>
    </div>
    <div id="tabs-2-3">
         <%@include file="myActivities.jsp" %>
    </div>
    <div id="activityDetail" style="display:none;">
        <%@include file="activityDetail.jsp" %>
    </div>
    
  <div id="untagUserDialog"  >
    <div  style="margin:1em 0 2em 0;">
      Are you sure you want to untag this user?
    </div> 
    <hr class="fit_hr">
    <!-- delete comment buttons -->
    <div style="float:right;" class="ui-widget fit_comment_text">
	<input type="button" id="btnUntag_Ok" ng-click="handleUntagUserDialog('ok')"
	       value="OK">
	  <input type="button"   id="btnUntag_Cancel" ng-click="handleUntagUserDialog('cancel')"
	           value="Cancel">
	</div>
   </div>
    
</div>

</body>
</html>