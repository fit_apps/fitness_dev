	  
	 
        <div class="container_16" ng-init="initViewProfile('<%=currentUser.getId() %>', '<%=contextPath%>', '${param.viewUserId}')">
       <div class="ui-state-highlight" style="padding: 10px;" ng-if="profileEditSuccess">
                <spring:message code="profile.txt.saveSuccess" />
            </div>
        <div   class="grid_16 ui-widget-content fit_widget_pad">
        
	        <div class="grid_8 fit_search_top_header fit_text_color fit_top_title_pad">
	                   <spring:message code="profile.txt.publicInfo" />
	         </div>
	         <div class="grid_6 fit_text_color" style="padding:20 0px 20px 10px;" ng-if="userData.isCurrentUser">
	            <a href="profile.jsp?profile=edit&viewUserId=${currentUser.id}">Edit</a>
	         </div>
	         <div class="clear"></div>
	          
	          <div class="grid_2" >
	               <span class="fit_text_color"><spring:message code="reg.label.displayName" /></span>
	          </div>
	          <div class="grid_4 fit_text_padding" >
	             <span>{{userData.profileData.displName}}</span>
	           </div>
	           <div class="clear"></div>
	           <div class="grid_2"   >
	             <span class="fit_text_color"><spring:message code="profile.label.location" /></span>
	           </div>
	           <div class="grid_6 fit_text_padding"   >
	              <span   ng-if="userData.profileData.zipCode != null">
	                 {{userData.profileData.zipCode}}
	              </span>   
	              <span ng-if="userData.profileData.zipCode == null || userData.profileData.zipCode.trim().length == 0">
	                 <span ng-if="userData.isCurrentUser == true">
	                 	<span ng-if="userData.profileData.address1 != null">{{userData.profileData.address1 }}</span>
	                 	<span ng-if="userData.profileData.address2 != null">, {{userData.profileData.address2 }}, </span>
	                 	<span ng-if="userData.profileData.city != null"> {{userData.profileData.city}}</span>
	                 </span>
		              <span   ng-if="userData.profileData.stateName != null">
		                 {{userData.profileData.stateName}}
		              </span>  
		              <span   ng-if="userData.profileData.countryName != null">
		                 {{userData.profileData.countryName}}
		              </span> 
	              </span>
	            </div>
	            <div class="clear"></div>
        </div>
        <div id="diagnosisContainer" class="grid_16 ui-widget-content fit_widget_pad"  >
             <div class="grid_6 fit_search_top_header fit_text_color fit_top_title_pad">
                     Diagnosis
             </div>
             <div class="clear"></div>
             <div  >
                <span ng-repeat="diag in userData.allDiagData">
		              <div class="grid_6 fit_search_sub_header"  ng-if="diag.userDiagIsPrimary && !isSameDiag($index)" style="padding-bottom:5px;">
		                     Primary Diagnosis
		             </div>
		             <div class="grid_6 fit_search_sub_header"  ng-if="!diag.userDiagIsPrimary && showSecondDiagTitle($index)"
                            style="padding-bottom:5px;">
                             Secondary Diagnosis
                     </div>
		              <div class="clear"></div>
		             <div ng-if="!isSameDiag($index)"  class="grid_14 fit_activity_item">
		                 <span class="fit_text_color fit_text_bold" >{{diag.refDiagTypeName}}</span>
		                 <span ng-if="diag.userDiagStartDt != null" class="fit_activity_item"> 
		                  {{diag.userDiagStartDt | date : 'MMMM dd, yyyy'}} - 
		                 </span> 
		                 <span ng-if="diag.userDiagEndDt != null" class="fit_activity_item">  
	                        {{diag.userDiagEndDt | date : 'MMMM dd, yyyy'}}
		                 </span>  
		                   <span ng-if="diag.userDiagEndDt == null" class="fit_activity_item">  
	                         present
	                     </span>  
	                     <hr class="fit_hr fit_hr_pink" > 
		             </div>
		             <div class="clear"></div>
		              
	                 <%--treatments --%>
	                 <div class="clear"></div>
	                 <div style="margin-left:3em;" ng-if="showTreatment($index)"> 
			             <div   class="grid_6 fit_search_header">
			                  Treatments
			             </div>
			              <div class="clear"></div>
			             <div  class="grid_4 fit_activity_item" >
			              
				               <span class="fit_text_color"><spring:message code="act.label.chemo"/>: </span>
				               <span  > {{diag.chemoCompleted}} of {{diag.chemoTotal}} </span>
			             </div>
			              <div  class="grid_6 fit_activity_item">
				                <span class="fit_text_color">Frequency:</span>
				                 {{diag.chemFreq}}
                         </div>
			              <div class="clear"></div>
			             <div  class="grid_12 fit_activity_item" >
                          
	                           <span class="fit_text_color"><spring:message code="act.label.rad"/>: </span>
	                           <span  > {{diag.radCompleted}} of {{diag.radTotal}} </span>
                         </div>
                         <div class="clear"></div>
		             </div> 
		             <div class="clear"></div>
		             <div class="grid_14">
		              <hr class="fit_hr "  ng-if="showTreatment($index)" > 
		             </div>
		             <%--procedures --%>
		              <div class="clear"></div>
                     <div style="margin-left:3em;" ng-if="showProcedure($index)"> 
                         <div   class="grid_6 fit_search_header" ng-if="showProcedureTitle($index)">
                           Procedures
                         </div>
                          <div class="clear"></div>
                         <div  class="grid_10 fit_activity_item" >
                          
                               <span  class="fit_text_color"> {{diag.procName}} </span>
                               <span ng-if="diag.procDate != null"> - {{diag.procDate | date : 'MMMM dd, yyyy'}} </span>
                         </div>
                     </div> 
                     <div class="clear"></div>
                     <div class="grid_14">
                      <hr class="fit_hr"  ng-if="showProcedureEndLine($index)" > 
                     </div>
	            </span> 
             </div> 
                
        </div>
        <div id="privateContainer" class="grid_16 ui-widget-content" style="margin-top:20px;" 
             ng-if="userData.isCurrentUser || userData.profileData.showPrivate">
            <div class="grid_16 fit_search_top_header fit_text_color" style="padding:20px 0px 20px 10px;">
                      <spring:message code="profile.txt.privateInfo" />
             </div>
            <div class="grid_16" style="padding:10px;">
                <div class="grid_2">
                    <span class="fit_text_color"><spring:message code="reg.label.firstName" /></span><br/>
                </div>
                 <div class="grid_4 fit_text_padding">
                    <span  > {{userData.profileData.firstName}}</span><br/>
                </div>
                 <div class="clear"></div>
                <div class="grid_2">
                    <span class="fit_text_color"><spring:message code="reg.label.lastName" /></span><br/>
                </div>
                 <div class="grid_4 fit_text_padding">
                    <span  > {{userData.profileData.lastName}}</span><br/>
                </div>
                 <div class="clear"></div>
                <div class="grid_2">
                    <span class="fit_text_color"><spring:message code="reg.label.email" /></span><br/>
                </div>
                <div class="grid_4 fit_text_padding">
                    <span> {{userData.profileData.emailAddress}}</span><br/>
                </div>
                <div class="clear"></div>
                <div class="grid_2">
                    <span class="fit_text_color"><spring:message code="profile.label.birthDate"/></span><br/>
                </div>
                <div class="grid_4 fit_text_padding">
                    <span> {{userData.profileData.birthDate}}</span><br/>
                </div>
                 <div class="clear"></div>
                <div class="grid_2">
                    <span class="fit_text_color"><spring message code="profile.label.cellPhone"/></span><br/>
                </div>
                <div class="grid_4 fit_text_padding">
                    <span> {{userData.profileData.cellPhone}}</span><br/>
                </div>
            </div>
        </div>
         
        </div>
