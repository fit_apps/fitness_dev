 
	                          <div class="fit_search_header fit_text_color">
	                             <spring:message code="act.search.diagnosis"/>
	                          </div>
	                          <div  ng-repeat="diag in diagHierarchy">
		                          	<div  class="fit_search_sub_header" ng-if="displayDiagParent($index, diagHierarchy)" >
		                          		{{diag.parentDiagName}}
		                          	</div>
		                          	 <div class="fit_checkboxes fit_text_color">
			                             <input ng-model="diag.isChecked"  
			                             	 type="checkbox" ng-true-value="true" ng-false-value="false"
			                             	 ng-change="applyFilterByDiag(this)"> {{diag.childDiagName}}
			                          </div>
	                          </div>
	                         