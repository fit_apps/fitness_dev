<%@include file="header.jsp" %>
 <script src="<%=contextPath%>/js/angular/angular.js"></script>
  <script src="<%=contextPath%>/js/angular/angular-route.js"></script>
  <link href="<%=contextPath%>/angular-xeditable/css/xeditable.css" rel="stylesheet">
  <script src="<%=contextPath%>/angular-xeditable/js/xeditable.js"></script>  
  <script src="<%=contextPath%>/js/angular/common/commonService.js"></script>
  <script src="<%=contextPath%>/js/angular/activities/viewActivity.js"></script>
  <script src="<%=contextPath%>/js/angular/activities/myActivities.js"></script>
  
<script type="text/javascript">
 $(function() {

	 $('#activitiesTable').on('click', 'a[id^="activity"]', function(){
   		 var activityId = $(this).closest('tr').attr('id');
   		 angular.element('#viewActivityTabs').scope().showViewActivity(activityId);
	       
     	});
    	 
    	 
    	 $( "#deleteActivityDialog" ).dialog({
             autoOpen: false,
             title:'Delete activity' ,
             modal: true,
             position: [400,150],
             width: 285,
             height:150
         });
    	 
    	 $( "#cheerDialog" ).dialog({
             autoOpen: false,
             title:'Cheers' ,
             modal: true,
             position: [400,150],
             width: 285,
             height:150
         });    	 
    	 
    	 $( "#deleteCommentDialog" ).dialog({
             autoOpen: false,
             title:'Delete comment' ,
             modal: true,
             position: [400,150],
             width: 285,
             height:140
         });    	 
    	 
});

 
</script>

<body ng-cloak ng-app="fitApp.viewActivityModule" ng-controller="viewActivityController"	>		

<!--  See the end of this page for variables being explicitly added to the $scope -->

<!-- Tabs -->       
	<div id="viewActivityTabs" >
		<ul>
			<li><a href="#tabs-1-1">{{userDisplayNamePossessive}} {{adc.activityTypeName}}</a></li>						
			<li><a href="#tabs-my_activities" ng-click="getMyActivities()"><spring:message code="act.view.txt.activities"/></a></li>
		</ul> 
	  
		<div id="tabs-1-1" style="margin-left:10px;padding-left:1px;max-width:1140px;overflow-x:visible;overflow-y:visible;">
			<span class="container_4" ng-show="messageToShow.length > 0">
				<div class="ui-state-highlight" style="padding: 10px;">{{messageToShow}}</div>
				<br />
			</span>	
			<div class="clear"></div> 
			<span class="container_4">
				<a href="<%=contextPath%>{{returnLoc}}"><spring:message code="act.view.txt.back"/> {{returnLocText}}</a>
			</span>
			<br /><br />
			<div class="clear"></div>  
		
			<div class="clear"></div> 
			<span class="container_14 fit_activity_item"> 			
				<img class="grid_1" ng-src="<%=contextPath%>/{{allActivityTypesById[adc.activityType-1].iconPath}}" 
					title="{{allActivityTypesById[adc.activityType-1].description}}" style="border:none;" /> 
				<div class="grid_12">{{adc.activityTitle}} 
					<span  class="fit_text_color"><spring:message code="act.view.txt.whenPrep"/></span> {{adc.activityDate | date : 'EEEE, MMMM dd, yyyy'}} 
					<span  class="fit_text_color"><spring:message code="act.view.txt.wherePrep"/></span> {{formattedLocation}}
				</div>
			
	        <span class="container_2" ng-if="adc.ownerUserId == userId"> 
	                <a href="javascript:void(0);" 
	                	title=<spring:message code='txt.edit'/>
						ng-click="showEditActivityDialog(adc.activityId)">
						<span class="ui-icon inline ui-icon-pencil" style="vertical-align:bottom;"></span></a> 
	                <a href="javascript:void(0);" 
						title=<spring:message code='txt.delete'/>
						ng-click="showDeleteActivityDialog(adc.activityId, adc.activityTitle)">
						<span class="ui-icon inline ui-icon-trash" style="vertical-align:bottom;"></span></a> 
           </span> 			
			
			
                 <span class="fit_activity_item">
                    <a href="javascript:void(0);" ng-click="toggleCheer();" >
                    		<img src="<%=contextPath%>/images/icon_cheer_red.png" 
                    			ng-if="adc.cheersByUserCount > 0" style="border:none;" 
                    			alt='<spring:message code="act.label.cheer" />'>
                    		<img src="<%=contextPath%>/images/icon_cheer_grey.png" 
                    			ng-if="adc.cheersByUserCount == 0 || adc.cheersByUserCount == null " style="border:none;" 
                    			alt='<spring:message code="act.label.cheer" />'></a>
                    <a href="javascript:void(0);" id="cheerLink_{{$index}}" 
                       ng-click="showCheersDialog('<spring:message code="act.label.cheers" />')"><spring:message code="act.label.cheer"/> ({{getCount(adc.actCheersCount)}})</a>
                </span>   				
           	</span>	
           	
           	<div class="clear"></div> 
			<span ng-if="adc.distance !=null" class="container_16 fit_activity_item">
				<div class="grid_3 fit_text_color"><spring:message code="act.view.txt.distance"/></div>  
				<div class="grid_3">{{adc.distance}}</div>
           	</span>	

			<span class="container_16 fit_activity_item">
				<span ng-if="adc.startTime !=null">
					<div class="grid_3 fit_text_color"><spring:message code="act.view.txt.start"/> </div>  
					<div class="grid_2">{{adc.startTime}}</div>         		
				</span>
				<span ng-if="adc.duration !=null">
					<div class="grid_1 fit_text_color"><spring:message code="act.view.txt.duration"/> </div>  
					<div class="grid_1">{{adc.duration}}</div>
	           	</span>
           	</span>  	           	           		

			<span class="container_16 fit_activity_item">
				<span ng-if="adc.energyBeforeRating !=null" >
					<div class="grid_5 fit_text_color"><spring:message code="act.view.txt.energyBefore"/>
						<img title="{{getEnergyLevelTitle(adc.energyBeforeRating)}}"
	                		ng-src="<%=contextPath%>/images/energyIcons/enLevel_{{adc.energyBeforeRating}}.png">               
					</div>         		
				</span>
				<span ng-if="adc.energyAfterRating !=null">
					<div class="grid_4 fit_text_color"><spring:message code="act.view.txt.energyAfter"/>
						<img title="{{getEnergyLevelTitle(adc.energyAfterRating)}}"
                			ng-src="<%=contextPath%>/images/energyIcons/enLevel_{{adc.energyAfterRating}}.png">              
					</div>
	           	</span>           		
           	</span>  
			<span ng-if="adc.symptomsHelped.length > 0" class="container_16 fit_activity_item">
				<div class="grid_3 fit_text_color"><spring:message code="act.view.txt.symptoms"/> </div>  
				<div class="grid_10"> {{adc.symptomsHelped}}</div>
           	</span>  

			<span ng-if="adc.notes.length > 0" class="container_16 fit_activity_item">
				<div class="grid_3 fit_text_color"><spring:message code="act.view.txt.notes"/> </div>
				<div class="grid_10">{{adc.notes}}</div>
           	</span>           		
           						                
			<!-- Public -->
           	<div class="clear"></div> 
			<span ng-if="adc.ownerUserId == userId" class="container_16 fit_activity_item">
				<span ng-if="adc.showPublic" class="container_16">
					<div class="grid_12 fit_text_color"><spring:message code="act.view.txt.shown"/> </div> 
	          	</span> 
				<span ng-if="!adc.showPublic" class="container_16">
					<div class="grid_12 fit_text_color"><spring:message code="act.view.txt.notShown"/> </div> 
	          	</span> 
          	</span>	
          	
          	<br />
			<div class="clear"></div>
			<hr  class="fit_hr fit_hr_margin"  >

			<!--  Comments -->
			<span class="fit_activity_item">
			<img ng-if="adc.commentsByUserCount > 0" 
				src="<%=contextPath%>/images/icon_comment_red.png" style="border:none;" 
				title="<spring:message code='act.label.comments'/>">
			<img ng-if="adc.commentsByUserCount == 0 || adc.commentsByUserCount == null" 
				src="<%=contextPath%>/images/icon_comment_grey.png" 
				style="border:none;" title="<spring:message code='act.label.comments'/>">				

			<span ng-if="adc.actComCount == 1">
				{{getCount(adc.actComCount)}} <spring:message code="act.label.comment"/> </span>
			<span ng-if="adc.actComCount != 1 || adc.actComCount == null">
				{{getCount(adc.actComCount)}} <spring:message code="act.label.comments"/> </span>

			<div class="ui-widget fit_comment_text" id="commentBox">
				<textarea class="fit_comment_text" rows="1" ng-model="adc.newComment" 
				id="commentTxtArea" cols="90" maxlength="500"></textarea>
				<br/>
				<input type="button" ng-disabled="adc.newComment == null || adc.newComment.length == 0 " 
					ng-click="postActivityComment()" id="btnComment" 
					value="<spring:message code='act.label.comment'/>">
				<br/>
				<span class="fit_text_expl fit_text_color">500 chars max ({{500-adc.newComment.length}} remaining)</span>
			</div>						
					
				<div class="ui-widget fit_comment_text" id="commentBox_readonly_{{comment.commentId}}"
                      ng-repeat="comment in adc.comments">
                     <div ng-if="($index == 0 && adc.lastCommentRow < adc.actComCount) " 
                     	   class="fit_comment_link">
                     	  <a style="font-size:12px;text-decoration: none;" href="javascript:void(0);"
                     	     ng-click="showActivityComments();"><spring:message code="act.txt.showMore"/> </a>
                     </div> 
                     
                      <span editable-textarea="comment.comment"  
                            onbeforesave="updateActivityComment($data, comment)" 
                            e-rows="1" e-cols="60" e-form="commentBtnForm" >{{comment.comment}}</span>
                     
					<!-- Delete comment icon if owner is viewing-->
					<span style="float:right" ng-if="userId == comment.createdById">
						<a href="javascript:void(0);"
							title=<spring:message code='txt.delete'/>
							ng-click="showDeleteCommentDialog(comment)">
							<span class="ui-icon inline ui-icon-trash" style="vertical-align:bottom;"></span></a> 
					</span> 
					<!-- Edit comment icon if owner is viewing-->
					<span style="float:right; padding-right:5px;" ng-if="userId == comment.createdById">
						<a href="javascript:void(0);"  
							title=<spring:message code='txt.edit'/>
							ng-click="commentBtnForm.$show()"
							ng-hide="commentBtnForm.$visible">
							<span class="ui-icon inline ui-icon-pencil" style="vertical-align:bottom;"></a> 
                         </span> 
                     <br/>
                    
                     <div id="commentHour_{{comment.commentId}}"  >
                      <spring:message code="act.txt.commentBy"/>
                       <a href="<%=contextPath%>/pages/secure/dashboard/profile.jsp?viewUserId={{comment.createdById}}">{{comment.displayName}}</a>
                     	{{getCommentElapsedTime(comment.commentCreateDt, comment.commentUpdateDt)}}
                     </div>
                     
                </div>					
				<div ng-if="adc.lastCommentRow < adc.actComCount"  class="ui-widget fit_comment_text" id="commentBox_lastLink">
					<span  class="fit_comment_link">
						<a style="font-size:12px;text-decoration: none;" href="javascript:void(0);"
							ng-click="showActivityComments();"> <spring:message code="act.txt.showMore" /> </a>
					</span>   
				</div> 					
					
			</span>
	    </div>
	    	    
		<div style="padding:15px 15px 15px 0;">
			<!-- various dialogs outside of the activity feed loop -->

			<div id="deleteActivityDialog"  >
				<div  style="margin:1em 0 2em 0;">
				<spring:message code="act.view.txt.deleteQuestion"/><br />
					<div  class="fit_text_color" style="text-align:center;">					
						'{{activityTitleToDelete}}'
					</div> 
					<hr class="fit_hr">
					<!-- delete comment buttons -->
					<div style="float:right;" class="ui-widget fit_comment_text">
						<input type="button" id="btnDelActivity_Ok" ng-click="handleDeleteActivityDialog('ok')" value="OK">
						<input type="button" id="btnDelActivity_Cancel" ng-click="handleDeleteActivityDialog('cancel')" value="Cancel">
					</div>
				</div>
			</div>	
			
			<div id="cheerDialog"  >
				<div ng-if="actCheerUsers.length > 0">
					<div ng-repeat="usr in actCheerUsers">
						<div><a href="javascript:void(0);" ng-click="cheerUserJump(usr.userId)" >{{usr.displayName}}</a>						
						</div>
				    </div>
				</div> 
				<div ng-if="actCheerUsers.length == 0 || actCheerUsers == null ">
					<spring:message code="act.label.noCheers" />
				</div>
			</div>	

			<div id="deleteCommentDialog"  >
				<div  style="margin:1em 0 2em 0;">Are you sure you want to delete this comment?</div> 
				<hr class="fit_hr">
				<!-- delete comment buttons -->
				<div style="float:right;" class="ui-widget fit_comment_text">
					<input type="button" id="btnDelComment_Ok" ng-click="handleDeleteCommentDialog('ok')" value="OK">
					<input type="button"   id="btnDelComment_Cancel" ng-click="handleDeleteCommentDialog('cancel')" value="Cancel">
				</div>
			</div>	

<!-- Start - Add variables to our scope -->	    	
	<scope-add key='returnLocParm' value='<%=request.getParameter("ret") %>'/>		    
	<scope-add key='activityId' value='<%=request.getParameter("actId") %>'/>		
	<scope-add key='userId' value='<%=currentUser.getId() %>'/>			
	<scope-add key='contextPath' value='<%=contextPath%>'/>			

	<%-- Setting readyToInit to true will cause the initialization routine to be called --%>
	<scope-add key='ReadyToInit' value='true'/>	
<!-- End - Add variables to our scope -->	    	
    
	    <div id="tabs-my_activities"> 
	        <%@include file="myActivities.jsp" %>  
	    </div>
		<div id="activityDetail" style="display:none;">
	        <%@include file="activityDetail.jsp" %>
	    </div>
  </div> 
</body>
</html>