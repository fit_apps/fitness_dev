<%@include file="header.jsp" %>
 <script src="<%=contextPath%>/js/angular/angular.js"></script>
  <script src="<%=contextPath%>/js/angular/angular-route.js"></script>
  <script src="<%=contextPath%>/js/angular/common/commonService.js"></script>
  <script src="<%=contextPath%>/js/angular/activities/editActivity.js"></script>
  <script src="<%=contextPath%>/js/angular/activities/myActivities.js"></script>
  
<script type="text/javascript">
 $(function() {

    	$('#activitiesTable').on('click', 'a[id^="activity"]', function(){
   		 var activityId = $(this).closest('tr').attr('id');
   		 angular.element('#editActivityTabs').scope().showViewActivity(activityId);
	       
     	});
});
</script>

<body ng-cloak ng-app="fitApp.editActivityModule" ng-controller="editActivityController">
<!--  See the end of this page for variables being explicitly added to the $scope -->
	
<!-- Tabs -->
	<div id="editActivityTabs" >
		<ul>
			<li ng-show="createMode" ><a href="#tabs-1-1"><spring:message code='act.add.title'></spring:message></a></li>
			<li ng-show="editMode" ><a href="#tabs-1-1"><spring:message code='act.edit.title'></spring:message></a></li>			
			<li><a href="#tabs-my_activities" ng-click="getMyActivities()">My Activities</a></li>
		</ul> 

		<div id="tabs-1-1" style="margin-left:10px;padding-left:1px;max-width:1140px;overflow-x:visible;overflow-y:visible;">
			<form id="activityForm" name="activityForm" class="formElements" ng-submit="processactivityForm(activityForm.$valid)" novalidate>
				<div class="container_16 ui-widget-content" >				
	               
	            <span class="container_4" ng-show="messageToShow.length > 0">
					<div class="ui-state-highlight" style="padding: 10px;">{{messageToShow}}</div>
					<br />
				</span>	
	               
				<div ng-show="createMode" class="grid_16 fit_search_top_header fit_text_color" style="padding:20 0px 20px 10px;">
					<spring:message code='act.add.instructions'/>
				</div>
				<div ng-show="editMode" class="grid_16 fit_search_top_header fit_text_color" style="padding:20 0px 20px 10px;">
					<spring:message code='act.edit.instructions'/>
				</div>
	          
				<div class="clear"></div>
				<hr  class="fit_hr fit_hr_margin"  >
	           
				<div class="grid_16 fit_required" style="padding:20 0px 10px 10px;">
                   <spring:message code='field.required'/>
				</div>
              
				<div class="clear"></div>
				<hr  class="fit_hr fit_hr_margin"  >
				
				<!-- Activity type  -->
				<div class="grid_2 fit_text_color" style="padding:10px;" >
                        <spring:message code='act.edit.actType'/> <span class="fit_required">*</span>
				</div>
				<div class="grid_4" style="padding:10px;">
					<select ng-model="activityTypeEntered"  name="activityType" value="" required
							ng-options="myActivityType.id as myActivityType.description for myActivityType in allActivityTypes"> 								
					</select> <br/>
					<span class="fit_error_orange" ng-show="submitted && activityForm.activityType.$error.required">
						<br /><spring:message code="err.activityType.required" /></span>				
				</div>

				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >
				
				<!-- Title -->
				<div class="grid_2 fit_text_color" style="padding:10px;" >
                       <spring:message code="act.edit.actTitle"/> <span class="fit_required">*</span>
				</div>
				<div class="grid_10" style="padding:10px;">
					<input type="text" maxlength="100" value="" name=activityTitle ng-model="activityFormData.activityTitle" style="width:80%;" required /> <br/>
					<span class="fit_error_orange" ng-show="submitted && activityForm.activityTitle.$error.required">
						<br /><spring:message code="err.activityTitle.required" /></span>
					<span class="fit_error_orange" ng-show="submitted && dataValidationErr.titleError">
						<br /><spring:message code="err.activityTitle.invalid" /></span>
				</div>
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin" >

				<!-- Zip -->
				<div class="grid_3" style="padding:10px;" >
					<input type="radio" ng-model=zipOrLocation ng-change="zipOrLocationChange()" value="zip" id="zipOrLocation" name="zipOrLocation" />
					<span class="fit_text_color"><spring:message code="act.edit.actZip"/> </span>   
					<p>
						<input type="text" maxlength="10" ng-model="activityFormData.zip" ng-change="dataValidationErr.zipError=false; dataValidationErr.zipMissing=false;" 
							value="" name=zip ng-disabled="zipOrLocation != 'zip'" />
						<span class="fit_error_orange" ng-show="submitted && dataValidationErr.zipMissing">
							<br /><spring:message code="err.zip.required"/> </span>
						<span class="fit_error_orange" ng-show="submitted && dataValidationErr.zipError">
							<br /><spring:message code="err.zip.invalid"/> </span>
					</p>
				</div>
				<div class="grid_10" style="padding:10px;" >
					<!-- Location -->
					<input type="radio" ng-model=zipOrLocation ng-change="zipOrLocationChange()" value="location"  id="zipOrLocation" name="zipOrLocation" /> 
					<span class="fit_text_color"><spring:message code="act.edit.location"/></span>
					<!-- City -->
					<p style="padding-left:25px;">
						<span class="grid_1 fit_col2_padding fit_text_color"><spring:message code="act.edit.actCity"/> </span>
						<span class="fit_col2_padding"><input ng-model="activityFormData.city" ng-change="dataValidationErr.locationError=false" ng-disabled="zipOrLocation != 'location'" 
									type="text" value="" maxlength=150 style="width:50%;"></span>
					</p>
					<!--  State -->
					<p style="padding-left:25px;">                            
						<span class="grid_1 fit_col2_padding fit_text_color" ><spring:message code="act.edit.actState"/> </span>
						<span class="fit_col2_padding">   
							<select ng-model="stateProvinceEntered" ng-change="dataValidationErr.locationError=false" ng-disabled="zipOrLocation != 'location'" 
										ng-options="stateProvince.id as stateProvince.stateName for stateProvince in allStateProvinces">
							</select>
						</span>
					</p>
					<p style="padding-left:25px;"> 
						<!-- Country -->
						<span class="grid_1 fit_col2_padding fit_text_color" ><spring:message code="act.edit.actCountry"/></span>
						<span class="fit_col2_padding">
							<select ng-model="countryEntered" ng-change="dataValidationErr.locationError=false" ng-disabled="zipOrLocation != 'location'" 
										ng-options="country.id as country.countryName for country in allCountries">
							</select>
						</span>
						<span class="fit_error_orange" ng-show="submitted && dataValidationErr.locationError">
							<br /><spring:message code="err.location.data.required"/> 
						</span>   
					</p>                  
					<div class="clear"></div> 
				</div>
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >
				
				<!-- Activity Date -->
				<div class="grid_2 fit_text_color" style="padding:10px;" >
                	<spring:message code="act.edit.actDate"/> <span class="fit_required">*</span>
				</div>
				<div class="grid_3" style="padding:10px;">
					<input type="text" ng-model="activityFormData.activityDate" name="activityDate" id="activityDate" 
						placeholder="mm/dd/yyyy" required
						ng-change="dataValidationErr.activityDateUnreasonable=false; 
								   dataValidationErr.activityDateInvalid=false;" 
						ng-blur="validateActivityDate()"  >
					<span class="fit_error_orange" ng-show="submitted && activityForm.activityDate.$error.required">
						<br /><spring:message code="err.activityDate.required" /></span>
					<span class="fit_error_orange" ng-show="dataValidationErr.activityDateUnreasonable ">
						<br /><spring:message code="err.activityDate.unreasonable"></spring:message> </span>
					<span class="fit_error_orange" ng-show="dataValidationErr.activityDateInvalid ">
						<br /><spring:message code="err.activityDate.invalid"></spring:message> </span>
				</div>
						                
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >

					<!-- Start time -->
				<div class="grid_2 fit_text_color" style="padding:10px;" >
	                <spring:message code="act.edit.actStart"/>
				</div>
				
				<div class="grid_3" style="padding:10px;">
					<input type="text" ng-model="startTimeEntered"  ng-change="dataValidationErr.startTimeError=false"  
						ng-blur="validateStartTime()" name="startTime" id="startTime" placeholder="00:00:00" >
					<span class="fit_error_orange" ng-show="dataValidationErr.startTimeError ">
						<br/><spring:message code="err.activityStartTime.invalid"></spring:message><br /></span>
					<span class="fit_text_expl fit_text_color">HH:MM:SS</span>
				</div>
				<div class="grid_2" style="padding:10px;">
                   	<input type="radio" ng-model=startTimeAmPm ng-change="validateStartTime()" value="AM" id="startTimeAmPm" name="startTimeAmPm" /> <span class="fit_text_color">AM</span>  
					<input type="radio" ng-model=startTimeAmPm ng-change="validateStartTime()" value="PM" id="startTimeAmPm" name="startTimeAmPm" /> <span class="fit_text_color">PM</span>
				</div>
					
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >
                    
				<!-- duration -->
				<div class="grid_2 fit_text_color" style="padding:10px;" >
					<spring:message code="act.edit.actDuration"/>
				</div>
				<div class="grid_3" style="padding:10px;">
					<input type="text" ng-model="durationEntered" ng-change="dataValidationErr.durationError=false" 
						ng-blur="validateDuration()" name="duration" id="duration" placeholder="00:00:00" />
					<span class="fit_error_orange" ng-show="dataValidationErr.durationError ">
						<br/><spring:message code="err.activityDuration.invalid"></spring:message><br /></span>
					<span class="fit_text_expl fit_text_color">HH:MM:SS</span>  
				</div>

				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >

				<!-- Distance -->
				<div class="grid_2 fit_text_color" style="padding:10px;" >
					<spring:message code="act.edit.actDistance"/>
				</div>
				<div class="grid_8" style="padding:10px;">					
					<input type="text" ng-model="activityFormData.distance" ng-change="dataValidationErr.distanceError=false" 
						ng-blur="validateDistance()" name="distance" id="distance" placeholder="0.0" /><br />
					<span class="fit_error_orange" ng-show="dataValidationErr.distanceError "> 
						<spring:message code="err.activityDistance.invalid"/><br/> </span>
					
					<span class="fit_text_expl fit_text_color">Only for those activities that include distance</span>
				</div> 
                   
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >
                                          
				<!-- Energy before -->
				<div class="grid_11 fit_text_color" style="padding:10px;" >
                        <spring:message code="act.label.energy.before"/>:
					<div class="container_11" style="margin:5px 10px 3px 0px;">
						<span class="prefix_2 grid_1 fit_text_expl fit_text_color">Tired</span>
						<hr  class="grid_5 fit_hr fit_hr_margin"  >
						<span class="grid_2 fit_text_expl fit_text_color">High energy</span> 
					</div> 							                 
	                <div class="prefix_2 grid_8 suffix_6" style="margin:-15px 10px 3px 10px; float:left; display:inline-block;">
       					<input ng-repeat-start="energyLevel in energyLevels" type="radio" value="{{energyLevel.val}}" name="energyBefore"
         						ng-model="$parent.energyBefore.rating" ng-change="energyBeforeChange(energyLevel)"      >
       					<span style="position:relative; top:40px; right:30px;"  ng-repeat-end>
       						<img title="{{energyLevel.messageText}}" class="images" ng-src="{{energyLevel.imageUrl}}"
							  	style="height: 32px; width: 32px;" />
       					</span>
   					</div>     
					<div class="clear"></div>   	
					<div class="container_14" style="margin:40px 10px 3px 0px;">
						<span ng-show="energyBefore.rating<=0" class="prefix_2 grid_3 suffix_2 fit_text_expl fit_text_color"><br /></span>
						<span  ng-show="energyBefore.rating>0" class="prefix_4 grid_3 fit_text_expl fit_text_color">
							({{energyBefore.text}})							
						</span> 
					</div> 							
						</div>

				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >

				<!-- Energy after -->                                          
				<div class="grid_11 fit_text_color" style="padding:10px;" >
                        <spring:message code="act.label.energy.after"/>::
					<div class="container_11" style="margin:5px 10px 3px 0px;">
						<span class="prefix_2 grid_1 fit_text_expl fit_text_color">Tired</span>
						<hr  class="grid_5 fit_hr fit_hr_margin"  >
						<span class="grid_2 fit_text_expl fit_text_color">High energy</span> 
					</div> 
	                <div class="prefix_2 grid_8 suffix_6" style="margin:-15px 10px 3px 10px; float:left; display:inline-block;">
       					<input ng-repeat-start="energyLevel in energyLevels" type="radio" value="{{energyLevel.val}}" name="energyAfter"
         						ng-model="$parent.energyAfter.rating" ng-change="energyAfterChange(energyLevel)"      >
       					<span style="position:relative; top:40px; right:30px;"  ng-repeat-end>
       						<img title="{{energyLevel.messageText}}" class="images" ng-src="{{energyLevel.imageUrl}}"
							  	style="height: 32px; width: 32px;" />
       					</span>
   					</div>     
					<div class="clear"></div>   	
					<div class="container_14" style="margin:40px 10px 3px 0px;">
						<span ng-show="energyAfter.rating<=0" class="prefix_2 grid_3 suffix_2 fit_text_expl fit_text_color"><br /></span>
						<span  ng-show="energyAfter.rating>0" class="prefix_4 grid_3 fit_text_expl fit_text_color">
							({{energyAfter.text}})							
						</span> 
					</div> 							
						</div>
                     
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >
                   
				<!-- Symptom help -->
				<div class="grid_12 fit_text_color" style="padding:10px;" >
					<spring:message code="act.edit.actHelped"/>
					<div class="grid_10" style="padding:10px;">
						<input type="text" maxlength="200" value="" name=symptomsHelped placeholder="Enter symptoms separated by commas" 
							ng-model="activityFormData.symptomsHelped" style="width:80%;" /> <br/>
						<span class="fit_error_orange" ng-show="submitted && dataValidationErr.symptomError">
							<spring:message code="err.symptomsHelped.invalid" /><br /></span>
						<span class="fit_text_expl fit_text_color">200 chars max ({{200-activityFormData.symptomsHelped.length}} remaining)</span>								
					</div>														
				</div>  
				
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >
                      
				<!-- Notes -->
				<div class="grid_12 fit_text_color" style="padding:10px;" >
					<spring:message code="act.edit.actNotes"/><br />
					<div class="grid_10" style="padding:10px;">
						<textarea rows="4" cols="100" maxlength="200" value="" name=notes ng-model="activityFormData.notes" style="width:80%;" >
						</textarea> <br/>
						<span class="fit_error_orange" ng-show="submitted && dataValidationErr.notesError">
							<spring:message code="err.notes.excessive" /><br /></span>
						<span class="fit_text_expl fit_text_color">200 chars max ({{200-activityFormData.notes.length}} remaining)</span>								
					</div>	                            
				</div>   
  
				<div class="clear"></div>   
				<hr  class="fit_hr fit_hr_margin"  >
                      
				<!-- Public -->
				<div class="grid_12 fit_text_color" style="padding:10px;" >
					<input type="radio" ng-model=showHidePublic value="Show" id="showHidePublic" name="showHidePublic" >
					<span class="fit_text_color"><spring:message code="act.edit.actShow"/> </span>
					<p/>
					<input type="radio" ng-model=showHidePublic value="Hide" id="showHidePublic" name="showHidePublic">
					<span class="fit_text_color"><spring:message code="act.edit.actHide"/> </span>
				</div>  
                       
				<div class="clear"></div>  
				<div class="grid_6"> &nbsp;</div>
				<div class="grid_9" style="padding:10px;" >
					<span ng-show="formSubmittable" style="margin-right:10px;">
						<input type="submit"  ng-show="editMode" buttonName="saveActivity" id="saveActivity" value="Save" title="<spring:message code='btn.save' />">
						<input type="submit"  ng-show="createMode" buttonName="createActivity" id="createActivity" value="Create" title="<spring:message code='btn.create' />">
					</span>
					<button buttonname="cancelActivity" id="cancelActivity"  name="cancelActivity"><spring:message code="btn.cancel"></spring:message> </button>
				</div>    
			  	</div>
	       </form>     

	    </div>

<!-- Start - Add variables to our scope -->	    
	<scope-add key='userId' value='<%=currentUser.getId() %>'/>			
	<scope-add key='contextPath' value='<%=contextPath%>'/>			
	<%-- 'create' will be true if we're to add a new activity, or null if we are editing an existing one. A change to '$scope.create' will call init()		 --%>
	<scope-add key='create' value='<%=request.getParameter("create")%>'/>	    
<!-- End - Add variables to our scope -->	    
	    
	    <div id="tabs-my_activities"> 
	       
	        <%@include file="myActivities.jsp" %>  
	    </div>
		<div id="activityDetail" style="display:none;">
	        <%@include file="activityDetail.jsp" %>
	    </div>
  </div> 
</body>
</html>