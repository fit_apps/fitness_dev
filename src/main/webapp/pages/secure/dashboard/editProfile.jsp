<script type="text/javascript">
 $(function() {
	 $('#addNewDiagContainer').on('click', 'a[id^="addNewDiag"]', function(){
		 $( "#diagFromDate" ).datepicker({
             showOn: "both",
             buttonImage: '<%=contextPath%>/images/icon_calendar.png',
             buttonImageOnly: true,
             dateFormat: "yy-mm-dd"
		 });
		$( "#diagToDate" ).datepicker({
		       showOn: "both",
		       buttonImage: '<%=contextPath%>/images/icon_calendar.png',
		       buttonImageOnly: true,
		       dateFormat: "yy-mm-dd"
		});
		
		$( "#treatDate" ).datepicker(
		{
		    showOn: "both",
		    buttonImage: '<%=contextPath%>/images/icon_calendar.png',
		    buttonImageOnly: true,
		    dateFormat: "yy-mm-dd"
		
		});
		$("input[id^='spin_profile']").spinner({
			  min: 0
		 });
	 });
	 
	   	 
	 $( "input[id^='diagFromDate']" ).datepicker({
         showOn: "both",
         buttonImage: '<%=contextPath%>/images/icon_calendar.png',
         buttonImageOnly: true,
         dateFormat: "yy-mm-dd"
     });
    $( "#diagToDate" ).datepicker({
           showOn: "both",
           buttonImage: '<%=contextPath%>/images/icon_calendar.png',
           buttonImageOnly: true,
           dateFormat: "yy-mm-dd"
    });
    
    $( "#treatDate" ).datepicker(
    {
        showOn: "both",
        buttonImage: '<%=contextPath%>/images/icon_calendar.png',
        buttonImageOnly: true,
        dateFormat: "yy-mm-dd"
    
    });
    $("input[id^='spin_profile']").spinner({
          min: 0
     }); 
    
  
  $( "#bday" ).datepicker({
      showOn: "both",
      buttonImage: '<%=contextPath%>/images/icon_calendar.png',
      buttonImageOnly: true,
      dateFormat: "yy-mm-dd"
 });
  
  $( "#editProfileSave" ).button();
  $( "#editProfileCancel").button();
  
  //procDate_
 
 <%--  $("input[id^='procDate']").datepicker({
	      showOn: "button",
	      buttonImage: '<%=contextPath%>/images/icon_calendar.png',
	      buttonImageOnly: true,
	      dateFormat: "yy-mm-dd"
	 }); --%>
  <%-- $("#procDate_1").datepicker({
      showOn: "button",
      buttonImage: '<%=contextPath%>/images/icon_calendar.png',
      buttonImageOnly: true,
      dateFormat: "yy-mm-dd"
 }); --%>
 $( "#deleteDiagnosisDialog" ).dialog({
     autoOpen: false,
     title:'Delete diagnosis' ,
     modal: true,
     position: [400,150],
     width: 285,
     height:140
 });
  
 });
 
 </script>
            
    <div ng-init="initEditProfile('<%=currentUser.getId() %>', '<%=contextPath%>', '${param.viewUserId}')" >
    <form id="editProfileForm" name="editProfileForm" ng-submit="submitEditProfile(editProfileForm.$valid)" novalidate ng-cloak>
        <div class="container_16">
        <div class="grid_16 fit_required" style="padding:20 0px 10px 10px;">
                  * <spring:message code="field.required" />
        </div>  
        <div id="requiredContainer" class="grid_16 ui-widget-content">
        <div class="grid_16 fit_search_top_header fit_text_color" style="padding:20 0px 20px 10px;">
                  <spring:message code="profile.txt.publicInfo" />
         </div>
         <div class="clear"></div>
            <div class="grid_2 fit_text_color" style="padding:10px;" >
                <spring:message code="reg.label.displayName" /><span class="fit_required"> *</span>
            </div>
            <div class="grid_10" style="padding:10px;">
              <input type="text" name="displayName" value="{{userProfData.displName}}" style="width:60%;" 
              		 ng-model="userProfData.displName" required/><br/>
               <span class="fit_error_nopad fit_text_expl"  ng-show="submittedEditProfileForm && editProfileForm.displayName.$dirty && editProfileForm.displayName.$error.required" >
					<spring:message code="err.displayName.required" />
			   </span>
             </div>
              <div class="clear"></div>
                
                  <div class="grid_2" style="padding:10px;" >
                   <input type="radio" ng-model="userProfData.addressRadio" value="zipCode" ng-change="selectedAddress('zipCode')" >
                   <span class="fit_text_color" style="padding-right:5px;"> <spring:message code="profile.label.zipCode" /> </span>   
                 </div>
                  <div class="grid_2 fit_text_color" style="padding:10px;" id="zipCodeContainer">
	              	    <input type="text" ng-model="userProfData.zipCode" size="8"/> &nbsp;
               			<spring:message  code="txt.or" /> </span>
                 </div>
                 <div class="clear"></div>
                 
                 <div class="grid_2 fit_text_color" style="padding:10px;" >
                    
                    <input type="radio" ng-model="userProfData.addressRadio" value="location"   ng-change="selectedAddress('location')">
                    <span style="padding-right:5px;"> <spring:message code="profile.label.location" /> </span>
                 </div>
                  <div id="locationContainer">
                  <div class="clear"></div>
                   <div class="grid_2 fit_text_color" style="padding:10px;" >
		                <span style="padding-right:5px;"> <spring:message code="profile.label.streetAddress" /> </span>   
		            </div>
                  <div class="grid_10 fit_text_color" style="padding:10px;">  
                  		<div style="padding-bottom:5px;">
                  			<input type="text"  style="width:60%;" ng-model="userProfData.address1" />
                  		</div>
                  		<div>
                  			<input type="text"  style="width:60%;" ng-model="userProfData.address2" />
                  		</div>
                  </div>
                   <div class="clear"></div>
                   <div class="grid_2 fit_text_color" style="padding:10px;" >
		                <span style="padding-right:5px;">  <spring:message code="profile.label.city" /> </span>   
		            </div>
                  <div class="grid_10 fit_text_color" style="padding:10px;">  
                  		<div style="padding-bottom:5px;">
                  			<input type="text"  style="width:60%;" ng-model="userProfData.city"/>
                  		</div>
                  </div>
                  <div class="clear"></div> 
                 <div class="grid_2 fit_text_color" style="padding:10px;">  
                    <span style="padding-right:5px;"> <spring:message code="profile.label.country" /> </span>  
                 </div>
                 <div class="grid_3 fit_text_color" style="padding:10px;">
                 {{userProfData.countrySelected}}
                      <select style="width:100%;" ng-model="userProfData.countrySelected"  
                      		  ng-options="country.id as country.countryName for country in allCountries">
					  </select>    
                 </div>
                  <div class="grid_1 fit_text_color" style="padding:10px;">  
                    <span > <spring:message code="profile.label.state" /> </span>  
                 </div>
                  <div class="grid_4 fit_text_color" style="padding:10px 10px 10px 0px;margin-left:0px;">   
                  {{userProfData.stateSelected}}
                       <select style="width:100%;" ng-model="userProfData.stateSelected"  
                               ng-options="state.id as state.stateName for state in allStateProvinces">
					    </select>   
                    </div>
            </div>
            
        </div>
        
        
        <div id="cancerSurvContainer" class="grid_16 ui-widget-content " style="margin-top:20px;padding-bottom:20px;">
        
            <div style=" margin:5px;padding:5px;" class="fit_text_color">
	              <div class="clear"></div>
	             
                 <div class="grid_2 fit_text_color fit_search_top_header"  style="padding:10px;">
                   <spring:message code="profile.label.cancerSurvivorDt" />
                </div>
                <%-- cancer survivor date --%>
                <div  class="fit_text_color grid_4" style="padding:10px;" >
                    <input type="date" id="treatDate" ng-model="userProfData.healthyDate" 
                           ng-change="isValidCancerSurvDate(userProfData.healthyDate)"> 
                    <span ng-show="cancSurvDt.isValid!=null && !cancSurvDt.isValid" class="fit_error_nopad fit_text_expl" >
                    	<br/>
                    	<spring:message code="err.date.invalid"/>
                    </span>
                </div>  
	        </div>
	    </div>         
        
        
        <div id="diagnosisContainer" class="grid_16 ui-widget-content " style="margin-top:20px;padding-bottom:20px;">
            <div style=" margin:5px;padding:5px;border-bottom:thin solid #d1e0e0;" class="fit_text_color">
	             <div class="grid_8 fit_search_top_header" style="padding:20px 0px 10px 10px;">
	                <spring:message code="profile.label.diagnosis" />
	             </div>
	             <div class="clear"></div>
	             <div  class="grid_3" style="padding:10px;" id="addNewDiagContainer">
	             
	                  <span ng-click="addDiagnosis()" class="ui-icon inline ui-icon-plus" style="vertical-align:bottom;"></span>  
                      <a href="javascript:void(0);" id="addNewDiagLink" ng-click="addDiagnosis()" ><spring:message code="profile.txt.addNewDiag" /> </a>  
                 </div>
                  <div  class="grid_1" style="padding:10px;" ng-if="selectedDiagnosis != null">
                      <spring:message code="txt.or" />
                 </div>
                 <div class="grid_10" style="padding:10px;" ng-if="userEditData.length > 0">
                     <spring:message code="profile.label.viewExistingDiag" /> 
                     {{selectedDiagnosis.refDiagTypeId}} <select  ng-change="loadDifferentDiagnosis(selectedDiagnosis);"   
                              ng-model="selectedDiagnosis" 
                              ng-options="diag as diag.refDiagTypeName for diag in userEditData">
                      </select>   
                    </div> 
                   <div class="clear"></div>
	               
             </div>
             <div id="deletedDiagConfirmContainer" class="grid_12 fit_search_top_header fit_hidden" style="padding:20px 0px 10px 10px;">
	        	<div class="ui-state-highlight" style="padding:5px;">
	        		{{deletedDiagName}} <spring:message code="profile.txt.diagDeleteSuccess" />
	        	</div>
	        </div>
	        <div class="clear"></div>
	        <div id="noer" ng-if="userEditData.length == 0">
	        	<div class="grid_8 fit_text_color"  style="padding:20px 0px 10px 10px;">
		         	<spring:message code="profile.txt.noDiagnosis" />
		         </div>
	        </div>
	        <div id="diagDetailsContainer" >
             <div class="grid_8 fit_search_top_header fit_text_color"  style="padding:20px 0px 10px 10px;">
	             
	             <span >{{selectedDiagnosis.refDiagTypeName}}
		             <span ng-if="selectedDiagnosis.userDiagIsPrimary"> - <spring:message code="profile.txt.primDiag" /> </span>
	             </span>
             </div>
             <div class="grid_2">
                 &nbsp;
             </div>
              <div class="grid_3"  >
                &nbsp;<br>
             	<span class="ui-icon ui-icon-trash inline" ng-click="showDeleteDiagnosisDialog()" ></span>
             	<a href="javascript:void(0);" ng-click="showDeleteDiagnosisDialog()"><spring:message code="profile.txt.deleteDiag" /> </a>
             </div>
             
              <div class="clear"></div>
              <div class="grid_10 fit_text_color" style="padding:10px;"> <spring:message code="profile.txt.onePrimDiag" />
                      <input type="checkbox" ng-checked="selectedDiagnosis.userDiagIsPrimary"> 

             </div>
              <div class="clear"></div>
                <div class="grid_4 fit_text_color" style="padding:10px;">
                    <spring:message code="profile.label.cancerType" /> <span class="fit_required"> *</span><br/>
                </div>
                <div class="grid_4"  style="padding:10px;">
                {{changedDiagnosis}}
                    <select required name="changedDiagnosisSelect"
	                    ng-model="changedDiagnosis" ng-change="changeDiagnosis(changedDiagnosis)"
	                    ng-options="diag.childDiagId as diag.childDiagName group by diag.parentDiagName for diag in diagHierarchy">
                    </select>  <br>
                     <span class="fit_error_nopad fit_text_expl"  
                     		ng-show="submittedEditProfileForm &&  changedDiagnosis==-1" >
						<spring:message code="profile.label.selectDiag" />
			   		</span> 
                </div>
             <div class="clear"></div>           
               <div class="grid_4 fit_text_color" style="padding:10px;" >
                  <span style="padding-right:10px;"><spring:message code="profile.label.from" /></span> <br/>
                  <input id="diagFromDate"  type="date" placeholder="yyyy-MM-dd" ng-model="selectedDiagnosis.userDiagStartDt"
                         ng-change="isValidDiagDate(selectedDiagnosis.userDiagStartDt, selectedDiagnosis.userDiagEndDt)">
                   <span class="fit_error_nopad fit_text_expl"  ng-show="diagDate.fromDateErr != null" >
                        <br/>
                   		<spring:message code="err.diag.dateFrom"/>
                   	</span>
                   	 <span class="fit_error_nopad fit_text_expl"  ng-show="diagDate.fromDateInvalid != null" >
                   	 	<br/>
                   		<spring:message code="err.date.invalid"/>
                   	</span>
                   	 
                </div>
                <div class="grid_4 fit_text_color" style="padding:10px;" id="diagToContainer">
                   <span style="padding-right:10px;"><spring:message code="profile.label.to" /></span> <br/>
                   
                  <input id="diagToDate" type="date"    
                  		 ng-model="selectedDiagnosis.userDiagEndDt"   
                  		 ng-change="isValidDiagDate(selectedDiagnosis.userDiagStartDt, selectedDiagnosis.userDiagEndDt)">
                  		
                  <span class="fit_error_nopad fit_text_expl"  ng-show="diagDate.toDateErr != null">
                  		 <br/>
                  		<spring:message code="err.diag.dateTo" /> 
                  </span>
                   <span class="fit_error_nopad fit_text_expl"  ng-show="diagDate.toDateInvalid" >
                   	 	<br/>
                   		<spring:message code="err.date.invalid"/>
                   	</span>
                </div>
                 <div class="clear"></div>
                <div class="grid_16 fit_search_top_header fit_text_color" style="padding:20 0px 20px 10px;">
                  <spring:message code="profile.txt.treatmPhases" />
                </div>
                 <div class="grid_5 fit_text_color" style="padding:10px;" >
                    <spring:message code="profile.label.numOfChemTreats" />
                </div>
                <div class="grid_6"  style="padding:10px;">
                    <input id="spin_profileChemoSpinnerStart" size="1" ng-model="selectedDiagnosis.chemoCompleted" name="value" value="0"> 
                    <span style="padding:0px 5px 0px 5px">Of</span>
                    <input id="spin_profileChemoSpinnerEnd" size="1" name="value" ng-model="selectedDiagnosis.chemoTotal"> 
                </div>
                <div class="clear"></div>
                 <div class="grid_5 fit_text_color" style="padding:10px;" >
                    <spring:message code="profile.label.freqOfChemTreats" />
                </div>
                <div class="grid_6"  style="padding:10px;">
                     <select 
                        ng-model="chemoFreqSelected" 
                        ng-options="freq.id as freq.displayName for freq in chemoFreqAll">
                    </select>  
                </div>
                <div class="clear"></div>
                <div  class="fit_text_color grid_5"   style="padding:10px;">
                       <spring:message code="profile.label.numOfRadTreats" />
                   </div>
                   <div class="grid_6" style="padding:10px;">
                       <input id="spin_profileRadioSpinnerStart" name="value" size="1" ng-model="selectedDiagnosis.radCompleted"> 
                         <span style="padding:0px 5px 0px 5px">Of</span>
                       <input id="spin_profileRadioSpinnerEnd"  name="value" ng-model="selectedDiagnosis.radTotal" size="1">   
                   </div>
                    
                 <div class="clear"></div>
                <div class="grid_16 fit_search_top_header fit_text_color" style="padding:20px 0px 20px 10px;">
                  <spring:message code="profile.txt.procedures" />
                </div>
                <div class="clear"></div>
                  <div class="grid_12" style="padding:0px 0px 0px 10px;">
                    
                    <span ng-click="addProcedure($index)" class="ui-icon inline ui-icon-plus" style="vertical-align:bottom;"></span>
                    <a href="javascript:void(0);" ng-click="addProcedure($index)"><spring:message code="profile.txt.addNewProcedure" /></a>
                  </div>
                <div class="clear"></div>
                
                <%--procedures --%>
                <div ng-repeat="procedure in selectedDiagnosis.procArr">
                     <div class="grid_3 fit_text_color"  style="padding:10px;">
	                  <spring:message code="profile.label.procType" /><br>
	                     <select 
	                        ng-model="procedure.procId" 
	                        ng-options="procType.id as procType.name for procType in cancerProcTypesAll">
                    </select>  
	                </div>
	                 <div class="grid_3 fit_text_color"  style="padding:10px 0px 10px 10px;">
                      <spring:message code="profile.label.procDate" /> <br>
                        <input type="text" ng-model="procedure.procDate" ui-date="dateOptions" 
                               ui-date-format="yy-mm-dd"  readonly="readonly" > 
                    </div>
                    <div class="grid_1" style="padding:10px 0px 10px 10px;">
                       &nbsp;<br>
                    	<span class="ui-icon ui-icon-trash" ng-click="deleteProcedure($index)"></span>
                    </div>
	                <div class="clear"></div>
                </div>
           </div>
        </div>
        <div id="privateContainer" class="grid_16 ui-widget-content" style="margin-top:20px;">
            <div class="grid_16 fit_search_top_header fit_text_color" style="padding:20px 0px 20px 10px;">
                      <spring:message code="profile.txt.privateInfo" />
             </div>
            <div class="grid_16">
                <input type="checkbox" ng-model="userProfData.showPrivate"><span class="fit_text_color" style="padding:20px 0px 20px 10px;">Make it public</span>
            </div>
            <div class="grid_16" style="padding:10px;">
                <div class="grid_4">
                    <span class="fit_text_color"><spring:message code="reg.label.firstName" /> </span><br/>
                    <input type="text" id="firstName" ng-model="userProfData.firstName">
                </div>
                <div class="grid_4">
                    <span class="fit_text_color"><spring:message code="reg.label.lastName" /></span><br/>
                    <input type="text" id="lastName" ng-model="userProfData.lastName">
                </div>
            </div>
            <div class="grid_16" style="padding:10px;">
                <div class="grid_4">
                    <span class="fit_text_color"><spring:message code="reg.label.email" /></span><br/>
                    <input type="email" name="email" ng-model="userProfData.emailAddress">
                    <br/>
                    <span class="fit_error_nopad fit_text_expl" ng-show="submittedEditProfileForm && editProfileForm.email.$error.email">
                    	<spring:message code="err.email.notValid" />
                    </span>
                    
                </div>
                <div class="grid_4">
                    <span class="fit_text_color"><spring:message code="profile.label.birthDate" /></span><br/>
                    <input type="date" placeholder="yyyy-MM-dd" id="bday" ng-model="userProfData.birthDate">
                </div>
            </div>
            <div class="grid_16" style="padding:10px;">
                <div class="grid_4">
                    <span class="fit_text_color"><spring:message code="profile.label.cellPhone" /></span><br/>
                    <input type="text" id="cell" ng-model="userProfData.cellPhone">
                </div>
                
            </div>
        </div>
        <div id="changePassword" class="grid_16 ui-widget-content" style="margin-top:20px;">
        allPasswordsValid {{allPasswordsValid}}
            <div class="grid_16 fit_search_top_header fit_text_color" style="padding:20px 0px 20px 10px;">
                      <spring:message code="profile.txt.changePass" />  
             </div>
             <div class="grid_16">
                <div class="grid_2 fit_text_color" style="padding:10px;" >
                    <spring:message code="profile.label.currentPass" />
                </div>
                <div class="grid_7" style="padding:10px;">
                  <input type="password" ng-change="resetSubmitted()" style="width:100%;"  
                  		 name="currentPassword" ng-model="userProfData.currentPass"/> <br/>
                  
                  <span class="fit_error_nopad fit_text_expl" ng-show="submittedEditProfileForm && passwordErr.emptyCurPass">
                  	<spring:message code="profile.label.currPass" />
                  </span>
                 
                  <span class="fit_error_nopad fit_text_expl"  ng-show="submittedEditProfileForm && !passwordErr.validCurrPass">
                  	<spring:message code="err.password.wrong" /> <a href="<%=contextPath%>/pages/registration/resetPassword.jsp"><spring:message code="profile.password.reset" /></a> it if you forgot it.
                  </span>
                 </div>
             </div>
             <div class="grid_16">
               
                <div class="grid_2 fit_text_color" style="padding:10px;" >
                    <spring:message code="profile.label.newPass" />
                </div>
                <div class="grid_7" style="padding:10px;">
	                  <input style="width:100%;" ng-change="resetSubmitted()" maxlength="15" 
	                   type="password" name="newPassword"  ng-model="userProfData.newPass"/>
	                   
	                   <span class="fit_text_expl fit_text_color" ng-show="userProfData.newPass.length > 0">
	                    	<br ng-show="userProfData.newPass.length > 0"/>
	                   		{{userProfData.newPass.length}} <spring:message code="profile.txt.characters" /> 
	                   	</span>
	                  
	                    <span class="fit_text_expl fit_error_nopad" ng-show="submittedEditProfileForm && passwordErr.hasRuleError && !passwordErr.passwordLengthErr">
	                    	 <br ng-show="submittedEditProfileForm && passwordErr.hasRuleError && !passwordErr.passwordLengthErr"/>
	                    	<spring:message code="err.password.rules"></spring:message> 
	                    </span>
	                     
	                    <span class="fit_text_expl fit_error_nopad" ng-show="submittedEditProfileForm && passwordErr.passwordLengthErr">
	                    	<spring:message code="err.password.length"></spring:message> 
	                    </span>
	                    
	                     <span class="fit_text_expl fit_error_nopad" ng-show="submittedEditProfileForm && passwordErr.sameAsCurrent">
	                    	<br>
	                    	<spring:message code="err.password.sameAsCurrent"></spring:message> 
	                    </span>
                </div>
                 <div class="grid_4 fit_text_color fit_text_expl" >
              		<span><spring:message code="reg.pass.length" /> </span><br/>
          			<span ><spring:message code="reg.pass.upperCase"/> </span><br/>
          			<span ><spring:message code="reg.pass.digit"/> </span> <br/>
          			<span ><spring:message code="reg.pass.chars"/> </span>
                </div>
             </div>
             <div class="grid_16">
                <div class="grid_2 fit_text_color" style="padding:10px;" >
                    <spring:message code="profile.label.confirmPass" />
                </div>
                <div class="grid_7" style="padding:10px;">
                  <input style="width:100%;" ng-change="resetSubmitted()" type="password" maxlength="15" 
                  		name="confPassword" ng-model="userProfData.confirmNewPass"/><br/>
                   <span class="fit_text_expl fit_text_color" ng-show="userProfData.confirmNewPass.length > 0">
                   		{{userProfData.confirmNewPass.length}} <spring:message code="profile.txt.characters" /> 
                   	</span>		
                  		
                   <span class="fit_error_nopad fit_text_expl" ng-show="submittedEditProfileForm && passwordErr.matches!= null && !passwordErr.matches">
                   	    <br/>
                   		<spring:message code="err.password.dontMatch"></spring:message> 
                   </span>
                </div>
             </div>
             <div class="clear"></div>  
             <div class="grid_6"> &nbsp;</div>
             <div class="grid_9" style="padding:10px;" >
                <span style="margin-right:10px;">
                   <input type="submit"   buttonname="editProfileSave"  id="editProfileSave" value="Save" title="<spring:message code='btn.save' />">
                </span>
                <button buttonname="editProfileCancel" id="editProfileCancel"><spring:message code="btn.cancel" /> </button>
             </div>
             
        </div>
        </div>
        </form>
        
        <div id="deleteDiagnosisDialog"  >
		    <div  style="margin:1em 0 2em 0;">
		      Are you sure you want to delete this diagnosis?
		    </div> 
		    <hr class="fit_hr">
		    <!-- delete comment buttons -->
		    <div style="float:right;" class="ui-widget fit_comment_text">
			<input type="button" id="btnUntag_Ok" ng-click="handleDeleteDiagnosisDialog('ok')"
			       value="OK">
			  <input type="button"   id="btnUntag_Cancel" ng-click="handleDeleteDiagnosisDialog('cancel')"
			           value="Cancel">
			</div>
   		</div>
      </div>  