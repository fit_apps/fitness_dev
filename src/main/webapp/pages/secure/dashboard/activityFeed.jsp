<%@include file="header.jsp" %>
 <script src="<%=contextPath%>/js/angular/angular.js"></script>
  <script src="<%=contextPath%>/js/angular/angular-route.js"></script>
      <link href="<%=contextPath%>/angular-xeditable/css/xeditable.css" rel="stylesheet">
    <script src="<%=contextPath%>/angular-xeditable/js/xeditable.js"></script>
    <script src="<%=contextPath%>/js/angular/common/commonService.js"></script>
  <script src="<%=contextPath%>/js/angular/diagnoses/diagnoses.js"></script>
  <script src="<%=contextPath%>/js/angular/activities/activities.js"></script>
  <script src="<%=contextPath%>/js/angular/activities/activityTypes.js"></script>
  <script src="<%=contextPath%>/js/angular/activities/myActivities.js"></script>
	<script src="<%=contextPath%>/js/angular/activities/activityDetail.js"></script>
    <script>
    
    $(function() {

    	$('#activitiesTable').on('click', 'a[id^="activity"]', function(){
   		 var activityId = $(this).closest('tr').attr('id');
   		 angular.element('#tabs').scope().showViewActivity(activityId);
	       
     	}); 
	    	 
    	 $( "#deleteCommentDialog" ).dialog({
             autoOpen: false,
             title:'Delete comment' ,
             modal: true,
             position: [400,150],
             width: 285,
             height:140
         });
    	 
    	 $("input[id^='btnComment']").button();
    	 
    	 $("input[id^='btnDelComment']").button();
    	 
    	  // all dialogs that starts with diagnosisInfoDialog
         $( "div[id^='diagnosisInfoDialog']" ).dialog({
             autoOpen: false,
             width:400,
             modal: true,
             title: "Diagnosis details",
             position: [400,100] 
         });
    	  
         $("#searchCancerFreeDt").datepicker({
             showOn: "button",
             buttonImage: '<%=contextPath%>/images/icon_calendar.png',
             buttonImageOnly: true

        });
    });
    
    </script>
<body ng-cloak ng-app="fitApp.activitiesModule" ng-controller="activitiesController" ng-init="init('<%=currentUser.getId() %>','<%=contextPath%>')">
  <!--   I am  {{userId}} {{contextPath}} -->
<!-- Tabs -->
    
   <div id="tabs" >
	    <ul>
	        <li><a href="#tabs-1-1"><spring:message code="act.title.actFeed"/></a></li>
	        <li><a href="#tabs-1-2" ng-click="getMyActivities()"><spring:message code="act.title.myActivities" /></a></li>
	    </ul> 
  
    <div id="tabs-1-1" class="fit_tab" style="min-height:500px;">
         
            <div class="container_16">
           
            	<span class="container_4" ng-show="lastMsgText.length > 0">
					<div class="ui-state-highlight" style="padding: 10px;">{{lastMsgText}}</div>
					<br />
				</span>	
            
            
	            <div class="grid_3"  style="margin-right:8px;" >
	            
	                 <div style="margin-top:25px;margin-bottom:25px;">
                      
                       <a href="newActivity.jsp"><span class="ui-icon inline ui-icon-plus" style="vertical-align:bottom;"></span> Add new activity</a> 
                    </div>  
                    <form id="actSearchForm" name="actSearchForm" novalidate >
                    <div class="ui-widget-content fit_search_container" id="search_container">
                    		
	                         <div class="fit_search_top_header fit_text_color">
	                             <spring:message code="act.search.title"/>
	                         </div> 
	                          <hr class="fit_hr">
	                          
	                          <%@include file="diagFilter.jsp" %>
	                         
                               <%--cancer filters, only show when cancer diagnosis is selected --%>
                               <div id="cancerFilters" class="fit_visible">
                               	    <hr class="fit_hr">
	                                <div class="fit_search_header fit_text_color">
	                                 <spring:message code="act.search.chemoMin"/>
	                              </div>  
	                              <div class="fit_checkboxes fit_text_color">
	                                   <input id="searchChemoCompleted" name="value" value="0" ng-model="chemoFilter"  style="width:30px;"> 
	                                     <span class="fit_search_header">&nbsp;<spring:message code="act.search.and"/> </span>
			                       </div>
			                       <div class="fit_search_header fit_text_color">
	                                 <spring:message code="act.search.radMin"/>
	                              </div>
			                       <div class="fit_checkboxes fit_text_color">
	                                   <input id="searchRadCompleted" name="value" value="0" ng-model="radFilter"   style="width:30px;">
			                       </div>
	                               <div class="fit_search_header fit_text_color">
	                                <spring:message code="act.search.cancerFreeDt"/> <br/>
	                                 <input type="date"  placeholder="mm/dd/yyyy"  id="searchCancerFreeDt" value="" style="width:60%;" ng-model="cancerFreeDt" 
	                                         ng-change="applyFilterByCancerFreeDt(this);">
	                              </div> 
                              </div>
                              <!-- activity types filter -->
	                          <hr class="fit_hr">
	                           <div class="fit_search_header fit_text_color">
	                             <spring:message code="act.search.actType"/>
	                          </div>  
	                           <div class="fit_checkboxes fit_text_color"  ng-repeat="actType in allActivityTypes">
	                              <input ng-model="actType.isChecked"  
			                             	 type="checkbox" ng-true-value="true" ng-false-value="false"
			                             	 ng-change="applyFilterByActType(this)"> {{actType.name}} <br/>
	                          </div>
	                    </div>   
	                    </form>    
	           </div>
		      <div class="grid_10" > 
		        <div class="ui-widget-content" style="padding:10px 5px 15px 10px;">
		              <div id="latest_link"  style="padding:10px 0px 10px 30%;">
                       <span class="ui-icon inline ui-icon-arrowrefresh-1-w" style="vertical-align:bottom;"></span>
                        <a href="#" ng-click="latestHandler()" > <spring:message code="act.label.latestActivities"/> </a>
                       </div>
                       
                       <div id="search_results"   >

                        <div style="padding-right:15px;float:right;" class="fit_text_color fit_text_expl">
                           <spring:message code="act.sort.label"/> 
                           			<select ng-model="selectedSort" ng-change="applySort()" >
                                        <option value="na" selected class="fit_text_color fit_text_expl" ><spring:message code="act.sort.select"/> </option>
                                        <option value="date_desc" class="fit_text_color fit_text_expl"  ><spring:message code="act.sort.date.desc"/> </option>
                                        <option value="date_asc" class="fit_text_color fit_text_expl" ><spring:message code="act.sort.date.asc"/> </option>
                                        <option value="state_asc" class="fit_text_color fit_text_expl" ><spring:message code="act.sort.state.asc"/> </option>
                                        <option value="state_desc" class="fit_text_color fit_text_expl" ><spring:message code="act.sort.state.desc"/> </option>
                                        <option value="popular_desc" class="fit_text_color fit_text_expl"><spring:message code="act.sort.popularity.desc"/> </option>
                                        <option value="popular_asc" class="fit_text_color fit_text_expl" ><spring:message code="act.sort.popularity.asc"/> </option>
                                    </select>
                        </div>
                         
                        <div style="padding:15px 15px 15px 0;">
                         <!-- various dialogs outside of the activity feed loop -->
                          <div id="cheerDialog"  >
                           <div ng-if="actCheerUsers.length > 0">
						    <div ng-repeat="usr in actCheerUsers">
						      <div >  
						      <a href="javascript:void(0);" ng-click="closeCheerDialog(usr.userId)" >{{usr.displayName}}</a>
						      </div>
						    </div>
						   </div> 
						   <div ng-if="actCheerUsers.length == 0 || actCheerUsers == null ">
						      <spring:message code="act.label.noCheers" />
						   </div>
						  </div>
						  <div id="deleteCommentDialog"  >
	                           <div  style="margin:1em 0 2em 0;">
	                             Are you sure you want to delete this comment?
	                           </div> 
	                           <hr class="fit_hr">
	                           <!-- delete comment buttons -->
	                           <div style="float:right;" class="ui-widget fit_comment_text">
									<input type="button" id="btnDelComment_Ok" ng-click="handleDeleteCommentDialog('ok')"
									       value="OK">
									  <input type="button"   id="btnDelComment_Cancel" ng-click="handleDeleteCommentDialog('cancel')"
									           value="Cancel">
								</div>
                          </div>
                          
                          <!-- show diagnosis info dialog for a selected activity -->
                          <div id="diagnosisInfoDialog"  >
                                <%--View more link --%>
                                <div class="fit_activity_item">
                                    <a href="javascript:void(0);" ng-click="closeDiagInfoDialog(true)" ><spring:message code="txt.viewMore"/> </a>
                                </div>
                               <div  ng-repeat="diag in activityDiagData">
	                                <span ng-if="diag.userDiagIsPrimary == true" >
			                               <div class="fit_search_sub_header"><spring:message code="act.label.primDiag"/></div>
			                              
		                                   <div class="fit_activity_item"> 
		                                       <span class="fit_text_color">{{diag.refDiagTypeName}} </span>
			                                   <span ng-if="diag.userDiagStartDt != null" class="fit_activity_item"> 
		                                           <span  >
		                                               {{diag.userDiagStartDt | date : 'MMMM dd, yyyy'}} - 
		                                               <span ng-if="diag.userDiagEndDt != null">
		                                                   {{diag.userDiagEndDt | date : 'MMMM dd, yyyy'}}
		                                               </span>
		                                                <span ng-if="diag.userDiagEndDt == null">
	                                                       present
	                                                   </span>
		                                            </span>    
		                                        </span>
		                                   </div>    
		                                   <hr class="fit_hr fit_hr_pink" > 
		                                   
		                                   <%-- show treatments only if they have them --%>
		                                   <div style="margin-left:3em;" ng-if="activityForDiagInfo != null">
			                                    <span class="fit_search_header"><spring:message code="act.label.treatments"/></span>
			                                    <div class="fit_activity_item">
				                                      <span class="fit_text_color"><spring:message code="act.label.chemo"/>: </span>
				                                      <span  >
				                                          {{activityForDiagInfo.chemoCompl}} of {{activityForDiagInfo.chemoTot}}
				                                      </span>    
			                                    </div>
			                                     <div class="fit_activity_item">
	                                                 <span class="fit_text_color"><spring:message code="act.label.rad"/>: </span>
	                                                <span >
	                                                    {{activityForDiagInfo.radCompl}} of {{activityForDiagInfo.radTot}}
	                                                </span>    
	                                            </div>
                                           </div> 
                                           <hr class="fit_hr fit_hr"  ng-if="activityForDiagInfo != null"> 
                                           
                                            <%-- show cancer procedures only if they have them --%>
                                           <div style="margin-left:3em;" ng-if="actDiagCancerProcData != null && actDiagCancerProcData.length > 0">
                                                <span class="fit_search_header">Procedures</span>
                                                <div class="fit_activity_item" ng-repeat="proc in actDiagCancerProcData">
                                                      <span class="fit_text_color">
                                                            {{proc.procTypeName}} 
                                                      </span>
                                                      <span >
                                                         - {{proc.procDt | date : 'MMMM dd, yyyy'}} 
                                                      </span>    
                                                </div>
                                           </div> 
                                           <hr class="fit_hr fit_hr"  ng-if="actDiagCancerProcData != null && actDiagCancerProcData.length > 0"> 
	                                </span> 
	                                 <span ng-if="diag.userDiagIsPrimary != true" >
                                           <div ng-if="$index == 1" class="fit_search_sub_header">
                                                <spring:message code="act.label.secondDiag"/> 
                                            </div>
                                          
                                           <div class="fit_activity_item"> 
                                               <span class="fit_text_color">{{diag.refDiagTypeName}} </span>
                                               <span ng-if="diag.userDiagStartDt != null" class="fit_activity_item"> 
                                                   <span  >  
                                                       {{diag.userDiagStartDt | date : 'MMMM dd, yyyy'}} - 
                                                       <span ng-if="diag.userDiagEndDt != null">
                                                           {{diag.userDiagEndDt | date : 'MMMM dd, yyyy'}}
                                                       </span>
                                                        <span ng-if="diag.userDiagEndDt == null">
                                                           present
                                                       </span>
                                                    </span>    
                                                </span>
                                           </div>    
                                           <hr class="fit_hr fit_hr_pink" > 
                                    </span>     
                               </div> 
                               <div style="float:right;margin-top:5px;" class="ui-widget fit_comment_text">
                                    <input type="button" id="btnDiagInfoClose" ng-click="closeDiagInfoDialog(false)"
                                           value="Close">
                                </div>
                          </div>
                            
                          <div  ng-repeat="activity in feedActivities">
                            <!-- only display first 5 records -->
                             
                             <div ng-if="$index <= 5">
                              <%-- only show activity once --%>
                            
                               <span ng-if="($index>0 && feedActivities[$index-1].actId != activity.actId)|| $index==0">
                            	<span ng-if="displayDate($index, feedActivities)">
                            	  <hr class="fit_hr" ng-if="$index == 0" >
                            	  
		                               {{ activity.actDate | date : 'EEEE, MMMM dd, yyyy'}} 
		                            <hr class="fit_hr fit_hr_pink" >
                            	</span>	  
                            	
                               <div class="grid_5">   
	                                 <div class="fit_activity_item">
	                                   <span class="fit_text_header"> 
	                                   		<a href="<%=contextPath%>/pages/secure/dashboard/viewActivity.jsp?actId={{activity.actId}}&ret=activityFeed">
	                                   			{{activity.actTitle}} {{activity.actId}}</a> 
	                                   </span> 
	                                   <span ng-if="activity.activityState != null"> in {{activity.activityState}} </span>
	                                  </div>
	                                <div class="fit_activity_item">
	                                     <a href="#"> <img src="<%=contextPath%>/images/icon_person.png" style="border:none;" 
	                                           title="{{activity.userDisplName}}"></a>
	                                     <a href="<%=contextPath%>/pages/secure/dashboard/profile.jsp?viewUserId={{activity.userId}}" style="padding-left:10px;">{{activity.userDisplName}}</a> 
	                                     {{activity.userId}}
	                                     <%--dont display tag if this is myself --%>
	                                     <span ng-if="userId != activity.userId">
		                                     <span ng-if="activity.tagsByUserCount == 1"  class="fit_activity_item" style="margin-left:30px">
		                                         <a href="javascript:void(0);" ng-click="updateTag($index)" title="<spring:message code='act.label.tagged'/>"><img src="<%=contextPath%>/images/icon_tag_red.png" style="border:none;" 
		                                         	alt="<spring:message code='act.label.tagged'/>"></a>
		                                         
		                                     </span> 
		                                      <span ng-if="activity.tagsByUserCount !=1"  class="fit_activity_item" style="margin-left:30px">
		                                         <a href="javascript:void(0);" ng-click="updateTag($index)" title="<spring:message code='act.label.nottagged'/>"><img src="<%=contextPath%>/images/icon_tag_grey.png" style="border:none;" 
		                                         	alt="<spring:message code='act.label.nottagged'/>"></a>
		                                         <a href=""> </a> 
		                                     </span> 
	                                     </span>
	                                 </div>
	                                 <div class="fit_activity_item">
	                                   <span class="fit_text_color"><spring:message code="act.label.diagnosis" /></span> 
	                                   		{{activity.refDiagTypeName}}
	                                     <a href="javascript:void(0);" ng-click="showDiagInfoDialog(activity, '<spring:message code="act.label.diagTitle" />')"
	                                         title="<spring:message code='act.search.diagnosis'/>"> <img src="<%=contextPath%>/images/icon_info.png" 
	                                                      style="border:none;" 
	                                                      alt='<spring:message code="act.search.diagnosis"/>'> </a>
	                                 </div>   
	                                  <div class="fit_activity_item" >
	                                    <span class="fit_text_color"><spring:message code="act.label.activity" /></span>
	                                    &nbsp;
	                                    <img ng-src="<%=contextPath%>/{{allActivityTypesById[activity.actTypeId-1].iconPath}}"  
	                                    	title="{{allActivityTypesById[activity.actTypeId-1].description}}" style="border:none;" > 
	                                     &nbsp;
	                                    {{activity.actTypeName}}
										<span ng-if="activity.actDistace != null"> - {{activity.actDistace}} <spring:message code="act.label.miles"/></span> 
	                                 </div>  
	                              </div>   
	                              
	                              <%--treatmentent info box, show only if it's cancer diagnosis --%> 
	                              <div  class="grid_3 ui-widget-content fit_text_expl fit_text_color" 
	                                   style="padding:7px;float:right;" ng-if="showTreatmentInfo(activity.refDiagTypeName)">
                                       
                                       <div style="padding:5px 0px 5px 0px;"> <spring:message code="act.label.chemo" /> </div>    
                                        <div >    
                                            <span class="fit_treatm_value" >
                                            	{{getCount(activity.chemoCompl)}} <span ng-if="activity.chemoTot >0">of {{activity.chemoTot}}</span>
                                            </span>
                                        </div>
                                        <div style="padding:5px 0px 5px 0px;"> <spring:message code="act.label.rad"/> </div>    
                                        <div >    
                                            <span class="fit_treatm_value">
                                            	{{getCount(activity.radCompl)}} <span ng-if="activity.radTot >0">of {{activity.radTot}}</span>
                                            </span>
                                        </div>
                                    
                                 </div>
                                 <div class="clear"></div>
                                  <div class="fit_activity_item">
                                    <div class=" grid_4"> 
                                       <span class="fit_text_color"> 
                                            <spring:message code="act.label.symptoms"/>  
                                        </span>     
                                         <span ng-if="activity.actSymptoms == null">
                                            <spring:message code="act.txt.symptoms.none"/>
                                         </span>
                                         {{activity.actSymptoms}}
                                       
                                    </div>   
                                 </div>   
                                 <div class="clear"></div>
                                  <div class="fit_activity_item fit_text_color">
                                   <div class="grid_8"  ng-if="activity.actEnerBef != null"> 
                                       <span><spring:message code="act.label.energy.before"/> </span> 
                                            <img  
                                                title="{{getEnergyLevelTitle(activity.actEnerBef)}}"
                                                 ng-src="<%=contextPath%>/images/energyIcons/enLevel_{{activity.actEnerBef}}.png"> 
                                         <span><spring:message code="act.label.energy.after"/> </span> 
                                            <img   title="{{getEnergyLevelTitle(activity.actEnerAfter)}}"
                                               ng-src="<%=contextPath%>/images/energyIcons/enLevel_{{activity.actEnerAfter}}.png"> 
                                      
                                   </div>
                                 </div>   
                             
                                 <div class="clear"></div>
                                  <div class="fit_item_tools">
                                    
                                     <span class="fit_activity_item">
                                         <!-- comments link--> 
                                         <a href="javascript:void(0);" ><img ng-if="activity.commentsByUserCount > 0" 
                                           		src="<%=contextPath%>/images/icon_comment_red.png" style="border:none;" 
                                           		title="<spring:message code='act.label.commentTip'/>"><img ng-if="activity.commentsByUserCount == 0 || activity.commentsByUserCount == null" 
                                           		src="<%=contextPath%>/images/icon_comment_grey.png" 
                                           		style="border:none;" title="<spring:message code='act.label.commentTip'/>"></a>
                                         
                                         <a href="javascript:void(0);" id="commentLink_{{activity.actId}}" title='<spring:message code="act.label.commentTip"/>' 
                                            ng-click="showActivityComments(activity);"><spring:message code="act.label.comment"/> ({{getCount(activity.actComCount)}})</a>
                                     </span>   
                                      <span class="fit_activity_item">
                                         <!-- cheers -->
                                         <a href="javascript:void(0);" ng-click="updateCheer($index);" ><img src="<%=contextPath%>/images/icon_cheer_red.png" 
                                         		ng-if="activity.cheersByUserCount > 0" style="border:none;" 
                                         		alt='<spring:message code="act.label.cheer" />'>
                                         		<img src="<%=contextPath%>/images/icon_cheer_grey.png" ng-if="activity.cheersByUserCount == 0 || activity.cheersByUserCount == null " 
                                         		style="border:none;" alt='<spring:message code="act.label.cheer" />'></a>
                                         <a href="javascript:void(0);" id="cheerLink_{{$index}}" 
                                            ng-click="showCheersDialog($index,'<spring:message code="act.label.cheers" />')"><spring:message code="act.label.cheer"/> ({{getCount(activity.actCheersCount)}})</a>
                                     </span>   
                                     <span class="fit_activity_item">
                                         <a href=""><img src="<%=contextPath%>/images/icon_email.png" style="border:none;" alt="Expand"></a>
                                         <a href=""><spring:message code="act.label.email"/></a>
                                     </span>
                                    
                                     <div id="commentContainer_{{activity.actId}}" style="display:none;" >
                                          <div class="ui-widget fit_comment_text" id="commentBox_my_{{activity.actId}}">
                                            <textarea class="fit_comment_text" rows="1" ng-model="activity.newComment" id="commentTxtArea_{{activity.actId}}" cols="90"></textarea>
                                            <br/>
                                            <input type="button" ng-click="postActivityComment(activity)" 
                                                    id="btnComment_{{activity.actId}}" 
                                                    value="<spring:message code='act.label.comment'/>">
                                           
                                         </div>
	                                      <div class="ui-widget fit_comment_text" id="commentBox_readonly_{{comment.commentId}}"
	                                           ng-repeat="comment in activity.comments">
	                                          <div ng-if="($index == 0 && activity.lastCommentRow < activity.actComCount) " 
	                                          	   class="fit_comment_link">
	                                          	  <a style="font-size:12px;text-decoration: none;" href="javascript:void(0);"
	                                          	     ng-click="showActivityComments(activity);"><spring:message code="act.txt.showMore"/> </a>
	                                          </div> 
	                                          
	                                           <span editable-textarea="comment.comment"  
	                                                 onbeforesave="updateActivityComment($data, comment, activity)" 
	                                                 e-rows="1" e-cols="60" e-form="commentBtnForm" >{{comment.comment}}</span>
	                                          
	                                          <!-- delete/edit comment icons only for logged in user-->
	                                           <span style="float:right" ng-if="userId == comment.createdById"> 
                                                    <a href="javascript:void(0);" 
                                                       ng-click="showDeleteCommentDialog(comment, activity)"><span class="ui-icon ui-icon-close"></span></a> 
                                               </span> 
                                               <span style="float:right; padding-right:5px;" ng-if="userId == comment.createdById"> 
                                                    <a href="javascript:void(0);"  
                                                    ng-click="commentBtnForm.$show()"
                                                     ng-hide="commentBtnForm.$visible"><span class="ui-icon ui-icon-pencil"></span></a> 
                                               </span> 
                                               
	                                          <br/>
	                                         
	                                          <div id="commentHour_{{comment.commentId}}"  >
	                                           <spring:message code="act.txt.commentBy"/>
	                                            <a href="<%=contextPath%>/pages/secure/dashboard/profile.jsp?viewUserId={{comment.createdById}}">{{comment.displayName}}</a>
	                                          	{{getCommentElapsedTime(comment.commentCreateDt, comment.commentUpdateDt)}}
	                                          </div>
	                                          
	                                     </div>
                                           <div ng-if="activity.lastCommentRow < activity.actComCount"  class="ui-widget fit_comment_text" id="commentBox_lastLink{{activity.actId}}">
                                                 <span  class="fit_comment_link">
                                                   <a style="font-size:12px;text-decoration: none;" href="javascript:void(0);"
                                                    ng-click="showActivityComments(activity);"> <spring:message code="act.txt.showMore" /> </a>
                                                 </span>   
                                            </div> 
	                                    
                                     </div>
                                  </div> 
                                  <hr class="fit_hr fit_hr_pink" >
                               
                               </span>
                               </div>     
                           </div>  
                           	<div id="paginator" style="margin:30px 0px 0px 10px;">
                                    {{feedActivities.length}}
                                     <span class="fit_activity_item" >
                                           <button value="Previous" title="Previous" ng-click="prevHandler()" 
                                           ng-disabled="startRow == 0" class="{{getPaginatorCssClass(startRow == 0)}}"><spring:message code="act.btn.previous"/> </button> 
                                       </span>
                                       <span class="fit_activity_item">
                                          <button value="Next" title="Next" ng-click="nextHandler()" 
                                           class="{{getPaginatorCssClass(feedActivities.length < 12)}}"
                                          ng-disabled="feedActivities.length < 12"><spring:message code="act.btn.next"/></button> 
                                      </span>
                                       
                            </div>     
                        </div>
                   </div>
		         </div>
		      </div> 
		     
		      <div class="grid_3">
		        			
		                    <div id="treatment_container"  class="ui-widget-content fit_text_color"   style="margin-bottom:15px;padding:7px;"
		                    	ng-show="curUserHasCancer">
		                    	<form  id="updateTreatmentsForm" name="updateTreatmentsForm" novalidate
		                    		   ng-submit="updateTreatments()" >
                                    <div class="fit_search_top_header">
                                      <spring:message code="act.treat.title"/>
                                   </div>
                                   <div style="padding:5px 0px 5px 0px;" ><spring:message code="act.treat.update.chemo"/> </div>   
	                                    <div >
		                                 
			                                 <input id="spin_updateTreatmentChemoStart" name="value" value="0" ng-model="profile.chemoCompleted" 
			                                    style="width:30px;"> of
			                                 
			                                  <input id="spin_updateTreatmentChemoEnd" name="value" value="0" ng-model="profile.chemoTotal" 
		                                  		 style="width:30px;"> 
			                       		</div> 
                                    
                                     <div style="padding:5px 0px 5px 0px;" >
                                     		<spring:message code="act.treat.update.rad"/>
                                     </div> 
                                     <div  >
		                                 <input id="spin_updateTreatmentRadStart" name="value" value="0" ng-model="profile.radCompleted"
		                                 		 style="width:30px;"> of
		                                  <input id="spin_updateTreatmentRadEnd" name="value" value="0" ng-model="profile.radTotal"
		                                        style="width:30px;"> 
	                                 
			                         </div> 
			                         <div style="margin-top:10px;margin-left:35px;">
			                         	<input type="submit"  id="updateTreatBtn" value="Update" 
			                         		title="<spring:message code='act.btn.treatment.update' />">
			                         </div>
                                 </form>   
                             </div>       

                            <div id="profile_container"  class="ui-widget-content"   style="margin-bottom:15px;padding:7px;">
                            
                                    Your profile is only 10% complete.
                                    <br/>
                                    <a href="<%=contextPath%>/pages/secure/dashboard/profile.jsp"><spring:message code="profile.txt.myProfile"/> </a>
                            </div>
		                  
		                  <div id="invite_container" class="ui-widget-content" style="margin-bottom:15px;padding:7px;" >
                              <a href="#">Invite via email</a>
                              <p>
                              <div style="padding:5px 0 5px 0;" class="fit_search_header fit_text_color">
                                <spring:message code="act.label.taggedByMe"/> :
                              </div>  
                                <div ng-repeat="tagged in taggedByMe">
	                                 <a href="#">{{tagged.displayName}}</a>
                                </div> 
                                <br/>
                            </div>
		       </div>
		  </div>
		  
            
    </div>
    
    
    <div id="tabs-1-2">
        <%@include file="myActivities.jsp" %>
      
    </div>
    
    <div id="activityDetail" style="display:none;">
        <%@include file="activityDetail.jsp" %>
    </div>
  
  
    
  </div> 
</body>
</html>
