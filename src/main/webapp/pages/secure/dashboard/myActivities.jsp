   <div class="container_16">
    
     <div class="grid_16 fit_search_top_header fit_text_color" style="padding:20px 0px 20px 10px;">
         Activities history 
     </div>
   </div>
   <div class="container_16 ui-widget-content fit_text_color" style="padding:20px 0px 20px 10px;margin-bottom:20px;">   
     <div class="clear"></div>
	 <div  ng-repeat="myAct in myActivitiesTotal">
     <div class="grid_6">
            <img src="<%=contextPath%>/images/icon_activities.png" alt="All activities" style="border:none;">
            <span style="margin-bottom:3px;"> {{myAct.activityCount}}</span>
            <div style="margin-left:17px;"> Activities </div>
       </div>
       
        <div class="grid_6">
            <img src="<%=contextPath%>/images/icon_timerange.png" alt="Time range" style="border:none;">
            <span style="margin-bottom:3px;"> {{myAct.sTime | date : 'EEEE, MMMM dd, yyyy'}}</span>
            <div style="margin-left:17px;"> {{myAct.eTime | date : 'EEEE, MMMM dd, yyyy'}}</div>
       </div>
       
        <div class="clear" ></div>
        
         <div class="grid_6" style="margin-top:24px;">
            <img src="<%=contextPath%>/images/icon_distance_total.png" alt="Total distance" style="border:none;">
            <span style="margin-bottom:3px;"> {{myAct.miles}}</span>
            <div style="margin-left:17px;"> Miles</div>
       </div>
       
        <div class="grid_6"  style="margin-top:24px;" >
            <img src="<%=contextPath%>/images/icon_hour_glass.png" alt="Time range" style="border:none;">
            <span style="margin-bottom:3px;"> {{myAct.totalHours}}:00 </span>
            <div style="margin-left:17px;"> Hours </div>
       </div>
   </div>
   </div>
   
     
   <table cellpadding="0" cellspacing="0" border="0" class="display fit_table" id="activitiesTable">
    <thead>
        <tr>
        	<th>id</th>
        	<th>Title</th>
            <th>Date</th>
            <th>Activity type</th>
            <th>Duration</th>
            <th>Distance</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
</table>


 