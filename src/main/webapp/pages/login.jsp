<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>     
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
String contextPath = request.getContextPath();
contextPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + contextPath;
System.out.println("----contextPath: " + contextPath);
pageContext.setAttribute("contextPath", contextPath);
%>
<title>Fit4Life</title>
 <link href="<%=contextPath%>/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="all" href="<%=contextPath%>/styles/960.css" />
    <script src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <script src="<%=contextPath%>/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
   <script src="<%=contextPath%>/js/angular/angular.js"></script>
  <script src="<%=contextPath%>/js/angular/angular-route.js"></script>
  <script src="<%=contextPath%>/js/angular/security/login.js"></script>
<link href="<%=contextPath%>/styles/main.css" rel="stylesheet">
   <script src="<%=contextPath%>/js/angular/autofill-event.js"></script>

<script>
$(function() {
	
	$( "#loginBtn" ).button();
});

</script>
</head>

<body style="background-color:#778899;"  ng-app="fitApp.loginModule" ng-controller="loginController" ng-cloak>
  <div class="container_16"  style="">
     <div class="grid_4" >	 
     		<img src="<%=contextPath%>/images/icon_tab.png" alt="some_text">
     </div> 	
   
    <div class="grid_6 homeTopMenu">
      <span style="padding-right:30px;">
         <a href="registration/register.jsp"><spring:message code="txt.signup" /></a>
     </span> 
     <span style="padding-right:30px;">
         <a href=""><spring:message code="txt.ourStory" /></a>
     </span>
      <span>
         <a href=""><spring:message code="txt.features" /></a>
     </span>
	</div>   
	<div class="clear"></div>
	<div  class="grid_16" >
	<p>
		<img alt="" src="<%=contextPath%>/images/home_background.jpg"  >	
	</p>

	<!-- ui-dialog -->
	<div  class="loginOverlayContainer ">
		<div class="homeOverlay loginOverlayPadding loginOverlayTitle" >
			  <spring:message code="txt.login"/>
		</div>
		<div ng-show="errorMessage.length > 0" class="ui-state-error fit_error" >{{errorMessage}}</div>
		<form id="loginPageForm" name="loginPageForm" novalidate ng-submit="processLoginPage(loginPageForm.$valid, '<%=contextPath%>');"
		   class="formElements"  method="post">
			 
			 	  <div  style="padding:10px;" >
	                     <span class="whiteLabel"> <spring:message code="reg.label.email"/></span> <br/>
	                       <input type="email" maxlength="50" value=""  id="j_username" name="j_username"
	                            ng-model="loginPageData.j_username" required  ><br/>
	                       <span class="fit_error_login" ng-show="loginPageSubmitted && loginPageForm.j_username.$error.required">Required</span>
	                       <span class="fit_error_login" ng-show="loginPageSubmitted && loginPageForm.j_username.$error.email"><spring:message code="err.email.notValid" /></span>
	                </div> 
	                 <div  style="padding:10px;">
	                      <span class="whiteLabel"><spring:message code="reg.label.password"/></span><br/>
	                       <input type="password" maxlength="50" value=""   id="j_password" name="j_password"
	                              ng-model="loginPageData.j_password" required ><br/>
	                       <span class="fit_error_login" ng-show="loginPageSubmitted && loginPageForm.j_password.$error.required">
	                       		<spring:message code="field.required" />
	                       	</span>
	                </div> 
	                 <div  style="padding:15px 0 0 100px;">
		                 <span >
		                 	<input type="submit"  id="loginBtn" value="<spring:message code='btn.login' />" title="<spring:message code='btn.login' />" >
		                 </span>
		              </div>
		 </form>
	    <div class="whiteLinkSmall" >
			<a href="<%=contextPath%>/pages/registration/resetPassword.jsp"><spring:message code="txt.forgotPassword" /></a>	    	
	    </div>
	</div>
	</div>  
  </div>
</body>
</html>