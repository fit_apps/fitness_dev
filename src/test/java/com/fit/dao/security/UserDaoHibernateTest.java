package com.fit.dao.security;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UserDaoHibernateTest extends BaseTest {
	
private static final Logger logger = LoggerFactory.getLogger(UserDaoHibernateTest.class);
	
	@Autowired
	private UserDaoHibernate userDao;
	
	@Test
	public void testGetTaggedByUser() {
		
		List<Map<String, Object>> taggedByUser = userDao.getTaggedByUser(14L);
		
		for(Map<String, Object> map:taggedByUser){
			logger.info("--userId: " + map.get("userId"));
			logger.info("--displayName: " + map.get("displayName"));
			logger.info("--userTagId: " + map.get("userTagId"));
		}
		
	}

}
