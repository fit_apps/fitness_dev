package com.fit.dao.refs;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;
import com.fit.dao.activities.ActivityDaoHibernate;
import com.fit.dao.reference.ActivityTypeDaoHibernate;
import com.fit.model.refs.RefActivityType;

/**
 * @author channah
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ActivityTypeDaoHibernateTest extends BaseTest {

	@Autowired
	private ActivityTypeDaoHibernate activityTypeDao;
	private static final Logger logger = LoggerFactory.getLogger(ActivityTypeDaoHibernateTest.class);
	
	
	@Test
	public void testGetAllActivtyTypes(){
		List<RefActivityType> activityTypeList = activityTypeDao.getAllActivityTypes();
		logger.info("--activityTypeList size: " + activityTypeList.size());
		
		logger.info(" Id\tName\tDescription");
		for(RefActivityType refActivityType:activityTypeList) {
			logger.info(" " + refActivityType.getId() + "\t" + refActivityType.getName() +"\t" + refActivityType.getDescription() );
		}
	}
}
