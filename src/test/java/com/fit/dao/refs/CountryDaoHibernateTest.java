package com.fit.dao.refs;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;
import com.fit.dao.reference.CountryDaoHibernate;
import com.fit.model.refs.RefCountries;
import com.fit.model.security.User;

/**
 * @author channah
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CountryDaoHibernateTest extends BaseTest {

	@Autowired
	private CountryDaoHibernate countryDao;
	private static final Logger logger = LoggerFactory.getLogger(CountryDaoHibernateTest.class);
	
	
	@Test
	public void testGetAllDiagnosis(){
		List<RefCountries> countryList = countryDao.getAllCountries();
		logger.info("--countryList size: " + countryList.size());
		
//		logger.info(" Id\tName\tKey");
//		for(RefCountries refCountry:countryList) {
//			logger.info(" " + refCountry.getId() + "\t" + refCountry.getCountryName() +"\t" + refCountry.getCountryKey() );
//		}
	}
}
