/**
 * 
 */
package com.fit.dao.tags;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;

/**
 * @author pavula1
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UserTagsDaoHibernateTest extends BaseTest{
	
	@Autowired
	private UserTagsDaoHibernate userTagsDao;

	@Test
	public void testUserTagsById() {

		List<Map<String, Object>> userTage = userTagsDao.getUserTagsById(2l);

		for (Map<String, Object> activityMap : userTage) {
			logger.info("--User Name: " + activityMap.get("name"));
			logger.info("--Diagnosis: " + activityMap.get("diagnosis"));
			logger.info("--chemotTotal: " + activityMap.get("chemoTotal"));
			logger.info("--radioTotal: " + activityMap.get("radioTotal"));
		}

	}
}
