package com.fit.dao.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;
import com.fit.model.activity.Activity;
import com.fit.model.diagnosis.UserDiagnosis;
import com.fit.model.refs.RefDiagnosisType;
import com.fit.model.security.User;
import com.fit.mvc.command.ActivityDisplayCommand;
import com.fit.mvc.command.SearchFilterCommand;


@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ActivityDaoHibernateTest extends BaseTest {

	private static final Logger logger = LoggerFactory.getLogger(ActivityDaoHibernateTest.class);
	
	@Autowired
	private ActivityDaoHibernate activityDao;
	
	
	 
	 @Transactional(propagation = Propagation.REQUIRED, readOnly=true)
	 @Test
	 public void testGefFeedActivitiesWithCounts() {
		 Set<UserDiagnosis> diagnosisSet = new HashSet<UserDiagnosis>();
			RefDiagnosisType diag1 = new RefDiagnosisType();
			diag1.setId(2L);
			//diag1.setDiagKey(RefDiagnosisType.PARENT_CANCER);
			diag1.setDiagKey("LUNG_CANCER");
			
			
			//diagnosisSet.add(diag2);
			UserDiagnosis userDiag = new UserDiagnosis();
			userDiag.setId(3L);
			userDiag.setRefDiagnosisType(diag1);
			
			diagnosisSet.add(userDiag);
			
			SearchFilterCommand filter = new SearchFilterCommand();
			List<Map<String, Object>> diagFilter = new ArrayList<Map<String, Object>>();
			Map<String, Object> diagMap1 =  populateDiagMap("CANCER" , "Lung Cancer", "LUNG_CANCER", 2, "Cancer", 1, "true");
		//	Map<String, Object> diagMap2 =  populateDiagMap("CANCER" , "Breast Cancer", "BREAST_CANCER", 1, "Cancer", 1, "true");
			diagFilter.add(diagMap1);
		//	diagFilter.add(diagMap2);
			filter.setDiagFilter(diagFilter);
			filter.setSelectedSort("state_asc");
			filter.setCancerFreeDt("11/12/2014");
			
			filter.setDiagFilter(diagFilter);
			
			List<Map<String, Object>> acts = this.activityDao.getFeedActivitiesWithCounts(new Long(12), diagnosisSet, filter, 0, 33);
			
			logger.info("total records: " + acts.size());
			
			/*act.id, act.title, act.distance, act.energyLevelBefore, act.energyLevelAfter, act.symptoms,  
			 * count(com.activity.id), u.id, u.displayName,  
		       ucd.id, udt.id, udt.name*/
			for(Map<String, Object> actMap:acts){
				
				Long actId = (Long)actMap.get("actId");
				String actTitle = (String)actMap.get("actTitle");
				//Long actTotCount = (Long)actMap.get("actCount");
				Long commentsCount = (Long)actMap.get("actComCount"); 
				Long userId = (Long)actMap.get("userId");
				String userDisplName = (String)actMap.get("userDisplName");
				Long userDiagId = (Long)actMap.get("usrDiagId");
				Long refDiagTypeId = (Long)actMap.get("refDiagTypeId");
				String refDiagTypeName = (String)actMap.get("refDiagTypeName");
				//String city = (String)actMap.get("userCity");
				String state =  (String)actMap.get("activityState");
				String country =  (String)actMap.get("activityCountry");
				Integer chemoCompl = (Integer)actMap.get("chemoCompl");
				Integer chemoTot = (Integer)actMap.get("chemoTot");
				Integer radCompl = (Integer)actMap.get("radCompl");
				Integer radTot = (Integer)actMap.get("radTot");
				String actTypeName = (String)actMap.get("actTypeName");
				Long cheerCount = (Long) actMap.get("actCheersCount");
				Long loggedUsrComsCount = (Long)actMap.get("commentsByUserCount");
				Long loggedUserCheersCount = (Long)actMap.get("cheersByUserCount");
				Long tagCount = (Long)actMap.get("tagsByUserCount");
				
				logger.info("--act id: " + actId +  ", user id: " +  userId + ", tagsByUserCount: " + tagCount );
				//logger.info("------act total count: " + actTotCount );
				logger.info("------actTypeName: " + actTypeName);
				logger.info("------count comments: " + commentsCount + ", loggedUsrComsCount: " + loggedUsrComsCount);
				logger.info("------count cheers: " +  cheerCount + ", loggedUserCheersCount: " + loggedUserCheersCount);
				logger.info("------------");
				
				logger.info("------refDiagTypeName: " + refDiagTypeName);
				//logger.info("------chemoCompl: " + chemoCompl);
				//logger.info("------chemoTot: " + chemoTot);
				logger.info("------state: " + state);
				logger.info("------country: " + country);
				//logger.info("------radCompl: " + radCompl);
				//logger.info("------radTot: " + radTot);
				//logger.info("------user city: " + city + ", state: " + state);
				//logger.info("------refDiagTypeId: " + refDiagTypeId);
				//logger.info("------refDiagTypeName: " + refDiagTypeName);
			}
	 }
	 
     private Map<String, Object> populateDiagMap(String parentKey, String childName, String childDiagKey, int parentDiagId, String parentName, int childDiagId,
    		 String isChecked){
    	 Map<String, Object> diagMap = new HashMap<String, Object>();
    	 diagMap.put("parentDiagKey", parentKey);
			diagMap.put("childDiagName", childName);
			diagMap.put("childDiagKey", childDiagKey);
			diagMap.put("parentDiagId", parentDiagId);
			diagMap.put("parentDiagName", parentName);
			diagMap.put("childDiagId",childDiagId);
			diagMap.put("isChecked", isChecked);
			return diagMap;
     }
     
	@Test
	public void testGetAllActivitiesByUser() {

		User user = new User();
		user.setId(1l);
		List<Map<String, Object>> activities = activityDao
				.getTotalActivitiesByUserId(1L);
		
		logger.info("--My Activities size: " + activities.size());
		
		for(Map<String, Object> activityMap:activities) {
			logger.info("--Activities count: " + activityMap.get("activityCount"));
			logger.info("--Miles count: " + activityMap.get("miles"));
			logger.info("--startTime: " + activityMap.get("sTime"));
			logger.info("--End Time: " + activityMap.get("eTime"));
			logger.info("--Total Hours: " + activityMap.get("totalHours"));
		}
	

	}

	@Test
	public void testGetActivityById() {
		Activity activity = null;
		Long startId = 100l;
		Long maxId = 200l;
		for (Long activityId = startId; activityId <= maxId; activityId++) {
			activity = activityDao.getActivityById(activityId);
			if (activity != null) {
				logger.info("--Id: " + activity.getId());		
				logger.info("--Title: " + activity.getTitle());
				logger.info("--Activity name: " + activity.getActivityType().getName());
				logger.info("--Date: " + activity.getActivityDate());
				break;
			}
		}
		if (activity == null) {
			logger.info("--Unable to locate a persisted Activity with an id between " + startId + " and " + maxId);
		}
	}	
	
	@Test
	public void testGetActivityDisplayCommand() {
		logger.info("--testGetActivityDisplayCommand Start ");		

		ActivityDisplayCommand adc = null;
		Long startId = 300l;
		Long maxId = 400l;
		for (Long activityId = startId; activityId <= maxId; activityId++) {
			adc = activityDao.getActivityDisplayCommand(1l, activityId);
			if (adc != null) {
				logger.info("--Id: " + adc.getActivityId());		
				logger.info("--Title: " + adc.getActivityTitle());
				logger.info("--Activity name: " + adc.getActivityTypeName());
				logger.info("--Date: " + adc.getActivityDate());
				break;
			}
		}
		if (adc == null) {
			logger.info("--Unable to locate a persisted Activity with an id between " + startId + " and " + maxId);
		}
	}		

/*	
	@Test
	public void testDeleteActivity() {
		logger.info("--testDeleteActivity Start ");		

		ActivityDisplayCommand adc = null;
		Activity actToAdd = new Activity();
		actToAdd.setTitle("Test activity");
		actToAdd.setActivityDate(new Date());
		actToAdd.setPublic(true);
		actToAdd.setUser(userDao.getUserByEmail("ap2@crhannah.com"));      //Fix this
		actToAdd.setActivityType(activityTypeDao.getAllActivityTypes().get(0));
		Activity actToDelete = activityDao.newActivity(actToAdd);
		assertTrue(actToDelete != null);
		logger.info("--Added Activity, Id: " + actToAdd.getId());		

		activityDao.deleteActivity(actToDelete.getId());
	}
	*/		
	
}
