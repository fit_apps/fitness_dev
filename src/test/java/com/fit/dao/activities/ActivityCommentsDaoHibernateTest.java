package com.fit.dao.activities;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;
import com.fit.model.security.User;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ActivityCommentsDaoHibernateTest extends BaseTest{

	private static final Logger logger = LoggerFactory.getLogger(ActivityCommentsDaoHibernateTest.class);
	
	@Autowired
	private ActivityCommentsDao actvityCommentsDao;
	
	@Test
	public void testGetUserCommentCount(){
		
		User user = new User();
		user.setId(14L);
		Long activityId = 5L;
		long numberOfComments = this.actvityCommentsDao.getUserActivityCommentCount(user, activityId);
		
		logger.info("--n of comments: " + numberOfComments);
	}
}
