package com.fit.dao.diagnosis;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;
import com.fit.model.security.User;

/**
 * @author amourad
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class DiagnosisDaoHibernateTest extends BaseTest {

	@Autowired
	private DiagnosisDaoHibernate diagDao;
	private static final Logger logger = LoggerFactory.getLogger(DiagnosisDaoHibernateTest.class);
	
	
	@Test
	public void testGetAllDiagnosis(){
		List<Map<String, Object>> diagList = diagDao.getAllParentDiagnosis();
		logger.info("--diagList size: " + diagList.size());
		
		for(Map<String, Object> diagMap:diagList) {
			logger.info("--diagType name: " + diagMap.get("diagKey"));
		}
	}
	
	
	@Test
	public void testGetUserDiagAll() {
		Long userId = 12L;
		List<Map<String, Object>> diagList = this.diagDao.getUserDiagAll(userId);
		
		for(Map<String, Object> diagMap:diagList) {
			if((Boolean)diagMap.get("userDiagIsPrimary")) {
				logger.info("-------Primary Diagnosis");
			} else {
				logger.info("-------Secondary Diagnosis");
			}
			logger.info("----------" + diagMap.get("refDiagTypeName") + " " + diagMap.get("userDiagStartDt") + " - " + diagMap.get("userDiagEndDt"));
			logger.info("----------Treatments");
			logger.info("---------------Chemo Treatments: " + diagMap.get("treatmId") + ", "+ diagMap.get("chemoCompleted") + 
					" of " + diagMap.get("chemoTotal") + ", " + diagMap.get("chemFreq") );
			
			logger.info("---------------Radiation Treatments: " +diagMap.get("procId") + ", " + 
					diagMap.get("radCompleted") + " of " + diagMap.get("radTotal") );
			logger.info("----------Procedures");
			logger.info("---------------" + diagMap.get("procName") + "- " + diagMap.get("procDate") );
		}
		
	}
	
	@Test
	public void testGetAllDiagnHierarchy() {
		List<Map<String, Object>> diagList = diagDao.getAllDiagnHierarchy();
		logger.info("--diagList size: " + diagList.size());
		
		for(Map<String, Object> diagMap:diagList) {
			logger.info("--childDiagId, childDiagKey, childDiagName, parentDiagId, parentDiagKey, parentDiagName ");
			logger.info( "--" + diagMap.get("childDiagId") + ",     " +  
					 diagMap.get("childDiagKey") + ",        " + 
					 diagMap.get("childDiagName") + ",        " + 
					 diagMap.get("parentDiagId")  + ",        " + 
					 diagMap.get("parentDiagKey")  + ",        " + 
					 diagMap.get("parentDiagName") 
			);
		}
		
	}
	
	@Test
	public void testGetUserTreatments() {
		User user = new User();
		user.setId(12L);
		Map<String, Object> trMap = diagDao.getUserTreatments(user);
		
		logger.info("--phaseId, chemoCompleted, chemoTotal, radCompleted, radTotal ");
		 

		if (trMap != null && trMap.size() > 0 ) {	
 
			logger.info("-----" + trMap.get("phaseId") + ",     "
					+ trMap.get("chemoCompleted") + ",        "
					+ trMap.get("chemoTotal") + ",        "
					+ trMap.get("radCompleted") + ",        "
					+ trMap.get("radTotal"));
		}
	}
	
	@Test
	public void testGetUserDiagnosis() {
		User user = new User();
		user.setId(24L);
		
		List<Map<String, Object>> diagList = diagDao.getUserDiagnosis(user);
		
		for(Map<String, Object> map:diagList) {
			logger.info("----userDiagId: " + map.get("userDiagId") );
			logger.info("----userDiagStartDt: " + map.get("userDiagStartDt") );
			logger.info("----userDiagEndDt: " + map.get("userDiagEndDt") );
			logger.info("----userDiagIsPrimary: " + map.get("userDiagIsPrimary") );
			logger.info("----refDiagTypeName: " + map.get("refDiagTypeName") );
			logger.info("----refDiagKey: " + map.get("refDiagKey") );
			logger.info("----userId: " + map.get("userId") );
		}
	}
	
	@Test
	public void testGetUserDiagProcedures() {
		Long userDiagId = 3L;
		List<Map<String, Object>> procList = this.diagDao.getUserDiagProcedures(userDiagId);
		
		for(Map<String, Object> map:procList) {
			logger.info("----userDiagId: " + map.get("userDiagId") );
			logger.info("----procId: " + map.get("procId") );
			logger.info("----procTypeId: " + map.get("procTypeId") );
			logger.info("----procTypeName: " + map.get("procTypeName") );
			logger.info("----procTypeDesc: " + map.get("procTypeDesc") );
		}
	}
	
}
