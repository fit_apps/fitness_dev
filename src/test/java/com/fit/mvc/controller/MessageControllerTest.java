package com.fit.mvc.controller;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class MessageControllerTest extends BaseTest {

	private static final String MSG_KEY = "TestKey";
	private static final String MSG_VALUE = "TestValue";
	private static final String SESSION_VALUE = "TestSession";
	
	private static final Logger logger = LoggerFactory.getLogger(MessageControllerTest.class);

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@Test
	public void testMessageController() {
		MessageController mc = new MessageController();
		
		assertTrue(mc.putMessageDetail(MSG_KEY, MSG_VALUE, SESSION_VALUE) == null);
		String retrievedValue = mc.getMessageDetail(MSG_KEY, SESSION_VALUE);
		assertTrue(MSG_VALUE.equals(retrievedValue));
		
		assertTrue(mc.deleteMessageDetail(MSG_KEY, SESSION_VALUE).equals(MSG_VALUE));
		assertTrue(mc.deleteMessageDetail(MSG_KEY, SESSION_VALUE) == null);
		
		assertTrue(mc.deleteMessageDetail("Xxx", SESSION_VALUE) == null);
		}
}
