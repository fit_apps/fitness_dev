package com.fit.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.BaseTest;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UserUtilsTest extends BaseTest {

	private static final String GEN_STRING_PATTERN_NORMAL_1 = "*lusnnsul*";
	private static final String GEN_STRING_PATTERN_INVALID_CHARS_1 = "*lusnxns4ul*";
	private static final String GEN_STRING_PATTERN_LONG_1 = "*llnlusnnsul*nnnluuuuusssssssss*******lu";
	private static final String GEN_STRING_NO_WILDS_1 = "usnnsulu";
	
	private static final Logger logger = LoggerFactory
			.getLogger(UserUtilsTest.class);

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@Test
	public void testGenString() {

		String result = UserUtils.genString(GEN_STRING_PATTERN_NORMAL_1, false);
		assertTrue(result.length() == GEN_STRING_PATTERN_NORMAL_1.length());
		for (int i = 0; i < GEN_STRING_PATTERN_NORMAL_1.length(); i++) {
			char testChar = result.charAt(i);
			switch (GEN_STRING_PATTERN_NORMAL_1.charAt(i)) {
			case '*':
				assertFalse(UserUtils.RANDOM_STRING_ALL_CHOICES.indexOf(testChar) == -1);
				break;
			case 'l':
				assertFalse(UserUtils.RANDOM_STRING_LOWER_CASE.indexOf(testChar) == -1);
				break;
			case 'u':
				assertFalse(UserUtils.RANDOM_STRING_UPPER_CASE.indexOf(testChar) == -1);
				break;
			case 's':
				assertFalse(UserUtils.RANDOM_STRING_SPECIAL_CHAR.indexOf(testChar) == -1);
				break;
			case 'n':
				assertFalse(UserUtils.RANDOM_STRING_INTEGER.indexOf(testChar) == -1);
				break;
			}
		}
		
		result = UserUtils.genString(GEN_STRING_PATTERN_INVALID_CHARS_1, false);
		assertFalse(result.length() == GEN_STRING_PATTERN_INVALID_CHARS_1.length());

		result = UserUtils.genString(GEN_STRING_PATTERN_LONG_1, false);
		assertTrue(result.length() == GEN_STRING_PATTERN_LONG_1.length());
		for (int i = 0; i < GEN_STRING_PATTERN_LONG_1.length(); i++) {
			char testChar = result.charAt(i);
			switch (GEN_STRING_PATTERN_LONG_1.charAt(i)) {
			case '*':
				assertFalse(UserUtils.RANDOM_STRING_ALL_CHOICES.indexOf(testChar) == -1);
				break;
			case 'l':
				assertFalse(UserUtils.RANDOM_STRING_LOWER_CASE.indexOf(testChar) == -1);
				break;
			case 'u':
				assertFalse(UserUtils.RANDOM_STRING_UPPER_CASE.indexOf(testChar) == -1);
				break;
			case 's':
				assertFalse(UserUtils.RANDOM_STRING_SPECIAL_CHAR.indexOf(testChar) == -1);
				break;
			case 'n':
				assertFalse(UserUtils.RANDOM_STRING_INTEGER.indexOf(testChar) == -1);
				break;
			}
		}
		
		result = UserUtils.genString(GEN_STRING_NO_WILDS_1, true);		//Generate in random order
		assertTrue(result.length() == GEN_STRING_NO_WILDS_1.length());
		
		List <Character> patternList = new ArrayList <Character>();
		for (char myPatternChar : GEN_STRING_NO_WILDS_1.toCharArray()) {
			patternList.add(myPatternChar);
		}
		for (char myResultChar : result.toCharArray()) {
			if (UserUtils.RANDOM_STRING_LOWER_CASE.indexOf(myResultChar) > -1 &&
					patternList.contains('l') ) {
				patternList.remove(patternList.indexOf('l'));
			}
			else if (UserUtils.RANDOM_STRING_UPPER_CASE.indexOf(myResultChar) > -1 &&
					patternList.contains('u') ) {
				patternList.remove(patternList.indexOf('u'));
			}
			else if (UserUtils.RANDOM_STRING_SPECIAL_CHAR.indexOf(myResultChar) > -1 &&
					patternList.contains('s') ) {
				patternList.remove(patternList.indexOf('s'));
			}
			else if (UserUtils.RANDOM_STRING_INTEGER.indexOf(myResultChar) > -1 &&
					patternList.contains('n') ) {
				patternList.remove(patternList.indexOf('n'));
			}
		}
		assertTrue(patternList.size() == 0);
	}
}
